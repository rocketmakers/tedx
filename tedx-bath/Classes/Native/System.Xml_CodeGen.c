﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String SR::GetString(System.String,System.Object[])
extern void SR_GetString_m4356FFC0B1BA9CB3F26E73B41B74BC39FE4E8A96 ();
// 0x00000002 System.String SR::GetString(System.Globalization.CultureInfo,System.String,System.Object[])
extern void SR_GetString_m7D86468A7BD047EB88A26F75A073C46A51B827E3 ();
// 0x00000003 System.Void System.Xml.Base64Encoder::WriteChars(System.Char[],System.Int32,System.Int32)
// 0x00000004 System.Void System.Xml.Base64Encoder::Flush()
extern void Base64Encoder_Flush_mCC2F11678DA21435D1A00BFDA1D9FF97B9605B6D ();
// 0x00000005 System.Void System.Xml.XmlTextWriterBase64Encoder::WriteChars(System.Char[],System.Int32,System.Int32)
extern void XmlTextWriterBase64Encoder_WriteChars_m339EA5F0483DFCD5D8A0EA35DE060C8E3071E97E ();
// 0x00000006 System.Void System.Xml.SecureStringHasher::.ctor()
extern void SecureStringHasher__ctor_mAAE07435BFBE6F3C9A4840F845DE0EB38EA73AD7 ();
// 0x00000007 System.Boolean System.Xml.SecureStringHasher::Equals(System.String,System.String)
extern void SecureStringHasher_Equals_m6E11A5878C2D8D9DA983636305AADB06AC3B400F ();
// 0x00000008 System.Int32 System.Xml.SecureStringHasher::GetHashCode(System.String)
extern void SecureStringHasher_GetHashCode_m867EF141E418F59C37589E420CAFE018C5072849 ();
// 0x00000009 System.Int32 System.Xml.SecureStringHasher::GetHashCodeOfString(System.String,System.Int32,System.Int64)
extern void SecureStringHasher_GetHashCodeOfString_mC56D819EA0533A7F2DCDE3260D618DCFE68F88D6 ();
// 0x0000000A System.Xml.SecureStringHasher_HashCodeOfStringDelegate System.Xml.SecureStringHasher::GetHashCodeDelegate()
extern void SecureStringHasher_GetHashCodeDelegate_m31822E4C8B12FCFD4E39A65D04BF5606A35EE022 ();
// 0x0000000B System.Void System.Xml.SecureStringHasher_HashCodeOfStringDelegate::.ctor(System.Object,System.IntPtr)
extern void HashCodeOfStringDelegate__ctor_m5DB801F642659F1D15EE9E458BCFAE393325DAC8 ();
// 0x0000000C System.Int32 System.Xml.SecureStringHasher_HashCodeOfStringDelegate::Invoke(System.String,System.Int32,System.Int64)
extern void HashCodeOfStringDelegate_Invoke_m39DA6A0DEEEEE55ABD7723E993744834314930A2 ();
// 0x0000000D System.IAsyncResult System.Xml.SecureStringHasher_HashCodeOfStringDelegate::BeginInvoke(System.String,System.Int32,System.Int64,System.AsyncCallback,System.Object)
extern void HashCodeOfStringDelegate_BeginInvoke_m86BB834B9D3C76936DA4190EB5523FF3C5C757ED ();
// 0x0000000E System.Int32 System.Xml.SecureStringHasher_HashCodeOfStringDelegate::EndInvoke(System.IAsyncResult)
extern void HashCodeOfStringDelegate_EndInvoke_m8539B72C9FC5BE2EE2ACCC2F3C4379314840F5CB ();
// 0x0000000F System.Void System.Xml.XmlReader::.cctor()
extern void XmlReader__cctor_m3EF2489C81E4EEF249A34242FC130A861972991E ();
// 0x00000010 System.Void System.Xml.XmlTextEncoder::.ctor(System.IO.TextWriter)
extern void XmlTextEncoder__ctor_mDC642C841EF38B20B5521384E3C9026D594CE332 ();
// 0x00000011 System.Void System.Xml.XmlTextEncoder::set_QuoteChar(System.Char)
extern void XmlTextEncoder_set_QuoteChar_m28C9CC005913B64E3444E1522EE8D26B4F5F0C88 ();
// 0x00000012 System.Void System.Xml.XmlTextEncoder::StartAttribute(System.Boolean)
extern void XmlTextEncoder_StartAttribute_mAFEF454A34E72F5B1457C8D634FA947EA0023D15 ();
// 0x00000013 System.Void System.Xml.XmlTextEncoder::EndAttribute()
extern void XmlTextEncoder_EndAttribute_m5988FFECF6A66F36D9079FD6CA67BB56A2A98D66 ();
// 0x00000014 System.String System.Xml.XmlTextEncoder::get_AttributeValue()
extern void XmlTextEncoder_get_AttributeValue_m7D58DB09133999166C984994CA05C48B469DEC7D ();
// 0x00000015 System.Void System.Xml.XmlTextEncoder::WriteSurrogateChar(System.Char,System.Char)
extern void XmlTextEncoder_WriteSurrogateChar_mCCE79921A16872FB57C3C5C21F7518CCE5F504F3 ();
// 0x00000016 System.Void System.Xml.XmlTextEncoder::Write(System.String)
extern void XmlTextEncoder_Write_mD02582AB249921681B2B7C85E71DC17E20AB3884 ();
// 0x00000017 System.Void System.Xml.XmlTextEncoder::WriteRaw(System.Char[],System.Int32,System.Int32)
extern void XmlTextEncoder_WriteRaw_mE2BB17DA3D685325F30E98E590CC58BBB7781628 ();
// 0x00000018 System.Void System.Xml.XmlTextEncoder::WriteStringFragment(System.String,System.Int32,System.Int32,System.Char[])
extern void XmlTextEncoder_WriteStringFragment_mBDB749E06C09EE5CDA591A6AB22893B2DDBB3BD1 ();
// 0x00000019 System.Void System.Xml.XmlTextEncoder::WriteCharEntityImpl(System.Char)
extern void XmlTextEncoder_WriteCharEntityImpl_mEA75C2144CCCF2DF29BB91D3B91B2C06C1E6920F ();
// 0x0000001A System.Void System.Xml.XmlTextEncoder::WriteCharEntityImpl(System.String)
extern void XmlTextEncoder_WriteCharEntityImpl_m8DB50E2042CA07D11456C0B67B6EE9D28E8A817E ();
// 0x0000001B System.Void System.Xml.XmlTextEncoder::WriteEntityRefImpl(System.String)
extern void XmlTextEncoder_WriteEntityRefImpl_m3DA44922F283F75B20D43E043A1E6D0F8010E5E9 ();
// 0x0000001C System.Void System.Xml.XmlTextWriter::.ctor()
extern void XmlTextWriter__ctor_mED6FB595B6052C306BE5175DC8E20DD974A2CEFC ();
// 0x0000001D System.Void System.Xml.XmlTextWriter::.ctor(System.IO.TextWriter)
extern void XmlTextWriter__ctor_m6FA653B70BB118A3B8BDA6E29CFF8911CA060EE2 ();
// 0x0000001E System.Void System.Xml.XmlTextWriter::WriteEndElement()
extern void XmlTextWriter_WriteEndElement_m8AC712948634628AB0BAD6C73F8E7CA2EF047612 ();
// 0x0000001F System.Xml.WriteState System.Xml.XmlTextWriter::get_WriteState()
extern void XmlTextWriter_get_WriteState_mCCEDE0A3A6DD8F7E97C11DF024B7345C0DB39451 ();
// 0x00000020 System.Void System.Xml.XmlTextWriter::Close()
extern void XmlTextWriter_Close_m8D3926EF1A95024EB7FBD9F69C589546238E6610 ();
// 0x00000021 System.Void System.Xml.XmlTextWriter::AutoComplete(System.Xml.XmlTextWriter_Token)
extern void XmlTextWriter_AutoComplete_mCFC6565788633CD667823E70E1FD8A9AF241B664 ();
// 0x00000022 System.Void System.Xml.XmlTextWriter::AutoCompleteAll()
extern void XmlTextWriter_AutoCompleteAll_mB42EF751B442A2213ED31F11AAA25A0A39FF302D ();
// 0x00000023 System.Void System.Xml.XmlTextWriter::InternalWriteEndElement(System.Boolean)
extern void XmlTextWriter_InternalWriteEndElement_m02FCE5CCA71DBFBA885AF81FFAD702B3BC39294F ();
// 0x00000024 System.Void System.Xml.XmlTextWriter::WriteEndStartTag(System.Boolean)
extern void XmlTextWriter_WriteEndStartTag_m5905B79D7EE7EE0049919B9B879979313D9664FA ();
// 0x00000025 System.Void System.Xml.XmlTextWriter::WriteEndAttributeQuote()
extern void XmlTextWriter_WriteEndAttributeQuote_m057DC07F77F3640F8A59AF53FCC77C6528136490 ();
// 0x00000026 System.Void System.Xml.XmlTextWriter::Indent(System.Boolean)
extern void XmlTextWriter_Indent_m5CB04F1718940F0069DD7C9F8E80A1D2134C4267 ();
// 0x00000027 System.Void System.Xml.XmlTextWriter::PushNamespace(System.String,System.String,System.Boolean)
extern void XmlTextWriter_PushNamespace_m3DA0494C202BD5C6AE32E9C5FD7FEAC6B4ED8548 ();
// 0x00000028 System.Void System.Xml.XmlTextWriter::AddNamespace(System.String,System.String,System.Boolean)
extern void XmlTextWriter_AddNamespace_m78549A0DDF0923A00664A6DE1B425157097E5ED0 ();
// 0x00000029 System.Void System.Xml.XmlTextWriter::AddToNamespaceHashtable(System.Int32)
extern void XmlTextWriter_AddToNamespaceHashtable_mEF08299106F8B3D846EC6D474D02508D5664D429 ();
// 0x0000002A System.Void System.Xml.XmlTextWriter::PopNamespaces(System.Int32,System.Int32)
extern void XmlTextWriter_PopNamespaces_m5D748E8059ED8B3F04DB600618EE9A7CA3A2F158 ();
// 0x0000002B System.Int32 System.Xml.XmlTextWriter::LookupNamespace(System.String)
extern void XmlTextWriter_LookupNamespace_mA477CDBF25662EC77FF5B025FB2E83B400C381FD ();
// 0x0000002C System.Void System.Xml.XmlTextWriter::HandleSpecialAttribute()
extern void XmlTextWriter_HandleSpecialAttribute_m8AFEBFA51F9B23D67C213F35E0ADF91171DB0ECB ();
// 0x0000002D System.Void System.Xml.XmlTextWriter::VerifyPrefixXml(System.String,System.String)
extern void XmlTextWriter_VerifyPrefixXml_m44A57489D6DFFAD820FE4DF6B113DB3B58E6F8E0 ();
// 0x0000002E System.Void System.Xml.XmlTextWriter::FlushEncoders()
extern void XmlTextWriter_FlushEncoders_m131B51A9CECD553DB91EC663D92AFE3D0197B725 ();
// 0x0000002F System.Void System.Xml.XmlTextWriter::.cctor()
extern void XmlTextWriter__cctor_mC61966BD08862DDA9FF9033C1B5834F9BD887CC1 ();
// 0x00000030 System.Void System.Xml.XmlTextWriter_TagInfo::Init(System.Int32)
extern void TagInfo_Init_m631D10B6F7160EC8D54CAAF1418215168F8E6F32_AdjustorThunk ();
// 0x00000031 System.Void System.Xml.XmlTextWriter_Namespace::Set(System.String,System.String,System.Boolean)
extern void Namespace_Set_mEF74FB920EF6CB43BAFDC1FFA58AA95235177979_AdjustorThunk ();
// 0x00000032 System.Void System.Xml.XmlWriter::WriteEndElement()
// 0x00000033 System.Xml.WriteState System.Xml.XmlWriter::get_WriteState()
// 0x00000034 System.Void System.Xml.XmlWriter::Close()
extern void XmlWriter_Close_m35D89F339C48DF5A94DAA4EFD2706DAAADB5B25F ();
// 0x00000035 System.Void System.Xml.XmlWriter::Dispose()
extern void XmlWriter_Dispose_m44475A2DD5076CED4A48FCBA03912752BA3A4FB3 ();
// 0x00000036 System.Void System.Xml.XmlWriter::Dispose(System.Boolean)
extern void XmlWriter_Dispose_m67E68BEEA19D569625D76E85E9399DF83B74A8F7 ();
// 0x00000037 System.Void System.Xml.XmlWriter::.ctor()
extern void XmlWriter__ctor_m1D2B58DC035709A720317204246AD58118C15740 ();
// 0x00000038 System.Void System.Xml.XmlChildEnumerator::.ctor(System.Xml.XmlNode)
extern void XmlChildEnumerator__ctor_m9D966497AE59A0784E4FE7CB883226B7A332F311 ();
// 0x00000039 System.Boolean System.Xml.XmlChildEnumerator::System.Collections.IEnumerator.MoveNext()
extern void XmlChildEnumerator_System_Collections_IEnumerator_MoveNext_m42A648E7F0B79C7F47D7EF9CE5095D54EA372EDB ();
// 0x0000003A System.Boolean System.Xml.XmlChildEnumerator::MoveNext()
extern void XmlChildEnumerator_MoveNext_mCF3EB51E714C7F2E5B627F515BF880F9BF1878CE ();
// 0x0000003B System.Void System.Xml.XmlChildEnumerator::System.Collections.IEnumerator.Reset()
extern void XmlChildEnumerator_System_Collections_IEnumerator_Reset_m3EF3533A19F3926CDCCD9CD82498D01B5F7EA08A ();
// 0x0000003C System.Object System.Xml.XmlChildEnumerator::System.Collections.IEnumerator.get_Current()
extern void XmlChildEnumerator_System_Collections_IEnumerator_get_Current_m2E52170DBEF6245A93126D3C7FF60ED17DDE2BD1 ();
// 0x0000003D System.Xml.XmlNode System.Xml.XmlChildEnumerator::get_Current()
extern void XmlChildEnumerator_get_Current_m8ED001FF27613B8D7040CBA88CD1BF9DE3AC9872 ();
// 0x0000003E System.Void System.Xml.XmlDOMTextWriter::.ctor(System.IO.TextWriter)
extern void XmlDOMTextWriter__ctor_mFF0BC0EC83712D01433AE7115529DCC96B38EE11 ();
// 0x0000003F System.Xml.XmlNode System.Xml.XmlLinkedNode::get_NextSibling()
extern void XmlLinkedNode_get_NextSibling_m6F858A5E1ED045F63D7885A243CD10FFDA891BA6 ();
// 0x00000040 System.Xml.XmlNodeType System.Xml.XmlNode::get_NodeType()
// 0x00000041 System.Xml.XmlNode System.Xml.XmlNode::get_ParentNode()
extern void XmlNode_get_ParentNode_m6AC4A7F4FEE5B469A86490C129F011C12201C3E6 ();
// 0x00000042 System.Xml.XmlNode System.Xml.XmlNode::get_NextSibling()
extern void XmlNode_get_NextSibling_m86E032192785FDB070EE88E07675B2FF304E383B ();
// 0x00000043 System.Xml.XmlNode System.Xml.XmlNode::get_FirstChild()
extern void XmlNode_get_FirstChild_m43F741CF5C0565011D515B92103F04E033952410 ();
// 0x00000044 System.Xml.XmlLinkedNode System.Xml.XmlNode::get_LastNode()
extern void XmlNode_get_LastNode_mAB2DD7365BF9428D4F8AC4C3AFAE7F229FCB1B08 ();
// 0x00000045 System.Collections.IEnumerator System.Xml.XmlNode::System.Collections.IEnumerable.GetEnumerator()
extern void XmlNode_System_Collections_IEnumerable_GetEnumerator_mC8BE3ACFBE87B8F5ABAE7B797FAF48A900679B27 ();
// 0x00000046 System.String System.Xml.XmlNode::get_OuterXml()
extern void XmlNode_get_OuterXml_m4198C72CA7F3F13BE8AA34E2F392B9BC7F1EFE98 ();
// 0x00000047 System.Void System.Xml.XmlNode::WriteTo(System.Xml.XmlWriter)
// 0x00000048 System.Object System.Xml.XmlCharType::get_StaticLock()
extern void XmlCharType_get_StaticLock_mE0FF2E8B12DC2AFFAFAB5718FDD49FAE726A6513 ();
// 0x00000049 System.Void System.Xml.XmlCharType::InitInstance()
extern void XmlCharType_InitInstance_m48449C6A7516A943668D92903B5D4203DD184AC6 ();
// 0x0000004A System.Void System.Xml.XmlCharType::SetProperties(System.String,System.Byte)
extern void XmlCharType_SetProperties_m892CAB57FF4A04EB120C6AA8D4ACA9645ED0A72C ();
// 0x0000004B System.Void System.Xml.XmlCharType::.ctor(System.Byte[])
extern void XmlCharType__ctor_m0B65BC6BD912979FA16676884EC3120565FD6DCD_AdjustorThunk ();
// 0x0000004C System.Xml.XmlCharType System.Xml.XmlCharType::get_Instance()
extern void XmlCharType_get_Instance_mEAAD3E43BD5AC72FA94C12096B2A9C9684557210 ();
// 0x0000004D System.Boolean System.Xml.XmlCharType::IsHighSurrogate(System.Int32)
extern void XmlCharType_IsHighSurrogate_m6E9E01B1A14D2CF127B6D39D333E506F90FFF98E ();
// 0x0000004E System.Boolean System.Xml.XmlCharType::IsLowSurrogate(System.Int32)
extern void XmlCharType_IsLowSurrogate_m0CB63DE5C97F9C09E2E7C67A53BB8682D8FD07D8 ();
// 0x0000004F System.Boolean System.Xml.XmlCharType::InRange(System.Int32,System.Int32,System.Int32)
extern void XmlCharType_InRange_m33777634EBF78BF6177E79CA362BA6A8138EB3B5 ();
// 0x00000050 System.String System.Xml.XmlConvert::TrimString(System.String)
extern void XmlConvert_TrimString_m89152D6729B89C0423168B5C60E0191A773AD1FA ();
// 0x00000051 System.Exception System.Xml.XmlConvert::CreateException(System.String,System.String,System.Xml.ExceptionType,System.Int32,System.Int32)
extern void XmlConvert_CreateException_m94C72E2D80667A0F1A8AB7B7DBC916CA6C527002 ();
// 0x00000052 System.Exception System.Xml.XmlConvert::CreateException(System.String,System.String[],System.Xml.ExceptionType,System.Int32,System.Int32)
extern void XmlConvert_CreateException_m956710818209FE5D8F7999590B30376FEFBC2F90 ();
// 0x00000053 System.Exception System.Xml.XmlConvert::CreateInvalidSurrogatePairException(System.Char,System.Char)
extern void XmlConvert_CreateInvalidSurrogatePairException_m7ADDD2E98288144ED30A75715AD4254E484FAC70 ();
// 0x00000054 System.Exception System.Xml.XmlConvert::CreateInvalidSurrogatePairException(System.Char,System.Char,System.Xml.ExceptionType)
extern void XmlConvert_CreateInvalidSurrogatePairException_mC493A88B7ED7BFEF845DD2ABA4824D395107F812 ();
// 0x00000055 System.Exception System.Xml.XmlConvert::CreateInvalidSurrogatePairException(System.Char,System.Char,System.Xml.ExceptionType,System.Int32,System.Int32)
extern void XmlConvert_CreateInvalidSurrogatePairException_mD7779092577F68BAD64FA0A0549CF1916E6B9914 ();
// 0x00000056 System.Exception System.Xml.XmlConvert::CreateInvalidHighSurrogateCharException(System.Char)
extern void XmlConvert_CreateInvalidHighSurrogateCharException_m4A1C627F6492AD6A98B4CA982A9197BF39C73DA8 ();
// 0x00000057 System.Exception System.Xml.XmlConvert::CreateInvalidHighSurrogateCharException(System.Char,System.Xml.ExceptionType)
extern void XmlConvert_CreateInvalidHighSurrogateCharException_m22453CD571E079C62CC668E732C74CC1EFE47CFD ();
// 0x00000058 System.Exception System.Xml.XmlConvert::CreateInvalidHighSurrogateCharException(System.Char,System.Xml.ExceptionType,System.Int32,System.Int32)
extern void XmlConvert_CreateInvalidHighSurrogateCharException_mAC251160BC8A0DB0389838A689D95A3C898B270B ();
// 0x00000059 System.Void System.Xml.XmlConvert::.cctor()
extern void XmlConvert__cctor_m1EE317B21041E9F0ABE85E4DB9EE427C21DB4F03 ();
// 0x0000005A System.Void System.Xml.XmlException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void XmlException__ctor_mC412B820BFDD1777E4423CA896912FAAD077B783 ();
// 0x0000005B System.Void System.Xml.XmlException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void XmlException_GetObjectData_m8C9F4943DC440A3FBF70905487652549D1B0BE45 ();
// 0x0000005C System.Void System.Xml.XmlException::.ctor()
extern void XmlException__ctor_m3290623E5F23862FF7D0DCEC2CA1FCB7AC02B469 ();
// 0x0000005D System.Void System.Xml.XmlException::.ctor(System.String)
extern void XmlException__ctor_m3C6F865CCA07051F36F465F1BBE24251906765A9 ();
// 0x0000005E System.Void System.Xml.XmlException::.ctor(System.String,System.Exception,System.Int32,System.Int32)
extern void XmlException__ctor_m9600B3E2828022680728997A9FCA2668850588E4 ();
// 0x0000005F System.Void System.Xml.XmlException::.ctor(System.String,System.Exception,System.Int32,System.Int32,System.String)
extern void XmlException__ctor_m2038069D4E6C83DC027DFDD97AD142B723C60C36 ();
// 0x00000060 System.Void System.Xml.XmlException::.ctor(System.String,System.String,System.Int32,System.Int32)
extern void XmlException__ctor_m2CA134DF0552F0FDBBDC49D5E823831CE5A0B325 ();
// 0x00000061 System.Void System.Xml.XmlException::.ctor(System.String,System.String[],System.Int32,System.Int32)
extern void XmlException__ctor_mEAC3FCF0B0088BA1343DA1293874C92ECE9217EB ();
// 0x00000062 System.Void System.Xml.XmlException::.ctor(System.String,System.String[],System.Exception,System.Int32,System.Int32,System.String)
extern void XmlException__ctor_m4B8E63B6F3EB704FFEF6BA4AA0546B90042FC100 ();
// 0x00000063 System.String System.Xml.XmlException::FormatUserMessage(System.String,System.Int32,System.Int32)
extern void XmlException_FormatUserMessage_mDFED8BDE616EF903472039DEE5F5B1F59E5FCCFF ();
// 0x00000064 System.String System.Xml.XmlException::CreateMessage(System.String,System.String[],System.Int32,System.Int32)
extern void XmlException_CreateMessage_mDD847D2C1685E48408F191251542EB3C754344D5 ();
// 0x00000065 System.String System.Xml.XmlException::get_Message()
extern void XmlException_get_Message_m8B67F069D39C92A254CAF168FAE0FFAF6424AC38 ();
// 0x00000066 System.String System.Xml.Res::GetString(System.String)
extern void Res_GetString_m47B81D62E4B5E4C48C06BCC7995B9ED5218EE7A2 ();
// 0x00000067 System.String System.Xml.Res::GetString(System.String,System.Object[])
extern void Res_GetString_mBDB7AFD1EB8C2C577012518DC77B8646A3045E78 ();
static Il2CppMethodPointer s_methodPointers[103] = 
{
	SR_GetString_m4356FFC0B1BA9CB3F26E73B41B74BC39FE4E8A96,
	SR_GetString_m7D86468A7BD047EB88A26F75A073C46A51B827E3,
	NULL,
	Base64Encoder_Flush_mCC2F11678DA21435D1A00BFDA1D9FF97B9605B6D,
	XmlTextWriterBase64Encoder_WriteChars_m339EA5F0483DFCD5D8A0EA35DE060C8E3071E97E,
	SecureStringHasher__ctor_mAAE07435BFBE6F3C9A4840F845DE0EB38EA73AD7,
	SecureStringHasher_Equals_m6E11A5878C2D8D9DA983636305AADB06AC3B400F,
	SecureStringHasher_GetHashCode_m867EF141E418F59C37589E420CAFE018C5072849,
	SecureStringHasher_GetHashCodeOfString_mC56D819EA0533A7F2DCDE3260D618DCFE68F88D6,
	SecureStringHasher_GetHashCodeDelegate_m31822E4C8B12FCFD4E39A65D04BF5606A35EE022,
	HashCodeOfStringDelegate__ctor_m5DB801F642659F1D15EE9E458BCFAE393325DAC8,
	HashCodeOfStringDelegate_Invoke_m39DA6A0DEEEEE55ABD7723E993744834314930A2,
	HashCodeOfStringDelegate_BeginInvoke_m86BB834B9D3C76936DA4190EB5523FF3C5C757ED,
	HashCodeOfStringDelegate_EndInvoke_m8539B72C9FC5BE2EE2ACCC2F3C4379314840F5CB,
	XmlReader__cctor_m3EF2489C81E4EEF249A34242FC130A861972991E,
	XmlTextEncoder__ctor_mDC642C841EF38B20B5521384E3C9026D594CE332,
	XmlTextEncoder_set_QuoteChar_m28C9CC005913B64E3444E1522EE8D26B4F5F0C88,
	XmlTextEncoder_StartAttribute_mAFEF454A34E72F5B1457C8D634FA947EA0023D15,
	XmlTextEncoder_EndAttribute_m5988FFECF6A66F36D9079FD6CA67BB56A2A98D66,
	XmlTextEncoder_get_AttributeValue_m7D58DB09133999166C984994CA05C48B469DEC7D,
	XmlTextEncoder_WriteSurrogateChar_mCCE79921A16872FB57C3C5C21F7518CCE5F504F3,
	XmlTextEncoder_Write_mD02582AB249921681B2B7C85E71DC17E20AB3884,
	XmlTextEncoder_WriteRaw_mE2BB17DA3D685325F30E98E590CC58BBB7781628,
	XmlTextEncoder_WriteStringFragment_mBDB749E06C09EE5CDA591A6AB22893B2DDBB3BD1,
	XmlTextEncoder_WriteCharEntityImpl_mEA75C2144CCCF2DF29BB91D3B91B2C06C1E6920F,
	XmlTextEncoder_WriteCharEntityImpl_m8DB50E2042CA07D11456C0B67B6EE9D28E8A817E,
	XmlTextEncoder_WriteEntityRefImpl_m3DA44922F283F75B20D43E043A1E6D0F8010E5E9,
	XmlTextWriter__ctor_mED6FB595B6052C306BE5175DC8E20DD974A2CEFC,
	XmlTextWriter__ctor_m6FA653B70BB118A3B8BDA6E29CFF8911CA060EE2,
	XmlTextWriter_WriteEndElement_m8AC712948634628AB0BAD6C73F8E7CA2EF047612,
	XmlTextWriter_get_WriteState_mCCEDE0A3A6DD8F7E97C11DF024B7345C0DB39451,
	XmlTextWriter_Close_m8D3926EF1A95024EB7FBD9F69C589546238E6610,
	XmlTextWriter_AutoComplete_mCFC6565788633CD667823E70E1FD8A9AF241B664,
	XmlTextWriter_AutoCompleteAll_mB42EF751B442A2213ED31F11AAA25A0A39FF302D,
	XmlTextWriter_InternalWriteEndElement_m02FCE5CCA71DBFBA885AF81FFAD702B3BC39294F,
	XmlTextWriter_WriteEndStartTag_m5905B79D7EE7EE0049919B9B879979313D9664FA,
	XmlTextWriter_WriteEndAttributeQuote_m057DC07F77F3640F8A59AF53FCC77C6528136490,
	XmlTextWriter_Indent_m5CB04F1718940F0069DD7C9F8E80A1D2134C4267,
	XmlTextWriter_PushNamespace_m3DA0494C202BD5C6AE32E9C5FD7FEAC6B4ED8548,
	XmlTextWriter_AddNamespace_m78549A0DDF0923A00664A6DE1B425157097E5ED0,
	XmlTextWriter_AddToNamespaceHashtable_mEF08299106F8B3D846EC6D474D02508D5664D429,
	XmlTextWriter_PopNamespaces_m5D748E8059ED8B3F04DB600618EE9A7CA3A2F158,
	XmlTextWriter_LookupNamespace_mA477CDBF25662EC77FF5B025FB2E83B400C381FD,
	XmlTextWriter_HandleSpecialAttribute_m8AFEBFA51F9B23D67C213F35E0ADF91171DB0ECB,
	XmlTextWriter_VerifyPrefixXml_m44A57489D6DFFAD820FE4DF6B113DB3B58E6F8E0,
	XmlTextWriter_FlushEncoders_m131B51A9CECD553DB91EC663D92AFE3D0197B725,
	XmlTextWriter__cctor_mC61966BD08862DDA9FF9033C1B5834F9BD887CC1,
	TagInfo_Init_m631D10B6F7160EC8D54CAAF1418215168F8E6F32_AdjustorThunk,
	Namespace_Set_mEF74FB920EF6CB43BAFDC1FFA58AA95235177979_AdjustorThunk,
	NULL,
	NULL,
	XmlWriter_Close_m35D89F339C48DF5A94DAA4EFD2706DAAADB5B25F,
	XmlWriter_Dispose_m44475A2DD5076CED4A48FCBA03912752BA3A4FB3,
	XmlWriter_Dispose_m67E68BEEA19D569625D76E85E9399DF83B74A8F7,
	XmlWriter__ctor_m1D2B58DC035709A720317204246AD58118C15740,
	XmlChildEnumerator__ctor_m9D966497AE59A0784E4FE7CB883226B7A332F311,
	XmlChildEnumerator_System_Collections_IEnumerator_MoveNext_m42A648E7F0B79C7F47D7EF9CE5095D54EA372EDB,
	XmlChildEnumerator_MoveNext_mCF3EB51E714C7F2E5B627F515BF880F9BF1878CE,
	XmlChildEnumerator_System_Collections_IEnumerator_Reset_m3EF3533A19F3926CDCCD9CD82498D01B5F7EA08A,
	XmlChildEnumerator_System_Collections_IEnumerator_get_Current_m2E52170DBEF6245A93126D3C7FF60ED17DDE2BD1,
	XmlChildEnumerator_get_Current_m8ED001FF27613B8D7040CBA88CD1BF9DE3AC9872,
	XmlDOMTextWriter__ctor_mFF0BC0EC83712D01433AE7115529DCC96B38EE11,
	XmlLinkedNode_get_NextSibling_m6F858A5E1ED045F63D7885A243CD10FFDA891BA6,
	NULL,
	XmlNode_get_ParentNode_m6AC4A7F4FEE5B469A86490C129F011C12201C3E6,
	XmlNode_get_NextSibling_m86E032192785FDB070EE88E07675B2FF304E383B,
	XmlNode_get_FirstChild_m43F741CF5C0565011D515B92103F04E033952410,
	XmlNode_get_LastNode_mAB2DD7365BF9428D4F8AC4C3AFAE7F229FCB1B08,
	XmlNode_System_Collections_IEnumerable_GetEnumerator_mC8BE3ACFBE87B8F5ABAE7B797FAF48A900679B27,
	XmlNode_get_OuterXml_m4198C72CA7F3F13BE8AA34E2F392B9BC7F1EFE98,
	NULL,
	XmlCharType_get_StaticLock_mE0FF2E8B12DC2AFFAFAB5718FDD49FAE726A6513,
	XmlCharType_InitInstance_m48449C6A7516A943668D92903B5D4203DD184AC6,
	XmlCharType_SetProperties_m892CAB57FF4A04EB120C6AA8D4ACA9645ED0A72C,
	XmlCharType__ctor_m0B65BC6BD912979FA16676884EC3120565FD6DCD_AdjustorThunk,
	XmlCharType_get_Instance_mEAAD3E43BD5AC72FA94C12096B2A9C9684557210,
	XmlCharType_IsHighSurrogate_m6E9E01B1A14D2CF127B6D39D333E506F90FFF98E,
	XmlCharType_IsLowSurrogate_m0CB63DE5C97F9C09E2E7C67A53BB8682D8FD07D8,
	XmlCharType_InRange_m33777634EBF78BF6177E79CA362BA6A8138EB3B5,
	XmlConvert_TrimString_m89152D6729B89C0423168B5C60E0191A773AD1FA,
	XmlConvert_CreateException_m94C72E2D80667A0F1A8AB7B7DBC916CA6C527002,
	XmlConvert_CreateException_m956710818209FE5D8F7999590B30376FEFBC2F90,
	XmlConvert_CreateInvalidSurrogatePairException_m7ADDD2E98288144ED30A75715AD4254E484FAC70,
	XmlConvert_CreateInvalidSurrogatePairException_mC493A88B7ED7BFEF845DD2ABA4824D395107F812,
	XmlConvert_CreateInvalidSurrogatePairException_mD7779092577F68BAD64FA0A0549CF1916E6B9914,
	XmlConvert_CreateInvalidHighSurrogateCharException_m4A1C627F6492AD6A98B4CA982A9197BF39C73DA8,
	XmlConvert_CreateInvalidHighSurrogateCharException_m22453CD571E079C62CC668E732C74CC1EFE47CFD,
	XmlConvert_CreateInvalidHighSurrogateCharException_mAC251160BC8A0DB0389838A689D95A3C898B270B,
	XmlConvert__cctor_m1EE317B21041E9F0ABE85E4DB9EE427C21DB4F03,
	XmlException__ctor_mC412B820BFDD1777E4423CA896912FAAD077B783,
	XmlException_GetObjectData_m8C9F4943DC440A3FBF70905487652549D1B0BE45,
	XmlException__ctor_m3290623E5F23862FF7D0DCEC2CA1FCB7AC02B469,
	XmlException__ctor_m3C6F865CCA07051F36F465F1BBE24251906765A9,
	XmlException__ctor_m9600B3E2828022680728997A9FCA2668850588E4,
	XmlException__ctor_m2038069D4E6C83DC027DFDD97AD142B723C60C36,
	XmlException__ctor_m2CA134DF0552F0FDBBDC49D5E823831CE5A0B325,
	XmlException__ctor_mEAC3FCF0B0088BA1343DA1293874C92ECE9217EB,
	XmlException__ctor_m4B8E63B6F3EB704FFEF6BA4AA0546B90042FC100,
	XmlException_FormatUserMessage_mDFED8BDE616EF903472039DEE5F5B1F59E5FCCFF,
	XmlException_CreateMessage_mDD847D2C1685E48408F191251542EB3C754344D5,
	XmlException_get_Message_m8B67F069D39C92A254CAF168FAE0FFAF6424AC38,
	Res_GetString_m47B81D62E4B5E4C48C06BCC7995B9ED5218EE7A2,
	Res_GetString_mBDB7AFD1EB8C2C577012518DC77B8646A3045E78,
};
static const int32_t s_InvokerIndices[103] = 
{
	1,
	2,
	35,
	23,
	35,
	23,
	115,
	116,
	1118,
	4,
	102,
	1119,
	1120,
	116,
	3,
	26,
	573,
	31,
	23,
	14,
	1086,
	26,
	35,
	1121,
	573,
	26,
	26,
	23,
	26,
	23,
	10,
	23,
	32,
	23,
	31,
	31,
	23,
	31,
	110,
	110,
	32,
	169,
	116,
	23,
	27,
	23,
	3,
	32,
	110,
	23,
	10,
	23,
	23,
	31,
	23,
	26,
	114,
	114,
	23,
	14,
	14,
	26,
	14,
	10,
	14,
	14,
	14,
	14,
	14,
	14,
	26,
	4,
	3,
	581,
	26,
	1122,
	46,
	46,
	644,
	0,
	1123,
	1123,
	1124,
	1125,
	1126,
	180,
	1127,
	1128,
	3,
	171,
	171,
	23,
	26,
	36,
	1129,
	36,
	36,
	1130,
	163,
	450,
	14,
	0,
	1,
};
extern const Il2CppCodeGenModule g_System_XmlCodeGenModule;
const Il2CppCodeGenModule g_System_XmlCodeGenModule = 
{
	"System.Xml.dll",
	103,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
