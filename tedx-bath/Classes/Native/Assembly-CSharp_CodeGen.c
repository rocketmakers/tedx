﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void FeaturePointBehaviour::.ctor()
extern void FeaturePointBehaviour__ctor_m8EF12977B54C264D9CE2C1C8DF8EFECA80B28EF7 ();
// 0x00000002 System.Void PinchZoom::.ctor()
extern void PinchZoom__ctor_m529F587C564C0D0E6C107C79BC5488A284F20AF9 ();
// 0x00000003 System.Void PinchZoom::Update()
extern void PinchZoom_Update_m0E9F07E9905E29A2023C39C038EFFC9695336F6D ();
// 0x00000004 T[] JsonHelper::FromJson(System.String)
// 0x00000005 System.String JsonHelper::ToJson(T[])
// 0x00000006 System.String JsonHelper::ToJson(T[],System.Boolean)
// 0x00000007 T[] JsonHelperForAnchor::FromJson(System.String)
// 0x00000008 System.String JsonHelperForAnchor::ToJson(T[])
// 0x00000009 System.String JsonHelperForAnchor::ToJson(T[],System.Boolean)
// 0x0000000A System.Void ARBehaviour::Init()
extern void ARBehaviour_Init_mD4E38288CD94824BBA16E03007DF5D5288146F2E ();
// 0x0000000B System.Void ARBehaviour::OnClickBackButton()
extern void ARBehaviour_OnClickBackButton_m1CDBEEAF944EC277E69D3452DF0480B8162028A7 ();
// 0x0000000C System.Void ARBehaviour::StartCamera()
extern void ARBehaviour_StartCamera_mA266F2C48457F2CFA48DC121B74E800ADB285589 ();
// 0x0000000D System.Void ARBehaviour::StopCamera()
extern void ARBehaviour_StopCamera_m1FAE7B6A1FCE6C28B38A58F602E1964E3B6840DB ();
// 0x0000000E System.Void ARBehaviour::.ctor()
extern void ARBehaviour__ctor_mB716FEBD9F200B66BF1BA18DE91F10DBF1A2821F ();
// 0x0000000F System.Void BackKeyHandler::Update()
extern void BackKeyHandler_Update_mAF0BCE38A5ADFB53E24E21E3CE49CD31625C099A ();
// 0x00000010 System.Void BackKeyHandler::.ctor()
extern void BackKeyHandler__ctor_m05A6EC638D9BA8211939B0FF8D4DC22DD7732998 ();
// 0x00000011 System.Void CameraConfigurationSample::Awake()
extern void CameraConfigurationSample_Awake_m032903D162A85619DE51CEACE308454A1E2A945D ();
// 0x00000012 System.Void CameraConfigurationSample::Start()
extern void CameraConfigurationSample_Start_mB4605E80F422EAAF9691D2EDAE28417A787B9141 ();
// 0x00000013 System.Void CameraConfigurationSample::AddTrackerData()
extern void CameraConfigurationSample_AddTrackerData_mB008D89B901819BBC35EF6BD77387A9BB301B26D ();
// 0x00000014 System.Void CameraConfigurationSample::DisableAllTrackables()
extern void CameraConfigurationSample_DisableAllTrackables_m0176CC222DB4DBAB1DDAE9764ABC45BF0FEBFBC8 ();
// 0x00000015 System.Void CameraConfigurationSample::Update()
extern void CameraConfigurationSample_Update_m8D46DAD5EBE48DF37EB27DF7DA4539A7E413329F ();
// 0x00000016 System.Void CameraConfigurationSample::SetFlashLightMode()
extern void CameraConfigurationSample_SetFlashLightMode_m5A2DA558211AF66EA86DF5552334CCA8EC17FD9B ();
// 0x00000017 System.Void CameraConfigurationSample::FlipHorizontal()
extern void CameraConfigurationSample_FlipHorizontal_mED719D3406C6938C0CADC6FCFF28CC0900D738C7 ();
// 0x00000018 System.Void CameraConfigurationSample::FlipVertical()
extern void CameraConfigurationSample_FlipVertical_mDE8A29143DB8AD212C083084FBD557AB94F5BCAC ();
// 0x00000019 System.Void CameraConfigurationSample::WhiteBalanceLock()
extern void CameraConfigurationSample_WhiteBalanceLock_m415ACE397A993668CBF921F83BB66354E01D0814 ();
// 0x0000001A System.Void CameraConfigurationSample::SetContinuousFocus()
extern void CameraConfigurationSample_SetContinuousFocus_mA04119B6F9FC78BFBAC4E94B129783CF59633DB0 ();
// 0x0000001B System.Void CameraConfigurationSample::ChangeCameraPosition()
extern void CameraConfigurationSample_ChangeCameraPosition_m9364ED8DA3FA84BD26E594482CBFCFDDD361EF99 ();
// 0x0000001C System.Void CameraConfigurationSample::OnApplicationPause(System.Boolean)
extern void CameraConfigurationSample_OnApplicationPause_m6E1733C7034B9B9E6816AC08F994A240EC066CEB ();
// 0x0000001D System.Void CameraConfigurationSample::OnDestroy()
extern void CameraConfigurationSample_OnDestroy_m1C882A9BAE60ADAEF7148CE12E30A2F06DCFD7D3 ();
// 0x0000001E System.Void CameraConfigurationSample::.ctor()
extern void CameraConfigurationSample__ctor_m1E8F3D6CF92303110BB9ED0A6B80CCC70F75AE29 ();
// 0x0000001F System.Void CloudTrackerSample::Awake()
extern void CloudTrackerSample_Awake_m86AF6D8DF38CB541819670346F6E1F1B9038BF8D ();
// 0x00000020 System.Void CloudTrackerSample::Start()
extern void CloudTrackerSample_Start_mB781D4016637877ABAD14A29C29413F70F0BA2F1 ();
// 0x00000021 System.Void CloudTrackerSample::DisableAllTrackables()
extern void CloudTrackerSample_DisableAllTrackables_m463AF556DB8EA2BBA6E8A42A111D810DB6B872FB ();
// 0x00000022 System.Void CloudTrackerSample::Update()
extern void CloudTrackerSample_Update_mD904BF7F8DE22E29801A24045A739BCAB8235690 ();
// 0x00000023 System.Void CloudTrackerSample::OnApplicationPause(System.Boolean)
extern void CloudTrackerSample_OnApplicationPause_m6B613371FBD322C68C5DE55DE2429FE7D1E8B22A ();
// 0x00000024 System.Void CloudTrackerSample::OnDestroy()
extern void CloudTrackerSample_OnDestroy_mA1CB2C4FC285BFB5ADFCDFE0C8C7CF96BD3E96E4 ();
// 0x00000025 System.Void CloudTrackerSample::.ctor()
extern void CloudTrackerSample__ctor_m9E8F7B0977C046908908C9916474EB21E9EA9755 ();
// 0x00000026 System.Void CodeScanSample::Awake()
extern void CodeScanSample_Awake_m6A7F6826051687FC6CF4F26E716F20B813AB75FB ();
// 0x00000027 System.Void CodeScanSample::Start()
extern void CodeScanSample_Start_mFC76C9C8EC6B1BD5D0509D7119806FCE34A772B3 ();
// 0x00000028 System.Void CodeScanSample::Update()
extern void CodeScanSample_Update_mD181C04149B1D3BF603206DC4C2984EA053F154A ();
// 0x00000029 System.Void CodeScanSample::OnApplicationPause(System.Boolean)
extern void CodeScanSample_OnApplicationPause_mDE957AC9F84521A589A7F8D48604FBB4EB0A5065 ();
// 0x0000002A System.Void CodeScanSample::OnDestroy()
extern void CodeScanSample_OnDestroy_m11E349A86223C26F90972D7989149B41FE52C9AC ();
// 0x0000002B System.Void CodeScanSample::StartCodeScan()
extern void CodeScanSample_StartCodeScan_m5378EEA37A93A18A3723DE67817952C14D02861A ();
// 0x0000002C System.Void CodeScanSample::StartCameraInternal()
extern void CodeScanSample_StartCameraInternal_m92BBF161E540E7B92D0CD477D0CB5B1DB9F665CA ();
// 0x0000002D System.Void CodeScanSample::StopCameraInternal()
extern void CodeScanSample_StopCameraInternal_mE429653C878DEBDDC219EDB180649FA85576F046 ();
// 0x0000002E System.Collections.IEnumerator CodeScanSample::AutoFocusCoroutine()
extern void CodeScanSample_AutoFocusCoroutine_m5534B5B3D78998D63FB687110279F14F0AF8334E ();
// 0x0000002F System.Void CodeScanSample::.ctor()
extern void CodeScanSample__ctor_m221BC4C30F1329AB9B8EDD1DAA0385F19569167A ();
// 0x00000030 System.Void HomeSceneManager::Update()
extern void HomeSceneManager_Update_m5D8CC99BEF2E66DEAC40336CCDF2E08A98FC5CA1 ();
// 0x00000031 System.Void HomeSceneManager::OnImageTargetClick()
extern void HomeSceneManager_OnImageTargetClick_m1D12C9277CCF4E0038D5AB7CAFDB96A508F3274F ();
// 0x00000032 System.Void HomeSceneManager::OnMarkerImageClick()
extern void HomeSceneManager_OnMarkerImageClick_m0224EEB9BBAF7A0688E92007DA23BE9794990BCA ();
// 0x00000033 System.Void HomeSceneManager::OnInstantImageClick()
extern void HomeSceneManager_OnInstantImageClick_m2CDF19DE82E890C12A3162D158ADCCAF1989354F ();
// 0x00000034 System.Void HomeSceneManager::OnObjectTargetClick()
extern void HomeSceneManager_OnObjectTargetClick_mD51601B267981E741953B7BD2D9E3D6785F91BCD ();
// 0x00000035 System.Void HomeSceneManager::OnQR_BarcodeClick()
extern void HomeSceneManager_OnQR_BarcodeClick_m28498EF62B6C01D38322E67749C6E3EFE15145FA ();
// 0x00000036 System.Void HomeSceneManager::OnCameraConfigClick()
extern void HomeSceneManager_OnCameraConfigClick_m5968E2C85DC8A5B2486B5C7250180C50A0EA6F68 ();
// 0x00000037 System.Void HomeSceneManager::OnCloudRecognizerClick()
extern void HomeSceneManager_OnCloudRecognizerClick_mC04D2457CBCB24EBC7E54215E4A1DEC72675339B ();
// 0x00000038 System.Void HomeSceneManager::OnQRTrackerClick()
extern void HomeSceneManager_OnQRTrackerClick_m9017C6F8A8230642F5C6F872AD89DB4932496B0E ();
// 0x00000039 System.Void HomeSceneManager::OnVideoTrackerClick()
extern void HomeSceneManager_OnVideoTrackerClick_mC6DADFF51147DAC117D654B505C10A32CA10BA2A ();
// 0x0000003A System.Void HomeSceneManager::.ctor()
extern void HomeSceneManager__ctor_m5DDAE860537A680793B962FDABA1AF1EE943C4A8 ();
// 0x0000003B System.Void ImageTrackerSample::Awake()
extern void ImageTrackerSample_Awake_m9262C0CF2D3EC8E76868545637211C56CC87CBE9 ();
// 0x0000003C System.Void ImageTrackerSample::Start()
extern void ImageTrackerSample_Start_m2C645EA04B21BDEB6AA7966CF02ACF48E9AD9A05 ();
// 0x0000003D System.Void ImageTrackerSample::AddTrackerData()
extern void ImageTrackerSample_AddTrackerData_mC687172A16F0F7E4A2F6736EA33DB67AE92BC4FF ();
// 0x0000003E System.Void ImageTrackerSample::DisableAllTrackables()
extern void ImageTrackerSample_DisableAllTrackables_m56E9448104CC58A10857521A1D6763F069EBDE7C ();
// 0x0000003F System.Void ImageTrackerSample::Update()
extern void ImageTrackerSample_Update_m08E61AFAD47EBB486A9D697130E3A25B3FECCC95 ();
// 0x00000040 System.Void ImageTrackerSample::SetNormalMode()
extern void ImageTrackerSample_SetNormalMode_m3D5546FEEAE79F8289BED2F9DCE89657C65190FC ();
// 0x00000041 System.Void ImageTrackerSample::SetExtendedMode()
extern void ImageTrackerSample_SetExtendedMode_m56612DD3D2E9AE105784632ED644917033A11063 ();
// 0x00000042 System.Void ImageTrackerSample::SetMultiMode()
extern void ImageTrackerSample_SetMultiMode_mCB708804F462A47D1129A8AD846DB0A847FD4354 ();
// 0x00000043 System.Void ImageTrackerSample::OnApplicationPause(System.Boolean)
extern void ImageTrackerSample_OnApplicationPause_mF12EC38848E0E41FEF9A1CD0F1E85D3C36F7ACF7 ();
// 0x00000044 System.Void ImageTrackerSample::OnDestroy()
extern void ImageTrackerSample_OnDestroy_m0BC99EEDDEBF5CA81367805CC452BCA1023FFD71 ();
// 0x00000045 System.Void ImageTrackerSample::.ctor()
extern void ImageTrackerSample__ctor_mF52ACDBB72409DA51E3EAE0465297D503C9C6074 ();
// 0x00000046 System.Void InstantTrackerSample::Awake()
extern void InstantTrackerSample_Awake_m3100CE6372E9352437CECCF4C86C93309C4B1852 ();
// 0x00000047 System.Void InstantTrackerSample::Start()
extern void InstantTrackerSample_Start_mA6C0FB477BDDCCE60C2CA262B4E6252875575D2A ();
// 0x00000048 System.Void InstantTrackerSample::Update()
extern void InstantTrackerSample_Update_mE9BE954766F027C2118653E9E503F788F0A89C91 ();
// 0x00000049 System.Void InstantTrackerSample::OnApplicationPause(System.Boolean)
extern void InstantTrackerSample_OnApplicationPause_mAB1298B971587B07646C25D334F1A30AD973677A ();
// 0x0000004A System.Void InstantTrackerSample::OnDestroy()
extern void InstantTrackerSample_OnDestroy_m89E2DC3AFE274C90681D9001A80A37041DDB6D16 ();
// 0x0000004B System.Void InstantTrackerSample::OnClickStart()
extern void InstantTrackerSample_OnClickStart_mD47367EA3BEA3FE5ABAA369673D70E680E763672 ();
// 0x0000004C System.Void InstantTrackerSample::.ctor()
extern void InstantTrackerSample__ctor_mEC3CFF57CD30FCAF606ABF7588B81B8264304179 ();
// 0x0000004D System.Void MarkerTrackerSample::Awake()
extern void MarkerTrackerSample_Awake_m1EBFA17E3FE9E50D3B9172250FAC1B92B4E18B4E ();
// 0x0000004E System.Void MarkerTrackerSample::Start()
extern void MarkerTrackerSample_Start_m45029486D5DF5E12066D5861CF124E121D737335 ();
// 0x0000004F System.Void MarkerTrackerSample::AddTrackerData()
extern void MarkerTrackerSample_AddTrackerData_m8CAA360A0EAA0C1EB888D7A266C660709D3DA236 ();
// 0x00000050 System.Void MarkerTrackerSample::DisableAllTrackables()
extern void MarkerTrackerSample_DisableAllTrackables_m14B9D20D55F64B62FD1D6DB9ECB960C84B4593E4 ();
// 0x00000051 System.Void MarkerTrackerSample::Update()
extern void MarkerTrackerSample_Update_m8E9043C6E1E2D4EE088E5F10F8B7705D13880947 ();
// 0x00000052 System.Void MarkerTrackerSample::OnClickedNormal()
extern void MarkerTrackerSample_OnClickedNormal_m5F2B147E3132E0F24EB222C26C13EF60C1793914 ();
// 0x00000053 System.Void MarkerTrackerSample::OnClickedEnhanced()
extern void MarkerTrackerSample_OnClickedEnhanced_mACBFF1E926AE267EB49ACA7988C3C63F6B6165B7 ();
// 0x00000054 System.Void MarkerTrackerSample::OnApplicationPause(System.Boolean)
extern void MarkerTrackerSample_OnApplicationPause_m544053D62F08A95D97908D820CF1B3A0117C59EB ();
// 0x00000055 System.Void MarkerTrackerSample::OnDestroy()
extern void MarkerTrackerSample_OnDestroy_mA6B72F4657AA30538810543A863630C6F3951DDC ();
// 0x00000056 System.Void MarkerTrackerSample::.ctor()
extern void MarkerTrackerSample__ctor_mB6E3EF9E9DB297ADCF886B74082611C67B015AE9 ();
// 0x00000057 System.Void ObjectTrackerSample::Awake()
extern void ObjectTrackerSample_Awake_m9889181ACDFD7AF8C2E0FC9CF8B2DBE4087616F8 ();
// 0x00000058 System.Void ObjectTrackerSample::Start()
extern void ObjectTrackerSample_Start_mEA5270596DF2891CC7ABB8B806A25BC8B2E16027 ();
// 0x00000059 System.Void ObjectTrackerSample::AddTrackerData()
extern void ObjectTrackerSample_AddTrackerData_mA81C4E9EAE4A494248FD5779B3871BA88AAE9DFF ();
// 0x0000005A System.Void ObjectTrackerSample::DisableAllTrackables()
extern void ObjectTrackerSample_DisableAllTrackables_m92EE1250AFEC986F0D328978BD9D976D9C61E425 ();
// 0x0000005B System.Void ObjectTrackerSample::Update()
extern void ObjectTrackerSample_Update_m46EE9FCA8A0E1E754EA701F847E8B153BDD55C12 ();
// 0x0000005C System.Void ObjectTrackerSample::OnApplicationPause(System.Boolean)
extern void ObjectTrackerSample_OnApplicationPause_m5D326E0FC47271C98E2E073213B98E5098313D3C ();
// 0x0000005D System.Void ObjectTrackerSample::OnDestroy()
extern void ObjectTrackerSample_OnDestroy_m3AFFD67C101D029A0982C757FF26A4A03580B58C ();
// 0x0000005E System.Void ObjectTrackerSample::.ctor()
extern void ObjectTrackerSample__ctor_mCFD44EACC4EC25167A967F6AF59DA340A04CCDC6 ();
// 0x0000005F System.Void QrCodeTrackerSample::Awake()
extern void QrCodeTrackerSample_Awake_m43C0A614CA2AEB402FC75BA9FFFE40062CA065EA ();
// 0x00000060 System.Void QrCodeTrackerSample::Start()
extern void QrCodeTrackerSample_Start_mDC9A5A2CF8AA4BC272B9BF82C978A0E77D929987 ();
// 0x00000061 System.Void QrCodeTrackerSample::AddTrackerData()
extern void QrCodeTrackerSample_AddTrackerData_mE645FF7B777682B77EC6CE27048D625E411FDE97 ();
// 0x00000062 System.Void QrCodeTrackerSample::DisableAllTrackables()
extern void QrCodeTrackerSample_DisableAllTrackables_m3F6ACFDB9FC0699758906FC6D12BF6713830317A ();
// 0x00000063 System.Void QrCodeTrackerSample::Update()
extern void QrCodeTrackerSample_Update_m7B80E0BFCD948E5C09E04AEDC98AE16075E64830 ();
// 0x00000064 System.Void QrCodeTrackerSample::OnApplicationPause(System.Boolean)
extern void QrCodeTrackerSample_OnApplicationPause_m257218249F9A17BEEAA781A1839040AB788D3741 ();
// 0x00000065 System.Void QrCodeTrackerSample::OnDestroy()
extern void QrCodeTrackerSample_OnDestroy_m103EE26C1C55D178369D4E5A84CEE091FEC67077 ();
// 0x00000066 System.Void QrCodeTrackerSample::.ctor()
extern void QrCodeTrackerSample__ctor_mC5C9B3DE725117906D8A9EBA7FB2B3AE7FF93945 ();
// 0x00000067 System.Void SceneStackManager::.ctor()
extern void SceneStackManager__ctor_mF41A6CAAB46D5C996DC48A7E8575E824611D7001 ();
// 0x00000068 SceneStackManager SceneStackManager::get_Instance()
extern void SceneStackManager_get_Instance_mA091272429BD229FA20C56A9420FBBAD9F913690 ();
// 0x00000069 System.Void SceneStackManager::LoadScene(System.String,System.String)
extern void SceneStackManager_LoadScene_m563E7258EC8A567FABCA166B9E2D8FDEF5D25424 ();
// 0x0000006A System.Void SceneStackManager::LoadPrevious()
extern void SceneStackManager_LoadPrevious_mF2A037DC910E06DCB1580013E660878972E1362B ();
// 0x0000006B System.Void SceneStackManager::.cctor()
extern void SceneStackManager__cctor_mEDAE47B4BCF323AE1A89F15BD9BE72A2F00A20BD ();
// 0x0000006C T Singleton`1::get_Instance()
// 0x0000006D System.Void Singleton`1::OnDestroy()
// 0x0000006E System.Void Singleton`1::.ctor()
// 0x0000006F System.Void Singleton`1::.cctor()
// 0x00000070 System.Void VideoTrackerSample::Awake()
extern void VideoTrackerSample_Awake_mB535577AE871203B4ED3D29A3FE920C3C5355372 ();
// 0x00000071 System.Void VideoTrackerSample::Start()
extern void VideoTrackerSample_Start_mC5D8CE18B12B05E6822245599676993F06ACC066 ();
// 0x00000072 System.Void VideoTrackerSample::AddTrackerData()
extern void VideoTrackerSample_AddTrackerData_mFDB2FB87E7482F9E7E23A42DF52DB60D247029BD ();
// 0x00000073 System.Void VideoTrackerSample::DisableAllTrackables()
extern void VideoTrackerSample_DisableAllTrackables_m07586D3029607AB7B973CF29EE901005E7A61F7C ();
// 0x00000074 System.Void VideoTrackerSample::OnNewFrame(UnityEngine.Video.VideoPlayer,System.Int64)
extern void VideoTrackerSample_OnNewFrame_mA8BF3E035FD2CF3266464CB36320B2647CC4C21E ();
// 0x00000075 System.Void VideoTrackerSample::SwitchCameraToVideo(System.Boolean)
extern void VideoTrackerSample_SwitchCameraToVideo_m0FCB5F923C3D58A6D73851A972F17FA545C3E707 ();
// 0x00000076 System.Void VideoTrackerSample::PauseAndPlayVideo()
extern void VideoTrackerSample_PauseAndPlayVideo_mABD39D39406105ABCB094A1371FA8A2495F0210E ();
// 0x00000077 System.Void VideoTrackerSample::Update()
extern void VideoTrackerSample_Update_m644E10D2DA66EA1CD685644BB4742D36DE99522B ();
// 0x00000078 System.Void VideoTrackerSample::SetNormalMode()
extern void VideoTrackerSample_SetNormalMode_m45C1F44F2E2584ABDDAC26E75B3329A58D203EBE ();
// 0x00000079 System.Void VideoTrackerSample::SetExtendedMode()
extern void VideoTrackerSample_SetExtendedMode_m37B612E91C612BC6C543F7FCC47FD7A08D781976 ();
// 0x0000007A System.Void VideoTrackerSample::SetMultiMode()
extern void VideoTrackerSample_SetMultiMode_mE2CBF14374DD307C4A6D0C5D5144F9BCC30A90AB ();
// 0x0000007B System.Void VideoTrackerSample::OnApplicationPause(System.Boolean)
extern void VideoTrackerSample_OnApplicationPause_mF8EF6F189E919C7FD3B06F4C93485D75F10605F5 ();
// 0x0000007C System.Void VideoTrackerSample::OnDestroy()
extern void VideoTrackerSample_OnDestroy_m18CB7D26FC538FF9B5E35010E49235F9FD961B81 ();
// 0x0000007D System.Void VideoTrackerSample::.ctor()
extern void VideoTrackerSample__ctor_mB8546DBAAD0FA139B5637942CA5C920727E1620D ();
// 0x0000007E System.Void VideoTrackerSample::<Start>b__5_0(System.String)
extern void VideoTrackerSample_U3CStartU3Eb__5_0_m2E0AE32DE63B1D5D7A9FD4028F89D6B523A3BA90 ();
// 0x0000007F System.Void MapOpen::Start()
extern void MapOpen_Start_mB59110895EC55250C6A472CBEA39A2D72F140CA4 ();
// 0x00000080 System.Void MapOpen::Update()
extern void MapOpen_Update_m7EC17380D44C0E35EED058BFBDC2E062E33B9E62 ();
// 0x00000081 System.Void MapOpen::AnimationEnd()
extern void MapOpen_AnimationEnd_mF3D40BC244419CE7618D8EC122A9868BBCA4BFB1 ();
// 0x00000082 System.Void MapOpen::AnimationStart()
extern void MapOpen_AnimationStart_m0C0DE66A366B10FD8959B56E17D9FB8848BE93B7 ();
// 0x00000083 System.Void MapOpen::Rocketmakers()
extern void MapOpen_Rocketmakers_m51EE6C0CD6FE495DFC2D7BBEF5738C4DA27E0145 ();
// 0x00000084 System.Void MapOpen::TEDxBath()
extern void MapOpen_TEDxBath_m08197DE3194964871C4EE34DF9A6051E7B81F3D5 ();
// 0x00000085 System.Void MapOpen::.ctor()
extern void MapOpen__ctor_mA28D68662F366F7D6BE7609A24D7CA3BDD6FB911 ();
// 0x00000086 System.Void PinLink::Start()
extern void PinLink_Start_mC21F42975E25A2B9F026F21D3A0D9273228C9D2C ();
// 0x00000087 System.Void PinLink::Update()
extern void PinLink_Update_m2E2402DD6C77C268C7445FD5E839FB6BE2EEBCF7 ();
// 0x00000088 System.Void PinLink::.ctor()
extern void PinLink__ctor_m9E42953956F78276AF72A07BC471EED1A4E5CD23 ();
// 0x00000089 System.Void Supyrb.AdaptingEventSystemDragThreshold::Awake()
extern void AdaptingEventSystemDragThreshold_Awake_mA3EB2D77065E5448B4AEBD1FC4029A20E4D7F9AD ();
// 0x0000008A System.Void Supyrb.AdaptingEventSystemDragThreshold::UpdatePixelDrag(System.Single)
extern void AdaptingEventSystemDragThreshold_UpdatePixelDrag_mA53845578B333B9EBCED5493AF3999B4CF1B81E6 ();
// 0x0000008B System.Void Supyrb.AdaptingEventSystemDragThreshold::Update()
extern void AdaptingEventSystemDragThreshold_Update_m3D2AF724D6792CFDBED5F1B24F233D016F24EF2D ();
// 0x0000008C System.Void Supyrb.AdaptingEventSystemDragThreshold::OnLevelWasLoaded(System.Int32)
extern void AdaptingEventSystemDragThreshold_OnLevelWasLoaded_m00A7A0A5419F34189F96C94CC4DDF4AC0069C99B ();
// 0x0000008D System.Void Supyrb.AdaptingEventSystemDragThreshold::DisableBtnControll()
extern void AdaptingEventSystemDragThreshold_DisableBtnControll_mCA744FD943D449D65EB9A3401C39B3044E92438B ();
// 0x0000008E System.Void Supyrb.AdaptingEventSystemDragThreshold::Reset()
extern void AdaptingEventSystemDragThreshold_Reset_mEBC97D5033D25FAF516F0CCA73F279E2CF36F6DB ();
// 0x0000008F System.Void Supyrb.AdaptingEventSystemDragThreshold::.ctor()
extern void AdaptingEventSystemDragThreshold__ctor_m14C87FB2BDE10B1DD42E0CAE7553F343EBBDD744 ();
// 0x00000090 System.Void maxstAR.AndroidEngine::.ctor()
extern void AndroidEngine__ctor_m3A1478CD369CE8A638CF0707300D38AF63551309 ();
// 0x00000091 System.Void maxstAR.AndroidEngine::Dispose()
extern void AndroidEngine_Dispose_m5D05907C2A6B5AAFC3EDECDBF54FCFE0EDC207AE ();
// 0x00000092 System.Void maxstAR.ARManager::Awake()
extern void ARManager_Awake_mCAA7B934B9AF8EC6E1FA9D630887BC94C407EE41 ();
// 0x00000093 System.Void maxstAR.ARManager::OnDestroy()
extern void ARManager_OnDestroy_m2DE49FE804E572A24833720CEB21729AECD6D06F ();
// 0x00000094 System.Void maxstAR.ARManager::.ctor()
extern void ARManager__ctor_m90D816F53C0AF630815612FA7D3635A9C0F1A8EA ();
// 0x00000095 System.Void maxstAR.CameraBackgroundBehaviour::.ctor()
extern void CameraBackgroundBehaviour__ctor_mBCBAB4B41E3BB5624CA4A833B1633F8E0E18343A ();
// 0x00000096 System.Void maxstAR.CloudTrackableBehaviour::OnTrackSuccess(System.String,System.String,UnityEngine.Matrix4x4)
extern void CloudTrackableBehaviour_OnTrackSuccess_m9162AAD0D68284C1162EF26696D0F851CCCBAEFA ();
// 0x00000097 System.Void maxstAR.CloudTrackableBehaviour::OnTrackFail()
extern void CloudTrackableBehaviour_OnTrackFail_m08694155B6579E63AE34DA7F8CA0F6DA5E65120A ();
// 0x00000098 System.Void maxstAR.CloudTrackableBehaviour::.ctor()
extern void CloudTrackableBehaviour__ctor_m951A041486CA4CAE2ACA10438AD33502296CF535 ();
// 0x00000099 System.Void maxstAR.ConfigurationScriptableObject::.ctor()
extern void ConfigurationScriptableObject__ctor_mDB256C96FB7EEBE4F3254EBBEC22FB0ED551D7BF ();
// 0x0000009A System.Void maxstAR.ImageTrackableBehaviour::OnTrackSuccess(System.String,System.String,UnityEngine.Matrix4x4)
extern void ImageTrackableBehaviour_OnTrackSuccess_m003C57413718FF949E40DAA30FCBE4AEDFBC43B4 ();
// 0x0000009B System.Void maxstAR.ImageTrackableBehaviour::OnTrackFail()
extern void ImageTrackableBehaviour_OnTrackFail_mC8CF3E74324B4F8A9499AC82C7DB3B7EBF088A4A ();
// 0x0000009C System.Void maxstAR.ImageTrackableBehaviour::.ctor()
extern void ImageTrackableBehaviour__ctor_mAB8E7917CD58680BC9C7F19BA1AF29478304D910 ();
// 0x0000009D System.Void maxstAR.InstantTrackableBehaviour::OnTrackSuccess(System.String,System.String,UnityEngine.Matrix4x4)
extern void InstantTrackableBehaviour_OnTrackSuccess_m664683767C38E5BFBF0511F85CDF8BB490BDD626 ();
// 0x0000009E System.Void maxstAR.InstantTrackableBehaviour::OnTrackFail()
extern void InstantTrackableBehaviour_OnTrackFail_m2174BE1EAA71EC6D1446A13EC2CE924E88110500 ();
// 0x0000009F System.Void maxstAR.InstantTrackableBehaviour::.ctor()
extern void InstantTrackableBehaviour__ctor_m89796AA45DF4CEAC32966440D2DA9B105F1CC3F9 ();
// 0x000000A0 System.Void maxstAR.MapViewerBehaviour::.ctor()
extern void MapViewerBehaviour__ctor_m7F615F416277801AE68490EE02314A58FDCD7425 ();
// 0x000000A1 System.Single maxstAR.MarkerGroupBehaviour::get_MarkerGroupSize()
extern void MarkerGroupBehaviour_get_MarkerGroupSize_mCB933BBE7629E85A142FF1EC95BD3F7B1E994FFC ();
// 0x000000A2 System.Void maxstAR.MarkerGroupBehaviour::set_MarkerGroupSize(System.Single)
extern void MarkerGroupBehaviour_set_MarkerGroupSize_m802447A2543617061280FC3EB460B695EE7D1E73 ();
// 0x000000A3 System.Boolean maxstAR.MarkerGroupBehaviour::get_ApplyAll()
extern void MarkerGroupBehaviour_get_ApplyAll_mD6592A4A3BD3863103FA2A33EDC3C2B813DA9AD6 ();
// 0x000000A4 System.Void maxstAR.MarkerGroupBehaviour::set_ApplyAll(System.Boolean)
extern void MarkerGroupBehaviour_set_ApplyAll_mD121911C92CCC897CBDE9A7196681C1646DC7040 ();
// 0x000000A5 System.Void maxstAR.MarkerGroupBehaviour::Start()
extern void MarkerGroupBehaviour_Start_m3D8CF7662EA814083510085B270EACB87D8DF81F ();
// 0x000000A6 System.Void maxstAR.MarkerGroupBehaviour::.ctor()
extern void MarkerGroupBehaviour__ctor_m822CAA0306BE5243348C180480C51475D5D2D134 ();
// 0x000000A7 System.Int32 maxstAR.MarkerTrackerBehaviour::get_MarkerID()
extern void MarkerTrackerBehaviour_get_MarkerID_m5808106EE5E2A5E451572515DB7B808C40AF7CAF ();
// 0x000000A8 System.Void maxstAR.MarkerTrackerBehaviour::set_MarkerID(System.Int32)
extern void MarkerTrackerBehaviour_set_MarkerID_mCD6EABD5BCB80BCBA7D1019948DB8113400BB449 ();
// 0x000000A9 System.Single maxstAR.MarkerTrackerBehaviour::get_MarkerSize()
extern void MarkerTrackerBehaviour_get_MarkerSize_mF25CDDF31F37D7AF110C0345872AB47C4163F976 ();
// 0x000000AA System.Void maxstAR.MarkerTrackerBehaviour::set_MarkerSize(System.Single)
extern void MarkerTrackerBehaviour_set_MarkerSize_m71BA7BFC94ABE9C3CB251E89D90905CB9573DC7B ();
// 0x000000AB System.Void maxstAR.MarkerTrackerBehaviour::SetMarkerTrackerFileName(System.Int32,System.Single)
extern void MarkerTrackerBehaviour_SetMarkerTrackerFileName_mA05CBF97DCB7717A44E0EC39660F3F72306E2C5B ();
// 0x000000AC System.Void maxstAR.MarkerTrackerBehaviour::OnTrackSuccess(System.String,System.String,UnityEngine.Matrix4x4)
extern void MarkerTrackerBehaviour_OnTrackSuccess_mD9A8BD8B14E776F45431C7974DC21E063F3DA93E ();
// 0x000000AD System.Void maxstAR.MarkerTrackerBehaviour::OnTrackFail()
extern void MarkerTrackerBehaviour_OnTrackFail_m14AA9054E91FB352F49605A6605CF6AA3CF19D15 ();
// 0x000000AE System.Void maxstAR.MarkerTrackerBehaviour::.ctor()
extern void MarkerTrackerBehaviour__ctor_m7E2ADF96F1D77CC24DCDC3EB5EF2E6443B98D077 ();
// 0x000000AF System.Void maxstAR.ObjectTrackableBehaviour::OnTrackSuccess(System.String,System.String,UnityEngine.Matrix4x4)
extern void ObjectTrackableBehaviour_OnTrackSuccess_m85E36967C60137B76E4BE3DDEE610A222D48EFC0 ();
// 0x000000B0 System.Void maxstAR.ObjectTrackableBehaviour::OnTrackFail()
extern void ObjectTrackableBehaviour_OnTrackFail_mCBCC54510C6305A4A61CFA092F6B3AC0F0B8AF0A ();
// 0x000000B1 System.Void maxstAR.ObjectTrackableBehaviour::.ctor()
extern void ObjectTrackableBehaviour__ctor_mC54648BBEE976461518110A1F39D7BDA6E5C03EA ();
// 0x000000B2 System.String maxstAR.QrCodeTrackableBehaviour::get_QrCodeSearchingWords()
extern void QrCodeTrackableBehaviour_get_QrCodeSearchingWords_m42AF3F5A445D1163AA6A36AA5021CB8E2BD596AF ();
// 0x000000B3 System.Void maxstAR.QrCodeTrackableBehaviour::set_QrCodeSearchingWords(System.String)
extern void QrCodeTrackableBehaviour_set_QrCodeSearchingWords_m650B90F2DA2300225C2E2906DB4914CDF45A94FB ();
// 0x000000B4 System.Void maxstAR.QrCodeTrackableBehaviour::OnTrackSuccess(System.String,System.String,UnityEngine.Matrix4x4)
extern void QrCodeTrackableBehaviour_OnTrackSuccess_mA052BB30766CF011D64024713C64605925B98F63 ();
// 0x000000B5 System.Void maxstAR.QrCodeTrackableBehaviour::OnTrackFail()
extern void QrCodeTrackableBehaviour_OnTrackFail_m9F348B8961826820969AB9E16B5EE7588B841599 ();
// 0x000000B6 System.Void maxstAR.QrCodeTrackableBehaviour::.ctor()
extern void QrCodeTrackableBehaviour__ctor_mF04F26B6F2853C13C716A9A6134558BDF0BA07BD ();
// 0x000000B7 System.Void maxstAR.WearableDeviceController::.ctor()
extern void WearableDeviceController__ctor_m831CA944A05E1A51C2B807D97E4590DB3ADC8897 ();
// 0x000000B8 System.Void maxstAR.WearableDeviceController::Init()
extern void WearableDeviceController_Init_m3E8465A7C1727F11B7FD53F0779105F88E964D61 ();
// 0x000000B9 System.Void maxstAR.WearableDeviceController::DeInit()
extern void WearableDeviceController_DeInit_mA817CAF9604E3C65A2D0063B568213E86CBC8DC5 ();
// 0x000000BA System.Boolean maxstAR.WearableDeviceController::IsSupportedWearableDevice()
extern void WearableDeviceController_IsSupportedWearableDevice_m707E8F7754330646B9D1E3C887E4473D1638D3DC ();
// 0x000000BB System.String maxstAR.WearableDeviceController::GetModelName()
extern void WearableDeviceController_GetModelName_m99A1302B5AAF1D4DE11EEBA587262B6C15025C99 ();
// 0x000000BC System.Void maxstAR.WearableDeviceController::SetStereoMode(System.Boolean)
extern void WearableDeviceController_SetStereoMode_m2ECFA9DC6D80551B52000B8456E57C8DC544BCF5 ();
// 0x000000BD System.Boolean maxstAR.WearableDeviceController::IsStereoEnabled()
extern void WearableDeviceController_IsStereoEnabled_m2DEF96147706CF9842E0B7C328799D95D475A5D3 ();
// 0x000000BE System.Boolean maxstAR.WearableDeviceController::IsSideBySideType()
extern void WearableDeviceController_IsSideBySideType_mCC4857F2F8C11AB9ACE7151DFE2A9A270DD70841 ();
// 0x000000BF maxstAR.WearableManager maxstAR.WearableManager::GetInstance()
extern void WearableManager_GetInstance_m55F007C74CC1B329AE9A8AE3C05715E7CD20BE83 ();
// 0x000000C0 System.Void maxstAR.WearableManager::.ctor()
extern void WearableManager__ctor_m797FE745D17CE179C55D14E0322372200010A04A ();
// 0x000000C1 maxstAR.WearableDeviceController maxstAR.WearableManager::GetDeviceController()
extern void WearableManager_GetDeviceController_m8186D9023B3B4E4783E802F6031D1887B7CD6675 ();
// 0x000000C2 maxstAR.WearableCalibration maxstAR.WearableManager::GetCalibration()
extern void WearableManager_GetCalibration_m8B08F7C53CB3208F1B886F55772A956D8C320096 ();
// 0x000000C3 System.Void maxstAR.WearableManager::.cctor()
extern void WearableManager__cctor_m96521043C146B2AAB2B20079FF72DD367BE8F39E ();
// 0x000000C4 maxstAR.AbstractARManager maxstAR.AbstractARManager::get_Instance()
extern void AbstractARManager_get_Instance_mC07EB5ACE0F0F4833142661BD1C74554899EB94E ();
// 0x000000C5 System.Void maxstAR.AbstractARManager::Init()
extern void AbstractARManager_Init_m0E2642E4674AD4FC6F221A26DDDEE6861FFE8E83 ();
// 0x000000C6 System.Void maxstAR.AbstractARManager::InitInternal()
extern void AbstractARManager_InitInternal_m58A55ED941CD9217540F6F076729DD79AF7230B3 ();
// 0x000000C7 System.Void maxstAR.AbstractARManager::Update()
extern void AbstractARManager_Update_m76FD1C6ADC4DF8E239555799BB95040BD2DADD62 ();
// 0x000000C8 System.Void maxstAR.AbstractARManager::Deinit()
extern void AbstractARManager_Deinit_m3A63EF2C292B74FB2758C43F1EF4808A61735361 ();
// 0x000000C9 maxstAR.AbstractARManager_WorldCenterMode maxstAR.AbstractARManager::get_WorldCenterModeSetting()
extern void AbstractARManager_get_WorldCenterModeSetting_mCF8F7602828C0F38B39A879BB852DB2EA48FAD5C ();
// 0x000000CA UnityEngine.Camera maxstAR.AbstractARManager::GetARCamera()
extern void AbstractARManager_GetARCamera_m40EF848C5AE6860CF3081C0572C016490F2D3748 ();
// 0x000000CB System.Void maxstAR.AbstractARManager::SetWorldCenterMode(maxstAR.AbstractARManager_WorldCenterMode)
extern void AbstractARManager_SetWorldCenterMode_m39E9C716D2DDF61F6B7C0D1CD2BA1B5D3BCD538E ();
// 0x000000CC System.Void maxstAR.AbstractARManager::OnPreRender()
extern void AbstractARManager_OnPreRender_mC66374C9E8C277AD93394B3E60297FE0F5FC7996 ();
// 0x000000CD System.Void maxstAR.AbstractARManager::TransformBackgroundPlane(UnityEngine.Camera,UnityEngine.Transform)
extern void AbstractARManager_TransformBackgroundPlane_mDC5F3FE51A8C826AC60962C5B3464243B2BFB073 ();
// 0x000000CE System.Void maxstAR.AbstractARManager::.ctor()
extern void AbstractARManager__ctor_m5DEC2B51CEC228C1978DB0ED57C35C37050F8080 ();
// 0x000000CF System.Void maxstAR.AbstractARManager::.cctor()
extern void AbstractARManager__cctor_m045C62233F00F458A487502075B44204770F0215 ();
// 0x000000D0 maxstAR.AbstractCameraBackgroundBehaviour maxstAR.AbstractCameraBackgroundBehaviour::get_Instance()
extern void AbstractCameraBackgroundBehaviour_get_Instance_m9D2996D21A0D8050AA0F5288B5521887AE847E8E ();
// 0x000000D1 System.Void maxstAR.AbstractCameraBackgroundBehaviour::Awake()
extern void AbstractCameraBackgroundBehaviour_Awake_m190F4415A916A492BF382487C730DD054F9336A3 ();
// 0x000000D2 System.Void maxstAR.AbstractCameraBackgroundBehaviour::OnApplicationPause(System.Boolean)
extern void AbstractCameraBackgroundBehaviour_OnApplicationPause_mF8DBF5D7451906B0C39F84A15D2E5DA05C7C9D68 ();
// 0x000000D3 System.Void maxstAR.AbstractCameraBackgroundBehaviour::OnEnable()
extern void AbstractCameraBackgroundBehaviour_OnEnable_mAA48410ADBD94E4795E08EEF63F52B14036E0097 ();
// 0x000000D4 System.Void maxstAR.AbstractCameraBackgroundBehaviour::OnDisable()
extern void AbstractCameraBackgroundBehaviour_OnDisable_m2D270461F2F8FE3D0902A9EB1398844013863E86 ();
// 0x000000D5 System.Void maxstAR.AbstractCameraBackgroundBehaviour::OnDestroy()
extern void AbstractCameraBackgroundBehaviour_OnDestroy_mDD7199DBD287259196BB154D2CC02CF329018CC0 ();
// 0x000000D6 System.Void maxstAR.AbstractCameraBackgroundBehaviour::StartRendering()
extern void AbstractCameraBackgroundBehaviour_StartRendering_m66E159259900BB45FE833F381CD6E8F9E9EC4D3F ();
// 0x000000D7 System.Void maxstAR.AbstractCameraBackgroundBehaviour::StopRendering()
extern void AbstractCameraBackgroundBehaviour_StopRendering_mD61A38C3463E2E78C8187024355F5B53019A6134 ();
// 0x000000D8 System.Void maxstAR.AbstractCameraBackgroundBehaviour::CreateCameraTexture(System.Int32,System.Int32,maxstAR.ColorFormat)
extern void AbstractCameraBackgroundBehaviour_CreateCameraTexture_m7C7A1AD142EB0C8A7C75133A62938A7897B953DA ();
// 0x000000D9 System.Void maxstAR.AbstractCameraBackgroundBehaviour::UpdateCameraBackgroundImage(maxstAR.TrackingState)
extern void AbstractCameraBackgroundBehaviour_UpdateCameraBackgroundImage_m5D83FCE15F353432F2F17A531E26849F6BDE5E45 ();
// 0x000000DA System.Void maxstAR.AbstractCameraBackgroundBehaviour::UpdateCameraTexture(maxstAR.TrackedImage)
extern void AbstractCameraBackgroundBehaviour_UpdateCameraTexture_m6D98D693F762352F26F1E14CBBAFC05AB27A7787 ();
// 0x000000DB System.Void maxstAR.AbstractCameraBackgroundBehaviour::TransformBackgroundPlane()
extern void AbstractCameraBackgroundBehaviour_TransformBackgroundPlane_m4300D135CB0E0935F1044051E642C42710CDDA59 ();
// 0x000000DC System.Boolean maxstAR.AbstractCameraBackgroundBehaviour::RenderingEnabled()
extern void AbstractCameraBackgroundBehaviour_RenderingEnabled_mA1BD67148F81C665239953DDB28ACEEB270D32EF ();
// 0x000000DD System.Void maxstAR.AbstractCameraBackgroundBehaviour::.ctor()
extern void AbstractCameraBackgroundBehaviour__ctor_mE5F9E2CBA898CC21C1CC1E75EF88B70AEBC33B74 ();
// 0x000000DE System.Void maxstAR.AbstractCameraBackgroundBehaviour::.cctor()
extern void AbstractCameraBackgroundBehaviour__cctor_m92C50B96C336BE693A724916081F63D3D2B34509 ();
// 0x000000DF System.Void maxstAR.AbstractCloudTrackableBehaviour::Start()
extern void AbstractCloudTrackableBehaviour_Start_mBB334333D7E477AA725B34B79490950486EE5948 ();
// 0x000000E0 System.Void maxstAR.AbstractCloudTrackableBehaviour::OnTrackerCloudName(System.String)
extern void AbstractCloudTrackableBehaviour_OnTrackerCloudName_m01CEB9454C910F6EB94B1E0BEE21502CCF6A8F30 ();
// 0x000000E1 System.Void maxstAR.AbstractCloudTrackableBehaviour::.ctor()
extern void AbstractCloudTrackableBehaviour__ctor_m979C0E58C9AF037ED2B1CB54742F177A1A25C94A ();
// 0x000000E2 maxstAR.AbstractConfigurationScriptableObject maxstAR.AbstractConfigurationScriptableObject::GetInstance()
extern void AbstractConfigurationScriptableObject_GetInstance_m81ED52A4428D1B6BA59009100F916A6ADC20E870 ();
// 0x000000E3 System.Void maxstAR.AbstractConfigurationScriptableObject::.ctor()
extern void AbstractConfigurationScriptableObject__ctor_m94E14C17AF1F9F9E4C811B54DC716F844301A539 ();
// 0x000000E4 System.Void maxstAR.AbstractConfigurationScriptableObject::.cctor()
extern void AbstractConfigurationScriptableObject__cctor_mF6523655EB8636AA61EE174DB0028A38B9D9E339 ();
// 0x000000E5 System.Void maxstAR.AbstractFeaturePointBehaviour::OnApplicationPause(System.Boolean)
extern void AbstractFeaturePointBehaviour_OnApplicationPause_m9A9715A3590A78C86E25BEAD6445E437A2D1BFD0 ();
// 0x000000E6 System.Void maxstAR.AbstractFeaturePointBehaviour::Generate(UnityEngine.Vector3[])
extern void AbstractFeaturePointBehaviour_Generate_m490E02913B2E4C37A40EF162C055E093A4490E6E ();
// 0x000000E7 UnityEngine.Vector3[] maxstAR.AbstractFeaturePointBehaviour::convertFloatToVertex3(System.Single[],System.Int32)
extern void AbstractFeaturePointBehaviour_convertFloatToVertex3_m1E652FDF127A463DE4FB4CBD23A42D9C37045654 ();
// 0x000000E8 System.Void maxstAR.AbstractFeaturePointBehaviour::Start()
extern void AbstractFeaturePointBehaviour_Start_mE40B14C76D700241C34C619DB009FC61662DD46C ();
// 0x000000E9 System.Void maxstAR.AbstractFeaturePointBehaviour::Update()
extern void AbstractFeaturePointBehaviour_Update_m2AEDB761F5AC6FC68674CEDEE4D73994FEBD3C99 ();
// 0x000000EA System.Void maxstAR.AbstractFeaturePointBehaviour::.ctor()
extern void AbstractFeaturePointBehaviour__ctor_mB72488D9E8A75A26C0C9C0F179AF79A342B1D502 ();
// 0x000000EB System.Single maxstAR.AbstractImageTrackableBehaviour::get_TargetWidth()
extern void AbstractImageTrackableBehaviour_get_TargetWidth_m6592CA747785A5C04E24097C6F12409C184B343C ();
// 0x000000EC System.Single maxstAR.AbstractImageTrackableBehaviour::get_TargetHeight()
extern void AbstractImageTrackableBehaviour_get_TargetHeight_m34193D131A326AC2C75F81CA29C98239AA1B35BD ();
// 0x000000ED System.Void maxstAR.AbstractImageTrackableBehaviour::Start()
extern void AbstractImageTrackableBehaviour_Start_m0D62D52EBACE71FBD4DE0BD93F3B37BEE4122F12 ();
// 0x000000EE System.Void maxstAR.AbstractImageTrackableBehaviour::OnTrackerDataFileChanged(System.String)
extern void AbstractImageTrackableBehaviour_OnTrackerDataFileChanged_m9403099F381E8F5F94FED0E1F1C1015C1DD0757C ();
// 0x000000EF System.Void maxstAR.AbstractImageTrackableBehaviour::ChangeObjectProperty(System.String,System.Single)
extern void AbstractImageTrackableBehaviour_ChangeObjectProperty_m06B185B0D3B2F10E59DC780FDB5C99F0ED97DA57 ();
// 0x000000F0 System.Void maxstAR.AbstractImageTrackableBehaviour::.ctor()
extern void AbstractImageTrackableBehaviour__ctor_m2BED57A7A526406E9E548648D469A4D04AB4EB42 ();
// 0x000000F1 System.Void maxstAR.AbstractInstantTrackableBehaviour::.ctor()
extern void AbstractInstantTrackableBehaviour__ctor_m29108B4DAFA91C6C963835A43F0D12D6474EAF30 ();
// 0x000000F2 System.Int32 maxstAR.AbstractMapViewerBehaviour::get_KeyframeIndex()
extern void AbstractMapViewerBehaviour_get_KeyframeIndex_mFA6D962E366A76368C21E754749D27C5837245E3 ();
// 0x000000F3 System.Void maxstAR.AbstractMapViewerBehaviour::set_KeyframeIndex(System.Int32)
extern void AbstractMapViewerBehaviour_set_KeyframeIndex_m1A8BCC80FBF102CA245EEAC9952A40B5A4B750E8 ();
// 0x000000F4 System.Boolean maxstAR.AbstractMapViewerBehaviour::get_ShowMesh()
extern void AbstractMapViewerBehaviour_get_ShowMesh_m5F7F9706C6A22FC14E2FFEA902D147682B90105C ();
// 0x000000F5 System.Void maxstAR.AbstractMapViewerBehaviour::set_ShowMesh(System.Boolean)
extern void AbstractMapViewerBehaviour_set_ShowMesh_m540F718770D31325FF69622AC0C5CA2EFD81AB46 ();
// 0x000000F6 System.Int32 maxstAR.AbstractMapViewerBehaviour::get_MaxKeyframeCount()
extern void AbstractMapViewerBehaviour_get_MaxKeyframeCount_m887CF998FF2AA16996AC613814A1313C2766BEC7 ();
// 0x000000F7 System.Void maxstAR.AbstractMapViewerBehaviour::set_MaxKeyframeCount(System.Int32)
extern void AbstractMapViewerBehaviour_set_MaxKeyframeCount_mD9739B607EAF10275B21F1C752523E431FEB95AF ();
// 0x000000F8 System.Boolean maxstAR.AbstractMapViewerBehaviour::get_Transparent()
extern void AbstractMapViewerBehaviour_get_Transparent_m936DD71E1C00DCEAAD9A08C12D1DE5E422116652 ();
// 0x000000F9 System.Void maxstAR.AbstractMapViewerBehaviour::set_Transparent(System.Boolean)
extern void AbstractMapViewerBehaviour_set_Transparent_m23285357CEA80840211493DE88239CC15E12FE1D ();
// 0x000000FA System.Boolean maxstAR.AbstractMapViewerBehaviour::Load(System.String)
extern void AbstractMapViewerBehaviour_Load_m635856C7EEDD2BA16C9C6EC77E63BAFAF157AE52 ();
// 0x000000FB System.Boolean maxstAR.AbstractMapViewerBehaviour::ReadMap(System.String)
extern void AbstractMapViewerBehaviour_ReadMap_mFE8434D6B24D888D264DE8E70B32F9AB2A89825F ();
// 0x000000FC System.Void maxstAR.AbstractMapViewerBehaviour::SetTransparent(System.Boolean)
extern void AbstractMapViewerBehaviour_SetTransparent_m93C17E0FB178C71184CE2D33878F8C112336C35F ();
// 0x000000FD UnityEngine.Texture2D maxstAR.AbstractMapViewerBehaviour::GetCameraTexture(System.Int32,System.Int32,System.Int32)
extern void AbstractMapViewerBehaviour_GetCameraTexture_m85C6EF0EA858200D661258327336896E8EEB3E7E ();
// 0x000000FE System.Void maxstAR.AbstractMapViewerBehaviour::UpdateMapViewer()
extern void AbstractMapViewerBehaviour_UpdateMapViewer_m811BC031009749CE1C1AFAE6806875BA0AC30F31 ();
// 0x000000FF System.Void maxstAR.AbstractMapViewerBehaviour::ApplyViewCamera(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void AbstractMapViewerBehaviour_ApplyViewCamera_m96768182640E57B0991DD9FD4E1FDC3BB0DBD3B4 ();
// 0x00000100 System.Void maxstAR.AbstractMapViewerBehaviour::.ctor()
extern void AbstractMapViewerBehaviour__ctor_m1CD61831FED8AB159FD6340D25BE94ECE5355EB9 ();
// 0x00000101 System.Void maxstAR.AbstractMarkerTrackableBehaviour::Start()
extern void AbstractMarkerTrackableBehaviour_Start_m105C6D475B6FB7D05D50E13734233AEBFC5390E3 ();
// 0x00000102 System.Void maxstAR.AbstractMarkerTrackableBehaviour::OnTrackerDataFileChanged(System.String)
extern void AbstractMarkerTrackableBehaviour_OnTrackerDataFileChanged_mEAC80939F2D2339DB018CAF4F28D5285F5EDD8D2 ();
// 0x00000103 System.Void maxstAR.AbstractMarkerTrackableBehaviour::SetTargetTexture(System.String)
extern void AbstractMarkerTrackableBehaviour_SetTargetTexture_mF3A53E58F514CFAD7A72F41087F90DF90DA4CADC ();
// 0x00000104 System.Void maxstAR.AbstractMarkerTrackableBehaviour::.ctor()
extern void AbstractMarkerTrackableBehaviour__ctor_m1E5AF2B4EEC2A84DB6E1E3A5723A9818AEDDE45B ();
// 0x00000105 System.Void maxstAR.AbstractMarkerTrackableBehaviour::<SetTargetTexture>b__3_0(System.Int32,System.Int32,UnityEngine.Texture2D)
extern void AbstractMarkerTrackableBehaviour_U3CSetTargetTextureU3Eb__3_0_m0D3C2CC8A2D4AEFAC159BE658FE197EB716F29F7 ();
// 0x00000106 System.Void maxstAR.AbstractObjectTrackableBehaviour::Start()
extern void AbstractObjectTrackableBehaviour_Start_mF51D8E3C1A59F03869AE9D42068D038AC8E809EF ();
// 0x00000107 System.Void maxstAR.AbstractObjectTrackableBehaviour::OnTrackerDataFileChanged(System.String)
extern void AbstractObjectTrackableBehaviour_OnTrackerDataFileChanged_m317A4A7F141FF9035A552FA779F8903A12E950E5 ();
// 0x00000108 System.Void maxstAR.AbstractObjectTrackableBehaviour::.ctor()
extern void AbstractObjectTrackableBehaviour__ctor_m96A3DB625F240201C4E849BCD7ED1953C2C889EA ();
// 0x00000109 System.Single maxstAR.AbstractQrCodeTrackableBehaviour::get_TargetWidth()
extern void AbstractQrCodeTrackableBehaviour_get_TargetWidth_mAA449D195DECE8DC3219C0960EB7199B147054A9 ();
// 0x0000010A System.Single maxstAR.AbstractQrCodeTrackableBehaviour::get_TargetHeight()
extern void AbstractQrCodeTrackableBehaviour_get_TargetHeight_m012C52A2F32635D2FF0B5130EDEFA091CA6C27E5 ();
// 0x0000010B System.Void maxstAR.AbstractQrCodeTrackableBehaviour::Start()
extern void AbstractQrCodeTrackableBehaviour_Start_m487CDDD2A547FB2283758DD8C65958FFC28F5C64 ();
// 0x0000010C System.Void maxstAR.AbstractQrCodeTrackableBehaviour::OnTrackerDataFileChanged(System.String)
extern void AbstractQrCodeTrackableBehaviour_OnTrackerDataFileChanged_mBAD1DC41697991768A1DE5F6E867050157475854 ();
// 0x0000010D System.Void maxstAR.AbstractQrCodeTrackableBehaviour::ChangeObjectProperty(System.String,System.Single)
extern void AbstractQrCodeTrackableBehaviour_ChangeObjectProperty_m0C7EFFEC999194F1DD0BC857EF8F375ED9468FCD ();
// 0x0000010E System.Void maxstAR.AbstractQrCodeTrackableBehaviour::.ctor()
extern void AbstractQrCodeTrackableBehaviour__ctor_mE9A8DF0B052262EB04E75A2C2CC1D39AD77CACC2 ();
// 0x0000010F maxstAR.StorageType maxstAR.AbstractTrackableBehaviour::get_StorageType()
extern void AbstractTrackableBehaviour_get_StorageType_mC7A295AA52826BDADBD481781148913AF4F7E026 ();
// 0x00000110 System.Void maxstAR.AbstractTrackableBehaviour::set_StorageType(maxstAR.StorageType)
extern void AbstractTrackableBehaviour_set_StorageType_m27FDD8887AB12106A46AFE6FE5553002724FB215 ();
// 0x00000111 UnityEngine.Object maxstAR.AbstractTrackableBehaviour::get_TrackerDataFileObject()
extern void AbstractTrackableBehaviour_get_TrackerDataFileObject_m3B0BDB0D8ECCFA7DE69F776386CB7DA224DDA20C ();
// 0x00000112 System.Void maxstAR.AbstractTrackableBehaviour::set_TrackerDataFileObject(UnityEngine.Object)
extern void AbstractTrackableBehaviour_set_TrackerDataFileObject_m44DDC9BDF18D7E891A413B986C34868C719A9299 ();
// 0x00000113 System.String maxstAR.AbstractTrackableBehaviour::get_TrackerDataFileName()
extern void AbstractTrackableBehaviour_get_TrackerDataFileName_m8F2964D031717DF66DF961E0B6B5715982FFCACD ();
// 0x00000114 System.Void maxstAR.AbstractTrackableBehaviour::set_TrackerDataFileName(System.String)
extern void AbstractTrackableBehaviour_set_TrackerDataFileName_mD2094FB00347228DBB4427F1FA10E7D4BE449C7A ();
// 0x00000115 System.String maxstAR.AbstractTrackableBehaviour::get_TrackableId()
extern void AbstractTrackableBehaviour_get_TrackableId_mE9A47E10CD2D52F94D6F3E795620E89B2485A646 ();
// 0x00000116 System.Void maxstAR.AbstractTrackableBehaviour::set_TrackableId(System.String)
extern void AbstractTrackableBehaviour_set_TrackableId_m853ACEBD7A7F765B7FB2F155D17579C3769CCF3A ();
// 0x00000117 System.String maxstAR.AbstractTrackableBehaviour::get_TrackableName()
extern void AbstractTrackableBehaviour_get_TrackableName_mE50729E2E01601A8F4F5AAF42B2C57D41465F5AE ();
// 0x00000118 System.Void maxstAR.AbstractTrackableBehaviour::set_TrackableName(System.String)
extern void AbstractTrackableBehaviour_set_TrackableName_m98B239C13AB46A9E4DF64EEEF98F8C3937D80B3B ();
// 0x00000119 System.Void maxstAR.AbstractTrackableBehaviour::OnTrackerDataFileChanged(System.String)
extern void AbstractTrackableBehaviour_OnTrackerDataFileChanged_m6FD6DF5136D70134A94C9F5F7B6AFDC710EF51CC ();
// 0x0000011A System.Void maxstAR.AbstractTrackableBehaviour::OnTrackFail()
extern void AbstractTrackableBehaviour_OnTrackFail_mC11808CC0433E4540B5AAD21C8205F32CC979CD9 ();
// 0x0000011B System.Void maxstAR.AbstractTrackableBehaviour::OnTrackSuccess(System.String,System.String,UnityEngine.Matrix4x4)
extern void AbstractTrackableBehaviour_OnTrackSuccess_m18AF64A28D6EFADF43AF25A7D46E3ED497CB97EF ();
// 0x0000011C System.Void maxstAR.AbstractTrackableBehaviour::.ctor()
extern void AbstractTrackableBehaviour__ctor_mE800EF82B026CA8CA826EBA956CF4DCB6CE62D0F ();
// 0x0000011D System.Collections.IEnumerator maxstAR.APIController::POST(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Int32,System.Action`1<System.String>)
extern void APIController_POST_m22001E476C5AC0D5DFC4872F055745A49BC986D7 ();
// 0x0000011E System.String maxstAR.APIController::Escape(System.String)
extern void APIController_Escape_mBF67D5D0BBE5326CC1006DBC912A2C6EF71CDDD7 ();
// 0x0000011F System.Collections.IEnumerator maxstAR.APIController::DownloadFile(System.String,System.String,System.Action`1<System.String>)
extern void APIController_DownloadFile_mCDED32769F041F50F5583B6D5986D254D529CF93 ();
// 0x00000120 System.Void maxstAR.APIController::.ctor()
extern void APIController__ctor_mEDA57D3BE210D67CFBF9AD07EC6BF0515D77A88B ();
// 0x00000121 maxstAR.CameraDevice maxstAR.CameraDevice::GetInstance()
extern void CameraDevice_GetInstance_mA48922A8C0348FF55BF8B26DDCDDE2E1AC53CC6E ();
// 0x00000122 System.Void maxstAR.CameraDevice::.ctor()
extern void CameraDevice__ctor_m7F86B4CCF5601E918726429121508095133CA5A5 ();
// 0x00000123 maxstAR.ResultCode maxstAR.CameraDevice::Start()
extern void CameraDevice_Start_m38B9E2D8F578691A5535A183341E18F2383FADFD ();
// 0x00000124 System.Boolean maxstAR.CameraDevice::SetCalibrationData(System.String)
extern void CameraDevice_SetCalibrationData_m6C0B9344018CCE7856BAD875F8A627D7F7F0514C ();
// 0x00000125 System.Boolean maxstAR.CameraDevice::SetNewFrame(System.Byte[],System.Int32,System.Int32,System.Int32,maxstAR.ColorFormat)
extern void CameraDevice_SetNewFrame_mFBEE45714E3C33C7268920B4E98E7E394428CAC2 ();
// 0x00000126 System.Boolean maxstAR.CameraDevice::SetNewFrame(System.UInt64,System.Int32,System.Int32,System.Int32,maxstAR.ColorFormat)
extern void CameraDevice_SetNewFrame_m68695726C2493ACD1FD4E29DEA1AE509C06BCDA4 ();
// 0x00000127 System.Boolean maxstAR.CameraDevice::SetNewFrameAndTimestamp(System.Byte[],System.Int32,System.Int32,System.Int32,maxstAR.ColorFormat,System.UInt64)
extern void CameraDevice_SetNewFrameAndTimestamp_m2CAF6FC48272A710ED78310F538636CA2209BFB0 ();
// 0x00000128 System.Boolean maxstAR.CameraDevice::SetNewFrameAndTimestamp(System.UInt64,System.Int32,System.Int32,System.Int32,maxstAR.ColorFormat,System.UInt64)
extern void CameraDevice_SetNewFrameAndTimestamp_mEC9EB78F80DF0141E18672C221FB39D89E9BDDF2 ();
// 0x00000129 System.Boolean maxstAR.CameraDevice::SetFocusMode(maxstAR.CameraDevice_FocusMode)
extern void CameraDevice_SetFocusMode_m619F937DC89FCA29233729B6522FC4093A335475 ();
// 0x0000012A System.Boolean maxstAR.CameraDevice::SetFlashLightMode(System.Boolean)
extern void CameraDevice_SetFlashLightMode_mAB9FD99D4DC343850D347E294FADB9493DEBE796 ();
// 0x0000012B System.Boolean maxstAR.CameraDevice::SetAutoWhiteBalanceLock(System.Boolean)
extern void CameraDevice_SetAutoWhiteBalanceLock_m3507B0FFC7ADAB276D0743625AD670A72936687D ();
// 0x0000012C System.Void maxstAR.CameraDevice::FlipVideo(maxstAR.CameraDevice_FlipDirection,System.Boolean)
extern void CameraDevice_FlipVideo_m4B04A82F63EA5914C6A31F01A56A23F127C2029C ();
// 0x0000012D System.Boolean maxstAR.CameraDevice::IsVideoFlipped(maxstAR.CameraDevice_FlipDirection)
extern void CameraDevice_IsVideoFlipped_m9927192680CEB357BF0067A8AA2696DDE0687DC2 ();
// 0x0000012E System.Boolean maxstAR.CameraDevice::SetZoom(System.Single)
extern void CameraDevice_SetZoom_m44875B87015F4EF39DC61858E5C5CB9AC846FA34 ();
// 0x0000012F System.Single maxstAR.CameraDevice::getMaxZoomValue()
extern void CameraDevice_getMaxZoomValue_m321F6A0CE8C2F64AA419D8EE3E28CD31021C5101 ();
// 0x00000130 System.Collections.Generic.List`1<System.String> maxstAR.CameraDevice::GetParamList()
extern void CameraDevice_GetParamList_m7EF99CBB431488D24017D14665A4F50F0C024D69 ();
// 0x00000131 System.Boolean maxstAR.CameraDevice::SetParam(System.String,System.Boolean)
extern void CameraDevice_SetParam_mDC993BA13FBA434A099E4C36E8E88C7DCB4775CC ();
// 0x00000132 System.Boolean maxstAR.CameraDevice::SetParam(System.String,System.Int32)
extern void CameraDevice_SetParam_m8F2E5BFDC30491DF0C96588BEE55809004777B65 ();
// 0x00000133 System.Boolean maxstAR.CameraDevice::SetParam(System.String,System.Int32,System.Int32)
extern void CameraDevice_SetParam_m1A1579B062E71FC9B411F852B4FC1AE02AD1D205 ();
// 0x00000134 System.Boolean maxstAR.CameraDevice::SetParam(System.String,System.String)
extern void CameraDevice_SetParam_m0E8A257D4996D2AC692BF919F2555B2421F99C5E ();
// 0x00000135 maxstAR.ResultCode maxstAR.CameraDevice::Stop()
extern void CameraDevice_Stop_m9AD3F5CBA20B26A6155856015E40268C43422434 ();
// 0x00000136 System.Int32 maxstAR.CameraDevice::GetWidth()
extern void CameraDevice_GetWidth_mF960539D19402BA112EF9E70F7CF37C3DE60C511 ();
// 0x00000137 System.Int32 maxstAR.CameraDevice::GetHeight()
extern void CameraDevice_GetHeight_mA84DAC395A177AEE9DAC29C8219A86F0AF82B4AD ();
// 0x00000138 UnityEngine.Matrix4x4 maxstAR.CameraDevice::GetProjectionMatrix()
extern void CameraDevice_GetProjectionMatrix_mE52D15EF98C58320BF029798CCCA601B135F80E2 ();
// 0x00000139 System.Single[] maxstAR.CameraDevice::GetBackgroundPlaneInfo()
extern void CameraDevice_GetBackgroundPlaneInfo_m450C07D6703523E17F2A9EDFDCF71329D2155A33 ();
// 0x0000013A System.Boolean maxstAR.CameraDevice::IsFlipHorizontal()
extern void CameraDevice_IsFlipHorizontal_mBCF32945B2CDD0539EE088C59646039AC426A00F ();
// 0x0000013B System.Boolean maxstAR.CameraDevice::IsFlipVertical()
extern void CameraDevice_IsFlipVertical_m368B58585E447E0AAA377440EBC3D93987CCFBBC ();
// 0x0000013C System.Boolean maxstAR.CameraDevice::CheckCameraMove(maxstAR.TrackedImage)
extern void CameraDevice_CheckCameraMove_m4A59F29AC505547AB4CF09E591035CA39A49A558 ();
// 0x0000013D System.Void maxstAR.CameraDevice::.cctor()
extern void CameraDevice__cctor_m2129F0A488EF4741B4C42FAE320B13CCD05369B5 ();
// 0x0000013E System.Void maxstAR.CloudRecognitionAPIController::Recognize(System.String,System.String,System.String,System.Action`1<System.String>)
extern void CloudRecognitionAPIController_Recognize_m397162603EB6B030F9BE9111112A8D2E0DBBDFC6 ();
// 0x0000013F System.Void maxstAR.CloudRecognitionAPIController::DownloadCloudDataAndSave(System.String,System.String,System.Action`1<System.String>)
extern void CloudRecognitionAPIController_DownloadCloudDataAndSave_mD7DDE367BF26219758EFB0490950196D5BD1AD13 ();
// 0x00000140 System.String maxstAR.CloudRecognitionAPIController::JWTEncode(System.String,System.String)
extern void CloudRecognitionAPIController_JWTEncode_m2269AD0516458B6C6A7D31A3BBB70BB27D9EC6DC ();
// 0x00000141 System.Void maxstAR.CloudRecognitionAPIController::destroyApi()
extern void CloudRecognitionAPIController_destroyApi_mCA6030F43C571475770E2AC4667FE406089069F3 ();
// 0x00000142 System.Void maxstAR.CloudRecognitionAPIController::.ctor()
extern void CloudRecognitionAPIController__ctor_m0D6CF69548E94B7C883E45129C02F9BBDF45516B ();
// 0x00000143 System.Void maxstAR.CloudRecognitionController::SetCloudRecognitionSecretIdAndSecretKey(System.String,System.String)
extern void CloudRecognitionController_SetCloudRecognitionSecretIdAndSecretKey_mC7C9F09C20600730341C96E2038A35258DD4EC27 ();
// 0x00000144 maxstAR.CloudRecognitionController_CloudState maxstAR.CloudRecognitionController::GetCloudStatus()
extern void CloudRecognitionController_GetCloudStatus_m58CEA89750D776137CE0B9367902C9ABE6126446 ();
// 0x00000145 System.Void maxstAR.CloudRecognitionController::StartTracker()
extern void CloudRecognitionController_StartTracker_m422BD955682D9242C165CB4AE1BC99754D52FB3C ();
// 0x00000146 System.Void maxstAR.CloudRecognitionController::StopTracker()
extern void CloudRecognitionController_StopTracker_mC79E9729FE27C56117D2EB767CA02D329A9E6678 ();
// 0x00000147 System.Void maxstAR.CloudRecognitionController::DestroyTracker()
extern void CloudRecognitionController_DestroyTracker_mDE30FC70302DD304368995A083933CF3C5C6EC1C ();
// 0x00000148 System.Void maxstAR.CloudRecognitionController::FindImageOfCloudRecognition()
extern void CloudRecognitionController_FindImageOfCloudRecognition_mF067EE8BD5A498574C4B817AFABBF1551B757EC0 ();
// 0x00000149 System.Void maxstAR.CloudRecognitionController::SetAutoEnable(System.Boolean)
extern void CloudRecognitionController_SetAutoEnable_mDE82AB035CC5D2A1A17710F5157AF552E9E44886 ();
// 0x0000014A System.Void maxstAR.CloudRecognitionController::Update()
extern void CloudRecognitionController_Update_m78E1C776BBEF39E0EA8F8AFF46B51E261AEF79A7 ();
// 0x0000014B System.Void maxstAR.CloudRecognitionController::StartCloud()
extern void CloudRecognitionController_StartCloud_m9D50BA355097C6C4EA25EF034DABCC4B83B8CCF0 ();
// 0x0000014C System.Boolean maxstAR.CloudRecognitionController::GetFeatureClient(maxstAR.TrackedImage,System.Byte[],System.Int32[])
extern void CloudRecognitionController_GetFeatureClient_mD96FF71E920652FF3E5534D49449D2B64461D671 ();
// 0x0000014D System.Void maxstAR.CloudRecognitionController::GetCloudRecognition(maxstAR.TrackedImage,System.Action`2<System.Boolean,System.String>)
extern void CloudRecognitionController_GetCloudRecognition_m0D2728CB92DA7FC6738034D3E847430D43F91353 ();
// 0x0000014E System.Void maxstAR.CloudRecognitionController::.ctor()
extern void CloudRecognitionController__ctor_m758A531901287DEFC97245BF62FAF2B6BEA781AF ();
// 0x0000014F System.Void maxstAR.CloudRecognitionController::<FindImageOfCloudRecognition>b__18_0()
extern void CloudRecognitionController_U3CFindImageOfCloudRecognitionU3Eb__18_0_mA3326E9222B1B58CFD2DE181A3FEE084F684B8EB ();
// 0x00000150 System.Void maxstAR.CloudRecognitionController::<FindImageOfCloudRecognition>b__18_1(System.Boolean,System.String)
extern void CloudRecognitionController_U3CFindImageOfCloudRecognitionU3Eb__18_1_m35E0FEAFEF769DBD4F43858759D9BFB98CEE705B ();
// 0x00000151 System.Void maxstAR.CloudRecognitionController::<Update>b__20_0(System.String)
extern void CloudRecognitionController_U3CUpdateU3Eb__20_0_m0E39C1D109E446C7C9522D4806CC90EFBFC5B27B ();
// 0x00000152 System.Void maxstAR.CloudRecognitionController::<StartCloud>b__21_0(System.Boolean,System.String)
extern void CloudRecognitionController_U3CStartCloudU3Eb__21_0_mFFD68D95DFFE4179499EB4A9F9111D3F95F28BB2 ();
// 0x00000153 System.String maxstAR.CloudRecognitionData::get_ImgId()
extern void CloudRecognitionData_get_ImgId_m254192D198AD881EB2EDD2102F6552178CB771DB ();
// 0x00000154 System.Void maxstAR.CloudRecognitionData::set_ImgId(System.String)
extern void CloudRecognitionData_set_ImgId_m79E71AD5E61416275A1529EB40219F8199EBD261 ();
// 0x00000155 System.String maxstAR.CloudRecognitionData::get_Custom()
extern void CloudRecognitionData_get_Custom_m83E7944A03AD6F805079781284CEA23A51465432 ();
// 0x00000156 System.Void maxstAR.CloudRecognitionData::set_Custom(System.String)
extern void CloudRecognitionData_set_Custom_m6ECBBB95C3AAE97C9BC3C03966603EDD9DD80E6A ();
// 0x00000157 System.String maxstAR.CloudRecognitionData::get_Track2dMapUrl()
extern void CloudRecognitionData_get_Track2dMapUrl_m39ED3D8D2F4B07522CD823351C406F482D2BAB6E ();
// 0x00000158 System.Void maxstAR.CloudRecognitionData::set_Track2dMapUrl(System.String)
extern void CloudRecognitionData_set_Track2dMapUrl_mC49B035F665C5B3C34A723B17E11C00103578C5F ();
// 0x00000159 System.String maxstAR.CloudRecognitionData::get_Name()
extern void CloudRecognitionData_get_Name_m92F272A1559A256F13912361BA8FE448F29D1EAD ();
// 0x0000015A System.Void maxstAR.CloudRecognitionData::set_Name(System.String)
extern void CloudRecognitionData_set_Name_m940BCAD241C3877F116CBDD38CBD452FCA9E566B ();
// 0x0000015B System.String maxstAR.CloudRecognitionData::get_ImgGSUrl()
extern void CloudRecognitionData_get_ImgGSUrl_m53803A165B9113F3AB3D1CCD81E46329FB980EF1 ();
// 0x0000015C System.Void maxstAR.CloudRecognitionData::set_ImgGSUrl(System.String)
extern void CloudRecognitionData_set_ImgGSUrl_m3726340E54AE3183E3100C61FB318156D38B0C84 ();
// 0x0000015D System.Single maxstAR.CloudRecognitionData::get_RealWidth()
extern void CloudRecognitionData_get_RealWidth_m99605A4843B4AA03CA1C10355E3347558FC32B62 ();
// 0x0000015E System.Void maxstAR.CloudRecognitionData::set_RealWidth(System.Single)
extern void CloudRecognitionData_set_RealWidth_m8C4CCC9EA7FC57F5F169D6790160F91A3A0329CF ();
// 0x0000015F System.Void maxstAR.CloudRecognitionData::.ctor()
extern void CloudRecognitionData__ctor_m26002B3B70787153170D066AD55F0EAEF33E07CB ();
// 0x00000160 maxstAR.CloudRecognizerCache maxstAR.CloudRecognizerCache::GetInstance()
extern void CloudRecognizerCache_GetInstance_m4E9D3D961ACC034CFDB8E959A11CE9D43C2F316E ();
// 0x00000161 System.Void maxstAR.CloudRecognizerCache::ADD(System.String,System.String)
extern void CloudRecognizerCache_ADD_mC6F54D314A9B67ACC4963D1FC0EAD14BC9994B93 ();
// 0x00000162 System.Void maxstAR.CloudRecognizerCache::LOAD()
extern void CloudRecognizerCache_LOAD_m17C141EB005E69600F26191EBBC537D88D5BCA46 ();
// 0x00000163 System.Void maxstAR.CloudRecognizerCache::.ctor()
extern void CloudRecognizerCache__ctor_m3B83EF7779715D5E8DD442EF272B7077988580CB ();
// 0x00000164 System.Void maxstAR.CloudRecognizerCache::.cctor()
extern void CloudRecognizerCache__cctor_m1D701C0F1C4097A3D2BB8B048AAE1C54AF52DB91 ();
// 0x00000165 System.String maxstAR.CloudRecognitionLocalData::get_cloud()
extern void CloudRecognitionLocalData_get_cloud_m69AF68D386CA09A1AAACD1726EF7E72915FAFCC4 ();
// 0x00000166 System.Void maxstAR.CloudRecognitionLocalData::set_cloud(System.String)
extern void CloudRecognitionLocalData_set_cloud_m7A3B26E99EDAE60ACFA7E5B6F5DCF80095EACA7E ();
// 0x00000167 System.String maxstAR.CloudRecognitionLocalData::get_cloud_2dmap_path()
extern void CloudRecognitionLocalData_get_cloud_2dmap_path_m5733C888E7FDE093E18014A693E10B44635A80AF ();
// 0x00000168 System.Void maxstAR.CloudRecognitionLocalData::set_cloud_2dmap_path(System.String)
extern void CloudRecognitionLocalData_set_cloud_2dmap_path_mBCE9BAC7DDEE7271518196F6FE10AA7E54BD5A99 ();
// 0x00000169 System.String maxstAR.CloudRecognitionLocalData::get_output_path()
extern void CloudRecognitionLocalData_get_output_path_mB248037ADBDEC9FD2B0B15B1AD7F711CACCE08B3 ();
// 0x0000016A System.Void maxstAR.CloudRecognitionLocalData::set_output_path(System.String)
extern void CloudRecognitionLocalData_set_output_path_mE6287221438801AB6718EE84823A7B0D0C8F8371 ();
// 0x0000016B System.String maxstAR.CloudRecognitionLocalData::get_cloud_image_path()
extern void CloudRecognitionLocalData_get_cloud_image_path_m648A0BA4481CA7500964E5C65EEADDEF8C176EFA ();
// 0x0000016C System.Void maxstAR.CloudRecognitionLocalData::set_cloud_image_path(System.String)
extern void CloudRecognitionLocalData_set_cloud_image_path_m4BB5E755B9C797A01AA32FADFDBC27ECA398D2D0 ();
// 0x0000016D System.Single maxstAR.CloudRecognitionLocalData::get_image_width()
extern void CloudRecognitionLocalData_get_image_width_m9C454940BD98B2131BECDB36F1E226D04F3F4D01 ();
// 0x0000016E System.Void maxstAR.CloudRecognitionLocalData::set_image_width(System.Single)
extern void CloudRecognitionLocalData_set_image_width_m09960AC0A6F8C13A27D2BAAF4D408E21CF5FCD5A ();
// 0x0000016F System.String maxstAR.CloudRecognitionLocalData::get_cloud_name()
extern void CloudRecognitionLocalData_get_cloud_name_m52ADE66D64318A603E158CEFCD91CEC25066602D ();
// 0x00000170 System.Void maxstAR.CloudRecognitionLocalData::set_cloud_name(System.String)
extern void CloudRecognitionLocalData_set_cloud_name_m0F6EE579CF256F714CAD2756C43F20F0D6944354 ();
// 0x00000171 System.String maxstAR.CloudRecognitionLocalData::get_cloud_meta()
extern void CloudRecognitionLocalData_get_cloud_meta_m636E7017F316441FDAEBF2B859BBA915CC1481D9 ();
// 0x00000172 System.Void maxstAR.CloudRecognitionLocalData::set_cloud_meta(System.String)
extern void CloudRecognitionLocalData_set_cloud_meta_mADCAE884F5623D351FFABB84886CFBC1C2B52CD9 ();
// 0x00000173 System.Void maxstAR.CloudRecognitionLocalData::.ctor()
extern void CloudRecognitionLocalData__ctor_m4B4DBECAC1A83498A6A251C169623A2920BDA21E ();
// 0x00000174 System.Void maxstAR.Dimensions::.ctor(System.Int32,System.Int32)
extern void Dimensions__ctor_m60DB342A815F12B3F7C159C5A648B3BB19169A8A ();
// 0x00000175 System.Int32 maxstAR.Dimensions::get_Width()
extern void Dimensions_get_Width_mE56251026269CBCB0C817537E7FDF9BBE0F5016B ();
// 0x00000176 System.Int32 maxstAR.Dimensions::get_Height()
extern void Dimensions_get_Height_m60A7D268171AD37488478E22A40B17199FD193C4 ();
// 0x00000177 System.String maxstAR.Dimensions::ToString()
extern void Dimensions_ToString_mCA998AA750E56F5C71BB7F8E6D418C8B91FDFE94 ();
// 0x00000178 System.Void maxstAR.GuideInfo::UpdateGuideInfo()
extern void GuideInfo_UpdateGuideInfo_mAB8A020344958F11C97A3248D2B8BC63450DF17C ();
// 0x00000179 System.Single maxstAR.GuideInfo::GetInitializingProgress()
extern void GuideInfo_GetInitializingProgress_m90957C6BE85766C79AE008E6DC838D6DA73B2BF0 ();
// 0x0000017A System.Int32 maxstAR.GuideInfo::GetFeatureCount()
extern void GuideInfo_GetFeatureCount_m933350BD15F080CF22DA2BECE81C63DCAD36CB62 ();
// 0x0000017B System.Int32 maxstAR.GuideInfo::GetKeyframeCount()
extern void GuideInfo_GetKeyframeCount_mB480D72801AAAC0DE91992C2A7D2CA41255EF442 ();
// 0x0000017C System.Single[] maxstAR.GuideInfo::GetFeatureBuffer()
extern void GuideInfo_GetFeatureBuffer_m466BA90E87FF6107E5D32635D641C7F8BD41B06E ();
// 0x0000017D maxstAR.TagAnchor[] maxstAR.GuideInfo::GetTagAnchors()
extern void GuideInfo_GetTagAnchors_m4695DCEAD01483639D9D7632AF7CCB659AC6CE04 ();
// 0x0000017E System.Void maxstAR.GuideInfo::.ctor()
extern void GuideInfo__ctor_m4D0A3F456C4757B7037423BEF13AABEE98A67315 ();
// 0x0000017F System.Void maxstAR.GuideInfo::.cctor()
extern void GuideInfo__cctor_mC987A78E4A073CD9B5CCD82EE455DCDE25ECF53F ();
// 0x00000180 maxstAR.Dimensions maxstAR.JpegUtils::GetJpegDimensions(System.Byte[])
extern void JpegUtils_GetJpegDimensions_mAD50BAD17C254EEC118501CA837370AE879A2E76 ();
// 0x00000181 maxstAR.Dimensions maxstAR.JpegUtils::GetJpegDimensions(System.String)
extern void JpegUtils_GetJpegDimensions_mD1F99AA3CC85370839A11D86BB084DCE942B2541 ();
// 0x00000182 maxstAR.Dimensions maxstAR.JpegUtils::GetJpegDimensions(System.IO.Stream)
extern void JpegUtils_GetJpegDimensions_mF6E0241011EDAFACE7D735B8756B7156703D9525 ();
// 0x00000183 System.Void maxstAR.JpegUtils::.ctor()
extern void JpegUtils__ctor_m46F7D922E5E095CFC7EFBE9DB9188EE8ED12EFB2 ();
// 0x00000184 System.Void maxstAR.Map3D::.ctor()
extern void Map3D__ctor_m16A1913FBE13F1D2B45DB15D0C0E86A3872C402F ();
// 0x00000185 System.Void maxstAR.MapRendererBehaviour::Create(UnityEngine.Vector3[],UnityEngine.Matrix4x4[],UnityEngine.Material[])
extern void MapRendererBehaviour_Create_m336B86387A3AB613D05CED38C12E6514B1045F24 ();
// 0x00000186 System.Void maxstAR.MapRendererBehaviour::CreateAnchors(maxstAR.TagAnchor[])
extern void MapRendererBehaviour_CreateAnchors_mF1E7E7A9A379D7A8981F9EB650187128619F8F85 ();
// 0x00000187 UnityEngine.Mesh maxstAR.MapRendererBehaviour::CreateMapViewerMesh(System.Int32,UnityEngine.Vector3[])
extern void MapRendererBehaviour_CreateMapViewerMesh_mB68C98165D6073379B4E497309F307BA047FA24B ();
// 0x00000188 System.Void maxstAR.MapRendererBehaviour::Clear()
extern void MapRendererBehaviour_Clear_mDD40104A3B386ED06F8B0AA35DAFBDEF971C5353 ();
// 0x00000189 System.Void maxstAR.MapRendererBehaviour::SetActiveImageObject(System.Int32)
extern void MapRendererBehaviour_SetActiveImageObject_mEE5C41C7E70264D421D37862EF7DBDAFC8296CE6 ();
// 0x0000018A System.Void maxstAR.MapRendererBehaviour::SetActiveMeshObject(System.Int32)
extern void MapRendererBehaviour_SetActiveMeshObject_m7927DA92E82F8226DC2C48AB1CF1FC879173FF1E ();
// 0x0000018B System.Void maxstAR.MapRendererBehaviour::SetDeactiveImageObjects()
extern void MapRendererBehaviour_SetDeactiveImageObjects_mB13D7EA38AA6357C4D139E47B659027D1F1ADE06 ();
// 0x0000018C System.Void maxstAR.MapRendererBehaviour::SetDeactiveMeshObjects()
extern void MapRendererBehaviour_SetDeactiveMeshObjects_mB7D864EACCDB5B0D1FC25F496850EFF4E287D70C ();
// 0x0000018D System.Void maxstAR.MapRendererBehaviour::OnRenderObject()
extern void MapRendererBehaviour_OnRenderObject_m99589949F1FAEA74F28E83CDF018C0EF8534D338 ();
// 0x0000018E UnityEngine.Vector2 maxstAR.MapRendererBehaviour::GetGameViewSize()
extern void MapRendererBehaviour_GetGameViewSize_m4A5EA759DA1E669F88E08579C4887B8A48B8652F ();
// 0x0000018F System.Void maxstAR.MapRendererBehaviour::.ctor()
extern void MapRendererBehaviour__ctor_m8743BB5B6F13A9B7B5FCC6275029820DF3249BBB ();
// 0x00000190 maxstAR.MapViewer maxstAR.MapViewer::GetInstance()
extern void MapViewer_GetInstance_m4A374B55A656147154BD9DECBE577BBF5B6D90F2 ();
// 0x00000191 System.Void maxstAR.MapViewer::.ctor()
extern void MapViewer__ctor_mC3877A88FEDA836FF3937881DF34C4B1C21668B6 ();
// 0x00000192 System.Int32 maxstAR.MapViewer::Initialize(System.String)
extern void MapViewer_Initialize_m104A0FC143BCCC3635B81742713D55AB1AC63C70 ();
// 0x00000193 System.Void maxstAR.MapViewer::Deinitialize()
extern void MapViewer_Deinitialize_m4A0C5B4E073905E48E0B3694824A9FBDD0BE6AB7 ();
// 0x00000194 System.Void maxstAR.MapViewer::GetJson(System.Byte[],System.Int32)
extern void MapViewer_GetJson_m36402694A37C448BA3D29E503DD77E2ED21B10BB ();
// 0x00000195 System.Int32 maxstAR.MapViewer::Create(System.Int32)
extern void MapViewer_Create_m4B72E832C71C814B55DF3A976BD2D92A67370DC0 ();
// 0x00000196 System.Void maxstAR.MapViewer::GetIndices(System.Int32&)
extern void MapViewer_GetIndices_mDA13E7F78DBD50E88995A12D952E13D21CDDF1E0 ();
// 0x00000197 System.Void maxstAR.MapViewer::GetTexCoords(System.Single&)
extern void MapViewer_GetTexCoords_m5004DB2175BFC5BDF947A064AA5109A4C4707978 ();
// 0x00000198 System.Int32 maxstAR.MapViewer::GetImageSize(System.Int32)
extern void MapViewer_GetImageSize_m0BF1EF31E754180977191C2C4DBA22C9CFE39135 ();
// 0x00000199 System.Void maxstAR.MapViewer::GetImage(System.Int32,System.Byte&)
extern void MapViewer_GetImage_mA3677402A1AE5A5977C241A991C0C5D344BAF0EC ();
// 0x0000019A System.Void maxstAR.MapViewer::.cctor()
extern void MapViewer__cctor_mF6E85FC48BAEAE0F6364C61EECD46D754004457F ();
// 0x0000019B UnityEngine.Matrix4x4 maxstAR.MatrixUtils::ConvertGLMatrixToUnityMatrix4x4(System.Single[])
extern void MatrixUtils_ConvertGLMatrixToUnityMatrix4x4_mA761C66DC6C2CDF0843F14319E2E4074EB13E42A ();
// 0x0000019C UnityEngine.Matrix4x4 maxstAR.MatrixUtils::ConvertGLProjectionToUnityProjection(System.Single[])
extern void MatrixUtils_ConvertGLProjectionToUnityProjection_mDB182F79BC65EB2063F2D87D95518EF34E3D96B0 ();
// 0x0000019D UnityEngine.Matrix4x4 maxstAR.MatrixUtils::GetUnityPoseMatrix(System.Single[])
extern void MatrixUtils_GetUnityPoseMatrix_m3F6D97B070AB4B2518E76AF1AE4653F936D0B7F5 ();
// 0x0000019E UnityEngine.Matrix4x4 maxstAR.MatrixUtils::GetUnityPoseMatrix(UnityEngine.Matrix4x4)
extern void MatrixUtils_GetUnityPoseMatrix_mAD582DC615E01600B46AE18E9071D0AE80309441 ();
// 0x0000019F System.Void maxstAR.MatrixUtils::ApplyLocalTransformFromMatrix(UnityEngine.Transform,UnityEngine.Matrix4x4)
extern void MatrixUtils_ApplyLocalTransformFromMatrix_m17ED61807C6A4E85ABDFC4F76972CA8B68939EA5 ();
// 0x000001A0 UnityEngine.Quaternion maxstAR.MatrixUtils::InvQuaternionFromMatrix(UnityEngine.Matrix4x4)
extern void MatrixUtils_InvQuaternionFromMatrix_m8292A4671B8F0F46F53FBA58D75EBAF4DF3E0DBA ();
// 0x000001A1 UnityEngine.Vector3 maxstAR.MatrixUtils::PositionFromMatrix(UnityEngine.Matrix4x4)
extern void MatrixUtils_PositionFromMatrix_m7C6A297E614128E22FB75E01B62F47050B77EF2C ();
// 0x000001A2 UnityEngine.Vector3 maxstAR.MatrixUtils::ScaleFromMatrix(UnityEngine.Matrix4x4)
extern void MatrixUtils_ScaleFromMatrix_m807628CAC045208C9DA8BC8F23D1B1AE9A4CAFA4 ();
// 0x000001A3 UnityEngine.Matrix4x4 maxstAR.MatrixUtils::MatrixFromQuaternionAndTranslate(UnityEngine.Quaternion,UnityEngine.Vector3)
extern void MatrixUtils_MatrixFromQuaternionAndTranslate_m754AE58EE324F7F31FF2B51A49663CF03D131DCD ();
// 0x000001A4 UnityEngine.Matrix4x4 maxstAR.MatrixUtils::MatrixFromQuaternion(UnityEngine.Quaternion)
extern void MatrixUtils_MatrixFromQuaternion_m795835D0FB9ADEF22E610F1EEE604C2E34D82550 ();
// 0x000001A5 UnityEngine.Quaternion maxstAR.MatrixUtils::QuaternionFromMatrix(UnityEngine.Matrix4x4)
extern void MatrixUtils_QuaternionFromMatrix_m9BD557FFF4F96359C9C363E6F31CDBADD5BD976D ();
// 0x000001A6 System.Void maxstAR.MatrixUtils::.ctor()
extern void MatrixUtils__ctor_mD2DA0C77721FCC59A6A0FF4C8C373D07821AE790 ();
// 0x000001A7 System.String maxstAR.MaxstAR::GetVersion()
extern void MaxstAR_GetVersion_m6A48EE10DAFC38B83913C8D5450BD4BA515F1E00 ();
// 0x000001A8 System.Void maxstAR.MaxstAR::OnSurfaceChanged(System.Int32,System.Int32)
extern void MaxstAR_OnSurfaceChanged_m2B3B1318E5737F4176BDD338BACEF35913F11C80 ();
// 0x000001A9 System.Void maxstAR.MaxstAR::SetScreenOrientation(System.Int32)
extern void MaxstAR_SetScreenOrientation_mF08F84A0D6BC4DC39E4276A60B7BB19A8648C892 ();
// 0x000001AA System.Void maxstAR.MaxstAR::.ctor()
extern void MaxstAR__ctor_m54878236B4B0E38D82522F3417FCE47CEB0E8B67 ();
// 0x000001AB System.Boolean maxstAR.MaxstARUtil::IsDirectXAPI()
extern void MaxstARUtil_IsDirectXAPI_m323AD4BDC91ED65E66769E6FD6D4BBC5C7510D62 ();
// 0x000001AC System.Collections.IEnumerator maxstAR.MaxstARUtil::LoadImageFromFileWithSizeAndTexture(System.String,System.Action`3<System.Int32,System.Int32,UnityEngine.Texture2D>)
extern void MaxstARUtil_LoadImageFromFileWithSizeAndTexture_m373EFD3D6445643EBD57C7487FB53DF50ABA36E2 ();
// 0x000001AD System.String maxstAR.MaxstARUtil::ChangeNewLine(System.String)
extern void MaxstARUtil_ChangeNewLine_mA97641ABD688D4BBEC525C53265B333ECA2E2B5B ();
// 0x000001AE System.String maxstAR.MaxstARUtil::DeleteNewLine(System.String)
extern void MaxstARUtil_DeleteNewLine_mE326859F84DBBE22B17F3DCAEA504E8C7C107532 ();
// 0x000001AF System.Single maxstAR.MaxstARUtil::GetPixelFromInch(System.Int32,System.Single)
extern void MaxstARUtil_GetPixelFromInch_m58182121E84B54ABAE74CBD431F338519EBC0095 ();
// 0x000001B0 System.Single maxstAR.MaxstARUtil::DeviceDiagonalSizeInInches()
extern void MaxstARUtil_DeviceDiagonalSizeInInches_m1BE6276090981E0C1229461A102DBF6DF545EDC4 ();
// 0x000001B1 System.Collections.IEnumerator maxstAR.MaxstARUtil::ExtractAssets(System.String,System.Action`1<System.String>)
extern void MaxstARUtil_ExtractAssets_mCAF52CCFE52335A154C03E5A97E405344F52E5A9 ();
// 0x000001B2 System.Void maxstAR.MaxstARUtil::.ctor()
extern void MaxstARUtil__ctor_m01A12C3D355E4DC6AF2F5E9C1840D50EB338A1AA ();
// 0x000001B3 T maxstAR.MaxstSingleton`1::get_Instance()
// 0x000001B4 System.Void maxstAR.MaxstSingleton`1::DestroySingleTon()
// 0x000001B5 System.Void maxstAR.MaxstSingleton`1::.ctor()
// 0x000001B6 System.Void maxstAR.MaxstSingleton`1::.cctor()
// 0x000001B7 System.Void maxstAR.NativeAPI::maxst_init(System.String)
extern void NativeAPI_maxst_init_mC1AAC75A8AE9670BC22F63457FDBB43C7766E6A7 ();
// 0x000001B8 System.Void maxstAR.NativeAPI::maxst_getVersion(System.Byte[],System.Int32)
extern void NativeAPI_maxst_getVersion_mE26BC475EC44BF8656B0B9FE49777912FAA8CE1C ();
// 0x000001B9 System.Void maxstAR.NativeAPI::maxst_onSurfaceChanged(System.Int32,System.Int32)
extern void NativeAPI_maxst_onSurfaceChanged_m7690D0F0731F47423A7CB6DF92C1095519B0E7DD ();
// 0x000001BA System.Void maxstAR.NativeAPI::maxst_setScreenOrientation(System.Int32)
extern void NativeAPI_maxst_setScreenOrientation_m8D3F14D7F9B5186BA9BB44F8284B6186D967D9DE ();
// 0x000001BB System.Int32 maxstAR.NativeAPI::maxst_CameraDevice_start(System.Int32,System.Int32,System.Int32)
extern void NativeAPI_maxst_CameraDevice_start_mA5EDF2E8BFD27E3E28B6E374616A989B83646B59 ();
// 0x000001BC System.Int32 maxstAR.NativeAPI::maxst_CameraDevice_stop()
extern void NativeAPI_maxst_CameraDevice_stop_m12167043DA6AB668081ACDD6A7CCA11CAABA98A1 ();
// 0x000001BD System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setCalibrationData(System.String)
extern void NativeAPI_maxst_CameraDevice_setCalibrationData_m587F4F4A01AB5BBBB42BC2DD01713373B14FF858 ();
// 0x000001BE System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setNewFrame(System.Byte[],System.Int32,System.Int32,System.Int32,System.Int32)
extern void NativeAPI_maxst_CameraDevice_setNewFrame_m5AC1A52756AD74C655E1BA877963E72E72469FA9 ();
// 0x000001BF System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setNewFramePtr(System.UInt64,System.Int32,System.Int32,System.Int32,System.Int32)
extern void NativeAPI_maxst_CameraDevice_setNewFramePtr_mC9369C76C946063E885314743D21321F1C4F81A0 ();
// 0x000001C0 System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setNewFrameAndTimestamp(System.Byte[],System.Int32,System.Int32,System.Int32,System.Int32,System.UInt64)
extern void NativeAPI_maxst_CameraDevice_setNewFrameAndTimestamp_mA57D3969EF91AC25B3F3E9B20FD0BF7515EA5110 ();
// 0x000001C1 System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setNewFramePtrAndTimestamp(System.UInt64,System.Int32,System.Int32,System.Int32,System.Int32,System.UInt64)
extern void NativeAPI_maxst_CameraDevice_setNewFramePtrAndTimestamp_m2C4355973A2A9C68BB3EAD9C926E567B0D2E64A5 ();
// 0x000001C2 System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setFocusMode(System.Int32)
extern void NativeAPI_maxst_CameraDevice_setFocusMode_m480F26C45A73ACE6BF49C7ADF2A80B5546D518D1 ();
// 0x000001C3 System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setFlashLightMode(System.Boolean)
extern void NativeAPI_maxst_CameraDevice_setFlashLightMode_m9B82A6F46EF0D5094D7FA8891EB6672BD85B3F5C ();
// 0x000001C4 System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setAutoWhiteBalanceLock(System.Boolean)
extern void NativeAPI_maxst_CameraDevice_setAutoWhiteBalanceLock_m415195884E839F12CE465AE0705524CA5D9C2ECA ();
// 0x000001C5 System.Void maxstAR.NativeAPI::maxst_CameraDevice_flipVideo(System.Int32,System.Boolean)
extern void NativeAPI_maxst_CameraDevice_flipVideo_m62342772920CF6123AF1D33BA4456EE6177F1BCB ();
// 0x000001C6 System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_isVideoFlipped(System.Int32)
extern void NativeAPI_maxst_CameraDevice_isVideoFlipped_m172D75CB004ACB86EF78779056C3090CF6250794 ();
// 0x000001C7 System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setZoom(System.Single)
extern void NativeAPI_maxst_CameraDevice_setZoom_m95118B352941DD6009C65B64BD91E2DF3EF1C64A ();
// 0x000001C8 System.Single maxstAR.NativeAPI::maxst_CameraDevice_getMaxZoomValue()
extern void NativeAPI_maxst_CameraDevice_getMaxZoomValue_m352526181B5BFBD10AE14F3A34197D14F452EA5B ();
// 0x000001C9 System.Int32 maxstAR.NativeAPI::maxst_CameraDevice_getParamList()
extern void NativeAPI_maxst_CameraDevice_getParamList_m733CCFA434F9BC8804846201D8A23DEBD851BE24 ();
// 0x000001CA System.Int32 maxstAR.NativeAPI::maxst_CameraDevice_Param_getKeyLength(System.Int32)
extern void NativeAPI_maxst_CameraDevice_Param_getKeyLength_mEB26C85CBBA25FDF23945A221325BCF7DA9D1747 ();
// 0x000001CB System.Void maxstAR.NativeAPI::maxst_CameraDevice_Param_getKey(System.Int32,System.Byte[])
extern void NativeAPI_maxst_CameraDevice_Param_getKey_m4217BEDAF8FF57FCCA097FD754BC11C7E30F2E21 ();
// 0x000001CC System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setBoolTypeParameter(System.String,System.Boolean)
extern void NativeAPI_maxst_CameraDevice_setBoolTypeParameter_mDCDD272E5B1E9C5C38F7D9E3EC9FB2D0D0D9DC94 ();
// 0x000001CD System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setIntTypeParameter(System.String,System.Int32)
extern void NativeAPI_maxst_CameraDevice_setIntTypeParameter_mF657FA7F7A8C5916B84F08E47D1A8FB6D44871A4 ();
// 0x000001CE System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setRangeTypeParameter(System.String,System.Int32,System.Int32)
extern void NativeAPI_maxst_CameraDevice_setRangeTypeParameter_mBBD598D47203A43530A044712E1000ECFB5934C0 ();
// 0x000001CF System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setStringTypeParameter(System.String,System.String)
extern void NativeAPI_maxst_CameraDevice_setStringTypeParameter_m92EF41905F8DA6690DD73DD5A1028C6C0CCC285D ();
// 0x000001D0 System.Int32 maxstAR.NativeAPI::maxst_CameraDevice_getWidth()
extern void NativeAPI_maxst_CameraDevice_getWidth_mA3A09F5F4397EFB824807005333759DB1F7F4C6F ();
// 0x000001D1 System.Int32 maxstAR.NativeAPI::maxst_CameraDevice_getHeight()
extern void NativeAPI_maxst_CameraDevice_getHeight_m549A526D0E3B00AAA8CE00A7DF29D99CD136C5D0 ();
// 0x000001D2 System.Void maxstAR.NativeAPI::maxst_CameraDevice_getProjectionMatrix(System.Single[])
extern void NativeAPI_maxst_CameraDevice_getProjectionMatrix_m7A7FA73573233264B63C3FEDC2F7E8EF45C1E80A ();
// 0x000001D3 System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_checkCameraMove(System.UInt64)
extern void NativeAPI_maxst_CameraDevice_checkCameraMove_mF17BBFE375895257ECAA4FD7263CFA3ED4D16657 ();
// 0x000001D4 System.Void maxstAR.NativeAPI::maxst_CameraDevice_getBackgroundPlaneInfo(System.Single[])
extern void NativeAPI_maxst_CameraDevice_getBackgroundPlaneInfo_m7C75B9FDE102ABF63B08C501982DA781B7661AD7 ();
// 0x000001D5 System.Void maxstAR.NativeAPI::maxst_TrackerManager_startTracker(System.Int32)
extern void NativeAPI_maxst_TrackerManager_startTracker_mD2A013E2FC6B619155F37192F9AB7DABB44CB121 ();
// 0x000001D6 System.Void maxstAR.NativeAPI::maxst_TrackerManager_stopTracker()
extern void NativeAPI_maxst_TrackerManager_stopTracker_m2AC86CAD2BE61A2E1451B47F8FA2488D813989F2 ();
// 0x000001D7 System.Void maxstAR.NativeAPI::maxst_TrackerManager_destroyTracker()
extern void NativeAPI_maxst_TrackerManager_destroyTracker_m1174ED1C73195AEE3AB49531497301A9D9366262 ();
// 0x000001D8 System.Void maxstAR.NativeAPI::maxst_TrackerManager_addTrackerData(System.String,System.Boolean)
extern void NativeAPI_maxst_TrackerManager_addTrackerData_m51C22A522B54749FDBC43568D1B0BAE9E3B6C497 ();
// 0x000001D9 System.Void maxstAR.NativeAPI::maxst_TrackerManager_removeTrackerData(System.String)
extern void NativeAPI_maxst_TrackerManager_removeTrackerData_mEB0715C35637542C19BEC1A33417F97CC370369B ();
// 0x000001DA System.Void maxstAR.NativeAPI::maxst_TrackerManager_loadTrackerData()
extern void NativeAPI_maxst_TrackerManager_loadTrackerData_mD7B98D711F885256BE5A3E37221387D6CB67EBAF ();
// 0x000001DB System.Void maxstAR.NativeAPI::maxst_TrackerManager_setTrackingOption(System.Int32)
extern void NativeAPI_maxst_TrackerManager_setTrackingOption_m002A7BE8981B92CF58464C56E6C7BD699699FADB ();
// 0x000001DC System.Boolean maxstAR.NativeAPI::maxst_TrackerManager_isTrackerDataLoadCompleted()
extern void NativeAPI_maxst_TrackerManager_isTrackerDataLoadCompleted_mE1E670296E50E476D4434C251FADEDB1F8CD48DA ();
// 0x000001DD System.Void maxstAR.NativeAPI::maxst_TrackerManager_setVocabulary(System.String,System.Boolean)
extern void NativeAPI_maxst_TrackerManager_setVocabulary_m9D20A8B8F7760E8A505E8B6FE179B70BB72C03BF ();
// 0x000001DE System.UInt64 maxstAR.NativeAPI::maxst_TrackerManager_updateTrackingState()
extern void NativeAPI_maxst_TrackerManager_updateTrackingState_m1FF8B6E79093D760E2FF3FC0FBDA947FAB86F11E ();
// 0x000001DF System.Void maxstAR.NativeAPI::maxst_TrackerManager_findSurface()
extern void NativeAPI_maxst_TrackerManager_findSurface_m2F01DD3DA099836016D8C7EB80CFA1EB7770D202 ();
// 0x000001E0 System.Void maxstAR.NativeAPI::maxst_TrackerManager_quitFindingSurface()
extern void NativeAPI_maxst_TrackerManager_quitFindingSurface_m383FB2DFD3D0404740C354D781CCA20509073AED ();
// 0x000001E1 System.UInt64 maxstAR.NativeAPI::maxst_TrackerManager_getGuideInfo()
extern void NativeAPI_maxst_TrackerManager_getGuideInfo_mC3CD90B50593B28C29E10927009327A4E24FEFF0 ();
// 0x000001E2 System.UInt64 maxstAR.NativeAPI::maxst_TrackerManager_saveSurfaceData(System.String)
extern void NativeAPI_maxst_TrackerManager_saveSurfaceData_m5FB3E8361B4713D63718A2D372961B299B94728A ();
// 0x000001E3 System.Void maxstAR.NativeAPI::maxst_TrackerManager_getWorldPositionFromScreenCoordinate(System.Single[],System.Single[])
extern void NativeAPI_maxst_TrackerManager_getWorldPositionFromScreenCoordinate_m06785004CF3907F36BA753A6E0293B466DC56990 ();
// 0x000001E4 System.Boolean maxstAR.NativeAPI::maxst_CloudManager_GetFeatureClient(System.UInt64,System.Byte[],System.Int32[])
extern void NativeAPI_maxst_CloudManager_GetFeatureClient_m76665EAADF00E6475C0A04F28E5B534D8EACE79F ();
// 0x000001E5 System.Void maxstAR.NativeAPI::maxst_CloudManager_JWTEncode(System.String,System.String,System.Byte[])
extern void NativeAPI_maxst_CloudManager_JWTEncode_m6BDF4BC161AE729828474140E31DB0FC185BD889 ();
// 0x000001E6 System.Int32 maxstAR.NativeAPI::maxst_TrackingResult_getCount(System.UInt64)
extern void NativeAPI_maxst_TrackingResult_getCount_mC3FBA8BCB14A66845A307310DED26C50257E5082 ();
// 0x000001E7 System.UInt64 maxstAR.NativeAPI::maxst_TrackingResult_getTrackable(System.UInt64,System.Int32)
extern void NativeAPI_maxst_TrackingResult_getTrackable_m0F5189AD387C9735BFF31D519CE9F0E27CA95B9B ();
// 0x000001E8 System.Void maxstAR.NativeAPI::maxst_Trackable_getId(System.UInt64,System.Byte[])
extern void NativeAPI_maxst_Trackable_getId_m65E02BA95C26BC662C8DE07DBCB80FF71F34A7F2 ();
// 0x000001E9 System.Void maxstAR.NativeAPI::maxst_Trackable_getName(System.UInt64,System.Byte[])
extern void NativeAPI_maxst_Trackable_getName_mDEAA3725E5E6CB1E45555AACC612F1F1BDAD114F ();
// 0x000001EA System.Void maxstAR.NativeAPI::maxst_Trackable_getCloudName(System.UInt64,System.Byte[])
extern void NativeAPI_maxst_Trackable_getCloudName_m45027EEE9B6AB68067EB5C233B50B1BEE93ECE3D ();
// 0x000001EB System.Void maxstAR.NativeAPI::maxst_Trackable_getCloudMeta(System.UInt64,System.Byte[],System.Int32[])
extern void NativeAPI_maxst_Trackable_getCloudMeta_mEE9F177E4A24BD1FCF987F11028E96FCD17C6515 ();
// 0x000001EC System.Void maxstAR.NativeAPI::maxst_Trackable_getPose(System.UInt64,System.Single[])
extern void NativeAPI_maxst_Trackable_getPose_m656155FAC79B8376B46E70AD1B284C27F088DB82 ();
// 0x000001ED System.Single maxstAR.NativeAPI::maxst_Trackable_getWidth(System.UInt64)
extern void NativeAPI_maxst_Trackable_getWidth_m98564B655F63F5CF6674E8EDA4F8BD0E4FB5A715 ();
// 0x000001EE System.Single maxstAR.NativeAPI::maxst_Trackable_getHeight(System.UInt64)
extern void NativeAPI_maxst_Trackable_getHeight_mD2C8D6D0DAEA052AFFDAF7E886EA4658A54D40AE ();
// 0x000001EF System.UInt64 maxstAR.NativeAPI::maxst_TrackingState_getTrackingResult(System.UInt64)
extern void NativeAPI_maxst_TrackingState_getTrackingResult_m663F7A5997448E4FF7726D09A8901FDCFD976FE1 ();
// 0x000001F0 System.UInt64 maxstAR.NativeAPI::maxst_TrackingState_getImage(System.UInt64)
extern void NativeAPI_maxst_TrackingState_getImage_mBEE338FF02F972166CA81F5117BE653C6156003B ();
// 0x000001F1 System.Int32 maxstAR.NativeAPI::maxst_TrackingState_getCodeScanResultLength(System.UInt64)
extern void NativeAPI_maxst_TrackingState_getCodeScanResultLength_mE478E4BA4BEC1CA111EA9A265209E2AF38A484D3 ();
// 0x000001F2 System.Void maxstAR.NativeAPI::maxst_TrackingState_getCodeScanResult(System.UInt64,System.Byte[],System.Int32)
extern void NativeAPI_maxst_TrackingState_getCodeScanResult_m88980E71855098278471653402255093CF47A43A ();
// 0x000001F3 System.Single maxstAR.NativeAPI::maxst_GuideInfo_getInitializingProgress(System.UInt64)
extern void NativeAPI_maxst_GuideInfo_getInitializingProgress_m473962B12C71875B6678C219BFDCA64BB9FD4BE3 ();
// 0x000001F4 System.Int32 maxstAR.NativeAPI::maxst_GuideInfo_getKeyframeCount(System.UInt64)
extern void NativeAPI_maxst_GuideInfo_getKeyframeCount_m60AA17AE8822D71F3C8272A1CF9348501C3E5154 ();
// 0x000001F5 System.Int32 maxstAR.NativeAPI::maxst_GuideInfo_getFeatureCount(System.UInt64)
extern void NativeAPI_maxst_GuideInfo_getFeatureCount_mEEBA38F8FD74F6415ECB0C2A6FAF6B786CEB43A8 ();
// 0x000001F6 System.Void maxstAR.NativeAPI::maxst_GuideInfo_getFeatureBuffer(System.UInt64,System.Single[],System.Int32)
extern void NativeAPI_maxst_GuideInfo_getFeatureBuffer_m9FBE90B9269C733F504CE5286E63A0704DBA8CDB ();
// 0x000001F7 System.Int32 maxstAR.NativeAPI::maxst_GuideInfo_getTagAnchorsLength(System.UInt64)
extern void NativeAPI_maxst_GuideInfo_getTagAnchorsLength_m01C8BA9DDA202B8A5D13EE294449CAEBD49A8235 ();
// 0x000001F8 System.Void maxstAR.NativeAPI::maxst_GuideInfo_getTagAnchors(System.UInt64,System.Byte[],System.Int32)
extern void NativeAPI_maxst_GuideInfo_getTagAnchors_mF729BEEB7CB35A14F3E3A5A300221AB9AFB2CEF1 ();
// 0x000001F9 System.Int32 maxstAR.NativeAPI::maxst_SurfaceThumbnail_getWidth(System.UInt64)
extern void NativeAPI_maxst_SurfaceThumbnail_getWidth_m5374309D1ED7CE91A09C299E8C112E8F20344A74 ();
// 0x000001FA System.Int32 maxstAR.NativeAPI::maxst_SurfaceThumbnail_getHeight(System.UInt64)
extern void NativeAPI_maxst_SurfaceThumbnail_getHeight_m68521172E9A9936326ED88B690E3E87E356000AE ();
// 0x000001FB System.Int32 maxstAR.NativeAPI::maxst_SurfaceThumbnail_getLength(System.UInt64)
extern void NativeAPI_maxst_SurfaceThumbnail_getLength_mCCC4613A0617DA641492CC0EC46F091D996725B8 ();
// 0x000001FC System.Int32 maxstAR.NativeAPI::maxst_SurfaceThumbnail_getBpp(System.UInt64)
extern void NativeAPI_maxst_SurfaceThumbnail_getBpp_m7BD8DCB38F1117093E38BF9E0996B0D3A829F026 ();
// 0x000001FD System.Int32 maxstAR.NativeAPI::maxst_SurfaceThumbnail_getData(System.UInt64,System.Byte[],System.Int32)
extern void NativeAPI_maxst_SurfaceThumbnail_getData_m72CC556F4243C8F4543F2902960EBB20C0117977 ();
// 0x000001FE System.Void maxstAR.NativeAPI::maxst_SensorDevice_startSensor()
extern void NativeAPI_maxst_SensorDevice_startSensor_mA7826A672D56DAB8BC392A3695B8E0C39752C85A ();
// 0x000001FF System.Void maxstAR.NativeAPI::maxst_SensorDevice_stopSensor()
extern void NativeAPI_maxst_SensorDevice_stopSensor_mB3D457114603E18053BC43A560581BE02752F5B6 ();
// 0x00000200 System.Int32 maxstAR.NativeAPI::maxst_MapViewer_initialize(System.String)
extern void NativeAPI_maxst_MapViewer_initialize_mE1D0936F186A301F61346D3DFEECC1A6A7BDE886 ();
// 0x00000201 System.Void maxstAR.NativeAPI::maxst_MapViewer_deInitialize()
extern void NativeAPI_maxst_MapViewer_deInitialize_m9803387B412EEA0C97F82BECF47CA2C4B90D3180 ();
// 0x00000202 System.Void maxstAR.NativeAPI::maxst_MapViewer_getJson(System.Byte[],System.Int32)
extern void NativeAPI_maxst_MapViewer_getJson_mFF2D1942D6D85FFDA8DBFBB37D9277D42CF4747E ();
// 0x00000203 System.Int32 maxstAR.NativeAPI::maxst_MapViewer_create(System.Int32)
extern void NativeAPI_maxst_MapViewer_create_mA8A51DE668DA01D1EEFCD1CDE0607DD9E05A3EA6 ();
// 0x00000204 System.Void maxstAR.NativeAPI::maxst_MapViewer_getIndices(System.Int32&)
extern void NativeAPI_maxst_MapViewer_getIndices_m7BE3DB492739E0227CED9F498E42E7A0C76987B1 ();
// 0x00000205 System.Void maxstAR.NativeAPI::maxst_MapViewer_getTexCoords(System.Single&)
extern void NativeAPI_maxst_MapViewer_getTexCoords_mC4F8441C2CEB2512FA632CBD82512FF06946429B ();
// 0x00000206 System.Int32 maxstAR.NativeAPI::maxst_MapViewer_getImageSize(System.Int32)
extern void NativeAPI_maxst_MapViewer_getImageSize_m3E303CD3A68C838A42E359CD9AB18E29B05C12F7 ();
// 0x00000207 System.Void maxstAR.NativeAPI::maxst_MapViewer_getImage(System.Int32,System.Byte&)
extern void NativeAPI_maxst_MapViewer_getImage_m39F99EEC2A336BC6610FD511FE28038836936687 ();
// 0x00000208 System.Boolean maxstAR.NativeAPI::maxst_WearableCalibration_isActivated()
extern void NativeAPI_maxst_WearableCalibration_isActivated_m9143F692C8392D25C750D38EC06B266105B72EBD ();
// 0x00000209 System.Boolean maxstAR.NativeAPI::maxst_WearableCalibration_init(System.String)
extern void NativeAPI_maxst_WearableCalibration_init_m772CE19706D3AE2AB4E896585B5FB7EC8A4704AF ();
// 0x0000020A System.Void maxstAR.NativeAPI::maxst_WearableCalibration_deinit()
extern void NativeAPI_maxst_WearableCalibration_deinit_m174A61A633706CCF0CCFAD86AF3CD91892AF8367 ();
// 0x0000020B System.Void maxstAR.NativeAPI::maxst_WearableCalibration_setSurfaceSize(System.Int32,System.Int32)
extern void NativeAPI_maxst_WearableCalibration_setSurfaceSize_mE84BDEFC9470F81B1CAB9DF7A2092787A182DCB4 ();
// 0x0000020C System.Void maxstAR.NativeAPI::maxst_WearableCalibration_getProjectionMatrix(System.Single[],System.Int32)
extern void NativeAPI_maxst_WearableCalibration_getProjectionMatrix_mCD2BC624AB1D8DEFEADA180420AA58535AB0353A ();
// 0x0000020D System.Int32 maxstAR.NativeAPI::maxst_TrackedImage_getIndex(System.UInt64)
extern void NativeAPI_maxst_TrackedImage_getIndex_mEEB773E5420A16C287B64850530C2430BCDF530E ();
// 0x0000020E System.Int32 maxstAR.NativeAPI::maxst_TrackedImage_getWidth(System.UInt64)
extern void NativeAPI_maxst_TrackedImage_getWidth_m668590037D74A9A8529245D389A52E7FA74E5933 ();
// 0x0000020F System.Int32 maxstAR.NativeAPI::maxst_TrackedImage_getHeight(System.UInt64)
extern void NativeAPI_maxst_TrackedImage_getHeight_m694B87A48F1267C12A88DCA22C5DFFD821C8E8A0 ();
// 0x00000210 System.Int32 maxstAR.NativeAPI::maxst_TrackedImage_getLength(System.UInt64)
extern void NativeAPI_maxst_TrackedImage_getLength_m4AD455C6AA5D2126478F92288A431367202CEFD2 ();
// 0x00000211 System.Int32 maxstAR.NativeAPI::maxst_TrackedImage_getFormat(System.UInt64)
extern void NativeAPI_maxst_TrackedImage_getFormat_m775B4491F0F0CDF95904BA5C7CCDCE8819ADFAA0 ();
// 0x00000212 System.Void maxstAR.NativeAPI::maxst_TrackedImage_getData(System.UInt64,System.Byte[],System.Int32)
extern void NativeAPI_maxst_TrackedImage_getData_mD323FBE5A761CD2CB39DB663212524FB1FB71D95 ();
// 0x00000213 System.UInt64 maxstAR.NativeAPI::maxst_TrackedImage_getDataPtr(System.UInt64)
extern void NativeAPI_maxst_TrackedImage_getDataPtr_mD62AC911086BD8845DBDB9547225539C37EFCA3F ();
// 0x00000214 System.Void maxstAR.NativeAPI::maxst_TrackedImage_getYuv420spY_UVPtr(System.UInt64,System.IntPtr&,System.IntPtr&)
extern void NativeAPI_maxst_TrackedImage_getYuv420spY_UVPtr_mC80CE7D9A89A78E3FF325ECB66907D7AEEAE418B ();
// 0x00000215 System.UInt64 maxstAR.NativeAPI::maxst_TrackedImage_getYuv420spY_U_VPtr(System.UInt64,System.IntPtr&,System.IntPtr&,System.IntPtr&)
extern void NativeAPI_maxst_TrackedImage_getYuv420spY_U_VPtr_m906C3214FA840F6D0B696010ACB4BDCC10F771BD ();
// 0x00000216 System.UInt64 maxstAR.NativeAPI::maxst_TrackedImage_getYuv420_888YUVPtr(System.UInt64,System.IntPtr&,System.IntPtr&,System.IntPtr&,System.Boolean)
extern void NativeAPI_maxst_TrackedImage_getYuv420_888YUVPtr_mCF529A3F15BDD5D195375005588D18D1EA2863CC ();
// 0x00000217 System.Void maxstAR.NativeAPI::.ctor()
extern void NativeAPI__ctor_m128CF9DC4569AE87E3E6BFA215BA49606269D5C1 ();
// 0x00000218 System.Void maxstAR.Point3Df::.ctor()
extern void Point3Df__ctor_m934B342CC4021CACF855676356D026CE130870B5 ();
// 0x00000219 maxstAR.SensorDevice maxstAR.SensorDevice::GetInstance()
extern void SensorDevice_GetInstance_m7F13A44D48036B73D11E9CD55A9EA7ABAE1D430F ();
// 0x0000021A System.Void maxstAR.SensorDevice::.ctor()
extern void SensorDevice__ctor_m7203FB445DDFA689277FCFDE08A9D69B89CA346C ();
// 0x0000021B System.Void maxstAR.SensorDevice::Start()
extern void SensorDevice_Start_m36EF038173578054700423CC007B17AC66DBD6D9 ();
// 0x0000021C System.Void maxstAR.SensorDevice::Stop()
extern void SensorDevice_Stop_mD2DE3E49BD054C25601EE1A274B7038C7BF7EABE ();
// 0x0000021D System.Void maxstAR.SensorDevice::.cctor()
extern void SensorDevice__cctor_m821F54269B8B6BB3947D6EA253CCC98AABE2FEC0 ();
// 0x0000021E System.Void maxstAR.SurfaceThumbnail::.ctor(System.UInt64)
extern void SurfaceThumbnail__ctor_m0DD58C3F68E19DECAA5378921816128B06360334 ();
// 0x0000021F System.Int32 maxstAR.SurfaceThumbnail::GetWidth()
extern void SurfaceThumbnail_GetWidth_m2DAAAE442D0F0D28B8ABB83DF3B96DAAAE74574D ();
// 0x00000220 System.Int32 maxstAR.SurfaceThumbnail::GetHeight()
extern void SurfaceThumbnail_GetHeight_m8DE428A2D928FA3960D8B1706012DAC5A4E4DD4B ();
// 0x00000221 System.Int32 maxstAR.SurfaceThumbnail::GetLength()
extern void SurfaceThumbnail_GetLength_m2A9A3AC873AB4069E48B88AF8D1D24AF81FFB47F ();
// 0x00000222 System.Byte[] maxstAR.SurfaceThumbnail::GetData()
extern void SurfaceThumbnail_GetData_mB4539CA663D4B080D677C046FFBBF33A03F3B428 ();
// 0x00000223 System.String maxstAR.TagAnchor::get_name()
extern void TagAnchor_get_name_m99C9E247B0771C777A7A09CD8B3A2D2FE0964240 ();
// 0x00000224 System.Void maxstAR.TagAnchor::set_name(System.String)
extern void TagAnchor_set_name_m443B327E4B533C8F67F3173D955C15526E1D0C79 ();
// 0x00000225 System.Single maxstAR.TagAnchor::get_positionX()
extern void TagAnchor_get_positionX_m4BA6F4722CB2624F28E04F70B1B669E1D44C6115 ();
// 0x00000226 System.Void maxstAR.TagAnchor::set_positionX(System.Single)
extern void TagAnchor_set_positionX_m9C85C1BE8641AA561F5E78C35ECD34B47B2E7068 ();
// 0x00000227 System.Single maxstAR.TagAnchor::get_positionY()
extern void TagAnchor_get_positionY_mA4FECF70AA30253F3A9C8379DBF7C28E11FBC46D ();
// 0x00000228 System.Void maxstAR.TagAnchor::set_positionY(System.Single)
extern void TagAnchor_set_positionY_m44F3448EBACBE4404CC470ADEF20003BA65842CE ();
// 0x00000229 System.Single maxstAR.TagAnchor::get_positionZ()
extern void TagAnchor_get_positionZ_m43C90B29A0D5F78F8CBA8370B50CD1E678718671 ();
// 0x0000022A System.Void maxstAR.TagAnchor::set_positionZ(System.Single)
extern void TagAnchor_set_positionZ_m8930567E2FAD7386F0B5FEED38AC4B2097E528C4 ();
// 0x0000022B System.Single maxstAR.TagAnchor::get_rotationX()
extern void TagAnchor_get_rotationX_mE3F3509DE799F285E067105834716D2118880FD3 ();
// 0x0000022C System.Void maxstAR.TagAnchor::set_rotationX(System.Single)
extern void TagAnchor_set_rotationX_mC19A47B642FF9C74C1D9A42DEAA0CEBC527A0609 ();
// 0x0000022D System.Single maxstAR.TagAnchor::get_rotationY()
extern void TagAnchor_get_rotationY_m3C25FA7B23C3900A234BA4A424B11B1EAAA40741 ();
// 0x0000022E System.Void maxstAR.TagAnchor::set_rotationY(System.Single)
extern void TagAnchor_set_rotationY_m79330569E15C6C5D3FE5963589D34EC5B46B772A ();
// 0x0000022F System.Single maxstAR.TagAnchor::get_rotationZ()
extern void TagAnchor_get_rotationZ_m53B878CB1D016601745D41E200784B4E373A1D28 ();
// 0x00000230 System.Void maxstAR.TagAnchor::set_rotationZ(System.Single)
extern void TagAnchor_set_rotationZ_m4B0052670F90E53A36CB302B99CE9B4951DA0F70 ();
// 0x00000231 System.Single maxstAR.TagAnchor::get_scaleX()
extern void TagAnchor_get_scaleX_m2E449DC6C0A2DC69CC442A296EF8ABD66053361B ();
// 0x00000232 System.Void maxstAR.TagAnchor::set_scaleX(System.Single)
extern void TagAnchor_set_scaleX_m343BCB76E871D5290A302F91E938311D78E8485F ();
// 0x00000233 System.Single maxstAR.TagAnchor::get_scaleY()
extern void TagAnchor_get_scaleY_m390023FE173DD6CB7DE0A8A109299F2E9D2B3922 ();
// 0x00000234 System.Void maxstAR.TagAnchor::set_scaleY(System.Single)
extern void TagAnchor_set_scaleY_m1F81421BD263D9D41332AFBC86CA014D56721256 ();
// 0x00000235 System.Single maxstAR.TagAnchor::get_scaleZ()
extern void TagAnchor_get_scaleZ_m772C067FCBA84BCFD7058A1F284253669FE30C6B ();
// 0x00000236 System.Void maxstAR.TagAnchor::set_scaleZ(System.Single)
extern void TagAnchor_set_scaleZ_m75F59D542E2F765CE2C0895282807A30F3238500 ();
// 0x00000237 System.Void maxstAR.TagAnchor::.ctor()
extern void TagAnchor__ctor_mD1A039D323CC5DBA758D093572C4E6BE8CCCE56B ();
// 0x00000238 System.Void maxstAR.TagAnchors::.ctor()
extern void TagAnchors__ctor_m5DB5FB2917283C43ACC648245EC4E876284AAB1F ();
// 0x00000239 System.Void maxstAR.Trackable::.ctor(System.UInt64)
extern void Trackable__ctor_m28195F5B5619EBC04AF14B18F8D5773F9A37B195 ();
// 0x0000023A System.String maxstAR.Trackable::GetId()
extern void Trackable_GetId_mA44F5BE3181F6C793190AF897E101D04DB40BCA9 ();
// 0x0000023B System.String maxstAR.Trackable::GetName()
extern void Trackable_GetName_mD9121445DC81017509E298724A29BF043A619D8D ();
// 0x0000023C System.String maxstAR.Trackable::GetCloudName()
extern void Trackable_GetCloudName_m591936B50E988289EBA0BDF07AB98935099EF404 ();
// 0x0000023D System.String maxstAR.Trackable::GetCloudMeta()
extern void Trackable_GetCloudMeta_m6B439B693DA1C113940981E0A27CABEAB5D90152 ();
// 0x0000023E UnityEngine.Matrix4x4 maxstAR.Trackable::GetPose()
extern void Trackable_GetPose_m755CCDD0E4B0A606A6C5DDEB16F1C3775030FF25 ();
// 0x0000023F UnityEngine.Matrix4x4 maxstAR.Trackable::GetTargetPose()
extern void Trackable_GetTargetPose_mB77AB3E7D80F5057056E86CE1FA689C6428BEF21 ();
// 0x00000240 System.Single maxstAR.Trackable::GetWidth()
extern void Trackable_GetWidth_mEF8DC3868A6261E258DD397994C1E9776720A4EF ();
// 0x00000241 System.Single maxstAR.Trackable::GetHeight()
extern void Trackable_GetHeight_m793558B204134A0CED986087BEA5F88770541607 ();
// 0x00000242 System.Void maxstAR.TrackedImage::.ctor(System.UInt64)
extern void TrackedImage__ctor_mD5DB1118197FD89E30C46B0AA42CC748E4928BD2 ();
// 0x00000243 System.Void maxstAR.TrackedImage::.ctor(System.UInt64,System.Boolean)
extern void TrackedImage__ctor_m0F370A499837DC0DD4580FEE1B99C728F4B8CE2E ();
// 0x00000244 System.Int32 maxstAR.TrackedImage::GetIndex()
extern void TrackedImage_GetIndex_m5D91235D3021505E5E96F1D3051D4669ABD6E906 ();
// 0x00000245 System.Int32 maxstAR.TrackedImage::GetWidth()
extern void TrackedImage_GetWidth_m1FAE46C61FDDB4C057A115D3FC9533E93091102D ();
// 0x00000246 System.Int32 maxstAR.TrackedImage::GetHeight()
extern void TrackedImage_GetHeight_m45AF66C5B35A8BEA2FC4E67C75015219043FFB8A ();
// 0x00000247 System.Int32 maxstAR.TrackedImage::GetLength()
extern void TrackedImage_GetLength_m6B837456F54502CC5F96968B358FB3322E71E3E6 ();
// 0x00000248 maxstAR.ColorFormat maxstAR.TrackedImage::GetFormat()
extern void TrackedImage_GetFormat_m933A4B083B65AF504D206645349A06EBC352F88B ();
// 0x00000249 System.UInt64 maxstAR.TrackedImage::GetImageCptr()
extern void TrackedImage_GetImageCptr_mF7E979531F3221DB5537277E7B8A62DCD0EAC8C0 ();
// 0x0000024A System.Byte[] maxstAR.TrackedImage::GetData()
extern void TrackedImage_GetData_mAC39DBDDAF5858CC99748102BC56B13BB2DA3724 ();
// 0x0000024B System.IntPtr maxstAR.TrackedImage::GetDataPtr()
extern void TrackedImage_GetDataPtr_mD5C6FA76DFF8AADD046F0E08EEFDE4D478B1719C ();
// 0x0000024C System.Void maxstAR.TrackedImage::GetYuv420spYUVPtr(System.IntPtr&,System.IntPtr&)
extern void TrackedImage_GetYuv420spYUVPtr_mB5B2AEE498E9F78116A88FCADE165F213D54EB6F ();
// 0x0000024D System.Void maxstAR.TrackedImage::GetYuv420spYUVPtr(System.IntPtr&,System.IntPtr&,System.IntPtr&)
extern void TrackedImage_GetYuv420spYUVPtr_m77B51E991E3C6BBA78F607391FA6EF22A0A9E418 ();
// 0x0000024E System.Void maxstAR.TrackedImage::GetYuv420_888YUVPtr(System.IntPtr&,System.IntPtr&,System.IntPtr&,System.Boolean)
extern void TrackedImage_GetYuv420_888YUVPtr_mC60F7DF572FABF68ED9B7F6B85807FEDDA305A8A ();
// 0x0000024F System.Void maxstAR.TrackedImage::.cctor()
extern void TrackedImage__cctor_mF38C28B4C55A79C5699CD5F605CBAD7668926A4B ();
// 0x00000250 maxstAR.TrackerManager maxstAR.TrackerManager::GetInstance()
extern void TrackerManager_GetInstance_m46A27E965CA5109148150899D4CD2B26CA6F53E6 ();
// 0x00000251 System.Void maxstAR.TrackerManager::.ctor()
extern void TrackerManager__ctor_m170A81D4AD8E76A78178882F1CC4CD2C7959D3A5 ();
// 0x00000252 System.Void maxstAR.TrackerManager::InitializeCloud()
extern void TrackerManager_InitializeCloud_m16BEED93FCF7207B81027A789BC393248ECB615A ();
// 0x00000253 System.Void maxstAR.TrackerManager::SetCloudRecognitionSecretIdAndSecretKey(System.String,System.String)
extern void TrackerManager_SetCloudRecognitionSecretIdAndSecretKey_m4D1B3EF4FB196AA715B5F3CDCBC080E649045429 ();
// 0x00000254 System.Void maxstAR.TrackerManager::StartTracker(System.Int32)
extern void TrackerManager_StartTracker_mEDE0B41014FC73A9DDD6F2FCB60BB548EDD69088 ();
// 0x00000255 System.Void maxstAR.TrackerManager::StopTracker()
extern void TrackerManager_StopTracker_m8A7C1E8BA74DA0EF19A7A822B17DCEF17420C165 ();
// 0x00000256 System.Void maxstAR.TrackerManager::DestroyTracker()
extern void TrackerManager_DestroyTracker_mF416B8798B95E8E4309A55AFEA7C2E437DA16DC1 ();
// 0x00000257 System.Void maxstAR.TrackerManager::AddTrackerData(System.String,System.Boolean)
extern void TrackerManager_AddTrackerData_mAC9FB7FDD7E4128DB351527636D9A2BA2CC9702C ();
// 0x00000258 System.Void maxstAR.TrackerManager::RemoveTrackerData(System.String)
extern void TrackerManager_RemoveTrackerData_mF161537F0A3A302653945CAFFE25468A43D82F5E ();
// 0x00000259 System.Void maxstAR.TrackerManager::LoadTrackerData()
extern void TrackerManager_LoadTrackerData_m45AF37872DD6F8EC6DB3BAEE185348EC314B7099 ();
// 0x0000025A System.Void maxstAR.TrackerManager::SetTrackingOption(maxstAR.TrackerManager_TrackingOption)
extern void TrackerManager_SetTrackingOption_m5AD2D75526B7CC533691AD083604EF3469B76538 ();
// 0x0000025B System.Boolean maxstAR.TrackerManager::IsTrackerDataLoadCompleted()
extern void TrackerManager_IsTrackerDataLoadCompleted_mAE91E8657349A70A17422998C138055642A9BCB0 ();
// 0x0000025C System.Void maxstAR.TrackerManager::SetVocabulary(System.String,System.Boolean)
extern void TrackerManager_SetVocabulary_mF6289C9A63280DA2F4C85C7F32AA889DFA3476A2 ();
// 0x0000025D maxstAR.TrackingState maxstAR.TrackerManager::UpdateTrackingState()
extern void TrackerManager_UpdateTrackingState_mDD67C1785159119C1897E1088BEEB38D0596067C ();
// 0x0000025E maxstAR.TrackingState maxstAR.TrackerManager::GetTrackingState()
extern void TrackerManager_GetTrackingState_m6B4BEF73784906EDB343CD03B15C4670972702A3 ();
// 0x0000025F UnityEngine.Vector3 maxstAR.TrackerManager::GetWorldPositionFromScreenCoordinate(UnityEngine.Vector2)
extern void TrackerManager_GetWorldPositionFromScreenCoordinate_m15F077BC5892CDE906BA94FFFE6C687E70413187 ();
// 0x00000260 System.Void maxstAR.TrackerManager::FindSurface()
extern void TrackerManager_FindSurface_m6457296E8C13EDD137679FE51E619DB2A4222B36 ();
// 0x00000261 System.Void maxstAR.TrackerManager::QuitFindingSurface()
extern void TrackerManager_QuitFindingSurface_mA995D66E070DF9FB38060B3A8EE211A8D1F8D750 ();
// 0x00000262 System.Void maxstAR.TrackerManager::FindImageOfCloudRecognition()
extern void TrackerManager_FindImageOfCloudRecognition_m65D09E068BC45015C4597C7F8C6495C079B8A0A9 ();
// 0x00000263 maxstAR.GuideInfo maxstAR.TrackerManager::GetGuideInfo()
extern void TrackerManager_GetGuideInfo_m4ABE739552D344AB9AD9695F0ECD15F9213CCF57 ();
// 0x00000264 maxstAR.SurfaceThumbnail maxstAR.TrackerManager::SaveSurfaceData(System.String)
extern void TrackerManager_SaveSurfaceData_m3C2BFE33F6C0CAB1814551F4276AC70D9DB09CFA ();
// 0x00000265 System.Void maxstAR.TrackerManager::.cctor()
extern void TrackerManager__cctor_m5D92506D52DF722C2DA70230DA94BFB2F9B3C63F ();
// 0x00000266 System.Void maxstAR.TrackingResult::.ctor(System.UInt64)
extern void TrackingResult__ctor_m16B9702EAC0B7D0B44B2B53B9521E31682724682 ();
// 0x00000267 System.Int32 maxstAR.TrackingResult::GetCount()
extern void TrackingResult_GetCount_mA693AF5B453F04E891DC992DE7F147425D4447C1 ();
// 0x00000268 maxstAR.Trackable maxstAR.TrackingResult::GetTrackable(System.Int32)
extern void TrackingResult_GetTrackable_mBAF43C221FEBE35839E049B9A727C47C265DE128 ();
// 0x00000269 System.Void maxstAR.TrackingState::.ctor(System.UInt64)
extern void TrackingState__ctor_m511846572F1B67AC12D0596AC945498728CB2844 ();
// 0x0000026A System.UInt64 maxstAR.TrackingState::GetTrackingStateCPtr()
extern void TrackingState_GetTrackingStateCPtr_m9FF15DA1E662E4D837B2FAAF53E6BB369F6D2ED9 ();
// 0x0000026B maxstAR.TrackingResult maxstAR.TrackingState::GetTrackingResult()
extern void TrackingState_GetTrackingResult_mD0779CB0C659620D558136BE5FCD7EA090ADB413 ();
// 0x0000026C System.String maxstAR.TrackingState::GetCodeScanResult()
extern void TrackingState_GetCodeScanResult_m2F19F1EC66C4563183E8784AE2ED355F28EBD7C0 ();
// 0x0000026D maxstAR.TrackedImage maxstAR.TrackingState::GetImage()
extern void TrackingState_GetImage_mD1C946E7955D0607CA070ABCB7164388D35F3627 ();
// 0x0000026E maxstAR.TrackedImage maxstAR.TrackingState::GetImage(System.Boolean)
extern void TrackingState_GetImage_m6B379D48A3D7E6D0C46970D92317C1BE3D38F4C8 ();
// 0x0000026F maxstAR.TrackingState_TrackingStatus maxstAR.TrackingState::GetTrackingStatus()
extern void TrackingState_GetTrackingStatus_m494F6BEA0863C65070ED186FC320FF5EB59C2A45 ();
// 0x00000270 System.Void maxstAR.WearableCalibration::.ctor()
extern void WearableCalibration__ctor_m1F9219EF3026C4A8C32ACCD9EA9880A19DC18955 ();
// 0x00000271 System.String maxstAR.WearableCalibration::get_activeProfile()
extern void WearableCalibration_get_activeProfile_m672CB16C8B236CD00926A73290B274563CC1FA5E ();
// 0x00000272 System.Void maxstAR.WearableCalibration::set_activeProfile(System.String)
extern void WearableCalibration_set_activeProfile_m407EFDCA52FF56603F4BD3338ACE6FACC2135357 ();
// 0x00000273 System.Boolean maxstAR.WearableCalibration::IsActivated()
extern void WearableCalibration_IsActivated_m26AFCBA7985A262D1A636BFD1909E6D7BEA21897 ();
// 0x00000274 System.Boolean maxstAR.WearableCalibration::Init(System.String,System.Int32,System.Int32)
extern void WearableCalibration_Init_mD794728749B60144E5CA54A448995426AF9EDD25 ();
// 0x00000275 System.Void maxstAR.WearableCalibration::Deinit()
extern void WearableCalibration_Deinit_m0B548B265954A3EF9F5D3B6DED4F63F4F8D01EDB ();
// 0x00000276 System.Single[] maxstAR.WearableCalibration::GetViewport(maxstAR.WearableCalibration_EyeType)
extern void WearableCalibration_GetViewport_mF25EA372885DF1EE51D921638F3311725335B469 ();
// 0x00000277 System.Single[] maxstAR.WearableCalibration::GetProjectionMatrix(maxstAR.WearableCalibration_EyeType)
extern void WearableCalibration_GetProjectionMatrix_mDDCBE1373398DEA571D8B1A9DB3DA9758992F1DC ();
// 0x00000278 System.Void maxstAR.WearableCalibration::CreateWearableEye(UnityEngine.Transform)
extern void WearableCalibration_CreateWearableEye_m0614E4FF2FF1B484383A0A15B950BFEAB7D33EE7 ();
// 0x00000279 System.Void JsonHelper_Wrapper`1::.ctor()
// 0x0000027A System.Void JsonHelperForAnchor_Wrapper`1::.ctor()
// 0x0000027B System.Void CodeScanSample_<AutoFocusCoroutine>d__13::.ctor(System.Int32)
extern void U3CAutoFocusCoroutineU3Ed__13__ctor_m9EF8221503DE06CBF9A9B5CEB249FA2394E2FEFF ();
// 0x0000027C System.Void CodeScanSample_<AutoFocusCoroutine>d__13::System.IDisposable.Dispose()
extern void U3CAutoFocusCoroutineU3Ed__13_System_IDisposable_Dispose_m8224EC15ADF2E03F8713CBD552D7F3FA740B9E55 ();
// 0x0000027D System.Boolean CodeScanSample_<AutoFocusCoroutine>d__13::MoveNext()
extern void U3CAutoFocusCoroutineU3Ed__13_MoveNext_mE5C27630BED7360AD9AE2DC3CD20E0132DD1C559 ();
// 0x0000027E System.Object CodeScanSample_<AutoFocusCoroutine>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAutoFocusCoroutineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0A95B6FF8E05F73DD1C1A4C9B6C7DC320D920DF4 ();
// 0x0000027F System.Void CodeScanSample_<AutoFocusCoroutine>d__13::System.Collections.IEnumerator.Reset()
extern void U3CAutoFocusCoroutineU3Ed__13_System_Collections_IEnumerator_Reset_mC86327D53150FDEF6213904AB2F74A298F28EBC2 ();
// 0x00000280 System.Object CodeScanSample_<AutoFocusCoroutine>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CAutoFocusCoroutineU3Ed__13_System_Collections_IEnumerator_get_Current_m4441C7AA354FC24658020D7468B0D16566110F82 ();
// 0x00000281 System.Void ImageTrackerSample_<>c::.cctor()
extern void U3CU3Ec__cctor_m636FF64D720EAA4C4E6DD5A980FA480A724E7D73 ();
// 0x00000282 System.Void ImageTrackerSample_<>c::.ctor()
extern void U3CU3Ec__ctor_mC3782195797FA3A76061BC89729A9920F206367E ();
// 0x00000283 System.Void ImageTrackerSample_<>c::<AddTrackerData>b__4_0(System.String)
extern void U3CU3Ec_U3CAddTrackerDataU3Eb__4_0_m1BA03104FB3A0E1C0B39DF4DE8895E5CF4A70BF1 ();
// 0x00000284 System.Void ObjectTrackerSample_<>c::.cctor()
extern void U3CU3Ec__cctor_m1FD5B74744D43A7642A768823273DDB674F55872 ();
// 0x00000285 System.Void ObjectTrackerSample_<>c::.ctor()
extern void U3CU3Ec__ctor_mC24F1A551A05932D30E1F5291184B93F61940C41 ();
// 0x00000286 System.Void ObjectTrackerSample_<>c::<AddTrackerData>b__4_0(System.String)
extern void U3CU3Ec_U3CAddTrackerDataU3Eb__4_0_m882F59A8CBB49F169BC81325906D3E809AA37BA5 ();
// 0x00000287 System.Void VideoTrackerSample_<>c::.cctor()
extern void U3CU3Ec__cctor_mF5BBD39FCC38DCF6F694FECE8D4AAC21E068CE6F ();
// 0x00000288 System.Void VideoTrackerSample_<>c::.ctor()
extern void U3CU3Ec__ctor_m6E38C81B3B341CCFD18204BC2D1CA52CA83A15A4 ();
// 0x00000289 System.Void VideoTrackerSample_<>c::<AddTrackerData>b__6_0(System.String)
extern void U3CU3Ec_U3CAddTrackerDataU3Eb__6_0_m004D49A68ACDAC2212590F65854BD422376A869F ();
// 0x0000028A System.Void maxstAR.AbstractImageTrackableBehaviour_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_mBA61E25FAE2A133A48EBD4DAA60D8D67395BCEEE ();
// 0x0000028B System.Void maxstAR.AbstractImageTrackableBehaviour_<>c__DisplayClass9_0::<ChangeObjectProperty>b__0(System.Int32,System.Int32,UnityEngine.Texture2D)
extern void U3CU3Ec__DisplayClass9_0_U3CChangeObjectPropertyU3Eb__0_m140E4D21775B05B5772A09B54C049A3E4F9D7DEA ();
// 0x0000028C System.Void maxstAR.AbstractQrCodeTrackableBehaviour_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m7E300FE781728DB124F9AC86A0BD0E0DD8AD9667 ();
// 0x0000028D System.Void maxstAR.AbstractQrCodeTrackableBehaviour_<>c__DisplayClass9_0::<ChangeObjectProperty>b__0(System.Int32,System.Int32,UnityEngine.Texture2D)
extern void U3CU3Ec__DisplayClass9_0_U3CChangeObjectPropertyU3Eb__0_m7BCF5CA7CDD14575117F4C7EF448FBC192C40493 ();
// 0x0000028E System.Void maxstAR.APIController_<POST>d__0::.ctor(System.Int32)
extern void U3CPOSTU3Ed__0__ctor_mBC750541480CB34150176D1B70E89527D35FA12F ();
// 0x0000028F System.Void maxstAR.APIController_<POST>d__0::System.IDisposable.Dispose()
extern void U3CPOSTU3Ed__0_System_IDisposable_Dispose_mC3DE24FE1AF3F4DB4A27FD7C495D9EBC73012EA8 ();
// 0x00000290 System.Boolean maxstAR.APIController_<POST>d__0::MoveNext()
extern void U3CPOSTU3Ed__0_MoveNext_mE7921183CEC3A66C407A6A642B025208C3004366 ();
// 0x00000291 System.Object maxstAR.APIController_<POST>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPOSTU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m34FC1B2DD2C8CC3536D619923334079A6BCCC8FA ();
// 0x00000292 System.Void maxstAR.APIController_<POST>d__0::System.Collections.IEnumerator.Reset()
extern void U3CPOSTU3Ed__0_System_Collections_IEnumerator_Reset_mF380F363F1B0562E6C637CE54F122DE46D6420E5 ();
// 0x00000293 System.Object maxstAR.APIController_<POST>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CPOSTU3Ed__0_System_Collections_IEnumerator_get_Current_m0129FCB873E852BEC517B7626EB14C074369837B ();
// 0x00000294 System.Void maxstAR.APIController_<DownloadFile>d__2::.ctor(System.Int32)
extern void U3CDownloadFileU3Ed__2__ctor_mB38297B4C1A44B9F882E2342FAA3AD03BEDB143D ();
// 0x00000295 System.Void maxstAR.APIController_<DownloadFile>d__2::System.IDisposable.Dispose()
extern void U3CDownloadFileU3Ed__2_System_IDisposable_Dispose_mF454B03B68C1C0AEC4D7504D949F0F11F06F48EF ();
// 0x00000296 System.Boolean maxstAR.APIController_<DownloadFile>d__2::MoveNext()
extern void U3CDownloadFileU3Ed__2_MoveNext_m86FBAA36935A552D553A0BA6A5984BD6FB33E485 ();
// 0x00000297 System.Object maxstAR.APIController_<DownloadFile>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadFileU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m005ADF7B863CA99E44BCEA393EC1154A90779BEF ();
// 0x00000298 System.Void maxstAR.APIController_<DownloadFile>d__2::System.Collections.IEnumerator.Reset()
extern void U3CDownloadFileU3Ed__2_System_Collections_IEnumerator_Reset_m9DDBCC7B886037A9F99FD305E1A6F36685BAA832 ();
// 0x00000299 System.Object maxstAR.APIController_<DownloadFile>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadFileU3Ed__2_System_Collections_IEnumerator_get_Current_m0B10C968746B3445CDF5D26FD087411E67AACCEB ();
// 0x0000029A System.Void maxstAR.CloudRecognitionAPIController_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m207FE7A67CB8718A792A90A069064B1CA0DAE2F3 ();
// 0x0000029B System.Void maxstAR.CloudRecognitionAPIController_<>c__DisplayClass1_0::<Recognize>b__0(System.String)
extern void U3CU3Ec__DisplayClass1_0_U3CRecognizeU3Eb__0_mA95C7D21D3E64D3D4F6A268B02A577D428AD642D ();
// 0x0000029C System.Void maxstAR.CloudRecognitionAPIController_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m52C1547808940B815C02CA3DC472F9784CB87E47 ();
// 0x0000029D System.Void maxstAR.CloudRecognitionAPIController_<>c__DisplayClass2_0::<DownloadCloudDataAndSave>b__0(System.String)
extern void U3CU3Ec__DisplayClass2_0_U3CDownloadCloudDataAndSaveU3Eb__0_mA43A40C18D449D090EE30AE22D691A5CE46B1F86 ();
// 0x0000029E System.Void maxstAR.CloudRecognitionController_<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_m040FFC949175C2E69D8AC21C72E58112DF4F0579 ();
// 0x0000029F System.Void maxstAR.CloudRecognitionController_<>c__DisplayClass20_1::.ctor()
extern void U3CU3Ec__DisplayClass20_1__ctor_mD805F9C507A48ABDCED80E2EB9E3B4E7348E7F0E ();
// 0x000002A0 System.Void maxstAR.CloudRecognitionController_<>c__DisplayClass20_1::<Update>b__1(System.String)
extern void U3CU3Ec__DisplayClass20_1_U3CUpdateU3Eb__1_m26C970D2DF7D762060110C4C0835E006A581E52C ();
// 0x000002A1 System.Void maxstAR.MaxstARUtil_<LoadImageFromFileWithSizeAndTexture>d__7::.ctor(System.Int32)
extern void U3CLoadImageFromFileWithSizeAndTextureU3Ed__7__ctor_m7C863569BE1D9F2FE5730F954A084AAC78CA68E6 ();
// 0x000002A2 System.Void maxstAR.MaxstARUtil_<LoadImageFromFileWithSizeAndTexture>d__7::System.IDisposable.Dispose()
extern void U3CLoadImageFromFileWithSizeAndTextureU3Ed__7_System_IDisposable_Dispose_m342B57C2800C42D381E992EE77C6914617A51A2E ();
// 0x000002A3 System.Boolean maxstAR.MaxstARUtil_<LoadImageFromFileWithSizeAndTexture>d__7::MoveNext()
extern void U3CLoadImageFromFileWithSizeAndTextureU3Ed__7_MoveNext_mB2EAD756196FFEB37E70CF188EF408DB8C321001 ();
// 0x000002A4 System.Object maxstAR.MaxstARUtil_<LoadImageFromFileWithSizeAndTexture>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadImageFromFileWithSizeAndTextureU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m03F447B5A13B7A6452201ED7DB7B811DE147D3F6 ();
// 0x000002A5 System.Void maxstAR.MaxstARUtil_<LoadImageFromFileWithSizeAndTexture>d__7::System.Collections.IEnumerator.Reset()
extern void U3CLoadImageFromFileWithSizeAndTextureU3Ed__7_System_Collections_IEnumerator_Reset_m273B802DC50B7B83799BB1E45B806807C42B3717 ();
// 0x000002A6 System.Object maxstAR.MaxstARUtil_<LoadImageFromFileWithSizeAndTexture>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CLoadImageFromFileWithSizeAndTextureU3Ed__7_System_Collections_IEnumerator_get_Current_m14D5DD893EA96429754FE11CB876BA87DD9CCC9A ();
// 0x000002A7 System.Void maxstAR.MaxstARUtil_<ExtractAssets>d__14::.ctor(System.Int32)
extern void U3CExtractAssetsU3Ed__14__ctor_m791BC8C9C57C738ABEABBF7CA6E6EAAC093D51FE ();
// 0x000002A8 System.Void maxstAR.MaxstARUtil_<ExtractAssets>d__14::System.IDisposable.Dispose()
extern void U3CExtractAssetsU3Ed__14_System_IDisposable_Dispose_mF324F4F165B44FFF571C900DC2B6AB23FBD3A138 ();
// 0x000002A9 System.Boolean maxstAR.MaxstARUtil_<ExtractAssets>d__14::MoveNext()
extern void U3CExtractAssetsU3Ed__14_MoveNext_m930C3C95F2F1487D3834B9BE20A6EC978767EAFE ();
// 0x000002AA System.Object maxstAR.MaxstARUtil_<ExtractAssets>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExtractAssetsU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE03F133C6B2A5190A80BDE152B1B3F43FF05A82 ();
// 0x000002AB System.Void maxstAR.MaxstARUtil_<ExtractAssets>d__14::System.Collections.IEnumerator.Reset()
extern void U3CExtractAssetsU3Ed__14_System_Collections_IEnumerator_Reset_m296B94A985566CF86693CEB99575D9E018FB66BE ();
// 0x000002AC System.Object maxstAR.MaxstARUtil_<ExtractAssets>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CExtractAssetsU3Ed__14_System_Collections_IEnumerator_get_Current_m5672504137D2D96E62F02809203A51C0A0DE00AE ();
static Il2CppMethodPointer s_methodPointers[684] = 
{
	FeaturePointBehaviour__ctor_m8EF12977B54C264D9CE2C1C8DF8EFECA80B28EF7,
	PinchZoom__ctor_m529F587C564C0D0E6C107C79BC5488A284F20AF9,
	PinchZoom_Update_m0E9F07E9905E29A2023C39C038EFFC9695336F6D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ARBehaviour_Init_mD4E38288CD94824BBA16E03007DF5D5288146F2E,
	ARBehaviour_OnClickBackButton_m1CDBEEAF944EC277E69D3452DF0480B8162028A7,
	ARBehaviour_StartCamera_mA266F2C48457F2CFA48DC121B74E800ADB285589,
	ARBehaviour_StopCamera_m1FAE7B6A1FCE6C28B38A58F602E1964E3B6840DB,
	ARBehaviour__ctor_mB716FEBD9F200B66BF1BA18DE91F10DBF1A2821F,
	BackKeyHandler_Update_mAF0BCE38A5ADFB53E24E21E3CE49CD31625C099A,
	BackKeyHandler__ctor_m05A6EC638D9BA8211939B0FF8D4DC22DD7732998,
	CameraConfigurationSample_Awake_m032903D162A85619DE51CEACE308454A1E2A945D,
	CameraConfigurationSample_Start_mB4605E80F422EAAF9691D2EDAE28417A787B9141,
	CameraConfigurationSample_AddTrackerData_mB008D89B901819BBC35EF6BD77387A9BB301B26D,
	CameraConfigurationSample_DisableAllTrackables_m0176CC222DB4DBAB1DDAE9764ABC45BF0FEBFBC8,
	CameraConfigurationSample_Update_m8D46DAD5EBE48DF37EB27DF7DA4539A7E413329F,
	CameraConfigurationSample_SetFlashLightMode_m5A2DA558211AF66EA86DF5552334CCA8EC17FD9B,
	CameraConfigurationSample_FlipHorizontal_mED719D3406C6938C0CADC6FCFF28CC0900D738C7,
	CameraConfigurationSample_FlipVertical_mDE8A29143DB8AD212C083084FBD557AB94F5BCAC,
	CameraConfigurationSample_WhiteBalanceLock_m415ACE397A993668CBF921F83BB66354E01D0814,
	CameraConfigurationSample_SetContinuousFocus_mA04119B6F9FC78BFBAC4E94B129783CF59633DB0,
	CameraConfigurationSample_ChangeCameraPosition_m9364ED8DA3FA84BD26E594482CBFCFDDD361EF99,
	CameraConfigurationSample_OnApplicationPause_m6E1733C7034B9B9E6816AC08F994A240EC066CEB,
	CameraConfigurationSample_OnDestroy_m1C882A9BAE60ADAEF7148CE12E30A2F06DCFD7D3,
	CameraConfigurationSample__ctor_m1E8F3D6CF92303110BB9ED0A6B80CCC70F75AE29,
	CloudTrackerSample_Awake_m86AF6D8DF38CB541819670346F6E1F1B9038BF8D,
	CloudTrackerSample_Start_mB781D4016637877ABAD14A29C29413F70F0BA2F1,
	CloudTrackerSample_DisableAllTrackables_m463AF556DB8EA2BBA6E8A42A111D810DB6B872FB,
	CloudTrackerSample_Update_mD904BF7F8DE22E29801A24045A739BCAB8235690,
	CloudTrackerSample_OnApplicationPause_m6B613371FBD322C68C5DE55DE2429FE7D1E8B22A,
	CloudTrackerSample_OnDestroy_mA1CB2C4FC285BFB5ADFCDFE0C8C7CF96BD3E96E4,
	CloudTrackerSample__ctor_m9E8F7B0977C046908908C9916474EB21E9EA9755,
	CodeScanSample_Awake_m6A7F6826051687FC6CF4F26E716F20B813AB75FB,
	CodeScanSample_Start_mFC76C9C8EC6B1BD5D0509D7119806FCE34A772B3,
	CodeScanSample_Update_mD181C04149B1D3BF603206DC4C2984EA053F154A,
	CodeScanSample_OnApplicationPause_mDE957AC9F84521A589A7F8D48604FBB4EB0A5065,
	CodeScanSample_OnDestroy_m11E349A86223C26F90972D7989149B41FE52C9AC,
	CodeScanSample_StartCodeScan_m5378EEA37A93A18A3723DE67817952C14D02861A,
	CodeScanSample_StartCameraInternal_m92BBF161E540E7B92D0CD477D0CB5B1DB9F665CA,
	CodeScanSample_StopCameraInternal_mE429653C878DEBDDC219EDB180649FA85576F046,
	CodeScanSample_AutoFocusCoroutine_m5534B5B3D78998D63FB687110279F14F0AF8334E,
	CodeScanSample__ctor_m221BC4C30F1329AB9B8EDD1DAA0385F19569167A,
	HomeSceneManager_Update_m5D8CC99BEF2E66DEAC40336CCDF2E08A98FC5CA1,
	HomeSceneManager_OnImageTargetClick_m1D12C9277CCF4E0038D5AB7CAFDB96A508F3274F,
	HomeSceneManager_OnMarkerImageClick_m0224EEB9BBAF7A0688E92007DA23BE9794990BCA,
	HomeSceneManager_OnInstantImageClick_m2CDF19DE82E890C12A3162D158ADCCAF1989354F,
	HomeSceneManager_OnObjectTargetClick_mD51601B267981E741953B7BD2D9E3D6785F91BCD,
	HomeSceneManager_OnQR_BarcodeClick_m28498EF62B6C01D38322E67749C6E3EFE15145FA,
	HomeSceneManager_OnCameraConfigClick_m5968E2C85DC8A5B2486B5C7250180C50A0EA6F68,
	HomeSceneManager_OnCloudRecognizerClick_mC04D2457CBCB24EBC7E54215E4A1DEC72675339B,
	HomeSceneManager_OnQRTrackerClick_m9017C6F8A8230642F5C6F872AD89DB4932496B0E,
	HomeSceneManager_OnVideoTrackerClick_mC6DADFF51147DAC117D654B505C10A32CA10BA2A,
	HomeSceneManager__ctor_m5DDAE860537A680793B962FDABA1AF1EE943C4A8,
	ImageTrackerSample_Awake_m9262C0CF2D3EC8E76868545637211C56CC87CBE9,
	ImageTrackerSample_Start_m2C645EA04B21BDEB6AA7966CF02ACF48E9AD9A05,
	ImageTrackerSample_AddTrackerData_mC687172A16F0F7E4A2F6736EA33DB67AE92BC4FF,
	ImageTrackerSample_DisableAllTrackables_m56E9448104CC58A10857521A1D6763F069EBDE7C,
	ImageTrackerSample_Update_m08E61AFAD47EBB486A9D697130E3A25B3FECCC95,
	ImageTrackerSample_SetNormalMode_m3D5546FEEAE79F8289BED2F9DCE89657C65190FC,
	ImageTrackerSample_SetExtendedMode_m56612DD3D2E9AE105784632ED644917033A11063,
	ImageTrackerSample_SetMultiMode_mCB708804F462A47D1129A8AD846DB0A847FD4354,
	ImageTrackerSample_OnApplicationPause_mF12EC38848E0E41FEF9A1CD0F1E85D3C36F7ACF7,
	ImageTrackerSample_OnDestroy_m0BC99EEDDEBF5CA81367805CC452BCA1023FFD71,
	ImageTrackerSample__ctor_mF52ACDBB72409DA51E3EAE0465297D503C9C6074,
	InstantTrackerSample_Awake_m3100CE6372E9352437CECCF4C86C93309C4B1852,
	InstantTrackerSample_Start_mA6C0FB477BDDCCE60C2CA262B4E6252875575D2A,
	InstantTrackerSample_Update_mE9BE954766F027C2118653E9E503F788F0A89C91,
	InstantTrackerSample_OnApplicationPause_mAB1298B971587B07646C25D334F1A30AD973677A,
	InstantTrackerSample_OnDestroy_m89E2DC3AFE274C90681D9001A80A37041DDB6D16,
	InstantTrackerSample_OnClickStart_mD47367EA3BEA3FE5ABAA369673D70E680E763672,
	InstantTrackerSample__ctor_mEC3CFF57CD30FCAF606ABF7588B81B8264304179,
	MarkerTrackerSample_Awake_m1EBFA17E3FE9E50D3B9172250FAC1B92B4E18B4E,
	MarkerTrackerSample_Start_m45029486D5DF5E12066D5861CF124E121D737335,
	MarkerTrackerSample_AddTrackerData_m8CAA360A0EAA0C1EB888D7A266C660709D3DA236,
	MarkerTrackerSample_DisableAllTrackables_m14B9D20D55F64B62FD1D6DB9ECB960C84B4593E4,
	MarkerTrackerSample_Update_m8E9043C6E1E2D4EE088E5F10F8B7705D13880947,
	MarkerTrackerSample_OnClickedNormal_m5F2B147E3132E0F24EB222C26C13EF60C1793914,
	MarkerTrackerSample_OnClickedEnhanced_mACBFF1E926AE267EB49ACA7988C3C63F6B6165B7,
	MarkerTrackerSample_OnApplicationPause_m544053D62F08A95D97908D820CF1B3A0117C59EB,
	MarkerTrackerSample_OnDestroy_mA6B72F4657AA30538810543A863630C6F3951DDC,
	MarkerTrackerSample__ctor_mB6E3EF9E9DB297ADCF886B74082611C67B015AE9,
	ObjectTrackerSample_Awake_m9889181ACDFD7AF8C2E0FC9CF8B2DBE4087616F8,
	ObjectTrackerSample_Start_mEA5270596DF2891CC7ABB8B806A25BC8B2E16027,
	ObjectTrackerSample_AddTrackerData_mA81C4E9EAE4A494248FD5779B3871BA88AAE9DFF,
	ObjectTrackerSample_DisableAllTrackables_m92EE1250AFEC986F0D328978BD9D976D9C61E425,
	ObjectTrackerSample_Update_m46EE9FCA8A0E1E754EA701F847E8B153BDD55C12,
	ObjectTrackerSample_OnApplicationPause_m5D326E0FC47271C98E2E073213B98E5098313D3C,
	ObjectTrackerSample_OnDestroy_m3AFFD67C101D029A0982C757FF26A4A03580B58C,
	ObjectTrackerSample__ctor_mCFD44EACC4EC25167A967F6AF59DA340A04CCDC6,
	QrCodeTrackerSample_Awake_m43C0A614CA2AEB402FC75BA9FFFE40062CA065EA,
	QrCodeTrackerSample_Start_mDC9A5A2CF8AA4BC272B9BF82C978A0E77D929987,
	QrCodeTrackerSample_AddTrackerData_mE645FF7B777682B77EC6CE27048D625E411FDE97,
	QrCodeTrackerSample_DisableAllTrackables_m3F6ACFDB9FC0699758906FC6D12BF6713830317A,
	QrCodeTrackerSample_Update_m7B80E0BFCD948E5C09E04AEDC98AE16075E64830,
	QrCodeTrackerSample_OnApplicationPause_m257218249F9A17BEEAA781A1839040AB788D3741,
	QrCodeTrackerSample_OnDestroy_m103EE26C1C55D178369D4E5A84CEE091FEC67077,
	QrCodeTrackerSample__ctor_mC5C9B3DE725117906D8A9EBA7FB2B3AE7FF93945,
	SceneStackManager__ctor_mF41A6CAAB46D5C996DC48A7E8575E824611D7001,
	SceneStackManager_get_Instance_mA091272429BD229FA20C56A9420FBBAD9F913690,
	SceneStackManager_LoadScene_m563E7258EC8A567FABCA166B9E2D8FDEF5D25424,
	SceneStackManager_LoadPrevious_mF2A037DC910E06DCB1580013E660878972E1362B,
	SceneStackManager__cctor_mEDAE47B4BCF323AE1A89F15BD9BE72A2F00A20BD,
	NULL,
	NULL,
	NULL,
	NULL,
	VideoTrackerSample_Awake_mB535577AE871203B4ED3D29A3FE920C3C5355372,
	VideoTrackerSample_Start_mC5D8CE18B12B05E6822245599676993F06ACC066,
	VideoTrackerSample_AddTrackerData_mFDB2FB87E7482F9E7E23A42DF52DB60D247029BD,
	VideoTrackerSample_DisableAllTrackables_m07586D3029607AB7B973CF29EE901005E7A61F7C,
	VideoTrackerSample_OnNewFrame_mA8BF3E035FD2CF3266464CB36320B2647CC4C21E,
	VideoTrackerSample_SwitchCameraToVideo_m0FCB5F923C3D58A6D73851A972F17FA545C3E707,
	VideoTrackerSample_PauseAndPlayVideo_mABD39D39406105ABCB094A1371FA8A2495F0210E,
	VideoTrackerSample_Update_m644E10D2DA66EA1CD685644BB4742D36DE99522B,
	VideoTrackerSample_SetNormalMode_m45C1F44F2E2584ABDDAC26E75B3329A58D203EBE,
	VideoTrackerSample_SetExtendedMode_m37B612E91C612BC6C543F7FCC47FD7A08D781976,
	VideoTrackerSample_SetMultiMode_mE2CBF14374DD307C4A6D0C5D5144F9BCC30A90AB,
	VideoTrackerSample_OnApplicationPause_mF8EF6F189E919C7FD3B06F4C93485D75F10605F5,
	VideoTrackerSample_OnDestroy_m18CB7D26FC538FF9B5E35010E49235F9FD961B81,
	VideoTrackerSample__ctor_mB8546DBAAD0FA139B5637942CA5C920727E1620D,
	VideoTrackerSample_U3CStartU3Eb__5_0_m2E0AE32DE63B1D5D7A9FD4028F89D6B523A3BA90,
	MapOpen_Start_mB59110895EC55250C6A472CBEA39A2D72F140CA4,
	MapOpen_Update_m7EC17380D44C0E35EED058BFBDC2E062E33B9E62,
	MapOpen_AnimationEnd_mF3D40BC244419CE7618D8EC122A9868BBCA4BFB1,
	MapOpen_AnimationStart_m0C0DE66A366B10FD8959B56E17D9FB8848BE93B7,
	MapOpen_Rocketmakers_m51EE6C0CD6FE495DFC2D7BBEF5738C4DA27E0145,
	MapOpen_TEDxBath_m08197DE3194964871C4EE34DF9A6051E7B81F3D5,
	MapOpen__ctor_mA28D68662F366F7D6BE7609A24D7CA3BDD6FB911,
	PinLink_Start_mC21F42975E25A2B9F026F21D3A0D9273228C9D2C,
	PinLink_Update_m2E2402DD6C77C268C7445FD5E839FB6BE2EEBCF7,
	PinLink__ctor_m9E42953956F78276AF72A07BC471EED1A4E5CD23,
	AdaptingEventSystemDragThreshold_Awake_mA3EB2D77065E5448B4AEBD1FC4029A20E4D7F9AD,
	AdaptingEventSystemDragThreshold_UpdatePixelDrag_mA53845578B333B9EBCED5493AF3999B4CF1B81E6,
	AdaptingEventSystemDragThreshold_Update_m3D2AF724D6792CFDBED5F1B24F233D016F24EF2D,
	AdaptingEventSystemDragThreshold_OnLevelWasLoaded_m00A7A0A5419F34189F96C94CC4DDF4AC0069C99B,
	AdaptingEventSystemDragThreshold_DisableBtnControll_mCA744FD943D449D65EB9A3401C39B3044E92438B,
	AdaptingEventSystemDragThreshold_Reset_mEBC97D5033D25FAF516F0CCA73F279E2CF36F6DB,
	AdaptingEventSystemDragThreshold__ctor_m14C87FB2BDE10B1DD42E0CAE7553F343EBBDD744,
	AndroidEngine__ctor_m3A1478CD369CE8A638CF0707300D38AF63551309,
	AndroidEngine_Dispose_m5D05907C2A6B5AAFC3EDECDBF54FCFE0EDC207AE,
	ARManager_Awake_mCAA7B934B9AF8EC6E1FA9D630887BC94C407EE41,
	ARManager_OnDestroy_m2DE49FE804E572A24833720CEB21729AECD6D06F,
	ARManager__ctor_m90D816F53C0AF630815612FA7D3635A9C0F1A8EA,
	CameraBackgroundBehaviour__ctor_mBCBAB4B41E3BB5624CA4A833B1633F8E0E18343A,
	CloudTrackableBehaviour_OnTrackSuccess_m9162AAD0D68284C1162EF26696D0F851CCCBAEFA,
	CloudTrackableBehaviour_OnTrackFail_m08694155B6579E63AE34DA7F8CA0F6DA5E65120A,
	CloudTrackableBehaviour__ctor_m951A041486CA4CAE2ACA10438AD33502296CF535,
	ConfigurationScriptableObject__ctor_mDB256C96FB7EEBE4F3254EBBEC22FB0ED551D7BF,
	ImageTrackableBehaviour_OnTrackSuccess_m003C57413718FF949E40DAA30FCBE4AEDFBC43B4,
	ImageTrackableBehaviour_OnTrackFail_mC8CF3E74324B4F8A9499AC82C7DB3B7EBF088A4A,
	ImageTrackableBehaviour__ctor_mAB8E7917CD58680BC9C7F19BA1AF29478304D910,
	InstantTrackableBehaviour_OnTrackSuccess_m664683767C38E5BFBF0511F85CDF8BB490BDD626,
	InstantTrackableBehaviour_OnTrackFail_m2174BE1EAA71EC6D1446A13EC2CE924E88110500,
	InstantTrackableBehaviour__ctor_m89796AA45DF4CEAC32966440D2DA9B105F1CC3F9,
	MapViewerBehaviour__ctor_m7F615F416277801AE68490EE02314A58FDCD7425,
	MarkerGroupBehaviour_get_MarkerGroupSize_mCB933BBE7629E85A142FF1EC95BD3F7B1E994FFC,
	MarkerGroupBehaviour_set_MarkerGroupSize_m802447A2543617061280FC3EB460B695EE7D1E73,
	MarkerGroupBehaviour_get_ApplyAll_mD6592A4A3BD3863103FA2A33EDC3C2B813DA9AD6,
	MarkerGroupBehaviour_set_ApplyAll_mD121911C92CCC897CBDE9A7196681C1646DC7040,
	MarkerGroupBehaviour_Start_m3D8CF7662EA814083510085B270EACB87D8DF81F,
	MarkerGroupBehaviour__ctor_m822CAA0306BE5243348C180480C51475D5D2D134,
	MarkerTrackerBehaviour_get_MarkerID_m5808106EE5E2A5E451572515DB7B808C40AF7CAF,
	MarkerTrackerBehaviour_set_MarkerID_mCD6EABD5BCB80BCBA7D1019948DB8113400BB449,
	MarkerTrackerBehaviour_get_MarkerSize_mF25CDDF31F37D7AF110C0345872AB47C4163F976,
	MarkerTrackerBehaviour_set_MarkerSize_m71BA7BFC94ABE9C3CB251E89D90905CB9573DC7B,
	MarkerTrackerBehaviour_SetMarkerTrackerFileName_mA05CBF97DCB7717A44E0EC39660F3F72306E2C5B,
	MarkerTrackerBehaviour_OnTrackSuccess_mD9A8BD8B14E776F45431C7974DC21E063F3DA93E,
	MarkerTrackerBehaviour_OnTrackFail_m14AA9054E91FB352F49605A6605CF6AA3CF19D15,
	MarkerTrackerBehaviour__ctor_m7E2ADF96F1D77CC24DCDC3EB5EF2E6443B98D077,
	ObjectTrackableBehaviour_OnTrackSuccess_m85E36967C60137B76E4BE3DDEE610A222D48EFC0,
	ObjectTrackableBehaviour_OnTrackFail_mCBCC54510C6305A4A61CFA092F6B3AC0F0B8AF0A,
	ObjectTrackableBehaviour__ctor_mC54648BBEE976461518110A1F39D7BDA6E5C03EA,
	QrCodeTrackableBehaviour_get_QrCodeSearchingWords_m42AF3F5A445D1163AA6A36AA5021CB8E2BD596AF,
	QrCodeTrackableBehaviour_set_QrCodeSearchingWords_m650B90F2DA2300225C2E2906DB4914CDF45A94FB,
	QrCodeTrackableBehaviour_OnTrackSuccess_mA052BB30766CF011D64024713C64605925B98F63,
	QrCodeTrackableBehaviour_OnTrackFail_m9F348B8961826820969AB9E16B5EE7588B841599,
	QrCodeTrackableBehaviour__ctor_mF04F26B6F2853C13C716A9A6134558BDF0BA07BD,
	WearableDeviceController__ctor_m831CA944A05E1A51C2B807D97E4590DB3ADC8897,
	WearableDeviceController_Init_m3E8465A7C1727F11B7FD53F0779105F88E964D61,
	WearableDeviceController_DeInit_mA817CAF9604E3C65A2D0063B568213E86CBC8DC5,
	WearableDeviceController_IsSupportedWearableDevice_m707E8F7754330646B9D1E3C887E4473D1638D3DC,
	WearableDeviceController_GetModelName_m99A1302B5AAF1D4DE11EEBA587262B6C15025C99,
	WearableDeviceController_SetStereoMode_m2ECFA9DC6D80551B52000B8456E57C8DC544BCF5,
	WearableDeviceController_IsStereoEnabled_m2DEF96147706CF9842E0B7C328799D95D475A5D3,
	WearableDeviceController_IsSideBySideType_mCC4857F2F8C11AB9ACE7151DFE2A9A270DD70841,
	WearableManager_GetInstance_m55F007C74CC1B329AE9A8AE3C05715E7CD20BE83,
	WearableManager__ctor_m797FE745D17CE179C55D14E0322372200010A04A,
	WearableManager_GetDeviceController_m8186D9023B3B4E4783E802F6031D1887B7CD6675,
	WearableManager_GetCalibration_m8B08F7C53CB3208F1B886F55772A956D8C320096,
	WearableManager__cctor_m96521043C146B2AAB2B20079FF72DD367BE8F39E,
	AbstractARManager_get_Instance_mC07EB5ACE0F0F4833142661BD1C74554899EB94E,
	AbstractARManager_Init_m0E2642E4674AD4FC6F221A26DDDEE6861FFE8E83,
	AbstractARManager_InitInternal_m58A55ED941CD9217540F6F076729DD79AF7230B3,
	AbstractARManager_Update_m76FD1C6ADC4DF8E239555799BB95040BD2DADD62,
	AbstractARManager_Deinit_m3A63EF2C292B74FB2758C43F1EF4808A61735361,
	AbstractARManager_get_WorldCenterModeSetting_mCF8F7602828C0F38B39A879BB852DB2EA48FAD5C,
	AbstractARManager_GetARCamera_m40EF848C5AE6860CF3081C0572C016490F2D3748,
	AbstractARManager_SetWorldCenterMode_m39E9C716D2DDF61F6B7C0D1CD2BA1B5D3BCD538E,
	AbstractARManager_OnPreRender_mC66374C9E8C277AD93394B3E60297FE0F5FC7996,
	AbstractARManager_TransformBackgroundPlane_mDC5F3FE51A8C826AC60962C5B3464243B2BFB073,
	AbstractARManager__ctor_m5DEC2B51CEC228C1978DB0ED57C35C37050F8080,
	AbstractARManager__cctor_m045C62233F00F458A487502075B44204770F0215,
	AbstractCameraBackgroundBehaviour_get_Instance_m9D2996D21A0D8050AA0F5288B5521887AE847E8E,
	AbstractCameraBackgroundBehaviour_Awake_m190F4415A916A492BF382487C730DD054F9336A3,
	AbstractCameraBackgroundBehaviour_OnApplicationPause_mF8DBF5D7451906B0C39F84A15D2E5DA05C7C9D68,
	AbstractCameraBackgroundBehaviour_OnEnable_mAA48410ADBD94E4795E08EEF63F52B14036E0097,
	AbstractCameraBackgroundBehaviour_OnDisable_m2D270461F2F8FE3D0902A9EB1398844013863E86,
	AbstractCameraBackgroundBehaviour_OnDestroy_mDD7199DBD287259196BB154D2CC02CF329018CC0,
	AbstractCameraBackgroundBehaviour_StartRendering_m66E159259900BB45FE833F381CD6E8F9E9EC4D3F,
	AbstractCameraBackgroundBehaviour_StopRendering_mD61A38C3463E2E78C8187024355F5B53019A6134,
	AbstractCameraBackgroundBehaviour_CreateCameraTexture_m7C7A1AD142EB0C8A7C75133A62938A7897B953DA,
	AbstractCameraBackgroundBehaviour_UpdateCameraBackgroundImage_m5D83FCE15F353432F2F17A531E26849F6BDE5E45,
	AbstractCameraBackgroundBehaviour_UpdateCameraTexture_m6D98D693F762352F26F1E14CBBAFC05AB27A7787,
	AbstractCameraBackgroundBehaviour_TransformBackgroundPlane_m4300D135CB0E0935F1044051E642C42710CDDA59,
	AbstractCameraBackgroundBehaviour_RenderingEnabled_mA1BD67148F81C665239953DDB28ACEEB270D32EF,
	AbstractCameraBackgroundBehaviour__ctor_mE5F9E2CBA898CC21C1CC1E75EF88B70AEBC33B74,
	AbstractCameraBackgroundBehaviour__cctor_m92C50B96C336BE693A724916081F63D3D2B34509,
	AbstractCloudTrackableBehaviour_Start_mBB334333D7E477AA725B34B79490950486EE5948,
	AbstractCloudTrackableBehaviour_OnTrackerCloudName_m01CEB9454C910F6EB94B1E0BEE21502CCF6A8F30,
	AbstractCloudTrackableBehaviour__ctor_m979C0E58C9AF037ED2B1CB54742F177A1A25C94A,
	AbstractConfigurationScriptableObject_GetInstance_m81ED52A4428D1B6BA59009100F916A6ADC20E870,
	AbstractConfigurationScriptableObject__ctor_m94E14C17AF1F9F9E4C811B54DC716F844301A539,
	AbstractConfigurationScriptableObject__cctor_mF6523655EB8636AA61EE174DB0028A38B9D9E339,
	AbstractFeaturePointBehaviour_OnApplicationPause_m9A9715A3590A78C86E25BEAD6445E437A2D1BFD0,
	AbstractFeaturePointBehaviour_Generate_m490E02913B2E4C37A40EF162C055E093A4490E6E,
	AbstractFeaturePointBehaviour_convertFloatToVertex3_m1E652FDF127A463DE4FB4CBD23A42D9C37045654,
	AbstractFeaturePointBehaviour_Start_mE40B14C76D700241C34C619DB009FC61662DD46C,
	AbstractFeaturePointBehaviour_Update_m2AEDB761F5AC6FC68674CEDEE4D73994FEBD3C99,
	AbstractFeaturePointBehaviour__ctor_mB72488D9E8A75A26C0C9C0F179AF79A342B1D502,
	AbstractImageTrackableBehaviour_get_TargetWidth_m6592CA747785A5C04E24097C6F12409C184B343C,
	AbstractImageTrackableBehaviour_get_TargetHeight_m34193D131A326AC2C75F81CA29C98239AA1B35BD,
	AbstractImageTrackableBehaviour_Start_m0D62D52EBACE71FBD4DE0BD93F3B37BEE4122F12,
	AbstractImageTrackableBehaviour_OnTrackerDataFileChanged_m9403099F381E8F5F94FED0E1F1C1015C1DD0757C,
	AbstractImageTrackableBehaviour_ChangeObjectProperty_m06B185B0D3B2F10E59DC780FDB5C99F0ED97DA57,
	AbstractImageTrackableBehaviour__ctor_m2BED57A7A526406E9E548648D469A4D04AB4EB42,
	AbstractInstantTrackableBehaviour__ctor_m29108B4DAFA91C6C963835A43F0D12D6474EAF30,
	AbstractMapViewerBehaviour_get_KeyframeIndex_mFA6D962E366A76368C21E754749D27C5837245E3,
	AbstractMapViewerBehaviour_set_KeyframeIndex_m1A8BCC80FBF102CA245EEAC9952A40B5A4B750E8,
	AbstractMapViewerBehaviour_get_ShowMesh_m5F7F9706C6A22FC14E2FFEA902D147682B90105C,
	AbstractMapViewerBehaviour_set_ShowMesh_m540F718770D31325FF69622AC0C5CA2EFD81AB46,
	AbstractMapViewerBehaviour_get_MaxKeyframeCount_m887CF998FF2AA16996AC613814A1313C2766BEC7,
	AbstractMapViewerBehaviour_set_MaxKeyframeCount_mD9739B607EAF10275B21F1C752523E431FEB95AF,
	AbstractMapViewerBehaviour_get_Transparent_m936DD71E1C00DCEAAD9A08C12D1DE5E422116652,
	AbstractMapViewerBehaviour_set_Transparent_m23285357CEA80840211493DE88239CC15E12FE1D,
	AbstractMapViewerBehaviour_Load_m635856C7EEDD2BA16C9C6EC77E63BAFAF157AE52,
	AbstractMapViewerBehaviour_ReadMap_mFE8434D6B24D888D264DE8E70B32F9AB2A89825F,
	AbstractMapViewerBehaviour_SetTransparent_m93C17E0FB178C71184CE2D33878F8C112336C35F,
	AbstractMapViewerBehaviour_GetCameraTexture_m85C6EF0EA858200D661258327336896E8EEB3E7E,
	AbstractMapViewerBehaviour_UpdateMapViewer_m811BC031009749CE1C1AFAE6806875BA0AC30F31,
	AbstractMapViewerBehaviour_ApplyViewCamera_m96768182640E57B0991DD9FD4E1FDC3BB0DBD3B4,
	AbstractMapViewerBehaviour__ctor_m1CD61831FED8AB159FD6340D25BE94ECE5355EB9,
	AbstractMarkerTrackableBehaviour_Start_m105C6D475B6FB7D05D50E13734233AEBFC5390E3,
	AbstractMarkerTrackableBehaviour_OnTrackerDataFileChanged_mEAC80939F2D2339DB018CAF4F28D5285F5EDD8D2,
	AbstractMarkerTrackableBehaviour_SetTargetTexture_mF3A53E58F514CFAD7A72F41087F90DF90DA4CADC,
	AbstractMarkerTrackableBehaviour__ctor_m1E5AF2B4EEC2A84DB6E1E3A5723A9818AEDDE45B,
	AbstractMarkerTrackableBehaviour_U3CSetTargetTextureU3Eb__3_0_m0D3C2CC8A2D4AEFAC159BE658FE197EB716F29F7,
	AbstractObjectTrackableBehaviour_Start_mF51D8E3C1A59F03869AE9D42068D038AC8E809EF,
	AbstractObjectTrackableBehaviour_OnTrackerDataFileChanged_m317A4A7F141FF9035A552FA779F8903A12E950E5,
	AbstractObjectTrackableBehaviour__ctor_m96A3DB625F240201C4E849BCD7ED1953C2C889EA,
	AbstractQrCodeTrackableBehaviour_get_TargetWidth_mAA449D195DECE8DC3219C0960EB7199B147054A9,
	AbstractQrCodeTrackableBehaviour_get_TargetHeight_m012C52A2F32635D2FF0B5130EDEFA091CA6C27E5,
	AbstractQrCodeTrackableBehaviour_Start_m487CDDD2A547FB2283758DD8C65958FFC28F5C64,
	AbstractQrCodeTrackableBehaviour_OnTrackerDataFileChanged_mBAD1DC41697991768A1DE5F6E867050157475854,
	AbstractQrCodeTrackableBehaviour_ChangeObjectProperty_m0C7EFFEC999194F1DD0BC857EF8F375ED9468FCD,
	AbstractQrCodeTrackableBehaviour__ctor_mE9A8DF0B052262EB04E75A2C2CC1D39AD77CACC2,
	AbstractTrackableBehaviour_get_StorageType_mC7A295AA52826BDADBD481781148913AF4F7E026,
	AbstractTrackableBehaviour_set_StorageType_m27FDD8887AB12106A46AFE6FE5553002724FB215,
	AbstractTrackableBehaviour_get_TrackerDataFileObject_m3B0BDB0D8ECCFA7DE69F776386CB7DA224DDA20C,
	AbstractTrackableBehaviour_set_TrackerDataFileObject_m44DDC9BDF18D7E891A413B986C34868C719A9299,
	AbstractTrackableBehaviour_get_TrackerDataFileName_m8F2964D031717DF66DF961E0B6B5715982FFCACD,
	AbstractTrackableBehaviour_set_TrackerDataFileName_mD2094FB00347228DBB4427F1FA10E7D4BE449C7A,
	AbstractTrackableBehaviour_get_TrackableId_mE9A47E10CD2D52F94D6F3E795620E89B2485A646,
	AbstractTrackableBehaviour_set_TrackableId_m853ACEBD7A7F765B7FB2F155D17579C3769CCF3A,
	AbstractTrackableBehaviour_get_TrackableName_mE50729E2E01601A8F4F5AAF42B2C57D41465F5AE,
	AbstractTrackableBehaviour_set_TrackableName_m98B239C13AB46A9E4DF64EEEF98F8C3937D80B3B,
	AbstractTrackableBehaviour_OnTrackerDataFileChanged_m6FD6DF5136D70134A94C9F5F7B6AFDC710EF51CC,
	AbstractTrackableBehaviour_OnTrackFail_mC11808CC0433E4540B5AAD21C8205F32CC979CD9,
	AbstractTrackableBehaviour_OnTrackSuccess_m18AF64A28D6EFADF43AF25A7D46E3ED497CB97EF,
	AbstractTrackableBehaviour__ctor_mE800EF82B026CA8CA826EBA956CF4DCB6CE62D0F,
	APIController_POST_m22001E476C5AC0D5DFC4872F055745A49BC986D7,
	APIController_Escape_mBF67D5D0BBE5326CC1006DBC912A2C6EF71CDDD7,
	APIController_DownloadFile_mCDED32769F041F50F5583B6D5986D254D529CF93,
	APIController__ctor_mEDA57D3BE210D67CFBF9AD07EC6BF0515D77A88B,
	CameraDevice_GetInstance_mA48922A8C0348FF55BF8B26DDCDDE2E1AC53CC6E,
	CameraDevice__ctor_m7F86B4CCF5601E918726429121508095133CA5A5,
	CameraDevice_Start_m38B9E2D8F578691A5535A183341E18F2383FADFD,
	CameraDevice_SetCalibrationData_m6C0B9344018CCE7856BAD875F8A627D7F7F0514C,
	CameraDevice_SetNewFrame_mFBEE45714E3C33C7268920B4E98E7E394428CAC2,
	CameraDevice_SetNewFrame_m68695726C2493ACD1FD4E29DEA1AE509C06BCDA4,
	CameraDevice_SetNewFrameAndTimestamp_m2CAF6FC48272A710ED78310F538636CA2209BFB0,
	CameraDevice_SetNewFrameAndTimestamp_mEC9EB78F80DF0141E18672C221FB39D89E9BDDF2,
	CameraDevice_SetFocusMode_m619F937DC89FCA29233729B6522FC4093A335475,
	CameraDevice_SetFlashLightMode_mAB9FD99D4DC343850D347E294FADB9493DEBE796,
	CameraDevice_SetAutoWhiteBalanceLock_m3507B0FFC7ADAB276D0743625AD670A72936687D,
	CameraDevice_FlipVideo_m4B04A82F63EA5914C6A31F01A56A23F127C2029C,
	CameraDevice_IsVideoFlipped_m9927192680CEB357BF0067A8AA2696DDE0687DC2,
	CameraDevice_SetZoom_m44875B87015F4EF39DC61858E5C5CB9AC846FA34,
	CameraDevice_getMaxZoomValue_m321F6A0CE8C2F64AA419D8EE3E28CD31021C5101,
	CameraDevice_GetParamList_m7EF99CBB431488D24017D14665A4F50F0C024D69,
	CameraDevice_SetParam_mDC993BA13FBA434A099E4C36E8E88C7DCB4775CC,
	CameraDevice_SetParam_m8F2E5BFDC30491DF0C96588BEE55809004777B65,
	CameraDevice_SetParam_m1A1579B062E71FC9B411F852B4FC1AE02AD1D205,
	CameraDevice_SetParam_m0E8A257D4996D2AC692BF919F2555B2421F99C5E,
	CameraDevice_Stop_m9AD3F5CBA20B26A6155856015E40268C43422434,
	CameraDevice_GetWidth_mF960539D19402BA112EF9E70F7CF37C3DE60C511,
	CameraDevice_GetHeight_mA84DAC395A177AEE9DAC29C8219A86F0AF82B4AD,
	CameraDevice_GetProjectionMatrix_mE52D15EF98C58320BF029798CCCA601B135F80E2,
	CameraDevice_GetBackgroundPlaneInfo_m450C07D6703523E17F2A9EDFDCF71329D2155A33,
	CameraDevice_IsFlipHorizontal_mBCF32945B2CDD0539EE088C59646039AC426A00F,
	CameraDevice_IsFlipVertical_m368B58585E447E0AAA377440EBC3D93987CCFBBC,
	CameraDevice_CheckCameraMove_m4A59F29AC505547AB4CF09E591035CA39A49A558,
	CameraDevice__cctor_m2129F0A488EF4741B4C42FAE320B13CCD05369B5,
	CloudRecognitionAPIController_Recognize_m397162603EB6B030F9BE9111112A8D2E0DBBDFC6,
	CloudRecognitionAPIController_DownloadCloudDataAndSave_mD7DDE367BF26219758EFB0490950196D5BD1AD13,
	CloudRecognitionAPIController_JWTEncode_m2269AD0516458B6C6A7D31A3BBB70BB27D9EC6DC,
	CloudRecognitionAPIController_destroyApi_mCA6030F43C571475770E2AC4667FE406089069F3,
	CloudRecognitionAPIController__ctor_m0D6CF69548E94B7C883E45129C02F9BBDF45516B,
	CloudRecognitionController_SetCloudRecognitionSecretIdAndSecretKey_mC7C9F09C20600730341C96E2038A35258DD4EC27,
	CloudRecognitionController_GetCloudStatus_m58CEA89750D776137CE0B9367902C9ABE6126446,
	CloudRecognitionController_StartTracker_m422BD955682D9242C165CB4AE1BC99754D52FB3C,
	CloudRecognitionController_StopTracker_mC79E9729FE27C56117D2EB767CA02D329A9E6678,
	CloudRecognitionController_DestroyTracker_mDE30FC70302DD304368995A083933CF3C5C6EC1C,
	CloudRecognitionController_FindImageOfCloudRecognition_mF067EE8BD5A498574C4B817AFABBF1551B757EC0,
	CloudRecognitionController_SetAutoEnable_mDE82AB035CC5D2A1A17710F5157AF552E9E44886,
	CloudRecognitionController_Update_m78E1C776BBEF39E0EA8F8AFF46B51E261AEF79A7,
	CloudRecognitionController_StartCloud_m9D50BA355097C6C4EA25EF034DABCC4B83B8CCF0,
	CloudRecognitionController_GetFeatureClient_mD96FF71E920652FF3E5534D49449D2B64461D671,
	CloudRecognitionController_GetCloudRecognition_m0D2728CB92DA7FC6738034D3E847430D43F91353,
	CloudRecognitionController__ctor_m758A531901287DEFC97245BF62FAF2B6BEA781AF,
	CloudRecognitionController_U3CFindImageOfCloudRecognitionU3Eb__18_0_mA3326E9222B1B58CFD2DE181A3FEE084F684B8EB,
	CloudRecognitionController_U3CFindImageOfCloudRecognitionU3Eb__18_1_m35E0FEAFEF769DBD4F43858759D9BFB98CEE705B,
	CloudRecognitionController_U3CUpdateU3Eb__20_0_m0E39C1D109E446C7C9522D4806CC90EFBFC5B27B,
	CloudRecognitionController_U3CStartCloudU3Eb__21_0_mFFD68D95DFFE4179499EB4A9F9111D3F95F28BB2,
	CloudRecognitionData_get_ImgId_m254192D198AD881EB2EDD2102F6552178CB771DB,
	CloudRecognitionData_set_ImgId_m79E71AD5E61416275A1529EB40219F8199EBD261,
	CloudRecognitionData_get_Custom_m83E7944A03AD6F805079781284CEA23A51465432,
	CloudRecognitionData_set_Custom_m6ECBBB95C3AAE97C9BC3C03966603EDD9DD80E6A,
	CloudRecognitionData_get_Track2dMapUrl_m39ED3D8D2F4B07522CD823351C406F482D2BAB6E,
	CloudRecognitionData_set_Track2dMapUrl_mC49B035F665C5B3C34A723B17E11C00103578C5F,
	CloudRecognitionData_get_Name_m92F272A1559A256F13912361BA8FE448F29D1EAD,
	CloudRecognitionData_set_Name_m940BCAD241C3877F116CBDD38CBD452FCA9E566B,
	CloudRecognitionData_get_ImgGSUrl_m53803A165B9113F3AB3D1CCD81E46329FB980EF1,
	CloudRecognitionData_set_ImgGSUrl_m3726340E54AE3183E3100C61FB318156D38B0C84,
	CloudRecognitionData_get_RealWidth_m99605A4843B4AA03CA1C10355E3347558FC32B62,
	CloudRecognitionData_set_RealWidth_m8C4CCC9EA7FC57F5F169D6790160F91A3A0329CF,
	CloudRecognitionData__ctor_m26002B3B70787153170D066AD55F0EAEF33E07CB,
	CloudRecognizerCache_GetInstance_m4E9D3D961ACC034CFDB8E959A11CE9D43C2F316E,
	CloudRecognizerCache_ADD_mC6F54D314A9B67ACC4963D1FC0EAD14BC9994B93,
	CloudRecognizerCache_LOAD_m17C141EB005E69600F26191EBBC537D88D5BCA46,
	CloudRecognizerCache__ctor_m3B83EF7779715D5E8DD442EF272B7077988580CB,
	CloudRecognizerCache__cctor_m1D701C0F1C4097A3D2BB8B048AAE1C54AF52DB91,
	CloudRecognitionLocalData_get_cloud_m69AF68D386CA09A1AAACD1726EF7E72915FAFCC4,
	CloudRecognitionLocalData_set_cloud_m7A3B26E99EDAE60ACFA7E5B6F5DCF80095EACA7E,
	CloudRecognitionLocalData_get_cloud_2dmap_path_m5733C888E7FDE093E18014A693E10B44635A80AF,
	CloudRecognitionLocalData_set_cloud_2dmap_path_mBCE9BAC7DDEE7271518196F6FE10AA7E54BD5A99,
	CloudRecognitionLocalData_get_output_path_mB248037ADBDEC9FD2B0B15B1AD7F711CACCE08B3,
	CloudRecognitionLocalData_set_output_path_mE6287221438801AB6718EE84823A7B0D0C8F8371,
	CloudRecognitionLocalData_get_cloud_image_path_m648A0BA4481CA7500964E5C65EEADDEF8C176EFA,
	CloudRecognitionLocalData_set_cloud_image_path_m4BB5E755B9C797A01AA32FADFDBC27ECA398D2D0,
	CloudRecognitionLocalData_get_image_width_m9C454940BD98B2131BECDB36F1E226D04F3F4D01,
	CloudRecognitionLocalData_set_image_width_m09960AC0A6F8C13A27D2BAAF4D408E21CF5FCD5A,
	CloudRecognitionLocalData_get_cloud_name_m52ADE66D64318A603E158CEFCD91CEC25066602D,
	CloudRecognitionLocalData_set_cloud_name_m0F6EE579CF256F714CAD2756C43F20F0D6944354,
	CloudRecognitionLocalData_get_cloud_meta_m636E7017F316441FDAEBF2B859BBA915CC1481D9,
	CloudRecognitionLocalData_set_cloud_meta_mADCAE884F5623D351FFABB84886CFBC1C2B52CD9,
	CloudRecognitionLocalData__ctor_m4B4DBECAC1A83498A6A251C169623A2920BDA21E,
	Dimensions__ctor_m60DB342A815F12B3F7C159C5A648B3BB19169A8A,
	Dimensions_get_Width_mE56251026269CBCB0C817537E7FDF9BBE0F5016B,
	Dimensions_get_Height_m60A7D268171AD37488478E22A40B17199FD193C4,
	Dimensions_ToString_mCA998AA750E56F5C71BB7F8E6D418C8B91FDFE94,
	GuideInfo_UpdateGuideInfo_mAB8A020344958F11C97A3248D2B8BC63450DF17C,
	GuideInfo_GetInitializingProgress_m90957C6BE85766C79AE008E6DC838D6DA73B2BF0,
	GuideInfo_GetFeatureCount_m933350BD15F080CF22DA2BECE81C63DCAD36CB62,
	GuideInfo_GetKeyframeCount_mB480D72801AAAC0DE91992C2A7D2CA41255EF442,
	GuideInfo_GetFeatureBuffer_m466BA90E87FF6107E5D32635D641C7F8BD41B06E,
	GuideInfo_GetTagAnchors_m4695DCEAD01483639D9D7632AF7CCB659AC6CE04,
	GuideInfo__ctor_m4D0A3F456C4757B7037423BEF13AABEE98A67315,
	GuideInfo__cctor_mC987A78E4A073CD9B5CCD82EE455DCDE25ECF53F,
	JpegUtils_GetJpegDimensions_mAD50BAD17C254EEC118501CA837370AE879A2E76,
	JpegUtils_GetJpegDimensions_mD1F99AA3CC85370839A11D86BB084DCE942B2541,
	JpegUtils_GetJpegDimensions_mF6E0241011EDAFACE7D735B8756B7156703D9525,
	JpegUtils__ctor_m46F7D922E5E095CFC7EFBE9DB9188EE8ED12EFB2,
	Map3D__ctor_m16A1913FBE13F1D2B45DB15D0C0E86A3872C402F,
	MapRendererBehaviour_Create_m336B86387A3AB613D05CED38C12E6514B1045F24,
	MapRendererBehaviour_CreateAnchors_mF1E7E7A9A379D7A8981F9EB650187128619F8F85,
	MapRendererBehaviour_CreateMapViewerMesh_mB68C98165D6073379B4E497309F307BA047FA24B,
	MapRendererBehaviour_Clear_mDD40104A3B386ED06F8B0AA35DAFBDEF971C5353,
	MapRendererBehaviour_SetActiveImageObject_mEE5C41C7E70264D421D37862EF7DBDAFC8296CE6,
	MapRendererBehaviour_SetActiveMeshObject_m7927DA92E82F8226DC2C48AB1CF1FC879173FF1E,
	MapRendererBehaviour_SetDeactiveImageObjects_mB13D7EA38AA6357C4D139E47B659027D1F1ADE06,
	MapRendererBehaviour_SetDeactiveMeshObjects_mB7D864EACCDB5B0D1FC25F496850EFF4E287D70C,
	MapRendererBehaviour_OnRenderObject_m99589949F1FAEA74F28E83CDF018C0EF8534D338,
	MapRendererBehaviour_GetGameViewSize_m4A5EA759DA1E669F88E08579C4887B8A48B8652F,
	MapRendererBehaviour__ctor_m8743BB5B6F13A9B7B5FCC6275029820DF3249BBB,
	MapViewer_GetInstance_m4A374B55A656147154BD9DECBE577BBF5B6D90F2,
	MapViewer__ctor_mC3877A88FEDA836FF3937881DF34C4B1C21668B6,
	MapViewer_Initialize_m104A0FC143BCCC3635B81742713D55AB1AC63C70,
	MapViewer_Deinitialize_m4A0C5B4E073905E48E0B3694824A9FBDD0BE6AB7,
	MapViewer_GetJson_m36402694A37C448BA3D29E503DD77E2ED21B10BB,
	MapViewer_Create_m4B72E832C71C814B55DF3A976BD2D92A67370DC0,
	MapViewer_GetIndices_mDA13E7F78DBD50E88995A12D952E13D21CDDF1E0,
	MapViewer_GetTexCoords_m5004DB2175BFC5BDF947A064AA5109A4C4707978,
	MapViewer_GetImageSize_m0BF1EF31E754180977191C2C4DBA22C9CFE39135,
	MapViewer_GetImage_mA3677402A1AE5A5977C241A991C0C5D344BAF0EC,
	MapViewer__cctor_mF6E85FC48BAEAE0F6364C61EECD46D754004457F,
	MatrixUtils_ConvertGLMatrixToUnityMatrix4x4_mA761C66DC6C2CDF0843F14319E2E4074EB13E42A,
	MatrixUtils_ConvertGLProjectionToUnityProjection_mDB182F79BC65EB2063F2D87D95518EF34E3D96B0,
	MatrixUtils_GetUnityPoseMatrix_m3F6D97B070AB4B2518E76AF1AE4653F936D0B7F5,
	MatrixUtils_GetUnityPoseMatrix_mAD582DC615E01600B46AE18E9071D0AE80309441,
	MatrixUtils_ApplyLocalTransformFromMatrix_m17ED61807C6A4E85ABDFC4F76972CA8B68939EA5,
	MatrixUtils_InvQuaternionFromMatrix_m8292A4671B8F0F46F53FBA58D75EBAF4DF3E0DBA,
	MatrixUtils_PositionFromMatrix_m7C6A297E614128E22FB75E01B62F47050B77EF2C,
	MatrixUtils_ScaleFromMatrix_m807628CAC045208C9DA8BC8F23D1B1AE9A4CAFA4,
	MatrixUtils_MatrixFromQuaternionAndTranslate_m754AE58EE324F7F31FF2B51A49663CF03D131DCD,
	MatrixUtils_MatrixFromQuaternion_m795835D0FB9ADEF22E610F1EEE604C2E34D82550,
	MatrixUtils_QuaternionFromMatrix_m9BD557FFF4F96359C9C363E6F31CDBADD5BD976D,
	MatrixUtils__ctor_mD2DA0C77721FCC59A6A0FF4C8C373D07821AE790,
	MaxstAR_GetVersion_m6A48EE10DAFC38B83913C8D5450BD4BA515F1E00,
	MaxstAR_OnSurfaceChanged_m2B3B1318E5737F4176BDD338BACEF35913F11C80,
	MaxstAR_SetScreenOrientation_mF08F84A0D6BC4DC39E4276A60B7BB19A8648C892,
	MaxstAR__ctor_m54878236B4B0E38D82522F3417FCE47CEB0E8B67,
	MaxstARUtil_IsDirectXAPI_m323AD4BDC91ED65E66769E6FD6D4BBC5C7510D62,
	MaxstARUtil_LoadImageFromFileWithSizeAndTexture_m373EFD3D6445643EBD57C7487FB53DF50ABA36E2,
	MaxstARUtil_ChangeNewLine_mA97641ABD688D4BBEC525C53265B333ECA2E2B5B,
	MaxstARUtil_DeleteNewLine_mE326859F84DBBE22B17F3DCAEA504E8C7C107532,
	MaxstARUtil_GetPixelFromInch_m58182121E84B54ABAE74CBD431F338519EBC0095,
	MaxstARUtil_DeviceDiagonalSizeInInches_m1BE6276090981E0C1229461A102DBF6DF545EDC4,
	MaxstARUtil_ExtractAssets_mCAF52CCFE52335A154C03E5A97E405344F52E5A9,
	MaxstARUtil__ctor_m01A12C3D355E4DC6AF2F5E9C1840D50EB338A1AA,
	NULL,
	NULL,
	NULL,
	NULL,
	NativeAPI_maxst_init_mC1AAC75A8AE9670BC22F63457FDBB43C7766E6A7,
	NativeAPI_maxst_getVersion_mE26BC475EC44BF8656B0B9FE49777912FAA8CE1C,
	NativeAPI_maxst_onSurfaceChanged_m7690D0F0731F47423A7CB6DF92C1095519B0E7DD,
	NativeAPI_maxst_setScreenOrientation_m8D3F14D7F9B5186BA9BB44F8284B6186D967D9DE,
	NativeAPI_maxst_CameraDevice_start_mA5EDF2E8BFD27E3E28B6E374616A989B83646B59,
	NativeAPI_maxst_CameraDevice_stop_m12167043DA6AB668081ACDD6A7CCA11CAABA98A1,
	NativeAPI_maxst_CameraDevice_setCalibrationData_m587F4F4A01AB5BBBB42BC2DD01713373B14FF858,
	NativeAPI_maxst_CameraDevice_setNewFrame_m5AC1A52756AD74C655E1BA877963E72E72469FA9,
	NativeAPI_maxst_CameraDevice_setNewFramePtr_mC9369C76C946063E885314743D21321F1C4F81A0,
	NativeAPI_maxst_CameraDevice_setNewFrameAndTimestamp_mA57D3969EF91AC25B3F3E9B20FD0BF7515EA5110,
	NativeAPI_maxst_CameraDevice_setNewFramePtrAndTimestamp_m2C4355973A2A9C68BB3EAD9C926E567B0D2E64A5,
	NativeAPI_maxst_CameraDevice_setFocusMode_m480F26C45A73ACE6BF49C7ADF2A80B5546D518D1,
	NativeAPI_maxst_CameraDevice_setFlashLightMode_m9B82A6F46EF0D5094D7FA8891EB6672BD85B3F5C,
	NativeAPI_maxst_CameraDevice_setAutoWhiteBalanceLock_m415195884E839F12CE465AE0705524CA5D9C2ECA,
	NativeAPI_maxst_CameraDevice_flipVideo_m62342772920CF6123AF1D33BA4456EE6177F1BCB,
	NativeAPI_maxst_CameraDevice_isVideoFlipped_m172D75CB004ACB86EF78779056C3090CF6250794,
	NativeAPI_maxst_CameraDevice_setZoom_m95118B352941DD6009C65B64BD91E2DF3EF1C64A,
	NativeAPI_maxst_CameraDevice_getMaxZoomValue_m352526181B5BFBD10AE14F3A34197D14F452EA5B,
	NativeAPI_maxst_CameraDevice_getParamList_m733CCFA434F9BC8804846201D8A23DEBD851BE24,
	NativeAPI_maxst_CameraDevice_Param_getKeyLength_mEB26C85CBBA25FDF23945A221325BCF7DA9D1747,
	NativeAPI_maxst_CameraDevice_Param_getKey_m4217BEDAF8FF57FCCA097FD754BC11C7E30F2E21,
	NativeAPI_maxst_CameraDevice_setBoolTypeParameter_mDCDD272E5B1E9C5C38F7D9E3EC9FB2D0D0D9DC94,
	NativeAPI_maxst_CameraDevice_setIntTypeParameter_mF657FA7F7A8C5916B84F08E47D1A8FB6D44871A4,
	NativeAPI_maxst_CameraDevice_setRangeTypeParameter_mBBD598D47203A43530A044712E1000ECFB5934C0,
	NativeAPI_maxst_CameraDevice_setStringTypeParameter_m92EF41905F8DA6690DD73DD5A1028C6C0CCC285D,
	NativeAPI_maxst_CameraDevice_getWidth_mA3A09F5F4397EFB824807005333759DB1F7F4C6F,
	NativeAPI_maxst_CameraDevice_getHeight_m549A526D0E3B00AAA8CE00A7DF29D99CD136C5D0,
	NativeAPI_maxst_CameraDevice_getProjectionMatrix_m7A7FA73573233264B63C3FEDC2F7E8EF45C1E80A,
	NativeAPI_maxst_CameraDevice_checkCameraMove_mF17BBFE375895257ECAA4FD7263CFA3ED4D16657,
	NativeAPI_maxst_CameraDevice_getBackgroundPlaneInfo_m7C75B9FDE102ABF63B08C501982DA781B7661AD7,
	NativeAPI_maxst_TrackerManager_startTracker_mD2A013E2FC6B619155F37192F9AB7DABB44CB121,
	NativeAPI_maxst_TrackerManager_stopTracker_m2AC86CAD2BE61A2E1451B47F8FA2488D813989F2,
	NativeAPI_maxst_TrackerManager_destroyTracker_m1174ED1C73195AEE3AB49531497301A9D9366262,
	NativeAPI_maxst_TrackerManager_addTrackerData_m51C22A522B54749FDBC43568D1B0BAE9E3B6C497,
	NativeAPI_maxst_TrackerManager_removeTrackerData_mEB0715C35637542C19BEC1A33417F97CC370369B,
	NativeAPI_maxst_TrackerManager_loadTrackerData_mD7B98D711F885256BE5A3E37221387D6CB67EBAF,
	NativeAPI_maxst_TrackerManager_setTrackingOption_m002A7BE8981B92CF58464C56E6C7BD699699FADB,
	NativeAPI_maxst_TrackerManager_isTrackerDataLoadCompleted_mE1E670296E50E476D4434C251FADEDB1F8CD48DA,
	NativeAPI_maxst_TrackerManager_setVocabulary_m9D20A8B8F7760E8A505E8B6FE179B70BB72C03BF,
	NativeAPI_maxst_TrackerManager_updateTrackingState_m1FF8B6E79093D760E2FF3FC0FBDA947FAB86F11E,
	NativeAPI_maxst_TrackerManager_findSurface_m2F01DD3DA099836016D8C7EB80CFA1EB7770D202,
	NativeAPI_maxst_TrackerManager_quitFindingSurface_m383FB2DFD3D0404740C354D781CCA20509073AED,
	NativeAPI_maxst_TrackerManager_getGuideInfo_mC3CD90B50593B28C29E10927009327A4E24FEFF0,
	NativeAPI_maxst_TrackerManager_saveSurfaceData_m5FB3E8361B4713D63718A2D372961B299B94728A,
	NativeAPI_maxst_TrackerManager_getWorldPositionFromScreenCoordinate_m06785004CF3907F36BA753A6E0293B466DC56990,
	NativeAPI_maxst_CloudManager_GetFeatureClient_m76665EAADF00E6475C0A04F28E5B534D8EACE79F,
	NativeAPI_maxst_CloudManager_JWTEncode_m6BDF4BC161AE729828474140E31DB0FC185BD889,
	NativeAPI_maxst_TrackingResult_getCount_mC3FBA8BCB14A66845A307310DED26C50257E5082,
	NativeAPI_maxst_TrackingResult_getTrackable_m0F5189AD387C9735BFF31D519CE9F0E27CA95B9B,
	NativeAPI_maxst_Trackable_getId_m65E02BA95C26BC662C8DE07DBCB80FF71F34A7F2,
	NativeAPI_maxst_Trackable_getName_mDEAA3725E5E6CB1E45555AACC612F1F1BDAD114F,
	NativeAPI_maxst_Trackable_getCloudName_m45027EEE9B6AB68067EB5C233B50B1BEE93ECE3D,
	NativeAPI_maxst_Trackable_getCloudMeta_mEE9F177E4A24BD1FCF987F11028E96FCD17C6515,
	NativeAPI_maxst_Trackable_getPose_m656155FAC79B8376B46E70AD1B284C27F088DB82,
	NativeAPI_maxst_Trackable_getWidth_m98564B655F63F5CF6674E8EDA4F8BD0E4FB5A715,
	NativeAPI_maxst_Trackable_getHeight_mD2C8D6D0DAEA052AFFDAF7E886EA4658A54D40AE,
	NativeAPI_maxst_TrackingState_getTrackingResult_m663F7A5997448E4FF7726D09A8901FDCFD976FE1,
	NativeAPI_maxst_TrackingState_getImage_mBEE338FF02F972166CA81F5117BE653C6156003B,
	NativeAPI_maxst_TrackingState_getCodeScanResultLength_mE478E4BA4BEC1CA111EA9A265209E2AF38A484D3,
	NativeAPI_maxst_TrackingState_getCodeScanResult_m88980E71855098278471653402255093CF47A43A,
	NativeAPI_maxst_GuideInfo_getInitializingProgress_m473962B12C71875B6678C219BFDCA64BB9FD4BE3,
	NativeAPI_maxst_GuideInfo_getKeyframeCount_m60AA17AE8822D71F3C8272A1CF9348501C3E5154,
	NativeAPI_maxst_GuideInfo_getFeatureCount_mEEBA38F8FD74F6415ECB0C2A6FAF6B786CEB43A8,
	NativeAPI_maxst_GuideInfo_getFeatureBuffer_m9FBE90B9269C733F504CE5286E63A0704DBA8CDB,
	NativeAPI_maxst_GuideInfo_getTagAnchorsLength_m01C8BA9DDA202B8A5D13EE294449CAEBD49A8235,
	NativeAPI_maxst_GuideInfo_getTagAnchors_mF729BEEB7CB35A14F3E3A5A300221AB9AFB2CEF1,
	NativeAPI_maxst_SurfaceThumbnail_getWidth_m5374309D1ED7CE91A09C299E8C112E8F20344A74,
	NativeAPI_maxst_SurfaceThumbnail_getHeight_m68521172E9A9936326ED88B690E3E87E356000AE,
	NativeAPI_maxst_SurfaceThumbnail_getLength_mCCC4613A0617DA641492CC0EC46F091D996725B8,
	NativeAPI_maxst_SurfaceThumbnail_getBpp_m7BD8DCB38F1117093E38BF9E0996B0D3A829F026,
	NativeAPI_maxst_SurfaceThumbnail_getData_m72CC556F4243C8F4543F2902960EBB20C0117977,
	NativeAPI_maxst_SensorDevice_startSensor_mA7826A672D56DAB8BC392A3695B8E0C39752C85A,
	NativeAPI_maxst_SensorDevice_stopSensor_mB3D457114603E18053BC43A560581BE02752F5B6,
	NativeAPI_maxst_MapViewer_initialize_mE1D0936F186A301F61346D3DFEECC1A6A7BDE886,
	NativeAPI_maxst_MapViewer_deInitialize_m9803387B412EEA0C97F82BECF47CA2C4B90D3180,
	NativeAPI_maxst_MapViewer_getJson_mFF2D1942D6D85FFDA8DBFBB37D9277D42CF4747E,
	NativeAPI_maxst_MapViewer_create_mA8A51DE668DA01D1EEFCD1CDE0607DD9E05A3EA6,
	NativeAPI_maxst_MapViewer_getIndices_m7BE3DB492739E0227CED9F498E42E7A0C76987B1,
	NativeAPI_maxst_MapViewer_getTexCoords_mC4F8441C2CEB2512FA632CBD82512FF06946429B,
	NativeAPI_maxst_MapViewer_getImageSize_m3E303CD3A68C838A42E359CD9AB18E29B05C12F7,
	NativeAPI_maxst_MapViewer_getImage_m39F99EEC2A336BC6610FD511FE28038836936687,
	NativeAPI_maxst_WearableCalibration_isActivated_m9143F692C8392D25C750D38EC06B266105B72EBD,
	NativeAPI_maxst_WearableCalibration_init_m772CE19706D3AE2AB4E896585B5FB7EC8A4704AF,
	NativeAPI_maxst_WearableCalibration_deinit_m174A61A633706CCF0CCFAD86AF3CD91892AF8367,
	NativeAPI_maxst_WearableCalibration_setSurfaceSize_mE84BDEFC9470F81B1CAB9DF7A2092787A182DCB4,
	NativeAPI_maxst_WearableCalibration_getProjectionMatrix_mCD2BC624AB1D8DEFEADA180420AA58535AB0353A,
	NativeAPI_maxst_TrackedImage_getIndex_mEEB773E5420A16C287B64850530C2430BCDF530E,
	NativeAPI_maxst_TrackedImage_getWidth_m668590037D74A9A8529245D389A52E7FA74E5933,
	NativeAPI_maxst_TrackedImage_getHeight_m694B87A48F1267C12A88DCA22C5DFFD821C8E8A0,
	NativeAPI_maxst_TrackedImage_getLength_m4AD455C6AA5D2126478F92288A431367202CEFD2,
	NativeAPI_maxst_TrackedImage_getFormat_m775B4491F0F0CDF95904BA5C7CCDCE8819ADFAA0,
	NativeAPI_maxst_TrackedImage_getData_mD323FBE5A761CD2CB39DB663212524FB1FB71D95,
	NativeAPI_maxst_TrackedImage_getDataPtr_mD62AC911086BD8845DBDB9547225539C37EFCA3F,
	NativeAPI_maxst_TrackedImage_getYuv420spY_UVPtr_mC80CE7D9A89A78E3FF325ECB66907D7AEEAE418B,
	NativeAPI_maxst_TrackedImage_getYuv420spY_U_VPtr_m906C3214FA840F6D0B696010ACB4BDCC10F771BD,
	NativeAPI_maxst_TrackedImage_getYuv420_888YUVPtr_mCF529A3F15BDD5D195375005588D18D1EA2863CC,
	NativeAPI__ctor_m128CF9DC4569AE87E3E6BFA215BA49606269D5C1,
	Point3Df__ctor_m934B342CC4021CACF855676356D026CE130870B5,
	SensorDevice_GetInstance_m7F13A44D48036B73D11E9CD55A9EA7ABAE1D430F,
	SensorDevice__ctor_m7203FB445DDFA689277FCFDE08A9D69B89CA346C,
	SensorDevice_Start_m36EF038173578054700423CC007B17AC66DBD6D9,
	SensorDevice_Stop_mD2DE3E49BD054C25601EE1A274B7038C7BF7EABE,
	SensorDevice__cctor_m821F54269B8B6BB3947D6EA253CCC98AABE2FEC0,
	SurfaceThumbnail__ctor_m0DD58C3F68E19DECAA5378921816128B06360334,
	SurfaceThumbnail_GetWidth_m2DAAAE442D0F0D28B8ABB83DF3B96DAAAE74574D,
	SurfaceThumbnail_GetHeight_m8DE428A2D928FA3960D8B1706012DAC5A4E4DD4B,
	SurfaceThumbnail_GetLength_m2A9A3AC873AB4069E48B88AF8D1D24AF81FFB47F,
	SurfaceThumbnail_GetData_mB4539CA663D4B080D677C046FFBBF33A03F3B428,
	TagAnchor_get_name_m99C9E247B0771C777A7A09CD8B3A2D2FE0964240,
	TagAnchor_set_name_m443B327E4B533C8F67F3173D955C15526E1D0C79,
	TagAnchor_get_positionX_m4BA6F4722CB2624F28E04F70B1B669E1D44C6115,
	TagAnchor_set_positionX_m9C85C1BE8641AA561F5E78C35ECD34B47B2E7068,
	TagAnchor_get_positionY_mA4FECF70AA30253F3A9C8379DBF7C28E11FBC46D,
	TagAnchor_set_positionY_m44F3448EBACBE4404CC470ADEF20003BA65842CE,
	TagAnchor_get_positionZ_m43C90B29A0D5F78F8CBA8370B50CD1E678718671,
	TagAnchor_set_positionZ_m8930567E2FAD7386F0B5FEED38AC4B2097E528C4,
	TagAnchor_get_rotationX_mE3F3509DE799F285E067105834716D2118880FD3,
	TagAnchor_set_rotationX_mC19A47B642FF9C74C1D9A42DEAA0CEBC527A0609,
	TagAnchor_get_rotationY_m3C25FA7B23C3900A234BA4A424B11B1EAAA40741,
	TagAnchor_set_rotationY_m79330569E15C6C5D3FE5963589D34EC5B46B772A,
	TagAnchor_get_rotationZ_m53B878CB1D016601745D41E200784B4E373A1D28,
	TagAnchor_set_rotationZ_m4B0052670F90E53A36CB302B99CE9B4951DA0F70,
	TagAnchor_get_scaleX_m2E449DC6C0A2DC69CC442A296EF8ABD66053361B,
	TagAnchor_set_scaleX_m343BCB76E871D5290A302F91E938311D78E8485F,
	TagAnchor_get_scaleY_m390023FE173DD6CB7DE0A8A109299F2E9D2B3922,
	TagAnchor_set_scaleY_m1F81421BD263D9D41332AFBC86CA014D56721256,
	TagAnchor_get_scaleZ_m772C067FCBA84BCFD7058A1F284253669FE30C6B,
	TagAnchor_set_scaleZ_m75F59D542E2F765CE2C0895282807A30F3238500,
	TagAnchor__ctor_mD1A039D323CC5DBA758D093572C4E6BE8CCCE56B,
	TagAnchors__ctor_m5DB5FB2917283C43ACC648245EC4E876284AAB1F,
	Trackable__ctor_m28195F5B5619EBC04AF14B18F8D5773F9A37B195,
	Trackable_GetId_mA44F5BE3181F6C793190AF897E101D04DB40BCA9,
	Trackable_GetName_mD9121445DC81017509E298724A29BF043A619D8D,
	Trackable_GetCloudName_m591936B50E988289EBA0BDF07AB98935099EF404,
	Trackable_GetCloudMeta_m6B439B693DA1C113940981E0A27CABEAB5D90152,
	Trackable_GetPose_m755CCDD0E4B0A606A6C5DDEB16F1C3775030FF25,
	Trackable_GetTargetPose_mB77AB3E7D80F5057056E86CE1FA689C6428BEF21,
	Trackable_GetWidth_mEF8DC3868A6261E258DD397994C1E9776720A4EF,
	Trackable_GetHeight_m793558B204134A0CED986087BEA5F88770541607,
	TrackedImage__ctor_mD5DB1118197FD89E30C46B0AA42CC748E4928BD2,
	TrackedImage__ctor_m0F370A499837DC0DD4580FEE1B99C728F4B8CE2E,
	TrackedImage_GetIndex_m5D91235D3021505E5E96F1D3051D4669ABD6E906,
	TrackedImage_GetWidth_m1FAE46C61FDDB4C057A115D3FC9533E93091102D,
	TrackedImage_GetHeight_m45AF66C5B35A8BEA2FC4E67C75015219043FFB8A,
	TrackedImage_GetLength_m6B837456F54502CC5F96968B358FB3322E71E3E6,
	TrackedImage_GetFormat_m933A4B083B65AF504D206645349A06EBC352F88B,
	TrackedImage_GetImageCptr_mF7E979531F3221DB5537277E7B8A62DCD0EAC8C0,
	TrackedImage_GetData_mAC39DBDDAF5858CC99748102BC56B13BB2DA3724,
	TrackedImage_GetDataPtr_mD5C6FA76DFF8AADD046F0E08EEFDE4D478B1719C,
	TrackedImage_GetYuv420spYUVPtr_mB5B2AEE498E9F78116A88FCADE165F213D54EB6F,
	TrackedImage_GetYuv420spYUVPtr_m77B51E991E3C6BBA78F607391FA6EF22A0A9E418,
	TrackedImage_GetYuv420_888YUVPtr_mC60F7DF572FABF68ED9B7F6B85807FEDDA305A8A,
	TrackedImage__cctor_mF38C28B4C55A79C5699CD5F605CBAD7668926A4B,
	TrackerManager_GetInstance_m46A27E965CA5109148150899D4CD2B26CA6F53E6,
	TrackerManager__ctor_m170A81D4AD8E76A78178882F1CC4CD2C7959D3A5,
	TrackerManager_InitializeCloud_m16BEED93FCF7207B81027A789BC393248ECB615A,
	TrackerManager_SetCloudRecognitionSecretIdAndSecretKey_m4D1B3EF4FB196AA715B5F3CDCBC080E649045429,
	TrackerManager_StartTracker_mEDE0B41014FC73A9DDD6F2FCB60BB548EDD69088,
	TrackerManager_StopTracker_m8A7C1E8BA74DA0EF19A7A822B17DCEF17420C165,
	TrackerManager_DestroyTracker_mF416B8798B95E8E4309A55AFEA7C2E437DA16DC1,
	TrackerManager_AddTrackerData_mAC9FB7FDD7E4128DB351527636D9A2BA2CC9702C,
	TrackerManager_RemoveTrackerData_mF161537F0A3A302653945CAFFE25468A43D82F5E,
	TrackerManager_LoadTrackerData_m45AF37872DD6F8EC6DB3BAEE185348EC314B7099,
	TrackerManager_SetTrackingOption_m5AD2D75526B7CC533691AD083604EF3469B76538,
	TrackerManager_IsTrackerDataLoadCompleted_mAE91E8657349A70A17422998C138055642A9BCB0,
	TrackerManager_SetVocabulary_mF6289C9A63280DA2F4C85C7F32AA889DFA3476A2,
	TrackerManager_UpdateTrackingState_mDD67C1785159119C1897E1088BEEB38D0596067C,
	TrackerManager_GetTrackingState_m6B4BEF73784906EDB343CD03B15C4670972702A3,
	TrackerManager_GetWorldPositionFromScreenCoordinate_m15F077BC5892CDE906BA94FFFE6C687E70413187,
	TrackerManager_FindSurface_m6457296E8C13EDD137679FE51E619DB2A4222B36,
	TrackerManager_QuitFindingSurface_mA995D66E070DF9FB38060B3A8EE211A8D1F8D750,
	TrackerManager_FindImageOfCloudRecognition_m65D09E068BC45015C4597C7F8C6495C079B8A0A9,
	TrackerManager_GetGuideInfo_m4ABE739552D344AB9AD9695F0ECD15F9213CCF57,
	TrackerManager_SaveSurfaceData_m3C2BFE33F6C0CAB1814551F4276AC70D9DB09CFA,
	TrackerManager__cctor_m5D92506D52DF722C2DA70230DA94BFB2F9B3C63F,
	TrackingResult__ctor_m16B9702EAC0B7D0B44B2B53B9521E31682724682,
	TrackingResult_GetCount_mA693AF5B453F04E891DC992DE7F147425D4447C1,
	TrackingResult_GetTrackable_mBAF43C221FEBE35839E049B9A727C47C265DE128,
	TrackingState__ctor_m511846572F1B67AC12D0596AC945498728CB2844,
	TrackingState_GetTrackingStateCPtr_m9FF15DA1E662E4D837B2FAAF53E6BB369F6D2ED9,
	TrackingState_GetTrackingResult_mD0779CB0C659620D558136BE5FCD7EA090ADB413,
	TrackingState_GetCodeScanResult_m2F19F1EC66C4563183E8784AE2ED355F28EBD7C0,
	TrackingState_GetImage_mD1C946E7955D0607CA070ABCB7164388D35F3627,
	TrackingState_GetImage_m6B379D48A3D7E6D0C46970D92317C1BE3D38F4C8,
	TrackingState_GetTrackingStatus_m494F6BEA0863C65070ED186FC320FF5EB59C2A45,
	WearableCalibration__ctor_m1F9219EF3026C4A8C32ACCD9EA9880A19DC18955,
	WearableCalibration_get_activeProfile_m672CB16C8B236CD00926A73290B274563CC1FA5E,
	WearableCalibration_set_activeProfile_m407EFDCA52FF56603F4BD3338ACE6FACC2135357,
	WearableCalibration_IsActivated_m26AFCBA7985A262D1A636BFD1909E6D7BEA21897,
	WearableCalibration_Init_mD794728749B60144E5CA54A448995426AF9EDD25,
	WearableCalibration_Deinit_m0B548B265954A3EF9F5D3B6DED4F63F4F8D01EDB,
	WearableCalibration_GetViewport_mF25EA372885DF1EE51D921638F3311725335B469,
	WearableCalibration_GetProjectionMatrix_mDDCBE1373398DEA571D8B1A9DB3DA9758992F1DC,
	WearableCalibration_CreateWearableEye_m0614E4FF2FF1B484383A0A15B950BFEAB7D33EE7,
	NULL,
	NULL,
	U3CAutoFocusCoroutineU3Ed__13__ctor_m9EF8221503DE06CBF9A9B5CEB249FA2394E2FEFF,
	U3CAutoFocusCoroutineU3Ed__13_System_IDisposable_Dispose_m8224EC15ADF2E03F8713CBD552D7F3FA740B9E55,
	U3CAutoFocusCoroutineU3Ed__13_MoveNext_mE5C27630BED7360AD9AE2DC3CD20E0132DD1C559,
	U3CAutoFocusCoroutineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0A95B6FF8E05F73DD1C1A4C9B6C7DC320D920DF4,
	U3CAutoFocusCoroutineU3Ed__13_System_Collections_IEnumerator_Reset_mC86327D53150FDEF6213904AB2F74A298F28EBC2,
	U3CAutoFocusCoroutineU3Ed__13_System_Collections_IEnumerator_get_Current_m4441C7AA354FC24658020D7468B0D16566110F82,
	U3CU3Ec__cctor_m636FF64D720EAA4C4E6DD5A980FA480A724E7D73,
	U3CU3Ec__ctor_mC3782195797FA3A76061BC89729A9920F206367E,
	U3CU3Ec_U3CAddTrackerDataU3Eb__4_0_m1BA03104FB3A0E1C0B39DF4DE8895E5CF4A70BF1,
	U3CU3Ec__cctor_m1FD5B74744D43A7642A768823273DDB674F55872,
	U3CU3Ec__ctor_mC24F1A551A05932D30E1F5291184B93F61940C41,
	U3CU3Ec_U3CAddTrackerDataU3Eb__4_0_m882F59A8CBB49F169BC81325906D3E809AA37BA5,
	U3CU3Ec__cctor_mF5BBD39FCC38DCF6F694FECE8D4AAC21E068CE6F,
	U3CU3Ec__ctor_m6E38C81B3B341CCFD18204BC2D1CA52CA83A15A4,
	U3CU3Ec_U3CAddTrackerDataU3Eb__6_0_m004D49A68ACDAC2212590F65854BD422376A869F,
	U3CU3Ec__DisplayClass9_0__ctor_mBA61E25FAE2A133A48EBD4DAA60D8D67395BCEEE,
	U3CU3Ec__DisplayClass9_0_U3CChangeObjectPropertyU3Eb__0_m140E4D21775B05B5772A09B54C049A3E4F9D7DEA,
	U3CU3Ec__DisplayClass9_0__ctor_m7E300FE781728DB124F9AC86A0BD0E0DD8AD9667,
	U3CU3Ec__DisplayClass9_0_U3CChangeObjectPropertyU3Eb__0_m7BCF5CA7CDD14575117F4C7EF448FBC192C40493,
	U3CPOSTU3Ed__0__ctor_mBC750541480CB34150176D1B70E89527D35FA12F,
	U3CPOSTU3Ed__0_System_IDisposable_Dispose_mC3DE24FE1AF3F4DB4A27FD7C495D9EBC73012EA8,
	U3CPOSTU3Ed__0_MoveNext_mE7921183CEC3A66C407A6A642B025208C3004366,
	U3CPOSTU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m34FC1B2DD2C8CC3536D619923334079A6BCCC8FA,
	U3CPOSTU3Ed__0_System_Collections_IEnumerator_Reset_mF380F363F1B0562E6C637CE54F122DE46D6420E5,
	U3CPOSTU3Ed__0_System_Collections_IEnumerator_get_Current_m0129FCB873E852BEC517B7626EB14C074369837B,
	U3CDownloadFileU3Ed__2__ctor_mB38297B4C1A44B9F882E2342FAA3AD03BEDB143D,
	U3CDownloadFileU3Ed__2_System_IDisposable_Dispose_mF454B03B68C1C0AEC4D7504D949F0F11F06F48EF,
	U3CDownloadFileU3Ed__2_MoveNext_m86FBAA36935A552D553A0BA6A5984BD6FB33E485,
	U3CDownloadFileU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m005ADF7B863CA99E44BCEA393EC1154A90779BEF,
	U3CDownloadFileU3Ed__2_System_Collections_IEnumerator_Reset_m9DDBCC7B886037A9F99FD305E1A6F36685BAA832,
	U3CDownloadFileU3Ed__2_System_Collections_IEnumerator_get_Current_m0B10C968746B3445CDF5D26FD087411E67AACCEB,
	U3CU3Ec__DisplayClass1_0__ctor_m207FE7A67CB8718A792A90A069064B1CA0DAE2F3,
	U3CU3Ec__DisplayClass1_0_U3CRecognizeU3Eb__0_mA95C7D21D3E64D3D4F6A268B02A577D428AD642D,
	U3CU3Ec__DisplayClass2_0__ctor_m52C1547808940B815C02CA3DC472F9784CB87E47,
	U3CU3Ec__DisplayClass2_0_U3CDownloadCloudDataAndSaveU3Eb__0_mA43A40C18D449D090EE30AE22D691A5CE46B1F86,
	U3CU3Ec__DisplayClass20_0__ctor_m040FFC949175C2E69D8AC21C72E58112DF4F0579,
	U3CU3Ec__DisplayClass20_1__ctor_mD805F9C507A48ABDCED80E2EB9E3B4E7348E7F0E,
	U3CU3Ec__DisplayClass20_1_U3CUpdateU3Eb__1_m26C970D2DF7D762060110C4C0835E006A581E52C,
	U3CLoadImageFromFileWithSizeAndTextureU3Ed__7__ctor_m7C863569BE1D9F2FE5730F954A084AAC78CA68E6,
	U3CLoadImageFromFileWithSizeAndTextureU3Ed__7_System_IDisposable_Dispose_m342B57C2800C42D381E992EE77C6914617A51A2E,
	U3CLoadImageFromFileWithSizeAndTextureU3Ed__7_MoveNext_mB2EAD756196FFEB37E70CF188EF408DB8C321001,
	U3CLoadImageFromFileWithSizeAndTextureU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m03F447B5A13B7A6452201ED7DB7B811DE147D3F6,
	U3CLoadImageFromFileWithSizeAndTextureU3Ed__7_System_Collections_IEnumerator_Reset_m273B802DC50B7B83799BB1E45B806807C42B3717,
	U3CLoadImageFromFileWithSizeAndTextureU3Ed__7_System_Collections_IEnumerator_get_Current_m14D5DD893EA96429754FE11CB876BA87DD9CCC9A,
	U3CExtractAssetsU3Ed__14__ctor_m791BC8C9C57C738ABEABBF7CA6E6EAAC093D51FE,
	U3CExtractAssetsU3Ed__14_System_IDisposable_Dispose_mF324F4F165B44FFF571C900DC2B6AB23FBD3A138,
	U3CExtractAssetsU3Ed__14_MoveNext_m930C3C95F2F1487D3834B9BE20A6EC978767EAFE,
	U3CExtractAssetsU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE03F133C6B2A5190A80BDE152B1B3F43FF05A82,
	U3CExtractAssetsU3Ed__14_System_Collections_IEnumerator_Reset_m296B94A985566CF86693CEB99575D9E018FB66BE,
	U3CExtractAssetsU3Ed__14_System_Collections_IEnumerator_get_Current_m5672504137D2D96E62F02809203A51C0A0DE00AE,
};
static const int32_t s_InvokerIndices[684] = 
{
	23,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	4,
	27,
	23,
	3,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	23,
	23,
	141,
	31,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	307,
	23,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1580,
	23,
	23,
	23,
	1580,
	23,
	23,
	1580,
	23,
	23,
	23,
	697,
	307,
	114,
	31,
	23,
	23,
	10,
	32,
	697,
	307,
	953,
	1580,
	23,
	23,
	1580,
	23,
	23,
	14,
	26,
	1580,
	23,
	23,
	23,
	23,
	23,
	114,
	14,
	31,
	114,
	114,
	4,
	23,
	14,
	14,
	3,
	4,
	23,
	23,
	23,
	23,
	10,
	14,
	32,
	23,
	27,
	23,
	3,
	4,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	38,
	26,
	26,
	23,
	114,
	23,
	3,
	23,
	26,
	23,
	4,
	23,
	3,
	31,
	26,
	58,
	23,
	23,
	23,
	697,
	697,
	23,
	26,
	883,
	23,
	23,
	10,
	32,
	114,
	31,
	10,
	32,
	114,
	31,
	9,
	9,
	31,
	161,
	23,
	1581,
	23,
	23,
	26,
	26,
	23,
	582,
	23,
	26,
	23,
	697,
	697,
	23,
	26,
	883,
	23,
	10,
	32,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	26,
	23,
	1580,
	23,
	1582,
	0,
	2,
	23,
	4,
	23,
	10,
	9,
	1583,
	1584,
	1585,
	1586,
	30,
	187,
	187,
	604,
	30,
	449,
	697,
	14,
	439,
	451,
	1587,
	115,
	10,
	10,
	10,
	1137,
	14,
	114,
	114,
	9,
	3,
	404,
	168,
	113,
	23,
	23,
	27,
	10,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	876,
	27,
	23,
	23,
	1115,
	26,
	1115,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	697,
	307,
	23,
	4,
	27,
	23,
	23,
	3,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	697,
	307,
	14,
	26,
	14,
	26,
	23,
	169,
	10,
	10,
	14,
	23,
	697,
	10,
	10,
	14,
	14,
	23,
	3,
	0,
	0,
	0,
	23,
	23,
	168,
	26,
	445,
	23,
	32,
	32,
	23,
	23,
	23,
	1191,
	23,
	4,
	23,
	116,
	23,
	136,
	37,
	6,
	6,
	37,
	795,
	3,
	1588,
	1588,
	1588,
	1248,
	1589,
	1590,
	1591,
	1591,
	1592,
	1593,
	1590,
	23,
	4,
	135,
	133,
	23,
	49,
	1,
	0,
	0,
	1594,
	1200,
	1,
	23,
	-1,
	-1,
	-1,
	-1,
	122,
	340,
	135,
	133,
	170,
	131,
	94,
	1595,
	1596,
	1597,
	1598,
	46,
	5,
	5,
	1599,
	46,
	218,
	1200,
	131,
	21,
	545,
	588,
	209,
	1600,
	111,
	131,
	131,
	122,
	217,
	122,
	133,
	3,
	3,
	581,
	122,
	3,
	133,
	49,
	581,
	118,
	3,
	3,
	118,
	119,
	134,
	1601,
	156,
	97,
	1602,
	1603,
	1603,
	1603,
	1470,
	1603,
	242,
	242,
	235,
	235,
	97,
	1604,
	242,
	97,
	97,
	1604,
	97,
	1604,
	97,
	97,
	97,
	97,
	1605,
	3,
	3,
	95,
	3,
	340,
	21,
	17,
	17,
	21,
	1314,
	49,
	94,
	3,
	135,
	340,
	97,
	97,
	97,
	97,
	97,
	1604,
	235,
	1606,
	1607,
	1608,
	23,
	23,
	4,
	23,
	23,
	23,
	3,
	172,
	10,
	10,
	10,
	14,
	14,
	26,
	697,
	307,
	697,
	307,
	697,
	307,
	697,
	307,
	697,
	307,
	697,
	307,
	697,
	307,
	697,
	307,
	697,
	307,
	23,
	23,
	172,
	14,
	14,
	14,
	14,
	1137,
	1137,
	697,
	697,
	172,
	1609,
	10,
	10,
	10,
	10,
	10,
	142,
	14,
	15,
	537,
	692,
	1610,
	3,
	4,
	23,
	23,
	27,
	32,
	23,
	23,
	417,
	26,
	23,
	32,
	114,
	417,
	14,
	14,
	1611,
	23,
	23,
	23,
	14,
	28,
	3,
	172,
	10,
	34,
	172,
	142,
	14,
	14,
	14,
	337,
	10,
	23,
	14,
	26,
	114,
	1587,
	23,
	34,
	34,
	26,
	-1,
	-1,
	32,
	23,
	114,
	14,
	23,
	14,
	3,
	23,
	26,
	3,
	23,
	26,
	3,
	23,
	26,
	23,
	582,
	23,
	582,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	23,
	26,
	23,
	26,
	23,
	23,
	26,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
};
static const Il2CppTokenRangePair s_rgctxIndices[8] = 
{
	{ 0x02000012, { 10, 4 } },
	{ 0x02000044, { 14, 4 } },
	{ 0x06000004, { 0, 1 } },
	{ 0x06000005, { 1, 2 } },
	{ 0x06000006, { 3, 2 } },
	{ 0x06000007, { 5, 1 } },
	{ 0x06000008, { 6, 2 } },
	{ 0x06000009, { 8, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[18] = 
{
	{ (Il2CppRGCTXDataType)3, 10483 },
	{ (Il2CppRGCTXDataType)2, 15320 },
	{ (Il2CppRGCTXDataType)3, 10484 },
	{ (Il2CppRGCTXDataType)2, 15321 },
	{ (Il2CppRGCTXDataType)3, 10485 },
	{ (Il2CppRGCTXDataType)3, 10486 },
	{ (Il2CppRGCTXDataType)2, 15323 },
	{ (Il2CppRGCTXDataType)3, 10487 },
	{ (Il2CppRGCTXDataType)2, 15324 },
	{ (Il2CppRGCTXDataType)3, 10488 },
	{ (Il2CppRGCTXDataType)2, 15325 },
	{ (Il2CppRGCTXDataType)1, 14904 },
	{ (Il2CppRGCTXDataType)2, 14904 },
	{ (Il2CppRGCTXDataType)3, 10489 },
	{ (Il2CppRGCTXDataType)2, 15326 },
	{ (Il2CppRGCTXDataType)2, 15082 },
	{ (Il2CppRGCTXDataType)1, 15082 },
	{ (Il2CppRGCTXDataType)3, 10490 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	684,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	8,
	s_rgctxIndices,
	18,
	s_rgctxValues,
	NULL,
};
