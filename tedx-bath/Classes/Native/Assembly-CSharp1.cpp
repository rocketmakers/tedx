﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct VirtFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};

// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.Text.DecoderFallback
struct DecoderFallback_t128445EB7676870485230893338EF044F6B72F60;
// System.Text.EncoderFallback
struct EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Threading.Semaphore
struct Semaphore_tA08EE5E666C7F069659F9559FC31B2E09E4517EA;
// System.Threading.Thread
struct Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0;
// UnityEngine.Collider
struct Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GlobalJavaObjectRef
struct GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// maxstAR.AbstractARManager
struct AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3;
// maxstAR.AbstractObjectTrackableBehaviour
struct AbstractObjectTrackableBehaviour_tB8F488F1D2ED54CBEA10F2124C4FC6C0D245D597;
// maxstAR.AbstractQrCodeTrackableBehaviour
struct AbstractQrCodeTrackableBehaviour_t6D50B1BFDE5EE1CB0914634E8CD8B7C61AB87AAB;
// maxstAR.CloudRecognitionController
struct CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033;
// maxstAR.GuideInfo
struct GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5;
// maxstAR.NativeAPI
struct NativeAPI_t0BA3114B7A4135C0933B0BE26E867AF74342D4BD;
// maxstAR.ObjectTrackableBehaviour
struct ObjectTrackableBehaviour_t2137BE450300155EE7BD6A66AC50B25B5511C2A0;
// maxstAR.Point3Df
struct Point3Df_t6B4A646639DE7A0C0BB07B29BE49F30B45433B94;
// maxstAR.QrCodeTrackableBehaviour
struct QrCodeTrackableBehaviour_tC5612CD43E2F7043C9E80D17C92CBF696CED617E;
// maxstAR.SensorDevice
struct SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA;
// maxstAR.SurfaceThumbnail
struct SurfaceThumbnail_t101EA84FE2D39AF996B5F5F8E89E7DE3638DC824;
// maxstAR.TagAnchor
struct TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50;
// maxstAR.TagAnchor[]
struct TagAnchorU5BU5D_t30DEC96FE86AABE23D09DAD4BDC6F24829E016AB;
// maxstAR.TagAnchors
struct TagAnchors_t475197F1A136EB47B1E2A2C92D7F1A1C2091311E;
// maxstAR.Trackable
struct Trackable_t525A99BA508C48FC5286F43525D4C12CB6CD1D27;
// maxstAR.TrackedImage
struct TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1;
// maxstAR.TrackerManager
struct TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5;
// maxstAR.TrackingResult
struct TrackingResult_t3B01C878A45EA0F96D8A10870D925EA0290F7E07;
// maxstAR.TrackingState
struct TrackingState_t1B55706E8B57210198976B3EC273F3ABAB00D365;
// maxstAR.WearableCalibration
struct WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D;
// maxstAR.WearableDeviceController
struct WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92;
// maxstAR.WearableManager
struct WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4;

IL2CPP_EXTERN_C RuntimeClass* AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Boolean_tB53F6830F670160873277339AA58F15CAED4399C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MaxstSingleton_1_t0187F7FDC19F311CF5D000320AA39A295136CFDB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SurfaceThumbnail_t101EA84FE2D39AF996B5F5F8E89E7DE3638DC824_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Trackable_t525A99BA508C48FC5286F43525D4C12CB6CD1D27_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TrackingResult_t3B01C878A45EA0F96D8A10870D925EA0290F7E07_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TrackingState_t1B55706E8B57210198976B3EC273F3ABAB00D365_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral129C423EB1EC8D09ABFD7D302E0CFF501BD642F5;
IL2CPP_EXTERN_C String_t* _stringLiteral2683ECBB7FBD84854A0C8F9B797D67FBE8535C56;
IL2CPP_EXTERN_C String_t* _stringLiteral320A422F13763FA7F9060ACA1310CF89A728DA71;
IL2CPP_EXTERN_C String_t* _stringLiteral32EAF9220C696B2C222E361F16ACA8FB08EC6083;
IL2CPP_EXTERN_C String_t* _stringLiteral3D7E79A85549F13CEB3CAA70B10CC3069177FB56;
IL2CPP_EXTERN_C String_t* _stringLiteral3ED58087C40E5F59ED8E8077F55D6D2738AED341;
IL2CPP_EXTERN_C String_t* _stringLiteral40804CE792BB4A6BA242156DAD17CBE6C9731E14;
IL2CPP_EXTERN_C String_t* _stringLiteral45C8223002A464F0E383D093C9D897062EE7C828;
IL2CPP_EXTERN_C String_t* _stringLiteral5F378B3813B277B3B57245C13A898B720C7FBFC1;
IL2CPP_EXTERN_C String_t* _stringLiteral69284A621D6E033E1C75AE520AB9A8F48A54609D;
IL2CPP_EXTERN_C String_t* _stringLiteral70E6EC9A382CC19D47F282CAD67350FA744B646D;
IL2CPP_EXTERN_C String_t* _stringLiteral782BCC6135CB96896F3B2086E7FA556CCBC0EC32;
IL2CPP_EXTERN_C String_t* _stringLiteralA004F7A71A664C60394CBEBEC2BC71B669D9F986;
IL2CPP_EXTERN_C String_t* _stringLiteralBDFA84592F3B00EDB1C3CB86DFD31858EC56A047;
IL2CPP_EXTERN_C String_t* _stringLiteralC434B6DED53825899F206B76DF0CEEA4304A2699;
IL2CPP_EXTERN_C String_t* _stringLiteralC4BF429D7BCF859C0958FCDF0E6DFD90A59FC4A6;
IL2CPP_EXTERN_C String_t* _stringLiteralC625B4E6C60DE430D2743BCDAC612D51309A4C94;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralFC5BC92101C3C283A92BB8C14DF6A1845CD34B65;
IL2CPP_EXTERN_C String_t* _stringLiteralFF66FF0BB5DF6B8CD0C3A592D5AEC5E39939C6EF;
IL2CPP_EXTERN_C const RuntimeMethod* AndroidJavaObject_CallStatic_TisAndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D_m6CAE75FB51C5A02521C239A7232735573C51EAE7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AndroidJavaObject_Call_TisBoolean_tB53F6830F670160873277339AA58F15CAED4399C_m57EE1ACB271D15DD0E2DDD6B28805C31799A0976_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AndroidJavaObject_Call_TisString_t_m5EAE53C9E2A8893FD8FEA710378D22C162A0FDEA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AndroidJavaObject_GetStatic_TisAndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D_m944E5987B38F1229EB3E8122E030D0439D4513B0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_mB090F51A34716700C0F4F1B08F9330C6F503DB9E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_mA906598D1F38C24AC5F6191124D6EE52D3F036D5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_m43A34BC668D3320EE3C92BC1BF613857DEA0154F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m7DF3B4DB4CA60855860B6760DBA2B1AE5883D20F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_mCF3938D33A4B2D1D9B65321455F867660E72C3FD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MaxstSingleton_1_get_Instance_m1346F6D4BE1CE718B2CD992D56A6E6FB8900A341_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t ObjectTrackableBehaviour_OnTrackFail_mCBCC54510C6305A4A61CFA092F6B3AC0F0B8AF0A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ObjectTrackableBehaviour_OnTrackSuccess_m85E36967C60137B76E4BE3DDEE610A222D48EFC0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t QrCodeTrackableBehaviour_OnTrackFail_m9F348B8961826820969AB9E16B5EE7588B841599_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t QrCodeTrackableBehaviour_OnTrackSuccess_mA052BB30766CF011D64024713C64605925B98F63_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t QrCodeTrackableBehaviour__ctor_mF04F26B6F2853C13C716A9A6134558BDF0BA07BD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SensorDevice_GetInstance_m7F13A44D48036B73D11E9CD55A9EA7ABAE1D430F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SurfaceThumbnail_GetData_mB4539CA663D4B080D677C046FFBBF33A03F3B428_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Trackable_GetCloudMeta_m6B439B693DA1C113940981E0A27CABEAB5D90152_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Trackable_GetCloudName_m591936B50E988289EBA0BDF07AB98935099EF404_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Trackable_GetId_mA44F5BE3181F6C793190AF897E101D04DB40BCA9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Trackable_GetName_mD9121445DC81017509E298724A29BF043A619D8D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Trackable_GetPose_m755CCDD0E4B0A606A6C5DDEB16F1C3775030FF25_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Trackable__ctor_m28195F5B5619EBC04AF14B18F8D5773F9A37B195_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackedImage_GetDataPtr_mD5C6FA76DFF8AADD046F0E08EEFDE4D478B1719C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackedImage_GetData_mAC39DBDDAF5858CC99748102BC56B13BB2DA3724_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackerManager_DestroyTracker_mF416B8798B95E8E4309A55AFEA7C2E437DA16DC1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackerManager_GetGuideInfo_m4ABE739552D344AB9AD9695F0ECD15F9213CCF57_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackerManager_GetInstance_m46A27E965CA5109148150899D4CD2B26CA6F53E6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackerManager_GetWorldPositionFromScreenCoordinate_m15F077BC5892CDE906BA94FFFE6C687E70413187_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackerManager_InitializeCloud_m16BEED93FCF7207B81027A789BC393248ECB615A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackerManager_SaveSurfaceData_m3C2BFE33F6C0CAB1814551F4276AC70D9DB09CFA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackerManager_SetTrackingOption_m5AD2D75526B7CC533691AD083604EF3469B76538_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackerManager_StartTracker_mEDE0B41014FC73A9DDD6F2FCB60BB548EDD69088_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackerManager_StopTracker_m8A7C1E8BA74DA0EF19A7A822B17DCEF17420C165_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackerManager_UpdateTrackingState_mDD67C1785159119C1897E1088BEEB38D0596067C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackerManager__ctor_m170A81D4AD8E76A78178882F1CC4CD2C7959D3A5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackingResult_GetTrackable_mBAF43C221FEBE35839E049B9A727C47C265DE128_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackingState_GetCodeScanResult_m2F19F1EC66C4563183E8784AE2ED355F28EBD7C0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackingState_GetImage_m6B379D48A3D7E6D0C46970D92317C1BE3D38F4C8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackingState_GetImage_mD1C946E7955D0607CA070ABCB7164388D35F3627_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackingState_GetTrackingResult_mD0779CB0C659620D558136BE5FCD7EA090ADB413_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TrackingState_GetTrackingStatus_m494F6BEA0863C65070ED186FC320FF5EB59C2A45_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WearableCalibration_CreateWearableEye_m0614E4FF2FF1B484383A0A15B950BFEAB7D33EE7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WearableCalibration_Deinit_m0B548B265954A3EF9F5D3B6DED4F63F4F8D01EDB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WearableCalibration_GetProjectionMatrix_mDDCBE1373398DEA571D8B1A9DB3DA9758992F1DC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WearableCalibration_GetViewport_mF25EA372885DF1EE51D921638F3311725335B469_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WearableCalibration_Init_mD794728749B60144E5CA54A448995426AF9EDD25_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WearableDeviceController_GetModelName_m99A1302B5AAF1D4DE11EEBA587262B6C15025C99_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WearableDeviceController_Init_m3E8465A7C1727F11B7FD53F0779105F88E964D61_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WearableDeviceController_IsSideBySideType_mCC4857F2F8C11AB9ACE7151DFE2A9A270DD70841_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WearableDeviceController_IsStereoEnabled_m2DEF96147706CF9842E0B7C328799D95D475A5D3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WearableDeviceController_IsSupportedWearableDevice_m707E8F7754330646B9D1E3C887E4473D1638D3DC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WearableDeviceController_SetStereoMode_m2ECFA9DC6D80551B52000B8456E57C8DC544BCF5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WearableManager_GetInstance_m55F007C74CC1B329AE9A8AE3C05715E7CD20BE83_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WearableManager__ctor_m797FE745D17CE179C55D14E0322372200010A04A_MetadataUsageId;

struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
struct ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252;
struct RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.EmptyArray`1<System.Object>
struct  EmptyArray_1_tCF137C88A5824F413EFB5A2F31664D8207E61D26  : public RuntimeObject
{
public:

public:
};

struct EmptyArray_1_tCF137C88A5824F413EFB5A2F31664D8207E61D26_StaticFields
{
public:
	// T[] System.EmptyArray`1::Value
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___Value_0;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(EmptyArray_1_tCF137C88A5824F413EFB5A2F31664D8207E61D26_StaticFields, ___Value_0)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_Value_0() const { return ___Value_0; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_0), (void*)value);
	}
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.Text.Encoding
struct  Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_9;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB * ___dataItem_10;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_11;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_12;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * ___encoderFallback_13;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_t128445EB7676870485230893338EF044F6B72F60 * ___decoderFallback_14;

public:
	inline static int32_t get_offset_of_m_codePage_9() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___m_codePage_9)); }
	inline int32_t get_m_codePage_9() const { return ___m_codePage_9; }
	inline int32_t* get_address_of_m_codePage_9() { return &___m_codePage_9; }
	inline void set_m_codePage_9(int32_t value)
	{
		___m_codePage_9 = value;
	}

	inline static int32_t get_offset_of_dataItem_10() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___dataItem_10)); }
	inline CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB * get_dataItem_10() const { return ___dataItem_10; }
	inline CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB ** get_address_of_dataItem_10() { return &___dataItem_10; }
	inline void set_dataItem_10(CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB * value)
	{
		___dataItem_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dataItem_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_deserializedFromEverett_11() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___m_deserializedFromEverett_11)); }
	inline bool get_m_deserializedFromEverett_11() const { return ___m_deserializedFromEverett_11; }
	inline bool* get_address_of_m_deserializedFromEverett_11() { return &___m_deserializedFromEverett_11; }
	inline void set_m_deserializedFromEverett_11(bool value)
	{
		___m_deserializedFromEverett_11 = value;
	}

	inline static int32_t get_offset_of_m_isReadOnly_12() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___m_isReadOnly_12)); }
	inline bool get_m_isReadOnly_12() const { return ___m_isReadOnly_12; }
	inline bool* get_address_of_m_isReadOnly_12() { return &___m_isReadOnly_12; }
	inline void set_m_isReadOnly_12(bool value)
	{
		___m_isReadOnly_12 = value;
	}

	inline static int32_t get_offset_of_encoderFallback_13() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___encoderFallback_13)); }
	inline EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * get_encoderFallback_13() const { return ___encoderFallback_13; }
	inline EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 ** get_address_of_encoderFallback_13() { return &___encoderFallback_13; }
	inline void set_encoderFallback_13(EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * value)
	{
		___encoderFallback_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___encoderFallback_13), (void*)value);
	}

	inline static int32_t get_offset_of_decoderFallback_14() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___decoderFallback_14)); }
	inline DecoderFallback_t128445EB7676870485230893338EF044F6B72F60 * get_decoderFallback_14() const { return ___decoderFallback_14; }
	inline DecoderFallback_t128445EB7676870485230893338EF044F6B72F60 ** get_address_of_decoderFallback_14() { return &___decoderFallback_14; }
	inline void set_decoderFallback_14(DecoderFallback_t128445EB7676870485230893338EF044F6B72F60 * value)
	{
		___decoderFallback_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___decoderFallback_14), (void*)value);
	}
};

struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields
{
public:
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___latin1Encoding_7;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_15;

public:
	inline static int32_t get_offset_of_defaultEncoding_0() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___defaultEncoding_0)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_defaultEncoding_0() const { return ___defaultEncoding_0; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_defaultEncoding_0() { return &___defaultEncoding_0; }
	inline void set_defaultEncoding_0(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___defaultEncoding_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultEncoding_0), (void*)value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_1() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___unicodeEncoding_1)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_unicodeEncoding_1() const { return ___unicodeEncoding_1; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_unicodeEncoding_1() { return &___unicodeEncoding_1; }
	inline void set_unicodeEncoding_1(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___unicodeEncoding_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___unicodeEncoding_1), (void*)value);
	}

	inline static int32_t get_offset_of_bigEndianUnicode_2() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___bigEndianUnicode_2)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_bigEndianUnicode_2() const { return ___bigEndianUnicode_2; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_bigEndianUnicode_2() { return &___bigEndianUnicode_2; }
	inline void set_bigEndianUnicode_2(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___bigEndianUnicode_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bigEndianUnicode_2), (void*)value);
	}

	inline static int32_t get_offset_of_utf7Encoding_3() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___utf7Encoding_3)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_utf7Encoding_3() const { return ___utf7Encoding_3; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_utf7Encoding_3() { return &___utf7Encoding_3; }
	inline void set_utf7Encoding_3(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___utf7Encoding_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf7Encoding_3), (void*)value);
	}

	inline static int32_t get_offset_of_utf8Encoding_4() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___utf8Encoding_4)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_utf8Encoding_4() const { return ___utf8Encoding_4; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_utf8Encoding_4() { return &___utf8Encoding_4; }
	inline void set_utf8Encoding_4(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___utf8Encoding_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf8Encoding_4), (void*)value);
	}

	inline static int32_t get_offset_of_utf32Encoding_5() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___utf32Encoding_5)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_utf32Encoding_5() const { return ___utf32Encoding_5; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_utf32Encoding_5() { return &___utf32Encoding_5; }
	inline void set_utf32Encoding_5(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___utf32Encoding_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf32Encoding_5), (void*)value);
	}

	inline static int32_t get_offset_of_asciiEncoding_6() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___asciiEncoding_6)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_asciiEncoding_6() const { return ___asciiEncoding_6; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_asciiEncoding_6() { return &___asciiEncoding_6; }
	inline void set_asciiEncoding_6(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___asciiEncoding_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___asciiEncoding_6), (void*)value);
	}

	inline static int32_t get_offset_of_latin1Encoding_7() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___latin1Encoding_7)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_latin1Encoding_7() const { return ___latin1Encoding_7; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_latin1Encoding_7() { return &___latin1Encoding_7; }
	inline void set_latin1Encoding_7(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___latin1Encoding_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___latin1Encoding_7), (void*)value);
	}

	inline static int32_t get_offset_of_encodings_8() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___encodings_8)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_encodings_8() const { return ___encodings_8; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_encodings_8() { return &___encodings_8; }
	inline void set_encodings_8(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___encodings_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___encodings_8), (void*)value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_15() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___s_InternalSyncObject_15)); }
	inline RuntimeObject * get_s_InternalSyncObject_15() const { return ___s_InternalSyncObject_15; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_15() { return &___s_InternalSyncObject_15; }
	inline void set_s_InternalSyncObject_15(RuntimeObject * value)
	{
		___s_InternalSyncObject_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InternalSyncObject_15), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UnityEngine.AndroidJavaObject
struct  AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D  : public RuntimeObject
{
public:
	// UnityEngine.GlobalJavaObjectRef UnityEngine.AndroidJavaObject::m_jobject
	GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 * ___m_jobject_1;
	// UnityEngine.GlobalJavaObjectRef UnityEngine.AndroidJavaObject::m_jclass
	GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 * ___m_jclass_2;

public:
	inline static int32_t get_offset_of_m_jobject_1() { return static_cast<int32_t>(offsetof(AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D, ___m_jobject_1)); }
	inline GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 * get_m_jobject_1() const { return ___m_jobject_1; }
	inline GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 ** get_address_of_m_jobject_1() { return &___m_jobject_1; }
	inline void set_m_jobject_1(GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 * value)
	{
		___m_jobject_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_jobject_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_jclass_2() { return static_cast<int32_t>(offsetof(AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D, ___m_jclass_2)); }
	inline GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 * get_m_jclass_2() const { return ___m_jclass_2; }
	inline GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 ** get_address_of_m_jclass_2() { return &___m_jclass_2; }
	inline void set_m_jclass_2(GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 * value)
	{
		___m_jclass_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_jclass_2), (void*)value);
	}
};

struct AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D_StaticFields
{
public:
	// System.Boolean UnityEngine.AndroidJavaObject::enableDebugPrints
	bool ___enableDebugPrints_0;

public:
	inline static int32_t get_offset_of_enableDebugPrints_0() { return static_cast<int32_t>(offsetof(AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D_StaticFields, ___enableDebugPrints_0)); }
	inline bool get_enableDebugPrints_0() const { return ___enableDebugPrints_0; }
	inline bool* get_address_of_enableDebugPrints_0() { return &___enableDebugPrints_0; }
	inline void set_enableDebugPrints_0(bool value)
	{
		___enableDebugPrints_0 = value;
	}
};


// maxstAR.GuideInfo
struct  GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5  : public RuntimeObject
{
public:
	// System.Single maxstAR.GuideInfo::progress
	float ___progress_1;
	// System.Int32 maxstAR.GuideInfo::keyframeCount
	int32_t ___keyframeCount_2;
	// System.Int32 maxstAR.GuideInfo::featureCount
	int32_t ___featureCount_3;
	// System.Int32 maxstAR.GuideInfo::tagAnchorsLength
	int32_t ___tagAnchorsLength_4;
	// System.Byte[] maxstAR.GuideInfo::tagAnchorBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___tagAnchorBuffer_6;
	// maxstAR.TagAnchor[] maxstAR.GuideInfo::tagAnchors
	TagAnchorU5BU5D_t30DEC96FE86AABE23D09DAD4BDC6F24829E016AB* ___tagAnchors_7;

public:
	inline static int32_t get_offset_of_progress_1() { return static_cast<int32_t>(offsetof(GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5, ___progress_1)); }
	inline float get_progress_1() const { return ___progress_1; }
	inline float* get_address_of_progress_1() { return &___progress_1; }
	inline void set_progress_1(float value)
	{
		___progress_1 = value;
	}

	inline static int32_t get_offset_of_keyframeCount_2() { return static_cast<int32_t>(offsetof(GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5, ___keyframeCount_2)); }
	inline int32_t get_keyframeCount_2() const { return ___keyframeCount_2; }
	inline int32_t* get_address_of_keyframeCount_2() { return &___keyframeCount_2; }
	inline void set_keyframeCount_2(int32_t value)
	{
		___keyframeCount_2 = value;
	}

	inline static int32_t get_offset_of_featureCount_3() { return static_cast<int32_t>(offsetof(GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5, ___featureCount_3)); }
	inline int32_t get_featureCount_3() const { return ___featureCount_3; }
	inline int32_t* get_address_of_featureCount_3() { return &___featureCount_3; }
	inline void set_featureCount_3(int32_t value)
	{
		___featureCount_3 = value;
	}

	inline static int32_t get_offset_of_tagAnchorsLength_4() { return static_cast<int32_t>(offsetof(GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5, ___tagAnchorsLength_4)); }
	inline int32_t get_tagAnchorsLength_4() const { return ___tagAnchorsLength_4; }
	inline int32_t* get_address_of_tagAnchorsLength_4() { return &___tagAnchorsLength_4; }
	inline void set_tagAnchorsLength_4(int32_t value)
	{
		___tagAnchorsLength_4 = value;
	}

	inline static int32_t get_offset_of_tagAnchorBuffer_6() { return static_cast<int32_t>(offsetof(GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5, ___tagAnchorBuffer_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_tagAnchorBuffer_6() const { return ___tagAnchorBuffer_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_tagAnchorBuffer_6() { return &___tagAnchorBuffer_6; }
	inline void set_tagAnchorBuffer_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___tagAnchorBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tagAnchorBuffer_6), (void*)value);
	}

	inline static int32_t get_offset_of_tagAnchors_7() { return static_cast<int32_t>(offsetof(GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5, ___tagAnchors_7)); }
	inline TagAnchorU5BU5D_t30DEC96FE86AABE23D09DAD4BDC6F24829E016AB* get_tagAnchors_7() const { return ___tagAnchors_7; }
	inline TagAnchorU5BU5D_t30DEC96FE86AABE23D09DAD4BDC6F24829E016AB** get_address_of_tagAnchors_7() { return &___tagAnchors_7; }
	inline void set_tagAnchors_7(TagAnchorU5BU5D_t30DEC96FE86AABE23D09DAD4BDC6F24829E016AB* value)
	{
		___tagAnchors_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tagAnchors_7), (void*)value);
	}
};

struct GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5_StaticFields
{
public:
	// System.Single[] maxstAR.GuideInfo::featureBuffer
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___featureBuffer_5;

public:
	inline static int32_t get_offset_of_featureBuffer_5() { return static_cast<int32_t>(offsetof(GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5_StaticFields, ___featureBuffer_5)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_featureBuffer_5() const { return ___featureBuffer_5; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_featureBuffer_5() { return &___featureBuffer_5; }
	inline void set_featureBuffer_5(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___featureBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___featureBuffer_5), (void*)value);
	}
};


// maxstAR.NativeAPI
struct  NativeAPI_t0BA3114B7A4135C0933B0BE26E867AF74342D4BD  : public RuntimeObject
{
public:

public:
};


// maxstAR.Point3Df
struct  Point3Df_t6B4A646639DE7A0C0BB07B29BE49F30B45433B94  : public RuntimeObject
{
public:
	// System.Single maxstAR.Point3Df::x
	float ___x_0;
	// System.Single maxstAR.Point3Df::y
	float ___y_1;
	// System.Single maxstAR.Point3Df::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Point3Df_t6B4A646639DE7A0C0BB07B29BE49F30B45433B94, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Point3Df_t6B4A646639DE7A0C0BB07B29BE49F30B45433B94, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Point3Df_t6B4A646639DE7A0C0BB07B29BE49F30B45433B94, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};


// maxstAR.SensorDevice
struct  SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA  : public RuntimeObject
{
public:

public:
};

struct SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA_StaticFields
{
public:
	// maxstAR.SensorDevice maxstAR.SensorDevice::instance
	SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA * ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA_StaticFields, ___instance_0)); }
	inline SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA * get_instance_0() const { return ___instance_0; }
	inline SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_0), (void*)value);
	}
};


// maxstAR.SurfaceThumbnail
struct  SurfaceThumbnail_t101EA84FE2D39AF996B5F5F8E89E7DE3638DC824  : public RuntimeObject
{
public:
	// System.UInt64 maxstAR.SurfaceThumbnail::cPtr
	uint64_t ___cPtr_0;

public:
	inline static int32_t get_offset_of_cPtr_0() { return static_cast<int32_t>(offsetof(SurfaceThumbnail_t101EA84FE2D39AF996B5F5F8E89E7DE3638DC824, ___cPtr_0)); }
	inline uint64_t get_cPtr_0() const { return ___cPtr_0; }
	inline uint64_t* get_address_of_cPtr_0() { return &___cPtr_0; }
	inline void set_cPtr_0(uint64_t value)
	{
		___cPtr_0 = value;
	}
};


// maxstAR.TagAnchor
struct  TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50  : public RuntimeObject
{
public:
	// System.String maxstAR.TagAnchor::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Single maxstAR.TagAnchor::<positionX>k__BackingField
	float ___U3CpositionXU3Ek__BackingField_1;
	// System.Single maxstAR.TagAnchor::<positionY>k__BackingField
	float ___U3CpositionYU3Ek__BackingField_2;
	// System.Single maxstAR.TagAnchor::<positionZ>k__BackingField
	float ___U3CpositionZU3Ek__BackingField_3;
	// System.Single maxstAR.TagAnchor::<rotationX>k__BackingField
	float ___U3CrotationXU3Ek__BackingField_4;
	// System.Single maxstAR.TagAnchor::<rotationY>k__BackingField
	float ___U3CrotationYU3Ek__BackingField_5;
	// System.Single maxstAR.TagAnchor::<rotationZ>k__BackingField
	float ___U3CrotationZU3Ek__BackingField_6;
	// System.Single maxstAR.TagAnchor::<scaleX>k__BackingField
	float ___U3CscaleXU3Ek__BackingField_7;
	// System.Single maxstAR.TagAnchor::<scaleY>k__BackingField
	float ___U3CscaleYU3Ek__BackingField_8;
	// System.Single maxstAR.TagAnchor::<scaleZ>k__BackingField
	float ___U3CscaleZU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpositionXU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50, ___U3CpositionXU3Ek__BackingField_1)); }
	inline float get_U3CpositionXU3Ek__BackingField_1() const { return ___U3CpositionXU3Ek__BackingField_1; }
	inline float* get_address_of_U3CpositionXU3Ek__BackingField_1() { return &___U3CpositionXU3Ek__BackingField_1; }
	inline void set_U3CpositionXU3Ek__BackingField_1(float value)
	{
		___U3CpositionXU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CpositionYU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50, ___U3CpositionYU3Ek__BackingField_2)); }
	inline float get_U3CpositionYU3Ek__BackingField_2() const { return ___U3CpositionYU3Ek__BackingField_2; }
	inline float* get_address_of_U3CpositionYU3Ek__BackingField_2() { return &___U3CpositionYU3Ek__BackingField_2; }
	inline void set_U3CpositionYU3Ek__BackingField_2(float value)
	{
		___U3CpositionYU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CpositionZU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50, ___U3CpositionZU3Ek__BackingField_3)); }
	inline float get_U3CpositionZU3Ek__BackingField_3() const { return ___U3CpositionZU3Ek__BackingField_3; }
	inline float* get_address_of_U3CpositionZU3Ek__BackingField_3() { return &___U3CpositionZU3Ek__BackingField_3; }
	inline void set_U3CpositionZU3Ek__BackingField_3(float value)
	{
		___U3CpositionZU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CrotationXU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50, ___U3CrotationXU3Ek__BackingField_4)); }
	inline float get_U3CrotationXU3Ek__BackingField_4() const { return ___U3CrotationXU3Ek__BackingField_4; }
	inline float* get_address_of_U3CrotationXU3Ek__BackingField_4() { return &___U3CrotationXU3Ek__BackingField_4; }
	inline void set_U3CrotationXU3Ek__BackingField_4(float value)
	{
		___U3CrotationXU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CrotationYU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50, ___U3CrotationYU3Ek__BackingField_5)); }
	inline float get_U3CrotationYU3Ek__BackingField_5() const { return ___U3CrotationYU3Ek__BackingField_5; }
	inline float* get_address_of_U3CrotationYU3Ek__BackingField_5() { return &___U3CrotationYU3Ek__BackingField_5; }
	inline void set_U3CrotationYU3Ek__BackingField_5(float value)
	{
		___U3CrotationYU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CrotationZU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50, ___U3CrotationZU3Ek__BackingField_6)); }
	inline float get_U3CrotationZU3Ek__BackingField_6() const { return ___U3CrotationZU3Ek__BackingField_6; }
	inline float* get_address_of_U3CrotationZU3Ek__BackingField_6() { return &___U3CrotationZU3Ek__BackingField_6; }
	inline void set_U3CrotationZU3Ek__BackingField_6(float value)
	{
		___U3CrotationZU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CscaleXU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50, ___U3CscaleXU3Ek__BackingField_7)); }
	inline float get_U3CscaleXU3Ek__BackingField_7() const { return ___U3CscaleXU3Ek__BackingField_7; }
	inline float* get_address_of_U3CscaleXU3Ek__BackingField_7() { return &___U3CscaleXU3Ek__BackingField_7; }
	inline void set_U3CscaleXU3Ek__BackingField_7(float value)
	{
		___U3CscaleXU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CscaleYU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50, ___U3CscaleYU3Ek__BackingField_8)); }
	inline float get_U3CscaleYU3Ek__BackingField_8() const { return ___U3CscaleYU3Ek__BackingField_8; }
	inline float* get_address_of_U3CscaleYU3Ek__BackingField_8() { return &___U3CscaleYU3Ek__BackingField_8; }
	inline void set_U3CscaleYU3Ek__BackingField_8(float value)
	{
		___U3CscaleYU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CscaleZU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50, ___U3CscaleZU3Ek__BackingField_9)); }
	inline float get_U3CscaleZU3Ek__BackingField_9() const { return ___U3CscaleZU3Ek__BackingField_9; }
	inline float* get_address_of_U3CscaleZU3Ek__BackingField_9() { return &___U3CscaleZU3Ek__BackingField_9; }
	inline void set_U3CscaleZU3Ek__BackingField_9(float value)
	{
		___U3CscaleZU3Ek__BackingField_9 = value;
	}
};


// maxstAR.TagAnchors
struct  TagAnchors_t475197F1A136EB47B1E2A2C92D7F1A1C2091311E  : public RuntimeObject
{
public:
	// maxstAR.TagAnchor[] maxstAR.TagAnchors::tagAnchors
	TagAnchorU5BU5D_t30DEC96FE86AABE23D09DAD4BDC6F24829E016AB* ___tagAnchors_0;

public:
	inline static int32_t get_offset_of_tagAnchors_0() { return static_cast<int32_t>(offsetof(TagAnchors_t475197F1A136EB47B1E2A2C92D7F1A1C2091311E, ___tagAnchors_0)); }
	inline TagAnchorU5BU5D_t30DEC96FE86AABE23D09DAD4BDC6F24829E016AB* get_tagAnchors_0() const { return ___tagAnchors_0; }
	inline TagAnchorU5BU5D_t30DEC96FE86AABE23D09DAD4BDC6F24829E016AB** get_address_of_tagAnchors_0() { return &___tagAnchors_0; }
	inline void set_tagAnchors_0(TagAnchorU5BU5D_t30DEC96FE86AABE23D09DAD4BDC6F24829E016AB* value)
	{
		___tagAnchors_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tagAnchors_0), (void*)value);
	}
};


// maxstAR.Trackable
struct  Trackable_t525A99BA508C48FC5286F43525D4C12CB6CD1D27  : public RuntimeObject
{
public:
	// System.UInt64 maxstAR.Trackable::cPtr
	uint64_t ___cPtr_0;
	// System.Single[] maxstAR.Trackable::glPoseMatrix
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___glPoseMatrix_1;
	// System.Byte[] maxstAR.Trackable::idBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___idBytes_2;
	// System.Byte[] maxstAR.Trackable::nameBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___nameBytes_3;
	// System.Byte[] maxstAR.Trackable::cloudNameBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___cloudNameBytes_4;
	// System.Byte[] maxstAR.Trackable::cloudMetaBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___cloudMetaBytes_5;

public:
	inline static int32_t get_offset_of_cPtr_0() { return static_cast<int32_t>(offsetof(Trackable_t525A99BA508C48FC5286F43525D4C12CB6CD1D27, ___cPtr_0)); }
	inline uint64_t get_cPtr_0() const { return ___cPtr_0; }
	inline uint64_t* get_address_of_cPtr_0() { return &___cPtr_0; }
	inline void set_cPtr_0(uint64_t value)
	{
		___cPtr_0 = value;
	}

	inline static int32_t get_offset_of_glPoseMatrix_1() { return static_cast<int32_t>(offsetof(Trackable_t525A99BA508C48FC5286F43525D4C12CB6CD1D27, ___glPoseMatrix_1)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_glPoseMatrix_1() const { return ___glPoseMatrix_1; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_glPoseMatrix_1() { return &___glPoseMatrix_1; }
	inline void set_glPoseMatrix_1(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___glPoseMatrix_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___glPoseMatrix_1), (void*)value);
	}

	inline static int32_t get_offset_of_idBytes_2() { return static_cast<int32_t>(offsetof(Trackable_t525A99BA508C48FC5286F43525D4C12CB6CD1D27, ___idBytes_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_idBytes_2() const { return ___idBytes_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_idBytes_2() { return &___idBytes_2; }
	inline void set_idBytes_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___idBytes_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___idBytes_2), (void*)value);
	}

	inline static int32_t get_offset_of_nameBytes_3() { return static_cast<int32_t>(offsetof(Trackable_t525A99BA508C48FC5286F43525D4C12CB6CD1D27, ___nameBytes_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_nameBytes_3() const { return ___nameBytes_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_nameBytes_3() { return &___nameBytes_3; }
	inline void set_nameBytes_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___nameBytes_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nameBytes_3), (void*)value);
	}

	inline static int32_t get_offset_of_cloudNameBytes_4() { return static_cast<int32_t>(offsetof(Trackable_t525A99BA508C48FC5286F43525D4C12CB6CD1D27, ___cloudNameBytes_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_cloudNameBytes_4() const { return ___cloudNameBytes_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_cloudNameBytes_4() { return &___cloudNameBytes_4; }
	inline void set_cloudNameBytes_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___cloudNameBytes_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cloudNameBytes_4), (void*)value);
	}

	inline static int32_t get_offset_of_cloudMetaBytes_5() { return static_cast<int32_t>(offsetof(Trackable_t525A99BA508C48FC5286F43525D4C12CB6CD1D27, ___cloudMetaBytes_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_cloudMetaBytes_5() const { return ___cloudMetaBytes_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_cloudMetaBytes_5() { return &___cloudMetaBytes_5; }
	inline void set_cloudMetaBytes_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___cloudMetaBytes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cloudMetaBytes_5), (void*)value);
	}
};


// maxstAR.TrackerManager
struct  TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5  : public RuntimeObject
{
public:
	// System.Single[] maxstAR.TrackerManager::glPoseMatrix
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___glPoseMatrix_9;
	// maxstAR.TrackingState maxstAR.TrackerManager::trackingState
	TrackingState_t1B55706E8B57210198976B3EC273F3ABAB00D365 * ___trackingState_10;
	// UnityEngine.GameObject maxstAR.TrackerManager::cloudGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___cloudGameObject_11;
	// System.String maxstAR.TrackerManager::secretId
	String_t* ___secretId_12;
	// System.String maxstAR.TrackerManager::secretKey
	String_t* ___secretKey_13;
	// maxstAR.GuideInfo maxstAR.TrackerManager::guideInfo
	GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5 * ___guideInfo_14;
	// maxstAR.CloudRecognitionController maxstAR.TrackerManager::cloudRecognitionController
	CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * ___cloudRecognitionController_15;

public:
	inline static int32_t get_offset_of_glPoseMatrix_9() { return static_cast<int32_t>(offsetof(TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5, ___glPoseMatrix_9)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_glPoseMatrix_9() const { return ___glPoseMatrix_9; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_glPoseMatrix_9() { return &___glPoseMatrix_9; }
	inline void set_glPoseMatrix_9(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___glPoseMatrix_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___glPoseMatrix_9), (void*)value);
	}

	inline static int32_t get_offset_of_trackingState_10() { return static_cast<int32_t>(offsetof(TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5, ___trackingState_10)); }
	inline TrackingState_t1B55706E8B57210198976B3EC273F3ABAB00D365 * get_trackingState_10() const { return ___trackingState_10; }
	inline TrackingState_t1B55706E8B57210198976B3EC273F3ABAB00D365 ** get_address_of_trackingState_10() { return &___trackingState_10; }
	inline void set_trackingState_10(TrackingState_t1B55706E8B57210198976B3EC273F3ABAB00D365 * value)
	{
		___trackingState_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trackingState_10), (void*)value);
	}

	inline static int32_t get_offset_of_cloudGameObject_11() { return static_cast<int32_t>(offsetof(TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5, ___cloudGameObject_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_cloudGameObject_11() const { return ___cloudGameObject_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_cloudGameObject_11() { return &___cloudGameObject_11; }
	inline void set_cloudGameObject_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___cloudGameObject_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cloudGameObject_11), (void*)value);
	}

	inline static int32_t get_offset_of_secretId_12() { return static_cast<int32_t>(offsetof(TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5, ___secretId_12)); }
	inline String_t* get_secretId_12() const { return ___secretId_12; }
	inline String_t** get_address_of_secretId_12() { return &___secretId_12; }
	inline void set_secretId_12(String_t* value)
	{
		___secretId_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___secretId_12), (void*)value);
	}

	inline static int32_t get_offset_of_secretKey_13() { return static_cast<int32_t>(offsetof(TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5, ___secretKey_13)); }
	inline String_t* get_secretKey_13() const { return ___secretKey_13; }
	inline String_t** get_address_of_secretKey_13() { return &___secretKey_13; }
	inline void set_secretKey_13(String_t* value)
	{
		___secretKey_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___secretKey_13), (void*)value);
	}

	inline static int32_t get_offset_of_guideInfo_14() { return static_cast<int32_t>(offsetof(TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5, ___guideInfo_14)); }
	inline GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5 * get_guideInfo_14() const { return ___guideInfo_14; }
	inline GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5 ** get_address_of_guideInfo_14() { return &___guideInfo_14; }
	inline void set_guideInfo_14(GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5 * value)
	{
		___guideInfo_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___guideInfo_14), (void*)value);
	}

	inline static int32_t get_offset_of_cloudRecognitionController_15() { return static_cast<int32_t>(offsetof(TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5, ___cloudRecognitionController_15)); }
	inline CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * get_cloudRecognitionController_15() const { return ___cloudRecognitionController_15; }
	inline CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 ** get_address_of_cloudRecognitionController_15() { return &___cloudRecognitionController_15; }
	inline void set_cloudRecognitionController_15(CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * value)
	{
		___cloudRecognitionController_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cloudRecognitionController_15), (void*)value);
	}
};

struct TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5_StaticFields
{
public:
	// maxstAR.TrackerManager maxstAR.TrackerManager::instance
	TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * ___instance_8;

public:
	inline static int32_t get_offset_of_instance_8() { return static_cast<int32_t>(offsetof(TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5_StaticFields, ___instance_8)); }
	inline TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * get_instance_8() const { return ___instance_8; }
	inline TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 ** get_address_of_instance_8() { return &___instance_8; }
	inline void set_instance_8(TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * value)
	{
		___instance_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_8), (void*)value);
	}
};


// maxstAR.TrackingResult
struct  TrackingResult_t3B01C878A45EA0F96D8A10870D925EA0290F7E07  : public RuntimeObject
{
public:
	// System.UInt64 maxstAR.TrackingResult::cPtr
	uint64_t ___cPtr_0;

public:
	inline static int32_t get_offset_of_cPtr_0() { return static_cast<int32_t>(offsetof(TrackingResult_t3B01C878A45EA0F96D8A10870D925EA0290F7E07, ___cPtr_0)); }
	inline uint64_t get_cPtr_0() const { return ___cPtr_0; }
	inline uint64_t* get_address_of_cPtr_0() { return &___cPtr_0; }
	inline void set_cPtr_0(uint64_t value)
	{
		___cPtr_0 = value;
	}
};


// maxstAR.TrackingState
struct  TrackingState_t1B55706E8B57210198976B3EC273F3ABAB00D365  : public RuntimeObject
{
public:
	// System.UInt64 maxstAR.TrackingState::cPtr
	uint64_t ___cPtr_0;

public:
	inline static int32_t get_offset_of_cPtr_0() { return static_cast<int32_t>(offsetof(TrackingState_t1B55706E8B57210198976B3EC273F3ABAB00D365, ___cPtr_0)); }
	inline uint64_t get_cPtr_0() const { return ___cPtr_0; }
	inline uint64_t* get_address_of_cPtr_0() { return &___cPtr_0; }
	inline void set_cPtr_0(uint64_t value)
	{
		___cPtr_0 = value;
	}
};


// maxstAR.WearableCalibration
struct  WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D  : public RuntimeObject
{
public:
	// System.String maxstAR.WearableCalibration::<activeProfile>k__BackingField
	String_t* ___U3CactiveProfileU3Ek__BackingField_0;
	// UnityEngine.GameObject maxstAR.WearableCalibration::eyeLeft
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___eyeLeft_1;
	// UnityEngine.GameObject maxstAR.WearableCalibration::eyeRight
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___eyeRight_2;

public:
	inline static int32_t get_offset_of_U3CactiveProfileU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D, ___U3CactiveProfileU3Ek__BackingField_0)); }
	inline String_t* get_U3CactiveProfileU3Ek__BackingField_0() const { return ___U3CactiveProfileU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CactiveProfileU3Ek__BackingField_0() { return &___U3CactiveProfileU3Ek__BackingField_0; }
	inline void set_U3CactiveProfileU3Ek__BackingField_0(String_t* value)
	{
		___U3CactiveProfileU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CactiveProfileU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_eyeLeft_1() { return static_cast<int32_t>(offsetof(WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D, ___eyeLeft_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_eyeLeft_1() const { return ___eyeLeft_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_eyeLeft_1() { return &___eyeLeft_1; }
	inline void set_eyeLeft_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___eyeLeft_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eyeLeft_1), (void*)value);
	}

	inline static int32_t get_offset_of_eyeRight_2() { return static_cast<int32_t>(offsetof(WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D, ___eyeRight_2)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_eyeRight_2() const { return ___eyeRight_2; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_eyeRight_2() { return &___eyeRight_2; }
	inline void set_eyeRight_2(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___eyeRight_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eyeRight_2), (void*)value);
	}
};


// maxstAR.WearableDeviceController
struct  WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92  : public RuntimeObject
{
public:
	// UnityEngine.AndroidJavaObject maxstAR.WearableDeviceController::javaObject
	AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * ___javaObject_0;

public:
	inline static int32_t get_offset_of_javaObject_0() { return static_cast<int32_t>(offsetof(WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92, ___javaObject_0)); }
	inline AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * get_javaObject_0() const { return ___javaObject_0; }
	inline AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D ** get_address_of_javaObject_0() { return &___javaObject_0; }
	inline void set_javaObject_0(AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * value)
	{
		___javaObject_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___javaObject_0), (void*)value);
	}
};


// maxstAR.WearableManager
struct  WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4  : public RuntimeObject
{
public:
	// maxstAR.WearableDeviceController maxstAR.WearableManager::wearableDeviceController
	WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92 * ___wearableDeviceController_1;
	// maxstAR.WearableCalibration maxstAR.WearableManager::wearableCalibration
	WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * ___wearableCalibration_2;

public:
	inline static int32_t get_offset_of_wearableDeviceController_1() { return static_cast<int32_t>(offsetof(WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4, ___wearableDeviceController_1)); }
	inline WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92 * get_wearableDeviceController_1() const { return ___wearableDeviceController_1; }
	inline WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92 ** get_address_of_wearableDeviceController_1() { return &___wearableDeviceController_1; }
	inline void set_wearableDeviceController_1(WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92 * value)
	{
		___wearableDeviceController_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___wearableDeviceController_1), (void*)value);
	}

	inline static int32_t get_offset_of_wearableCalibration_2() { return static_cast<int32_t>(offsetof(WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4, ___wearableCalibration_2)); }
	inline WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * get_wearableCalibration_2() const { return ___wearableCalibration_2; }
	inline WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D ** get_address_of_wearableCalibration_2() { return &___wearableCalibration_2; }
	inline void set_wearableCalibration_2(WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * value)
	{
		___wearableCalibration_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___wearableCalibration_2), (void*)value);
	}
};

struct WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4_StaticFields
{
public:
	// maxstAR.WearableManager maxstAR.WearableManager::Instance
	WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4_StaticFields, ___Instance_0)); }
	inline WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4 * get_Instance_0() const { return ___Instance_0; }
	inline WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Instance_0), (void*)value);
	}
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct  Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.Char
struct  Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Int64
struct  Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.UInt64
struct  UInt64_tA02DF3B59C8FC4A849BD207DA11038CC64E4CB4E 
{
public:
	// System.UInt64 System.UInt64::m_value
	uint64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt64_tA02DF3B59C8FC4A849BD207DA11038CC64E4CB4E, ___m_value_0)); }
	inline uint64_t get_m_value_0() const { return ___m_value_0; }
	inline uint64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint64_t value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.AndroidJavaClass
struct  AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE  : public AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D
{
public:

public:
};


// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};


// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};


// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.CameraClearFlags
struct  CameraClearFlags_tAC22BD22D12708CBDC63F6CFB31109E5E17CF239 
{
public:
	// System.Int32 UnityEngine.CameraClearFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraClearFlags_tAC22BD22D12708CBDC63F6CFB31109E5E17CF239, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.RuntimePlatform
struct  RuntimePlatform_tD5F5737C1BBBCBB115EB104DF2B7876387E80132 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RuntimePlatform_tD5F5737C1BBBCBB115EB104DF2B7876387E80132, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ScreenOrientation
struct  ScreenOrientation_t4AB8E2E02033B0EAEA0260B05B1D88DA8058BB51 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScreenOrientation_t4AB8E2E02033B0EAEA0260B05B1D88DA8058BB51, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// maxstAR.AbstractARManager_WorldCenterMode
struct  WorldCenterMode_t822ADD8FC2B6BC55884EB49D3A7EEC4299090022 
{
public:
	// System.Int32 maxstAR.AbstractARManager_WorldCenterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WorldCenterMode_t822ADD8FC2B6BC55884EB49D3A7EEC4299090022, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// maxstAR.CloudRecognitionController_CloudState
struct  CloudState_tB6E952419F48AC15EB2ADE934735478E73790AB0 
{
public:
	// System.Int32 maxstAR.CloudRecognitionController_CloudState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CloudState_tB6E952419F48AC15EB2ADE934735478E73790AB0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// maxstAR.ColorFormat
struct  ColorFormat_t69E008FC0BF6EC1F1C01FE105C4F32F714A48B54 
{
public:
	// System.Int32 maxstAR.ColorFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorFormat_t69E008FC0BF6EC1F1C01FE105C4F32F714A48B54, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// maxstAR.ResultCode
struct  ResultCode_t0BDFA7DBAF892995DC212D6ECAB7F90351BA0B52 
{
public:
	// System.Int32 maxstAR.ResultCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ResultCode_t0BDFA7DBAF892995DC212D6ECAB7F90351BA0B52, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// maxstAR.StorageType
struct  StorageType_t6EB369F1D5EE7B4B4A44B9F40F1C57CD845C6318 
{
public:
	// System.Int32 maxstAR.StorageType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StorageType_t6EB369F1D5EE7B4B4A44B9F40F1C57CD845C6318, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// maxstAR.TrackerManager_TrackingOption
struct  TrackingOption_t86008DF11C3E7A986ACA55949BE3CF8288D2254B 
{
public:
	// System.Int32 maxstAR.TrackerManager_TrackingOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackingOption_t86008DF11C3E7A986ACA55949BE3CF8288D2254B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// maxstAR.TrackingState_TrackingStatus
struct  TrackingStatus_tBE29D934D18AD38CF3F7D9A99007B97519F8B50D 
{
public:
	// System.Int32 maxstAR.TrackingState_TrackingStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackingStatus_tBE29D934D18AD38CF3F7D9A99007B97519F8B50D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// maxstAR.WearableCalibration_EyeType
struct  EyeType_tD291AD64C626FA20E6FE4C97CB85024CC5F694E7 
{
public:
	// System.Int32 maxstAR.WearableCalibration_EyeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EyeType_tD291AD64C626FA20E6FE4C97CB85024CC5F694E7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// maxstAR.WearableCalibration_WearableType
struct  WearableType_t655BA19CF495AEDD4F358AD177FC726FA76A8FAD 
{
public:
	// System.Int32 maxstAR.WearableCalibration_WearableType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WearableType_t655BA19CF495AEDD4F358AD177FC726FA76A8FAD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// maxstAR.TrackedImage
struct  TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1  : public RuntimeObject
{
public:
	// System.UInt64 maxstAR.TrackedImage::trackedImageCPtr
	uint64_t ___trackedImageCPtr_1;
	// System.Int32 maxstAR.TrackedImage::index
	int32_t ___index_2;
	// System.Int32 maxstAR.TrackedImage::width
	int32_t ___width_3;
	// System.Int32 maxstAR.TrackedImage::height
	int32_t ___height_4;
	// System.Int32 maxstAR.TrackedImage::length
	int32_t ___length_5;
	// maxstAR.ColorFormat maxstAR.TrackedImage::colorFormat
	int32_t ___colorFormat_6;
	// System.Boolean maxstAR.TrackedImage::splitYuv
	bool ___splitYuv_7;

public:
	inline static int32_t get_offset_of_trackedImageCPtr_1() { return static_cast<int32_t>(offsetof(TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1, ___trackedImageCPtr_1)); }
	inline uint64_t get_trackedImageCPtr_1() const { return ___trackedImageCPtr_1; }
	inline uint64_t* get_address_of_trackedImageCPtr_1() { return &___trackedImageCPtr_1; }
	inline void set_trackedImageCPtr_1(uint64_t value)
	{
		___trackedImageCPtr_1 = value;
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1, ___width_3)); }
	inline int32_t get_width_3() const { return ___width_3; }
	inline int32_t* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(int32_t value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1, ___height_4)); }
	inline int32_t get_height_4() const { return ___height_4; }
	inline int32_t* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(int32_t value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_length_5() { return static_cast<int32_t>(offsetof(TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1, ___length_5)); }
	inline int32_t get_length_5() const { return ___length_5; }
	inline int32_t* get_address_of_length_5() { return &___length_5; }
	inline void set_length_5(int32_t value)
	{
		___length_5 = value;
	}

	inline static int32_t get_offset_of_colorFormat_6() { return static_cast<int32_t>(offsetof(TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1, ___colorFormat_6)); }
	inline int32_t get_colorFormat_6() const { return ___colorFormat_6; }
	inline int32_t* get_address_of_colorFormat_6() { return &___colorFormat_6; }
	inline void set_colorFormat_6(int32_t value)
	{
		___colorFormat_6 = value;
	}

	inline static int32_t get_offset_of_splitYuv_7() { return static_cast<int32_t>(offsetof(TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1, ___splitYuv_7)); }
	inline bool get_splitYuv_7() const { return ___splitYuv_7; }
	inline bool* get_address_of_splitYuv_7() { return &___splitYuv_7; }
	inline void set_splitYuv_7(bool value)
	{
		___splitYuv_7 = value;
	}
};

struct TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1_StaticFields
{
public:
	// System.Byte[] maxstAR.TrackedImage::data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1_StaticFields, ___data_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_data_0() const { return ___data_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_0), (void*)value);
	}
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Collider
struct  Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Renderer
struct  Renderer_t0556D67DD582620D1F495627EDE30D03284151F4  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Camera
struct  Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields
{
public:
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreCull_4;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreRender_5;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// maxstAR.AbstractARManager
struct  AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 maxstAR.AbstractARManager::screenWidth
	int32_t ___screenWidth_5;
	// System.Int32 maxstAR.AbstractARManager::screenHeight
	int32_t ___screenHeight_6;
	// UnityEngine.ScreenOrientation maxstAR.AbstractARManager::orientation
	int32_t ___orientation_7;
	// System.Single maxstAR.AbstractARManager::nearClipPlane
	float ___nearClipPlane_8;
	// System.Single maxstAR.AbstractARManager::farClipPlane
	float ___farClipPlane_9;
	// UnityEngine.Camera maxstAR.AbstractARManager::arCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___arCamera_10;
	// System.Int32 maxstAR.AbstractARManager::cameraWidth
	int32_t ___cameraWidth_11;
	// System.Int32 maxstAR.AbstractARManager::cameraHeight
	int32_t ___cameraHeight_12;
	// maxstAR.AbstractARManager_WorldCenterMode maxstAR.AbstractARManager::worldCenterMode
	int32_t ___worldCenterMode_13;

public:
	inline static int32_t get_offset_of_screenWidth_5() { return static_cast<int32_t>(offsetof(AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3, ___screenWidth_5)); }
	inline int32_t get_screenWidth_5() const { return ___screenWidth_5; }
	inline int32_t* get_address_of_screenWidth_5() { return &___screenWidth_5; }
	inline void set_screenWidth_5(int32_t value)
	{
		___screenWidth_5 = value;
	}

	inline static int32_t get_offset_of_screenHeight_6() { return static_cast<int32_t>(offsetof(AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3, ___screenHeight_6)); }
	inline int32_t get_screenHeight_6() const { return ___screenHeight_6; }
	inline int32_t* get_address_of_screenHeight_6() { return &___screenHeight_6; }
	inline void set_screenHeight_6(int32_t value)
	{
		___screenHeight_6 = value;
	}

	inline static int32_t get_offset_of_orientation_7() { return static_cast<int32_t>(offsetof(AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3, ___orientation_7)); }
	inline int32_t get_orientation_7() const { return ___orientation_7; }
	inline int32_t* get_address_of_orientation_7() { return &___orientation_7; }
	inline void set_orientation_7(int32_t value)
	{
		___orientation_7 = value;
	}

	inline static int32_t get_offset_of_nearClipPlane_8() { return static_cast<int32_t>(offsetof(AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3, ___nearClipPlane_8)); }
	inline float get_nearClipPlane_8() const { return ___nearClipPlane_8; }
	inline float* get_address_of_nearClipPlane_8() { return &___nearClipPlane_8; }
	inline void set_nearClipPlane_8(float value)
	{
		___nearClipPlane_8 = value;
	}

	inline static int32_t get_offset_of_farClipPlane_9() { return static_cast<int32_t>(offsetof(AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3, ___farClipPlane_9)); }
	inline float get_farClipPlane_9() const { return ___farClipPlane_9; }
	inline float* get_address_of_farClipPlane_9() { return &___farClipPlane_9; }
	inline void set_farClipPlane_9(float value)
	{
		___farClipPlane_9 = value;
	}

	inline static int32_t get_offset_of_arCamera_10() { return static_cast<int32_t>(offsetof(AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3, ___arCamera_10)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_arCamera_10() const { return ___arCamera_10; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_arCamera_10() { return &___arCamera_10; }
	inline void set_arCamera_10(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___arCamera_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arCamera_10), (void*)value);
	}

	inline static int32_t get_offset_of_cameraWidth_11() { return static_cast<int32_t>(offsetof(AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3, ___cameraWidth_11)); }
	inline int32_t get_cameraWidth_11() const { return ___cameraWidth_11; }
	inline int32_t* get_address_of_cameraWidth_11() { return &___cameraWidth_11; }
	inline void set_cameraWidth_11(int32_t value)
	{
		___cameraWidth_11 = value;
	}

	inline static int32_t get_offset_of_cameraHeight_12() { return static_cast<int32_t>(offsetof(AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3, ___cameraHeight_12)); }
	inline int32_t get_cameraHeight_12() const { return ___cameraHeight_12; }
	inline int32_t* get_address_of_cameraHeight_12() { return &___cameraHeight_12; }
	inline void set_cameraHeight_12(int32_t value)
	{
		___cameraHeight_12 = value;
	}

	inline static int32_t get_offset_of_worldCenterMode_13() { return static_cast<int32_t>(offsetof(AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3, ___worldCenterMode_13)); }
	inline int32_t get_worldCenterMode_13() const { return ___worldCenterMode_13; }
	inline int32_t* get_address_of_worldCenterMode_13() { return &___worldCenterMode_13; }
	inline void set_worldCenterMode_13(int32_t value)
	{
		___worldCenterMode_13 = value;
	}
};

struct AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3_StaticFields
{
public:
	// maxstAR.AbstractARManager maxstAR.AbstractARManager::instance
	AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3_StaticFields, ___instance_4)); }
	inline AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3 * get_instance_4() const { return ___instance_4; }
	inline AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}
};


// maxstAR.AbstractTrackableBehaviour
struct  AbstractTrackableBehaviour_t4523BC4C60BEDA7DCF91DFF10288812FF3AD721E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// maxstAR.StorageType maxstAR.AbstractTrackableBehaviour::storageType
	int32_t ___storageType_4;
	// UnityEngine.Object maxstAR.AbstractTrackableBehaviour::trackerDataFileObject
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___trackerDataFileObject_5;
	// System.String maxstAR.AbstractTrackableBehaviour::trackerDataFileName
	String_t* ___trackerDataFileName_6;
	// System.String maxstAR.AbstractTrackableBehaviour::trackableId
	String_t* ___trackableId_7;
	// System.String maxstAR.AbstractTrackableBehaviour::trackableName
	String_t* ___trackableName_8;

public:
	inline static int32_t get_offset_of_storageType_4() { return static_cast<int32_t>(offsetof(AbstractTrackableBehaviour_t4523BC4C60BEDA7DCF91DFF10288812FF3AD721E, ___storageType_4)); }
	inline int32_t get_storageType_4() const { return ___storageType_4; }
	inline int32_t* get_address_of_storageType_4() { return &___storageType_4; }
	inline void set_storageType_4(int32_t value)
	{
		___storageType_4 = value;
	}

	inline static int32_t get_offset_of_trackerDataFileObject_5() { return static_cast<int32_t>(offsetof(AbstractTrackableBehaviour_t4523BC4C60BEDA7DCF91DFF10288812FF3AD721E, ___trackerDataFileObject_5)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get_trackerDataFileObject_5() const { return ___trackerDataFileObject_5; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of_trackerDataFileObject_5() { return &___trackerDataFileObject_5; }
	inline void set_trackerDataFileObject_5(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		___trackerDataFileObject_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trackerDataFileObject_5), (void*)value);
	}

	inline static int32_t get_offset_of_trackerDataFileName_6() { return static_cast<int32_t>(offsetof(AbstractTrackableBehaviour_t4523BC4C60BEDA7DCF91DFF10288812FF3AD721E, ___trackerDataFileName_6)); }
	inline String_t* get_trackerDataFileName_6() const { return ___trackerDataFileName_6; }
	inline String_t** get_address_of_trackerDataFileName_6() { return &___trackerDataFileName_6; }
	inline void set_trackerDataFileName_6(String_t* value)
	{
		___trackerDataFileName_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trackerDataFileName_6), (void*)value);
	}

	inline static int32_t get_offset_of_trackableId_7() { return static_cast<int32_t>(offsetof(AbstractTrackableBehaviour_t4523BC4C60BEDA7DCF91DFF10288812FF3AD721E, ___trackableId_7)); }
	inline String_t* get_trackableId_7() const { return ___trackableId_7; }
	inline String_t** get_address_of_trackableId_7() { return &___trackableId_7; }
	inline void set_trackableId_7(String_t* value)
	{
		___trackableId_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trackableId_7), (void*)value);
	}

	inline static int32_t get_offset_of_trackableName_8() { return static_cast<int32_t>(offsetof(AbstractTrackableBehaviour_t4523BC4C60BEDA7DCF91DFF10288812FF3AD721E, ___trackableName_8)); }
	inline String_t* get_trackableName_8() const { return ___trackableName_8; }
	inline String_t** get_address_of_trackableName_8() { return &___trackableName_8; }
	inline void set_trackableName_8(String_t* value)
	{
		___trackableName_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trackableName_8), (void*)value);
	}
};


// maxstAR.MaxstSingleton`1<maxstAR.CloudRecognitionController>
struct  MaxstSingleton_1_t0187F7FDC19F311CF5D000320AA39A295136CFDB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct MaxstSingleton_1_t0187F7FDC19F311CF5D000320AA39A295136CFDB_StaticFields
{
public:
	// T maxstAR.MaxstSingleton`1::_instance
	CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * ____instance_4;
	// System.Object maxstAR.MaxstSingleton`1::_lock
	RuntimeObject * ____lock_5;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(MaxstSingleton_1_t0187F7FDC19F311CF5D000320AA39A295136CFDB_StaticFields, ____instance_4)); }
	inline CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * get__instance_4() const { return ____instance_4; }
	inline CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_4), (void*)value);
	}

	inline static int32_t get_offset_of__lock_5() { return static_cast<int32_t>(offsetof(MaxstSingleton_1_t0187F7FDC19F311CF5D000320AA39A295136CFDB_StaticFields, ____lock_5)); }
	inline RuntimeObject * get__lock_5() const { return ____lock_5; }
	inline RuntimeObject ** get_address_of__lock_5() { return &____lock_5; }
	inline void set__lock_5(RuntimeObject * value)
	{
		____lock_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____lock_5), (void*)value);
	}
};


// maxstAR.AbstractObjectTrackableBehaviour
struct  AbstractObjectTrackableBehaviour_tB8F488F1D2ED54CBEA10F2124C4FC6C0D245D597  : public AbstractTrackableBehaviour_t4523BC4C60BEDA7DCF91DFF10288812FF3AD721E
{
public:

public:
};


// maxstAR.AbstractQrCodeTrackableBehaviour
struct  AbstractQrCodeTrackableBehaviour_t6D50B1BFDE5EE1CB0914634E8CD8B7C61AB87AAB  : public AbstractTrackableBehaviour_t4523BC4C60BEDA7DCF91DFF10288812FF3AD721E
{
public:
	// System.String maxstAR.AbstractQrCodeTrackableBehaviour::textureDir
	String_t* ___textureDir_9;
	// System.Single maxstAR.AbstractQrCodeTrackableBehaviour::targetWidth
	float ___targetWidth_10;
	// System.Single maxstAR.AbstractQrCodeTrackableBehaviour::targetHeight
	float ___targetHeight_11;

public:
	inline static int32_t get_offset_of_textureDir_9() { return static_cast<int32_t>(offsetof(AbstractQrCodeTrackableBehaviour_t6D50B1BFDE5EE1CB0914634E8CD8B7C61AB87AAB, ___textureDir_9)); }
	inline String_t* get_textureDir_9() const { return ___textureDir_9; }
	inline String_t** get_address_of_textureDir_9() { return &___textureDir_9; }
	inline void set_textureDir_9(String_t* value)
	{
		___textureDir_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textureDir_9), (void*)value);
	}

	inline static int32_t get_offset_of_targetWidth_10() { return static_cast<int32_t>(offsetof(AbstractQrCodeTrackableBehaviour_t6D50B1BFDE5EE1CB0914634E8CD8B7C61AB87AAB, ___targetWidth_10)); }
	inline float get_targetWidth_10() const { return ___targetWidth_10; }
	inline float* get_address_of_targetWidth_10() { return &___targetWidth_10; }
	inline void set_targetWidth_10(float value)
	{
		___targetWidth_10 = value;
	}

	inline static int32_t get_offset_of_targetHeight_11() { return static_cast<int32_t>(offsetof(AbstractQrCodeTrackableBehaviour_t6D50B1BFDE5EE1CB0914634E8CD8B7C61AB87AAB, ___targetHeight_11)); }
	inline float get_targetHeight_11() const { return ___targetHeight_11; }
	inline float* get_address_of_targetHeight_11() { return &___targetHeight_11; }
	inline void set_targetHeight_11(float value)
	{
		___targetHeight_11 = value;
	}
};


// maxstAR.CloudRecognitionController
struct  CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033  : public MaxstSingleton_1_t0187F7FDC19F311CF5D000320AA39A295136CFDB
{
public:
	// System.String maxstAR.CloudRecognitionController::secretId
	String_t* ___secretId_6;
	// System.String maxstAR.CloudRecognitionController::secretKey
	String_t* ___secretKey_7;
	// maxstAR.CloudRecognitionController_CloudState maxstAR.CloudRecognitionController::cloudState
	int32_t ___cloudState_8;
	// System.Boolean maxstAR.CloudRecognitionController::loopState
	bool ___loopState_9;
	// System.Boolean maxstAR.CloudRecognitionController::restart
	bool ___restart_10;
	// System.Boolean maxstAR.CloudRecognitionController::autoState
	bool ___autoState_11;
	// System.Byte[] maxstAR.CloudRecognitionController::cloudFeatureData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___cloudFeatureData_12;
	// System.Threading.Thread maxstAR.CloudRecognitionController::cloudThread
	Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * ___cloudThread_13;
	// System.Threading.Semaphore maxstAR.CloudRecognitionController::cloudSemaphore
	Semaphore_tA08EE5E666C7F069659F9559FC31B2E09E4517EA * ___cloudSemaphore_14;
	// System.String maxstAR.CloudRecognitionController::featureBase64
	String_t* ___featureBase64_15;
	// System.Boolean maxstAR.CloudRecognitionController::isGetFeatureState
	bool ___isGetFeatureState_16;
	// System.Boolean maxstAR.CloudRecognitionController::isDestroy
	bool ___isDestroy_17;

public:
	inline static int32_t get_offset_of_secretId_6() { return static_cast<int32_t>(offsetof(CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033, ___secretId_6)); }
	inline String_t* get_secretId_6() const { return ___secretId_6; }
	inline String_t** get_address_of_secretId_6() { return &___secretId_6; }
	inline void set_secretId_6(String_t* value)
	{
		___secretId_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___secretId_6), (void*)value);
	}

	inline static int32_t get_offset_of_secretKey_7() { return static_cast<int32_t>(offsetof(CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033, ___secretKey_7)); }
	inline String_t* get_secretKey_7() const { return ___secretKey_7; }
	inline String_t** get_address_of_secretKey_7() { return &___secretKey_7; }
	inline void set_secretKey_7(String_t* value)
	{
		___secretKey_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___secretKey_7), (void*)value);
	}

	inline static int32_t get_offset_of_cloudState_8() { return static_cast<int32_t>(offsetof(CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033, ___cloudState_8)); }
	inline int32_t get_cloudState_8() const { return ___cloudState_8; }
	inline int32_t* get_address_of_cloudState_8() { return &___cloudState_8; }
	inline void set_cloudState_8(int32_t value)
	{
		___cloudState_8 = value;
	}

	inline static int32_t get_offset_of_loopState_9() { return static_cast<int32_t>(offsetof(CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033, ___loopState_9)); }
	inline bool get_loopState_9() const { return ___loopState_9; }
	inline bool* get_address_of_loopState_9() { return &___loopState_9; }
	inline void set_loopState_9(bool value)
	{
		___loopState_9 = value;
	}

	inline static int32_t get_offset_of_restart_10() { return static_cast<int32_t>(offsetof(CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033, ___restart_10)); }
	inline bool get_restart_10() const { return ___restart_10; }
	inline bool* get_address_of_restart_10() { return &___restart_10; }
	inline void set_restart_10(bool value)
	{
		___restart_10 = value;
	}

	inline static int32_t get_offset_of_autoState_11() { return static_cast<int32_t>(offsetof(CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033, ___autoState_11)); }
	inline bool get_autoState_11() const { return ___autoState_11; }
	inline bool* get_address_of_autoState_11() { return &___autoState_11; }
	inline void set_autoState_11(bool value)
	{
		___autoState_11 = value;
	}

	inline static int32_t get_offset_of_cloudFeatureData_12() { return static_cast<int32_t>(offsetof(CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033, ___cloudFeatureData_12)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_cloudFeatureData_12() const { return ___cloudFeatureData_12; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_cloudFeatureData_12() { return &___cloudFeatureData_12; }
	inline void set_cloudFeatureData_12(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___cloudFeatureData_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cloudFeatureData_12), (void*)value);
	}

	inline static int32_t get_offset_of_cloudThread_13() { return static_cast<int32_t>(offsetof(CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033, ___cloudThread_13)); }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * get_cloudThread_13() const { return ___cloudThread_13; }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 ** get_address_of_cloudThread_13() { return &___cloudThread_13; }
	inline void set_cloudThread_13(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * value)
	{
		___cloudThread_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cloudThread_13), (void*)value);
	}

	inline static int32_t get_offset_of_cloudSemaphore_14() { return static_cast<int32_t>(offsetof(CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033, ___cloudSemaphore_14)); }
	inline Semaphore_tA08EE5E666C7F069659F9559FC31B2E09E4517EA * get_cloudSemaphore_14() const { return ___cloudSemaphore_14; }
	inline Semaphore_tA08EE5E666C7F069659F9559FC31B2E09E4517EA ** get_address_of_cloudSemaphore_14() { return &___cloudSemaphore_14; }
	inline void set_cloudSemaphore_14(Semaphore_tA08EE5E666C7F069659F9559FC31B2E09E4517EA * value)
	{
		___cloudSemaphore_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cloudSemaphore_14), (void*)value);
	}

	inline static int32_t get_offset_of_featureBase64_15() { return static_cast<int32_t>(offsetof(CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033, ___featureBase64_15)); }
	inline String_t* get_featureBase64_15() const { return ___featureBase64_15; }
	inline String_t** get_address_of_featureBase64_15() { return &___featureBase64_15; }
	inline void set_featureBase64_15(String_t* value)
	{
		___featureBase64_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___featureBase64_15), (void*)value);
	}

	inline static int32_t get_offset_of_isGetFeatureState_16() { return static_cast<int32_t>(offsetof(CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033, ___isGetFeatureState_16)); }
	inline bool get_isGetFeatureState_16() const { return ___isGetFeatureState_16; }
	inline bool* get_address_of_isGetFeatureState_16() { return &___isGetFeatureState_16; }
	inline void set_isGetFeatureState_16(bool value)
	{
		___isGetFeatureState_16 = value;
	}

	inline static int32_t get_offset_of_isDestroy_17() { return static_cast<int32_t>(offsetof(CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033, ___isDestroy_17)); }
	inline bool get_isDestroy_17() const { return ___isDestroy_17; }
	inline bool* get_address_of_isDestroy_17() { return &___isDestroy_17; }
	inline void set_isDestroy_17(bool value)
	{
		___isDestroy_17 = value;
	}
};


// maxstAR.ObjectTrackableBehaviour
struct  ObjectTrackableBehaviour_t2137BE450300155EE7BD6A66AC50B25B5511C2A0  : public AbstractObjectTrackableBehaviour_tB8F488F1D2ED54CBEA10F2124C4FC6C0D245D597
{
public:

public:
};


// maxstAR.QrCodeTrackableBehaviour
struct  QrCodeTrackableBehaviour_tC5612CD43E2F7043C9E80D17C92CBF696CED617E  : public AbstractQrCodeTrackableBehaviour_t6D50B1BFDE5EE1CB0914634E8CD8B7C61AB87AAB
{
public:
	// System.String maxstAR.QrCodeTrackableBehaviour::qrCodeSearchingWords
	String_t* ___qrCodeSearchingWords_12;

public:
	inline static int32_t get_offset_of_qrCodeSearchingWords_12() { return static_cast<int32_t>(offsetof(QrCodeTrackableBehaviour_tC5612CD43E2F7043C9E80D17C92CBF696CED617E, ___qrCodeSearchingWords_12)); }
	inline String_t* get_qrCodeSearchingWords_12() const { return ___qrCodeSearchingWords_12; }
	inline String_t** get_address_of_qrCodeSearchingWords_12() { return &___qrCodeSearchingWords_12; }
	inline void set_qrCodeSearchingWords_12(String_t* value)
	{
		___qrCodeSearchingWords_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___qrCodeSearchingWords_12), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Collider[]
struct ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * m_Items[1];

public:
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Renderer[]
struct RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * m_Items[1];

public:
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* Component_GetComponentsInChildren_TisRuntimeObject_m8EDDAD383E0869517344FACCD43A329D35AAA0CE_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, bool ___includeInactive0, const RuntimeMethod* method);
// T maxstAR.MaxstSingleton`1<System.Object>::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * MaxstSingleton_1_get_Instance_mD35FAEBA7D3D8959EEEB7AA911F4A0A3D2C430FC_gshared (const RuntimeMethod* method);
// !!0 UnityEngine.AndroidJavaObject::GetStatic<System.Object>(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * AndroidJavaObject_GetStatic_TisRuntimeObject_mDA42018F7419EC3301EBC8FDB8EB6B3016D88209_gshared (AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * __this, String_t* ___fieldName0, const RuntimeMethod* method);
// !!0[] System.Array::Empty<System.Object>()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_gshared_inline (const RuntimeMethod* method);
// !!0 UnityEngine.AndroidJavaObject::CallStatic<System.Object>(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * AndroidJavaObject_CallStatic_TisRuntimeObject_mC00F70734976E6B3DD8281EB6EBC457B19762E9F_gshared (AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * __this, String_t* ___methodName0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method);
// !!0 UnityEngine.AndroidJavaObject::Call<System.Boolean>(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AndroidJavaObject_Call_TisBoolean_tB53F6830F670160873277339AA58F15CAED4399C_m57EE1ACB271D15DD0E2DDD6B28805C31799A0976_gshared (AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * __this, String_t* ___methodName0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method);
// !!0 UnityEngine.AndroidJavaObject::Call<System.Object>(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * AndroidJavaObject_Call_TisRuntimeObject_m38064E69DD787BA971B0757788FD11E7239A03B7_gshared (AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * __this, String_t* ___methodName0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m80EDFEAC4927F588A7A702F81524EDBFA8603FE2_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m15E3130603CE5400743CCCDEE7600FB9EEFAE5C0_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Renderer>(System.Boolean)
inline RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_m43A34BC668D3320EE3C92BC1BF613857DEA0154F (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, bool ___includeInactive0, const RuntimeMethod* method)
{
	return ((  RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, bool, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m8EDDAD383E0869517344FACCD43A329D35AAA0CE_gshared)(__this, ___includeInactive0, method);
}
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Collider>(System.Boolean)
inline ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_mA906598D1F38C24AC5F6191124D6EE52D3F036D5 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, bool ___includeInactive0, const RuntimeMethod* method)
{
	return ((  ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, bool, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m8EDDAD383E0869517344FACCD43A329D35AAA0CE_gshared)(__this, ___includeInactive0, method);
}
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Renderer_set_enabled_m0933766657F2685BAAE3340B0A984C0E63925303 (Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Collider::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Collider_set_enabled_mF84DE8B0C8CAF33ACDB7F29BC055D9C8CFACB57B (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 maxstAR.MatrixUtils::PositionFromMatrix(UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  MatrixUtils_PositionFromMatrix_m7C6A297E614128E22FB75E01B62F47050B77EF2C (Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___input0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// UnityEngine.Quaternion maxstAR.MatrixUtils::QuaternionFromMatrix(UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  MatrixUtils_QuaternionFromMatrix_m9BD557FFF4F96359C9C363E6F31CDBADD5BD976D (Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___m0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 maxstAR.MatrixUtils::ScaleFromMatrix(UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  MatrixUtils_ScaleFromMatrix_m807628CAC045208C9DA8BC8F23D1B1AE9A4CAFA4 (Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___input0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// System.Void maxstAR.AbstractObjectTrackableBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AbstractObjectTrackableBehaviour__ctor_m96A3DB625F240201C4E849BCD7ED1953C2C889EA (AbstractObjectTrackableBehaviour_tB8F488F1D2ED54CBEA10F2124C4FC6C0D245D597 * __this, const RuntimeMethod* method);
// System.Void maxstAR.AbstractQrCodeTrackableBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AbstractQrCodeTrackableBehaviour__ctor_mE9A8DF0B052262EB04E75A2C2CC1D39AD77CACC2 (AbstractQrCodeTrackableBehaviour_t6D50B1BFDE5EE1CB0914634E8CD8B7C61AB87AAB * __this, const RuntimeMethod* method);
// System.Void maxstAR.SensorDevice::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SensorDevice__ctor_m7203FB445DDFA689277FCFDE08A9D69B89CA346C (SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA * __this, const RuntimeMethod* method);
// System.Void maxstAR.NativeAPI::maxst_SensorDevice_startSensor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_SensorDevice_startSensor_mA7826A672D56DAB8BC392A3695B8E0C39752C85A (const RuntimeMethod* method);
// System.Void maxstAR.NativeAPI::maxst_SensorDevice_stopSensor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_SensorDevice_stopSensor_mB3D457114603E18053BC43A560581BE02752F5B6 (const RuntimeMethod* method);
// System.Int32 maxstAR.NativeAPI::maxst_SurfaceThumbnail_getWidth(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_SurfaceThumbnail_getWidth_m5374309D1ED7CE91A09C299E8C112E8F20344A74 (uint64_t ___SurfaceThumbnail_cPtr0, const RuntimeMethod* method);
// System.Int32 maxstAR.NativeAPI::maxst_SurfaceThumbnail_getHeight(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_SurfaceThumbnail_getHeight_m68521172E9A9936326ED88B690E3E87E356000AE (uint64_t ___SurfaceThumbnail_cPtr0, const RuntimeMethod* method);
// System.Int32 maxstAR.NativeAPI::maxst_SurfaceThumbnail_getLength(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_SurfaceThumbnail_getLength_mCCC4613A0617DA641492CC0EC46F091D996725B8 (uint64_t ___SurfaceThumbnail_cPtr0, const RuntimeMethod* method);
// System.Int32 maxstAR.SurfaceThumbnail::GetLength()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SurfaceThumbnail_GetLength_m2A9A3AC873AB4069E48B88AF8D1D24AF81FFB47F (SurfaceThumbnail_t101EA84FE2D39AF996B5F5F8E89E7DE3638DC824 * __this, const RuntimeMethod* method);
// System.Int32 maxstAR.NativeAPI::maxst_SurfaceThumbnail_getData(System.UInt64,System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_SurfaceThumbnail_getData_m72CC556F4243C8F4543F2902960EBB20C0117977 (uint64_t ___SurfaceThumbnail_cPtr0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data1, int32_t ___length2, const RuntimeMethod* method);
// System.Void System.Array::Clear(System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E (RuntimeArray * ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method);
// System.Void maxstAR.NativeAPI::maxst_Trackable_getId(System.UInt64,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_Trackable_getId_m65E02BA95C26BC662C8DE07DBCB80FF71F34A7F2 (uint64_t ___Trackable_cPtr0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___id1, const RuntimeMethod* method);
// System.Text.Encoding System.Text.Encoding::get_UTF8()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * Encoding_get_UTF8_m67C8652936B681E7BC7505E459E88790E0FF16D9 (const RuntimeMethod* method);
// System.String System.String::TrimEnd(System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_TrimEnd_m8D4905B71A4AEBF9D0BC36C6003FC9A5AD630403 (String_t* __this, CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___trimChars0, const RuntimeMethod* method);
// System.Void maxstAR.NativeAPI::maxst_Trackable_getName(System.UInt64,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_Trackable_getName_mDEAA3725E5E6CB1E45555AACC612F1F1BDAD114F (uint64_t ___Trackable_cPtr0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___name1, const RuntimeMethod* method);
// System.Void maxstAR.NativeAPI::maxst_Trackable_getCloudName(System.UInt64,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_Trackable_getCloudName_m45027EEE9B6AB68067EB5C233B50B1BEE93ECE3D (uint64_t ___Trackable_cPtr0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___cloudName1, const RuntimeMethod* method);
// System.Void maxstAR.NativeAPI::maxst_Trackable_getCloudMeta(System.UInt64,System.Byte[],System.Int32[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_Trackable_getCloudMeta_mEE9F177E4A24BD1FCF987F11028E96FCD17C6515 (uint64_t ___Trackable_cPtr0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___cloudMeta1, Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___length2, const RuntimeMethod* method);
// System.Void maxstAR.NativeAPI::maxst_Trackable_getPose(System.UInt64,System.Single[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_Trackable_getPose_m656155FAC79B8376B46E70AD1B284C27F088DB82 (uint64_t ___Trackable_cPtr0, SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___pose1, const RuntimeMethod* method);
// maxstAR.AbstractARManager maxstAR.AbstractARManager::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3 * AbstractARManager_get_Instance_mC07EB5ACE0F0F4833142661BD1C74554899EB94E (const RuntimeMethod* method);
// maxstAR.AbstractARManager/WorldCenterMode maxstAR.AbstractARManager::get_WorldCenterModeSetting()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t AbstractARManager_get_WorldCenterModeSetting_mCF8F7602828C0F38B39A879BB852DB2EA48FAD5C_inline (AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3 * __this, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_identity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  Matrix4x4_get_identity_mA0CECDE2A5E85CF014375084624F3770B5B7B79B (const RuntimeMethod* method);
// UnityEngine.Matrix4x4 maxstAR.MatrixUtils::GetUnityPoseMatrix(System.Single[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  MatrixUtils_GetUnityPoseMatrix_m3F6D97B070AB4B2518E76AF1AE4653F936D0B7F5 (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___glMatrix0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05 (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// UnityEngine.Camera maxstAR.AbstractARManager::GetARCamera()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * AbstractARManager_GetARCamera_m40EF848C5AE6860CF3081C0572C016490F2D3748_inline (AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  Matrix4x4_TRS_m5BB2EBA1152301BAC92FDC7F33ECA732BAE57990 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pos0, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___q1, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___s2, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  Matrix4x4_op_Multiply_mF6693A950E1917204E356366892C3CCB0553436E (Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___lhs0, Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___rhs1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2 (const RuntimeMethod* method);
// System.Single maxstAR.NativeAPI::maxst_Trackable_getWidth(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float NativeAPI_maxst_Trackable_getWidth_m98564B655F63F5CF6674E8EDA4F8BD0E4FB5A715 (uint64_t ___Trackable_cPtr0, const RuntimeMethod* method);
// System.Single maxstAR.NativeAPI::maxst_Trackable_getHeight(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float NativeAPI_maxst_Trackable_getHeight_mD2C8D6D0DAEA052AFFDAF7E886EA4658A54D40AE (uint64_t ___Trackable_cPtr0, const RuntimeMethod* method);
// System.Int32 maxstAR.NativeAPI::maxst_TrackedImage_getIndex(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_TrackedImage_getIndex_mEEB773E5420A16C287B64850530C2430BCDF530E (uint64_t ___Image_cPtr0, const RuntimeMethod* method);
// System.Int32 maxstAR.NativeAPI::maxst_TrackedImage_getWidth(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_TrackedImage_getWidth_m668590037D74A9A8529245D389A52E7FA74E5933 (uint64_t ___Image_cPtr0, const RuntimeMethod* method);
// System.Int32 maxstAR.NativeAPI::maxst_TrackedImage_getHeight(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_TrackedImage_getHeight_m694B87A48F1267C12A88DCA22C5DFFD821C8E8A0 (uint64_t ___Image_cPtr0, const RuntimeMethod* method);
// System.Int32 maxstAR.NativeAPI::maxst_TrackedImage_getLength(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_TrackedImage_getLength_m4AD455C6AA5D2126478F92288A431367202CEFD2 (uint64_t ___Image_cPtr0, const RuntimeMethod* method);
// System.Int32 maxstAR.NativeAPI::maxst_TrackedImage_getFormat(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_TrackedImage_getFormat_m775B4491F0F0CDF95904BA5C7CCDCE8819ADFAA0 (uint64_t ___Image_cPtr0, const RuntimeMethod* method);
// System.Void maxstAR.TrackedImage::.ctor(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackedImage__ctor_mD5DB1118197FD89E30C46B0AA42CC748E4928BD2 (TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1 * __this, uint64_t ___cPtr0, const RuntimeMethod* method);
// System.Void maxstAR.NativeAPI::maxst_TrackedImage_getData(System.UInt64,System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackedImage_getData_mD323FBE5A761CD2CB39DB663212524FB1FB71D95 (uint64_t ___Image_cPtr0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer1, int32_t ___size2, const RuntimeMethod* method);
// System.UInt64 maxstAR.NativeAPI::maxst_TrackedImage_getDataPtr(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint64_t NativeAPI_maxst_TrackedImage_getDataPtr_mD62AC911086BD8845DBDB9547225539C37EFCA3F (uint64_t ___Image_cPtr0, const RuntimeMethod* method);
// System.IntPtr System.IntPtr::op_Explicit(System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t IntPtr_op_Explicit_m534152049CE3084CEFA1CD8B1BA74F3C085A2ABF (int64_t ___value0, const RuntimeMethod* method);
// System.Void maxstAR.NativeAPI::maxst_TrackedImage_getYuv420spY_UVPtr(System.UInt64,System.IntPtr&,System.IntPtr&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackedImage_getYuv420spY_UVPtr_mC80CE7D9A89A78E3FF325ECB66907D7AEEAE418B (uint64_t ___Image_cPtr0, intptr_t* ___y_Ptr1, intptr_t* ___uv_Ptr2, const RuntimeMethod* method);
// System.UInt64 maxstAR.NativeAPI::maxst_TrackedImage_getYuv420spY_U_VPtr(System.UInt64,System.IntPtr&,System.IntPtr&,System.IntPtr&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint64_t NativeAPI_maxst_TrackedImage_getYuv420spY_U_VPtr_m906C3214FA840F6D0B696010ACB4BDCC10F771BD (uint64_t ___Image_cPtr0, intptr_t* ___y_Ptr1, intptr_t* ___u_Ptr2, intptr_t* ___v_Ptr3, const RuntimeMethod* method);
// System.UInt64 maxstAR.NativeAPI::maxst_TrackedImage_getYuv420_888YUVPtr(System.UInt64,System.IntPtr&,System.IntPtr&,System.IntPtr&,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint64_t NativeAPI_maxst_TrackedImage_getYuv420_888YUVPtr_mCF529A3F15BDD5D195375005588D18D1EA2863CC (uint64_t ___Image_cPtr0, intptr_t* ___y_Ptr1, intptr_t* ___u_Ptr2, intptr_t* ___v_Ptr3, bool ___supportRG16Texture4, const RuntimeMethod* method);
// System.Void maxstAR.TrackerManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackerManager__ctor_m170A81D4AD8E76A78178882F1CC4CD2C7959D3A5 (TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * __this, const RuntimeMethod* method);
// T maxstAR.MaxstSingleton`1<maxstAR.CloudRecognitionController>::get_Instance()
inline CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * MaxstSingleton_1_get_Instance_m1346F6D4BE1CE718B2CD992D56A6E6FB8900A341 (const RuntimeMethod* method)
{
	return ((  CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * (*) (const RuntimeMethod*))MaxstSingleton_1_get_Instance_mD35FAEBA7D3D8959EEEB7AA911F4A0A3D2C430FC_gshared)(method);
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Void maxstAR.TrackerManager::InitializeCloud()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackerManager_InitializeCloud_m16BEED93FCF7207B81027A789BC393248ECB615A (TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * __this, const RuntimeMethod* method);
// System.Void maxstAR.CloudRecognitionController::SetCloudRecognitionSecretIdAndSecretKey(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CloudRecognitionController_SetCloudRecognitionSecretIdAndSecretKey_mC7C9F09C20600730341C96E2038A35258DD4EC27 (CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * __this, String_t* ___secretId0, String_t* ___secretKey1, const RuntimeMethod* method);
// System.Void maxstAR.CloudRecognitionController::StartTracker()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CloudRecognitionController_StartTracker_m422BD955682D9242C165CB4AE1BC99754D52FB3C (CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * __this, const RuntimeMethod* method);
// System.Void maxstAR.NativeAPI::maxst_TrackerManager_startTracker(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackerManager_startTracker_mD2A013E2FC6B619155F37192F9AB7DABB44CB121 (int32_t ___trackerMask0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Void maxstAR.CloudRecognitionController::StopTracker()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CloudRecognitionController_StopTracker_mC79E9729FE27C56117D2EB767CA02D329A9E6678 (CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * __this, const RuntimeMethod* method);
// System.Void maxstAR.NativeAPI::maxst_TrackerManager_stopTracker()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackerManager_stopTracker_m2AC86CAD2BE61A2E1451B47F8FA2488D813989F2 (const RuntimeMethod* method);
// System.Void maxstAR.CloudRecognitionController::DestroyTracker()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CloudRecognitionController_DestroyTracker_mDE30FC70302DD304368995A083933CF3C5C6EC1C (CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * __this, const RuntimeMethod* method);
// System.Void maxstAR.NativeAPI::maxst_TrackerManager_destroyTracker()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackerManager_destroyTracker_m1174ED1C73195AEE3AB49531497301A9D9366262 (const RuntimeMethod* method);
// System.Void maxstAR.NativeAPI::maxst_TrackerManager_addTrackerData(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackerManager_addTrackerData_m51C22A522B54749FDBC43568D1B0BAE9E3B6C497 (String_t* ___trackingFileName0, bool ___isAndroidAssetFile1, const RuntimeMethod* method);
// System.Void maxstAR.NativeAPI::maxst_TrackerManager_removeTrackerData(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackerManager_removeTrackerData_mEB0715C35637542C19BEC1A33417F97CC370369B (String_t* ___trackingFileName0, const RuntimeMethod* method);
// System.Void maxstAR.NativeAPI::maxst_TrackerManager_loadTrackerData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackerManager_loadTrackerData_mD7B98D711F885256BE5A3E37221387D6CB67EBAF (const RuntimeMethod* method);
// System.Void maxstAR.CloudRecognitionController::SetAutoEnable(System.Boolean)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void CloudRecognitionController_SetAutoEnable_mDE82AB035CC5D2A1A17710F5157AF552E9E44886_inline (CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * __this, bool ___enable0, const RuntimeMethod* method);
// System.Void maxstAR.NativeAPI::maxst_TrackerManager_setTrackingOption(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackerManager_setTrackingOption_m002A7BE8981B92CF58464C56E6C7BD699699FADB (int32_t ___option0, const RuntimeMethod* method);
// System.Void maxstAR.AbstractARManager::SetWorldCenterMode(maxstAR.AbstractARManager/WorldCenterMode)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AbstractARManager_SetWorldCenterMode_m39E9C716D2DDF61F6B7C0D1CD2BA1B5D3BCD538E_inline (AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3 * __this, int32_t ___worldCenterMode0, const RuntimeMethod* method);
// System.Boolean maxstAR.NativeAPI::maxst_TrackerManager_isTrackerDataLoadCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeAPI_maxst_TrackerManager_isTrackerDataLoadCompleted_mE1E670296E50E476D4434C251FADEDB1F8CD48DA (const RuntimeMethod* method);
// System.Void maxstAR.NativeAPI::maxst_TrackerManager_setVocabulary(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackerManager_setVocabulary_m9D20A8B8F7760E8A505E8B6FE179B70BB72C03BF (String_t* ___vocFilename0, bool ___assetFile1, const RuntimeMethod* method);
// System.UInt64 maxstAR.NativeAPI::maxst_TrackerManager_updateTrackingState()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint64_t NativeAPI_maxst_TrackerManager_updateTrackingState_m1FF8B6E79093D760E2FF3FC0FBDA947FAB86F11E (const RuntimeMethod* method);
// System.Void maxstAR.TrackingState::.ctor(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackingState__ctor_m511846572F1B67AC12D0596AC945498728CB2844 (TrackingState_t1B55706E8B57210198976B3EC273F3ABAB00D365 * __this, uint64_t ___trackingStateCPtr0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150 (const RuntimeMethod* method);
// System.Void maxstAR.NativeAPI::maxst_TrackerManager_getWorldPositionFromScreenCoordinate(System.Single[],System.Single[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackerManager_getWorldPositionFromScreenCoordinate_m06785004CF3907F36BA753A6E0293B466DC56990 (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___screen0, SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___world1, const RuntimeMethod* method);
// System.Void maxstAR.NativeAPI::maxst_TrackerManager_findSurface()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackerManager_findSurface_m2F01DD3DA099836016D8C7EB80CFA1EB7770D202 (const RuntimeMethod* method);
// System.Void maxstAR.NativeAPI::maxst_TrackerManager_quitFindingSurface()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackerManager_quitFindingSurface_m383FB2DFD3D0404740C354D781CCA20509073AED (const RuntimeMethod* method);
// System.Void maxstAR.CloudRecognitionController::FindImageOfCloudRecognition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CloudRecognitionController_FindImageOfCloudRecognition_mF067EE8BD5A498574C4B817AFABBF1551B757EC0 (CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * __this, const RuntimeMethod* method);
// System.Void maxstAR.GuideInfo::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GuideInfo__ctor_m4D0A3F456C4757B7037423BEF13AABEE98A67315 (GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5 * __this, const RuntimeMethod* method);
// System.Void maxstAR.GuideInfo::UpdateGuideInfo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GuideInfo_UpdateGuideInfo_mAB8A020344958F11C97A3248D2B8BC63450DF17C (GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5 * __this, const RuntimeMethod* method);
// System.UInt64 maxstAR.NativeAPI::maxst_TrackerManager_saveSurfaceData(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint64_t NativeAPI_maxst_TrackerManager_saveSurfaceData_m5FB3E8361B4713D63718A2D372961B299B94728A (String_t* ___outputFileName0, const RuntimeMethod* method);
// System.Void maxstAR.SurfaceThumbnail::.ctor(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SurfaceThumbnail__ctor_m0DD58C3F68E19DECAA5378921816128B06360334 (SurfaceThumbnail_t101EA84FE2D39AF996B5F5F8E89E7DE3638DC824 * __this, uint64_t ___cPtr0, const RuntimeMethod* method);
// System.Int32 maxstAR.NativeAPI::maxst_TrackingResult_getCount(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_TrackingResult_getCount_mC3FBA8BCB14A66845A307310DED26C50257E5082 (uint64_t ___TrackingResult_cPtr0, const RuntimeMethod* method);
// System.UInt64 maxstAR.NativeAPI::maxst_TrackingResult_getTrackable(System.UInt64,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint64_t NativeAPI_maxst_TrackingResult_getTrackable_m0F5189AD387C9735BFF31D519CE9F0E27CA95B9B (uint64_t ___TrackingResult_cPtr0, int32_t ___index1, const RuntimeMethod* method);
// System.Void maxstAR.Trackable::.ctor(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Trackable__ctor_m28195F5B5619EBC04AF14B18F8D5773F9A37B195 (Trackable_t525A99BA508C48FC5286F43525D4C12CB6CD1D27 * __this, uint64_t ___cPtr0, const RuntimeMethod* method);
// System.UInt64 maxstAR.NativeAPI::maxst_TrackingState_getTrackingResult(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint64_t NativeAPI_maxst_TrackingState_getTrackingResult_m663F7A5997448E4FF7726D09A8901FDCFD976FE1 (uint64_t ___TrackingState_cPtr0, const RuntimeMethod* method);
// System.Void maxstAR.TrackingResult::.ctor(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackingResult__ctor_m16B9702EAC0B7D0B44B2B53B9521E31682724682 (TrackingResult_t3B01C878A45EA0F96D8A10870D925EA0290F7E07 * __this, uint64_t ___cPtr0, const RuntimeMethod* method);
// System.Int32 maxstAR.NativeAPI::maxst_TrackingState_getCodeScanResultLength(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_TrackingState_getCodeScanResultLength_mE478E4BA4BEC1CA111EA9A265209E2AF38A484D3 (uint64_t ___TrackingState_cPtr0, const RuntimeMethod* method);
// System.Void maxstAR.NativeAPI::maxst_TrackingState_getCodeScanResult(System.UInt64,System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackingState_getCodeScanResult_m88980E71855098278471653402255093CF47A43A (uint64_t ___TrackingState_cPtr0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___result1, int32_t ___length2, const RuntimeMethod* method);
// System.UInt64 maxstAR.NativeAPI::maxst_TrackingState_getImage(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint64_t NativeAPI_maxst_TrackingState_getImage_mBEE338FF02F972166CA81F5117BE653C6156003B (uint64_t ___TrackingState_cPtr0, const RuntimeMethod* method);
// System.Void maxstAR.TrackedImage::.ctor(System.UInt64,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackedImage__ctor_m0F370A499837DC0DD4580FEE1B99C728F4B8CE2E (TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1 * __this, uint64_t ___cPtr0, bool ___splitYuv1, const RuntimeMethod* method);
// maxstAR.CloudRecognitionController/CloudState maxstAR.CloudRecognitionController::GetCloudStatus()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t CloudRecognitionController_GetCloudStatus_m58CEA89750D776137CE0B9367902C9ABE6126446_inline (CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * __this, const RuntimeMethod* method);
// System.Boolean maxstAR.NativeAPI::maxst_WearableCalibration_isActivated()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeAPI_maxst_WearableCalibration_isActivated_m9143F692C8392D25C750D38EC06B266105B72EBD (const RuntimeMethod* method);
// System.Boolean maxstAR.NativeAPI::maxst_WearableCalibration_init(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeAPI_maxst_WearableCalibration_init_m772CE19706D3AE2AB4E896585B5FB7EC8A4704AF (String_t* ___modelName0, const RuntimeMethod* method);
// System.String System.Boolean::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Boolean_ToString_m62D1EFD5F6D5F6B6AF0D14A07BF5741C94413301 (bool* __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void UnityEngine.AndroidJavaClass::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidJavaClass__ctor_mAE416E812DB3911279C0FE87A7760247CE1BBFA8 (AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE * __this, String_t* ___className0, const RuntimeMethod* method);
// !!0 UnityEngine.AndroidJavaObject::GetStatic<UnityEngine.AndroidJavaObject>(System.String)
inline AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * AndroidJavaObject_GetStatic_TisAndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D_m944E5987B38F1229EB3E8122E030D0439D4513B0 (AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * __this, String_t* ___fieldName0, const RuntimeMethod* method)
{
	return ((  AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * (*) (AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D *, String_t*, const RuntimeMethod*))AndroidJavaObject_GetStatic_TisRuntimeObject_mDA42018F7419EC3301EBC8FDB8EB6B3016D88209_gshared)(__this, ___fieldName0, method);
}
// !!0[] System.Array::Empty<System.Object>()
inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_inline (const RuntimeMethod* method)
{
	return ((  ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* (*) (const RuntimeMethod*))Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_gshared_inline)(method);
}
// !!0 UnityEngine.AndroidJavaObject::CallStatic<UnityEngine.AndroidJavaObject>(System.String,System.Object[])
inline AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * AndroidJavaObject_CallStatic_TisAndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D_m6CAE75FB51C5A02521C239A7232735573C51EAE7 (AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * __this, String_t* ___methodName0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method)
{
	return ((  AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * (*) (AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D *, String_t*, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*, const RuntimeMethod*))AndroidJavaObject_CallStatic_TisRuntimeObject_mC00F70734976E6B3DD8281EB6EBC457B19762E9F_gshared)(__this, ___methodName0, ___args1, method);
}
// !!0 UnityEngine.AndroidJavaObject::Call<System.Boolean>(System.String,System.Object[])
inline bool AndroidJavaObject_Call_TisBoolean_tB53F6830F670160873277339AA58F15CAED4399C_m57EE1ACB271D15DD0E2DDD6B28805C31799A0976 (AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * __this, String_t* ___methodName0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method)
{
	return ((  bool (*) (AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D *, String_t*, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*, const RuntimeMethod*))AndroidJavaObject_Call_TisBoolean_tB53F6830F670160873277339AA58F15CAED4399C_m57EE1ACB271D15DD0E2DDD6B28805C31799A0976_gshared)(__this, ___methodName0, ___args1, method);
}
// !!0 UnityEngine.AndroidJavaObject::Call<System.String>(System.String,System.Object[])
inline String_t* AndroidJavaObject_Call_TisString_t_m5EAE53C9E2A8893FD8FEA710378D22C162A0FDEA (AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * __this, String_t* ___methodName0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method)
{
	return ((  String_t* (*) (AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D *, String_t*, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*, const RuntimeMethod*))AndroidJavaObject_Call_TisRuntimeObject_m38064E69DD787BA971B0757788FD11E7239A03B7_gshared)(__this, ___methodName0, ___args1, method);
}
// System.Void maxstAR.WearableCalibration::set_activeProfile(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void WearableCalibration_set_activeProfile_m407EFDCA52FF56603F4BD3338ACE6FACC2135357_inline (WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AndroidJavaObject::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidJavaObject_Dispose_m02D1B6D8F3E902E5F0D181BF6C1753856B0DE144 (AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * __this, const RuntimeMethod* method);
// System.Void maxstAR.NativeAPI::maxst_WearableCalibration_deinit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_WearableCalibration_deinit_m174A61A633706CCF0CCFAD86AF3CD91892AF8367 (const RuntimeMethod* method);
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method);
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672 (const RuntimeMethod* method);
// System.Void maxstAR.NativeAPI::maxst_WearableCalibration_getProjectionMatrix(System.Single[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_WearableCalibration_getProjectionMatrix_mCD2BC624AB1D8DEFEADA180420AA58535AB0353A (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___projection0, int32_t ___eyeType1, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, String_t* ___name0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Camera>()
inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * GameObject_AddComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m7DF3B4DB4CA60855860B6760DBA2B1AE5883D20F (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m80EDFEAC4927F588A7A702F81524EDBFA8603FE2_gshared)(__this, method);
}
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_parent_m65B8E4660B2C554069C57A957D9E55FECA7AA73E (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64 (const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_one_mA11B83037CB269C6076CBCF754E24C8F3ACEC2AB (const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_mB090F51A34716700C0F4F1B08F9330C6F503DB9E (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m15E3130603CE5400743CCCDEE7600FB9EEFAE5C0_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Camera>()
inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * GameObject_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_mCF3938D33A4B2D1D9B65321455F867660E72C3FD (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared)(__this, method);
}
// UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Camera_get_clearFlags_m1D02BA1ABD7310269F6121C58AF41DCDEF1E0266 (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Camera::get_backgroundColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Camera_get_backgroundColor_m14496C5DC24582D7227277AF71DBE96F8E9E64FF (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Camera::get_depth()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Camera_get_depth_m436C49A1C7669E4AD5665A1F1107BDFBA38742CD (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Camera::get_nearClipPlane()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Camera_get_nearClipPlane_mD9D3E3D27186BBAC2CC354CE3609E6118A5BF66C (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Camera::get_farClipPlane()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Camera_get_farClipPlane_mF51F1FF5BE87719CFAC293E272B1138DC1EFFD4B (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, const RuntimeMethod* method);
// System.Single[] maxstAR.WearableCalibration::GetProjectionMatrix(maxstAR.WearableCalibration/EyeType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* WearableCalibration_GetProjectionMatrix_mDDCBE1373398DEA571D8B1A9DB3DA9758992F1DC (WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * __this, int32_t ___eyeType0, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 maxstAR.MatrixUtils::ConvertGLProjectionToUnityProjection(System.Single[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  MatrixUtils_ConvertGLProjectionToUnityProjection_mDB182F79BC65EB2063F2D87D95518EF34E3D96B0 (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___projection0, const RuntimeMethod* method);
// System.Void UnityEngine.Camera::set_clearFlags(UnityEngine.CameraClearFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Camera_set_clearFlags_m805DFBD136AA3E1E46A2E61441965D174E87FE50 (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Camera::set_backgroundColor(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Camera_set_backgroundColor_mDB9CA1B37FE2D52493823914AC5BC9F8C1935D6F (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Camera::set_depth(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Camera_set_depth_m4A83CCCF7370B8AD4BDB2CD5528A6E12A409AE58 (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Camera::set_nearClipPlane(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Camera_set_nearClipPlane_m9D81E50F8658C16319BEF3774E78B93DEB208C6B (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Camera::set_farClipPlane(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Camera_set_farClipPlane_m52986DC40B7F577255C4D5A4F780FD8A7D862626 (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method);
// System.Void UnityEngine.Camera::set_rect(UnityEngine.Rect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Camera_set_rect_m6DB9964EA6E519E2B07561C8CE6AA423980FEC11 (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Camera::set_projectionMatrix(UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Camera_set_projectionMatrix_mC726156CC9AE07A46297C91212655D836E1C6720 (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AndroidJavaObject::Call(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidJavaObject_Call_m0FEBE4E59445D8527C88C992AA2D00EEF749AB56 (AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * __this, String_t* ___methodName0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method);
// System.Void maxstAR.WearableManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WearableManager__ctor_m797FE745D17CE179C55D14E0322372200010A04A (WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4 * __this, const RuntimeMethod* method);
// System.Void maxstAR.WearableDeviceController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WearableDeviceController__ctor_m831CA944A05E1A51C2B807D97E4590DB3ADC8897 (WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92 * __this, const RuntimeMethod* method);
// System.Void maxstAR.WearableDeviceController::Init()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WearableDeviceController_Init_m3E8465A7C1727F11B7FD53F0779105F88E964D61 (WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92 * __this, const RuntimeMethod* method);
// System.Boolean maxstAR.WearableDeviceController::IsSupportedWearableDevice()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WearableDeviceController_IsSupportedWearableDevice_m707E8F7754330646B9D1E3C887E4473D1638D3DC (WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92 * __this, const RuntimeMethod* method);
// System.String maxstAR.WearableDeviceController::GetModelName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* WearableDeviceController_GetModelName_m99A1302B5AAF1D4DE11EEBA587262B6C15025C99 (WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92 * __this, const RuntimeMethod* method);
// System.Void maxstAR.WearableCalibration::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WearableCalibration__ctor_m1F9219EF3026C4A8C32ACCD9EA9880A19DC18955 (WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3 (const RuntimeMethod* method);
// System.Boolean maxstAR.WearableCalibration::Init(System.String,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WearableCalibration_Init_mD794728749B60144E5CA54A448995426AF9EDD25 (WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * __this, String_t* ___modelName0, int32_t ___width1, int32_t ___height2, const RuntimeMethod* method);
// System.String maxstAR.WearableCalibration::get_activeProfile()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* WearableCalibration_get_activeProfile_m672CB16C8B236CD00926A73290B274563CC1FA5E_inline (WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_init(char*);
// System.Void maxstAR.NativeAPI::maxst_init(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_init_mC1AAC75A8AE9670BC22F63457FDBB43C7766E6A7 (String_t* ___licenseKey0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___licenseKey0' to native representation
	char* ____licenseKey0_marshaled = NULL;
	____licenseKey0_marshaled = il2cpp_codegen_marshal_string(___licenseKey0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_init)(____licenseKey0_marshaled);

	// Marshaling cleanup of parameter '___licenseKey0' native representation
	il2cpp_codegen_marshal_free(____licenseKey0_marshaled);
	____licenseKey0_marshaled = NULL;

}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_getVersion(uint8_t*, int32_t);
// System.Void maxstAR.NativeAPI::maxst_getVersion(System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_getVersion_mE26BC475EC44BF8656B0B9FE49777912FAA8CE1C (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___versionBytes0, int32_t ___bytesLength1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (uint8_t*, int32_t);

	// Marshaling of parameter '___versionBytes0' to native representation
	uint8_t* ____versionBytes0_marshaled = NULL;
	if (___versionBytes0 != NULL)
	{
		____versionBytes0_marshaled = reinterpret_cast<uint8_t*>((___versionBytes0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_getVersion)(____versionBytes0_marshaled, ___bytesLength1);

}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_onSurfaceChanged(int32_t, int32_t);
// System.Void maxstAR.NativeAPI::maxst_onSurfaceChanged(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_onSurfaceChanged_m7690D0F0731F47423A7CB6DF92C1095519B0E7DD (int32_t ___surfaceWidth0, int32_t ___surfaceHeight1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_onSurfaceChanged)(___surfaceWidth0, ___surfaceHeight1);

}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_setScreenOrientation(int32_t);
// System.Void maxstAR.NativeAPI::maxst_setScreenOrientation(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_setScreenOrientation_m8D3F14D7F9B5186BA9BB44F8284B6186D967D9DE (int32_t ___orientation0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_setScreenOrientation)(___orientation0);

}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_CameraDevice_start(int32_t, int32_t, int32_t);
// System.Int32 maxstAR.NativeAPI::maxst_CameraDevice_start(System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_CameraDevice_start_mA5EDF2E8BFD27E3E28B6E374616A989B83646B59 (int32_t ___cameraId0, int32_t ___preferredWidth1, int32_t ___preferredHeight2, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t, int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_start)(___cameraId0, ___preferredWidth1, ___preferredHeight2);

	return returnValue;
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_CameraDevice_stop();
// System.Int32 maxstAR.NativeAPI::maxst_CameraDevice_stop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_CameraDevice_stop_m12167043DA6AB668081ACDD6A7CCA11CAABA98A1 (const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_stop)();

	return returnValue;
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_CameraDevice_setCalibrationData(char*);
// System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setCalibrationData(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeAPI_maxst_CameraDevice_setCalibrationData_m587F4F4A01AB5BBBB42BC2DD01713373B14FF858 (String_t* ___filePath0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___filePath0' to native representation
	char* ____filePath0_marshaled = NULL;
	____filePath0_marshaled = il2cpp_codegen_marshal_string(___filePath0);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_setCalibrationData)(____filePath0_marshaled);

	// Marshaling cleanup of parameter '___filePath0' native representation
	il2cpp_codegen_marshal_free(____filePath0_marshaled);
	____filePath0_marshaled = NULL;

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_CameraDevice_setNewFrame(uint8_t*, int32_t, int32_t, int32_t, int32_t);
// System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setNewFrame(System.Byte[],System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeAPI_maxst_CameraDevice_setNewFrame_m5AC1A52756AD74C655E1BA877963E72E72469FA9 (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data0, int32_t ___length1, int32_t ___width2, int32_t ___height3, int32_t ___format4, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint8_t*, int32_t, int32_t, int32_t, int32_t);

	// Marshaling of parameter '___data0' to native representation
	uint8_t* ____data0_marshaled = NULL;
	if (___data0 != NULL)
	{
		____data0_marshaled = reinterpret_cast<uint8_t*>((___data0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_setNewFrame)(____data0_marshaled, ___length1, ___width2, ___height3, ___format4);

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_CameraDevice_setNewFramePtr(uint64_t, int32_t, int32_t, int32_t, int32_t);
// System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setNewFramePtr(System.UInt64,System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeAPI_maxst_CameraDevice_setNewFramePtr_mC9369C76C946063E885314743D21321F1C4F81A0 (uint64_t ___data0, int32_t ___length1, int32_t ___width2, int32_t ___height3, int32_t ___format4, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint64_t, int32_t, int32_t, int32_t, int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_setNewFramePtr)(___data0, ___length1, ___width2, ___height3, ___format4);

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_CameraDevice_setNewFrameAndTimestamp(uint8_t*, int32_t, int32_t, int32_t, int32_t, uint64_t);
// System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setNewFrameAndTimestamp(System.Byte[],System.Int32,System.Int32,System.Int32,System.Int32,System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeAPI_maxst_CameraDevice_setNewFrameAndTimestamp_mA57D3969EF91AC25B3F3E9B20FD0BF7515EA5110 (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data0, int32_t ___length1, int32_t ___width2, int32_t ___height3, int32_t ___format4, uint64_t ___timestamp5, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint8_t*, int32_t, int32_t, int32_t, int32_t, uint64_t);

	// Marshaling of parameter '___data0' to native representation
	uint8_t* ____data0_marshaled = NULL;
	if (___data0 != NULL)
	{
		____data0_marshaled = reinterpret_cast<uint8_t*>((___data0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_setNewFrameAndTimestamp)(____data0_marshaled, ___length1, ___width2, ___height3, ___format4, ___timestamp5);

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_CameraDevice_setNewFramePtrAndTimestamp(uint64_t, int32_t, int32_t, int32_t, int32_t, uint64_t);
// System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setNewFramePtrAndTimestamp(System.UInt64,System.Int32,System.Int32,System.Int32,System.Int32,System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeAPI_maxst_CameraDevice_setNewFramePtrAndTimestamp_m2C4355973A2A9C68BB3EAD9C926E567B0D2E64A5 (uint64_t ___data0, int32_t ___length1, int32_t ___width2, int32_t ___height3, int32_t ___format4, uint64_t ___timestamp5, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint64_t, int32_t, int32_t, int32_t, int32_t, uint64_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_setNewFramePtrAndTimestamp)(___data0, ___length1, ___width2, ___height3, ___format4, ___timestamp5);

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_CameraDevice_setFocusMode(int32_t);
// System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setFocusMode(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeAPI_maxst_CameraDevice_setFocusMode_m480F26C45A73ACE6BF49C7ADF2A80B5546D518D1 (int32_t ___focusMode0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_setFocusMode)(___focusMode0);

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_CameraDevice_setFlashLightMode(int32_t);
// System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setFlashLightMode(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeAPI_maxst_CameraDevice_setFlashLightMode_m9B82A6F46EF0D5094D7FA8891EB6672BD85B3F5C (bool ___toggle0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_setFlashLightMode)(static_cast<int32_t>(___toggle0));

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_CameraDevice_setAutoWhiteBalanceLock(int32_t);
// System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setAutoWhiteBalanceLock(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeAPI_maxst_CameraDevice_setAutoWhiteBalanceLock_m415195884E839F12CE465AE0705524CA5D9C2ECA (bool ___toggle0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_setAutoWhiteBalanceLock)(static_cast<int32_t>(___toggle0));

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_CameraDevice_flipVideo(int32_t, int32_t);
// System.Void maxstAR.NativeAPI::maxst_CameraDevice_flipVideo(System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_CameraDevice_flipVideo_m62342772920CF6123AF1D33BA4456EE6177F1BCB (int32_t ___direction0, bool ___toggle1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_flipVideo)(___direction0, static_cast<int32_t>(___toggle1));

}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_CameraDevice_isVideoFlipped(int32_t);
// System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_isVideoFlipped(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeAPI_maxst_CameraDevice_isVideoFlipped_m172D75CB004ACB86EF78779056C3090CF6250794 (int32_t ___direction0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_isVideoFlipped)(___direction0);

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_CameraDevice_setZoom(float);
// System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setZoom(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeAPI_maxst_CameraDevice_setZoom_m95118B352941DD6009C65B64BD91E2DF3EF1C64A (float ___value0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (float);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_setZoom)(___value0);

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C float DEFAULT_CALL maxst_CameraDevice_getMaxZoomValue();
// System.Single maxstAR.NativeAPI::maxst_CameraDevice_getMaxZoomValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float NativeAPI_maxst_CameraDevice_getMaxZoomValue_m352526181B5BFBD10AE14F3A34197D14F452EA5B (const RuntimeMethod* method)
{
	typedef float (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	float returnValue = reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_getMaxZoomValue)();

	return returnValue;
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_CameraDevice_getParamList();
// System.Int32 maxstAR.NativeAPI::maxst_CameraDevice_getParamList()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_CameraDevice_getParamList_m733CCFA434F9BC8804846201D8A23DEBD851BE24 (const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_getParamList)();

	return returnValue;
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_CameraDevice_Param_getKeyLength(int32_t);
// System.Int32 maxstAR.NativeAPI::maxst_CameraDevice_Param_getKeyLength(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_CameraDevice_Param_getKeyLength_mEB26C85CBBA25FDF23945A221325BCF7DA9D1747 (int32_t ___index0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_Param_getKeyLength)(___index0);

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_CameraDevice_Param_getKey(int32_t, uint8_t*);
// System.Void maxstAR.NativeAPI::maxst_CameraDevice_Param_getKey(System.Int32,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_CameraDevice_Param_getKey_m4217BEDAF8FF57FCCA097FD754BC11C7E30F2E21 (int32_t ___index0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___key1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, uint8_t*);

	// Marshaling of parameter '___key1' to native representation
	uint8_t* ____key1_marshaled = NULL;
	if (___key1 != NULL)
	{
		____key1_marshaled = reinterpret_cast<uint8_t*>((___key1)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_Param_getKey)(___index0, ____key1_marshaled);

}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_CameraDevice_setBoolTypeParameter(char*, int32_t);
// System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setBoolTypeParameter(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeAPI_maxst_CameraDevice_setBoolTypeParameter_mDCDD272E5B1E9C5C38F7D9E3EC9FB2D0D0D9DC94 (String_t* ___key0, bool ___boolValue1, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*, int32_t);

	// Marshaling of parameter '___key0' to native representation
	char* ____key0_marshaled = NULL;
	____key0_marshaled = il2cpp_codegen_marshal_string(___key0);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_setBoolTypeParameter)(____key0_marshaled, static_cast<int32_t>(___boolValue1));

	// Marshaling cleanup of parameter '___key0' native representation
	il2cpp_codegen_marshal_free(____key0_marshaled);
	____key0_marshaled = NULL;

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_CameraDevice_setIntTypeParameter(char*, int32_t);
// System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setIntTypeParameter(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeAPI_maxst_CameraDevice_setIntTypeParameter_mF657FA7F7A8C5916B84F08E47D1A8FB6D44871A4 (String_t* ___key0, int32_t ___intValue1, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*, int32_t);

	// Marshaling of parameter '___key0' to native representation
	char* ____key0_marshaled = NULL;
	____key0_marshaled = il2cpp_codegen_marshal_string(___key0);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_setIntTypeParameter)(____key0_marshaled, ___intValue1);

	// Marshaling cleanup of parameter '___key0' native representation
	il2cpp_codegen_marshal_free(____key0_marshaled);
	____key0_marshaled = NULL;

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_CameraDevice_setRangeTypeParameter(char*, int32_t, int32_t);
// System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setRangeTypeParameter(System.String,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeAPI_maxst_CameraDevice_setRangeTypeParameter_mBBD598D47203A43530A044712E1000ECFB5934C0 (String_t* ___key0, int32_t ___min1, int32_t ___max2, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*, int32_t, int32_t);

	// Marshaling of parameter '___key0' to native representation
	char* ____key0_marshaled = NULL;
	____key0_marshaled = il2cpp_codegen_marshal_string(___key0);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_setRangeTypeParameter)(____key0_marshaled, ___min1, ___max2);

	// Marshaling cleanup of parameter '___key0' native representation
	il2cpp_codegen_marshal_free(____key0_marshaled);
	____key0_marshaled = NULL;

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_CameraDevice_setStringTypeParameter(char*, char*);
// System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_setStringTypeParameter(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeAPI_maxst_CameraDevice_setStringTypeParameter_m92EF41905F8DA6690DD73DD5A1028C6C0CCC285D (String_t* ___key0, String_t* ___stringValue1, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*, char*);

	// Marshaling of parameter '___key0' to native representation
	char* ____key0_marshaled = NULL;
	____key0_marshaled = il2cpp_codegen_marshal_string(___key0);

	// Marshaling of parameter '___stringValue1' to native representation
	char* ____stringValue1_marshaled = NULL;
	____stringValue1_marshaled = il2cpp_codegen_marshal_string(___stringValue1);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_setStringTypeParameter)(____key0_marshaled, ____stringValue1_marshaled);

	// Marshaling cleanup of parameter '___key0' native representation
	il2cpp_codegen_marshal_free(____key0_marshaled);
	____key0_marshaled = NULL;

	// Marshaling cleanup of parameter '___stringValue1' native representation
	il2cpp_codegen_marshal_free(____stringValue1_marshaled);
	____stringValue1_marshaled = NULL;

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_CameraDevice_getWidth();
// System.Int32 maxstAR.NativeAPI::maxst_CameraDevice_getWidth()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_CameraDevice_getWidth_mA3A09F5F4397EFB824807005333759DB1F7F4C6F (const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_getWidth)();

	return returnValue;
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_CameraDevice_getHeight();
// System.Int32 maxstAR.NativeAPI::maxst_CameraDevice_getHeight()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_CameraDevice_getHeight_m549A526D0E3B00AAA8CE00A7DF29D99CD136C5D0 (const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_getHeight)();

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_CameraDevice_getProjectionMatrix(float*);
// System.Void maxstAR.NativeAPI::maxst_CameraDevice_getProjectionMatrix(System.Single[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_CameraDevice_getProjectionMatrix_m7A7FA73573233264B63C3FEDC2F7E8EF45C1E80A (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___matrix0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (float*);

	// Marshaling of parameter '___matrix0' to native representation
	float* ____matrix0_marshaled = NULL;
	if (___matrix0 != NULL)
	{
		____matrix0_marshaled = reinterpret_cast<float*>((___matrix0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_getProjectionMatrix)(____matrix0_marshaled);

}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_CameraDevice_checkCameraMove(uint64_t);
// System.Boolean maxstAR.NativeAPI::maxst_CameraDevice_checkCameraMove(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeAPI_maxst_CameraDevice_checkCameraMove_mF17BBFE375895257ECAA4FD7263CFA3ED4D16657 (uint64_t ___TrackedImage_cPtr0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint64_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_checkCameraMove)(___TrackedImage_cPtr0);

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_CameraDevice_getBackgroundPlaneInfo(float*);
// System.Void maxstAR.NativeAPI::maxst_CameraDevice_getBackgroundPlaneInfo(System.Single[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_CameraDevice_getBackgroundPlaneInfo_m7C75B9FDE102ABF63B08C501982DA781B7661AD7 (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___values0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (float*);

	// Marshaling of parameter '___values0' to native representation
	float* ____values0_marshaled = NULL;
	if (___values0 != NULL)
	{
		____values0_marshaled = reinterpret_cast<float*>((___values0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_CameraDevice_getBackgroundPlaneInfo)(____values0_marshaled);

}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_TrackerManager_startTracker(int32_t);
// System.Void maxstAR.NativeAPI::maxst_TrackerManager_startTracker(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackerManager_startTracker_mD2A013E2FC6B619155F37192F9AB7DABB44CB121 (int32_t ___trackerMask0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_TrackerManager_startTracker)(___trackerMask0);

}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_TrackerManager_stopTracker();
// System.Void maxstAR.NativeAPI::maxst_TrackerManager_stopTracker()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackerManager_stopTracker_m2AC86CAD2BE61A2E1451B47F8FA2488D813989F2 (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_TrackerManager_stopTracker)();

}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_TrackerManager_destroyTracker();
// System.Void maxstAR.NativeAPI::maxst_TrackerManager_destroyTracker()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackerManager_destroyTracker_m1174ED1C73195AEE3AB49531497301A9D9366262 (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_TrackerManager_destroyTracker)();

}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_TrackerManager_addTrackerData(char*, int32_t);
// System.Void maxstAR.NativeAPI::maxst_TrackerManager_addTrackerData(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackerManager_addTrackerData_m51C22A522B54749FDBC43568D1B0BAE9E3B6C497 (String_t* ___trackingFileName0, bool ___isAndroidAssetFile1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, int32_t);

	// Marshaling of parameter '___trackingFileName0' to native representation
	char* ____trackingFileName0_marshaled = NULL;
	____trackingFileName0_marshaled = il2cpp_codegen_marshal_string(___trackingFileName0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_TrackerManager_addTrackerData)(____trackingFileName0_marshaled, static_cast<int32_t>(___isAndroidAssetFile1));

	// Marshaling cleanup of parameter '___trackingFileName0' native representation
	il2cpp_codegen_marshal_free(____trackingFileName0_marshaled);
	____trackingFileName0_marshaled = NULL;

}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_TrackerManager_removeTrackerData(char*);
// System.Void maxstAR.NativeAPI::maxst_TrackerManager_removeTrackerData(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackerManager_removeTrackerData_mEB0715C35637542C19BEC1A33417F97CC370369B (String_t* ___trackingFileName0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___trackingFileName0' to native representation
	char* ____trackingFileName0_marshaled = NULL;
	____trackingFileName0_marshaled = il2cpp_codegen_marshal_string(___trackingFileName0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_TrackerManager_removeTrackerData)(____trackingFileName0_marshaled);

	// Marshaling cleanup of parameter '___trackingFileName0' native representation
	il2cpp_codegen_marshal_free(____trackingFileName0_marshaled);
	____trackingFileName0_marshaled = NULL;

}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_TrackerManager_loadTrackerData();
// System.Void maxstAR.NativeAPI::maxst_TrackerManager_loadTrackerData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackerManager_loadTrackerData_mD7B98D711F885256BE5A3E37221387D6CB67EBAF (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_TrackerManager_loadTrackerData)();

}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_TrackerManager_setTrackingOption(int32_t);
// System.Void maxstAR.NativeAPI::maxst_TrackerManager_setTrackingOption(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackerManager_setTrackingOption_m002A7BE8981B92CF58464C56E6C7BD699699FADB (int32_t ___option0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_TrackerManager_setTrackingOption)(___option0);

}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_TrackerManager_isTrackerDataLoadCompleted();
// System.Boolean maxstAR.NativeAPI::maxst_TrackerManager_isTrackerDataLoadCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeAPI_maxst_TrackerManager_isTrackerDataLoadCompleted_mE1E670296E50E476D4434C251FADEDB1F8CD48DA (const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_TrackerManager_isTrackerDataLoadCompleted)();

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_TrackerManager_setVocabulary(char*, int32_t);
// System.Void maxstAR.NativeAPI::maxst_TrackerManager_setVocabulary(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackerManager_setVocabulary_m9D20A8B8F7760E8A505E8B6FE179B70BB72C03BF (String_t* ___vocFilename0, bool ___assetFile1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, int32_t);

	// Marshaling of parameter '___vocFilename0' to native representation
	char* ____vocFilename0_marshaled = NULL;
	____vocFilename0_marshaled = il2cpp_codegen_marshal_string(___vocFilename0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_TrackerManager_setVocabulary)(____vocFilename0_marshaled, static_cast<int32_t>(___assetFile1));

	// Marshaling cleanup of parameter '___vocFilename0' native representation
	il2cpp_codegen_marshal_free(____vocFilename0_marshaled);
	____vocFilename0_marshaled = NULL;

}
IL2CPP_EXTERN_C uint64_t DEFAULT_CALL maxst_TrackerManager_updateTrackingState();
// System.UInt64 maxstAR.NativeAPI::maxst_TrackerManager_updateTrackingState()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint64_t NativeAPI_maxst_TrackerManager_updateTrackingState_m1FF8B6E79093D760E2FF3FC0FBDA947FAB86F11E (const RuntimeMethod* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_TrackerManager_updateTrackingState)();

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_TrackerManager_findSurface();
// System.Void maxstAR.NativeAPI::maxst_TrackerManager_findSurface()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackerManager_findSurface_m2F01DD3DA099836016D8C7EB80CFA1EB7770D202 (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_TrackerManager_findSurface)();

}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_TrackerManager_quitFindingSurface();
// System.Void maxstAR.NativeAPI::maxst_TrackerManager_quitFindingSurface()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackerManager_quitFindingSurface_m383FB2DFD3D0404740C354D781CCA20509073AED (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_TrackerManager_quitFindingSurface)();

}
IL2CPP_EXTERN_C uint64_t DEFAULT_CALL maxst_TrackerManager_getGuideInfo();
// System.UInt64 maxstAR.NativeAPI::maxst_TrackerManager_getGuideInfo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint64_t NativeAPI_maxst_TrackerManager_getGuideInfo_mC3CD90B50593B28C29E10927009327A4E24FEFF0 (const RuntimeMethod* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_TrackerManager_getGuideInfo)();

	return returnValue;
}
IL2CPP_EXTERN_C uint64_t DEFAULT_CALL maxst_TrackerManager_saveSurfaceData(char*);
// System.UInt64 maxstAR.NativeAPI::maxst_TrackerManager_saveSurfaceData(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint64_t NativeAPI_maxst_TrackerManager_saveSurfaceData_m5FB3E8361B4713D63718A2D372961B299B94728A (String_t* ___outputFileName0, const RuntimeMethod* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___outputFileName0' to native representation
	char* ____outputFileName0_marshaled = NULL;
	____outputFileName0_marshaled = il2cpp_codegen_marshal_string(___outputFileName0);

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_TrackerManager_saveSurfaceData)(____outputFileName0_marshaled);

	// Marshaling cleanup of parameter '___outputFileName0' native representation
	il2cpp_codegen_marshal_free(____outputFileName0_marshaled);
	____outputFileName0_marshaled = NULL;

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_TrackerManager_getWorldPositionFromScreenCoordinate(float*, float*);
// System.Void maxstAR.NativeAPI::maxst_TrackerManager_getWorldPositionFromScreenCoordinate(System.Single[],System.Single[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackerManager_getWorldPositionFromScreenCoordinate_m06785004CF3907F36BA753A6E0293B466DC56990 (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___screen0, SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___world1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (float*, float*);

	// Marshaling of parameter '___screen0' to native representation
	float* ____screen0_marshaled = NULL;
	if (___screen0 != NULL)
	{
		____screen0_marshaled = reinterpret_cast<float*>((___screen0)->GetAddressAtUnchecked(0));
	}

	// Marshaling of parameter '___world1' to native representation
	float* ____world1_marshaled = NULL;
	if (___world1 != NULL)
	{
		____world1_marshaled = reinterpret_cast<float*>((___world1)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_TrackerManager_getWorldPositionFromScreenCoordinate)(____screen0_marshaled, ____world1_marshaled);

}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_CloudManager_GetFeatureClient(uint64_t, uint8_t*, int32_t*);
// System.Boolean maxstAR.NativeAPI::maxst_CloudManager_GetFeatureClient(System.UInt64,System.Byte[],System.Int32[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeAPI_maxst_CloudManager_GetFeatureClient_m76665EAADF00E6475C0A04F28E5B534D8EACE79F (uint64_t ___TrackedImage_cPtr0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___descriptData1, Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___resultLength2, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint64_t, uint8_t*, int32_t*);

	// Marshaling of parameter '___descriptData1' to native representation
	uint8_t* ____descriptData1_marshaled = NULL;
	if (___descriptData1 != NULL)
	{
		____descriptData1_marshaled = reinterpret_cast<uint8_t*>((___descriptData1)->GetAddressAtUnchecked(0));
	}

	// Marshaling of parameter '___resultLength2' to native representation
	int32_t* ____resultLength2_marshaled = NULL;
	if (___resultLength2 != NULL)
	{
		____resultLength2_marshaled = reinterpret_cast<int32_t*>((___resultLength2)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_CloudManager_GetFeatureClient)(___TrackedImage_cPtr0, ____descriptData1_marshaled, ____resultLength2_marshaled);

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_CloudManager_JWTEncode(char*, char*, uint8_t*);
// System.Void maxstAR.NativeAPI::maxst_CloudManager_JWTEncode(System.String,System.String,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_CloudManager_JWTEncode_m6BDF4BC161AE729828474140E31DB0FC185BD889 (String_t* ___secretKey0, String_t* ___payloadString1, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___resultData2, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, uint8_t*);

	// Marshaling of parameter '___secretKey0' to native representation
	char* ____secretKey0_marshaled = NULL;
	____secretKey0_marshaled = il2cpp_codegen_marshal_string(___secretKey0);

	// Marshaling of parameter '___payloadString1' to native representation
	char* ____payloadString1_marshaled = NULL;
	____payloadString1_marshaled = il2cpp_codegen_marshal_string(___payloadString1);

	// Marshaling of parameter '___resultData2' to native representation
	uint8_t* ____resultData2_marshaled = NULL;
	if (___resultData2 != NULL)
	{
		____resultData2_marshaled = reinterpret_cast<uint8_t*>((___resultData2)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_CloudManager_JWTEncode)(____secretKey0_marshaled, ____payloadString1_marshaled, ____resultData2_marshaled);

	// Marshaling cleanup of parameter '___secretKey0' native representation
	il2cpp_codegen_marshal_free(____secretKey0_marshaled);
	____secretKey0_marshaled = NULL;

	// Marshaling cleanup of parameter '___payloadString1' native representation
	il2cpp_codegen_marshal_free(____payloadString1_marshaled);
	____payloadString1_marshaled = NULL;

}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_TrackingResult_getCount(uint64_t);
// System.Int32 maxstAR.NativeAPI::maxst_TrackingResult_getCount(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_TrackingResult_getCount_mC3FBA8BCB14A66845A307310DED26C50257E5082 (uint64_t ___TrackingResult_cPtr0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint64_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_TrackingResult_getCount)(___TrackingResult_cPtr0);

	return returnValue;
}
IL2CPP_EXTERN_C uint64_t DEFAULT_CALL maxst_TrackingResult_getTrackable(uint64_t, int32_t);
// System.UInt64 maxstAR.NativeAPI::maxst_TrackingResult_getTrackable(System.UInt64,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint64_t NativeAPI_maxst_TrackingResult_getTrackable_m0F5189AD387C9735BFF31D519CE9F0E27CA95B9B (uint64_t ___TrackingResult_cPtr0, int32_t ___index1, const RuntimeMethod* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (uint64_t, int32_t);

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_TrackingResult_getTrackable)(___TrackingResult_cPtr0, ___index1);

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_Trackable_getId(uint64_t, uint8_t*);
// System.Void maxstAR.NativeAPI::maxst_Trackable_getId(System.UInt64,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_Trackable_getId_m65E02BA95C26BC662C8DE07DBCB80FF71F34A7F2 (uint64_t ___Trackable_cPtr0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___id1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (uint64_t, uint8_t*);

	// Marshaling of parameter '___id1' to native representation
	uint8_t* ____id1_marshaled = NULL;
	if (___id1 != NULL)
	{
		____id1_marshaled = reinterpret_cast<uint8_t*>((___id1)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_Trackable_getId)(___Trackable_cPtr0, ____id1_marshaled);

}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_Trackable_getName(uint64_t, uint8_t*);
// System.Void maxstAR.NativeAPI::maxst_Trackable_getName(System.UInt64,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_Trackable_getName_mDEAA3725E5E6CB1E45555AACC612F1F1BDAD114F (uint64_t ___Trackable_cPtr0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___name1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (uint64_t, uint8_t*);

	// Marshaling of parameter '___name1' to native representation
	uint8_t* ____name1_marshaled = NULL;
	if (___name1 != NULL)
	{
		____name1_marshaled = reinterpret_cast<uint8_t*>((___name1)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_Trackable_getName)(___Trackable_cPtr0, ____name1_marshaled);

}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_Trackable_getCloudName(uint64_t, uint8_t*);
// System.Void maxstAR.NativeAPI::maxst_Trackable_getCloudName(System.UInt64,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_Trackable_getCloudName_m45027EEE9B6AB68067EB5C233B50B1BEE93ECE3D (uint64_t ___Trackable_cPtr0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___cloudName1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (uint64_t, uint8_t*);

	// Marshaling of parameter '___cloudName1' to native representation
	uint8_t* ____cloudName1_marshaled = NULL;
	if (___cloudName1 != NULL)
	{
		____cloudName1_marshaled = reinterpret_cast<uint8_t*>((___cloudName1)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_Trackable_getCloudName)(___Trackable_cPtr0, ____cloudName1_marshaled);

}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_Trackable_getCloudMeta(uint64_t, uint8_t*, int32_t*);
// System.Void maxstAR.NativeAPI::maxst_Trackable_getCloudMeta(System.UInt64,System.Byte[],System.Int32[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_Trackable_getCloudMeta_mEE9F177E4A24BD1FCF987F11028E96FCD17C6515 (uint64_t ___Trackable_cPtr0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___cloudMeta1, Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___length2, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (uint64_t, uint8_t*, int32_t*);

	// Marshaling of parameter '___cloudMeta1' to native representation
	uint8_t* ____cloudMeta1_marshaled = NULL;
	if (___cloudMeta1 != NULL)
	{
		____cloudMeta1_marshaled = reinterpret_cast<uint8_t*>((___cloudMeta1)->GetAddressAtUnchecked(0));
	}

	// Marshaling of parameter '___length2' to native representation
	int32_t* ____length2_marshaled = NULL;
	if (___length2 != NULL)
	{
		____length2_marshaled = reinterpret_cast<int32_t*>((___length2)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_Trackable_getCloudMeta)(___Trackable_cPtr0, ____cloudMeta1_marshaled, ____length2_marshaled);

}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_Trackable_getPose(uint64_t, float*);
// System.Void maxstAR.NativeAPI::maxst_Trackable_getPose(System.UInt64,System.Single[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_Trackable_getPose_m656155FAC79B8376B46E70AD1B284C27F088DB82 (uint64_t ___Trackable_cPtr0, SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___pose1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (uint64_t, float*);

	// Marshaling of parameter '___pose1' to native representation
	float* ____pose1_marshaled = NULL;
	if (___pose1 != NULL)
	{
		____pose1_marshaled = reinterpret_cast<float*>((___pose1)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_Trackable_getPose)(___Trackable_cPtr0, ____pose1_marshaled);

}
IL2CPP_EXTERN_C float DEFAULT_CALL maxst_Trackable_getWidth(uint64_t);
// System.Single maxstAR.NativeAPI::maxst_Trackable_getWidth(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float NativeAPI_maxst_Trackable_getWidth_m98564B655F63F5CF6674E8EDA4F8BD0E4FB5A715 (uint64_t ___Trackable_cPtr0, const RuntimeMethod* method)
{
	typedef float (DEFAULT_CALL *PInvokeFunc) (uint64_t);

	// Native function invocation
	float returnValue = reinterpret_cast<PInvokeFunc>(maxst_Trackable_getWidth)(___Trackable_cPtr0);

	return returnValue;
}
IL2CPP_EXTERN_C float DEFAULT_CALL maxst_Trackable_getHeight(uint64_t);
// System.Single maxstAR.NativeAPI::maxst_Trackable_getHeight(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float NativeAPI_maxst_Trackable_getHeight_mD2C8D6D0DAEA052AFFDAF7E886EA4658A54D40AE (uint64_t ___Trackable_cPtr0, const RuntimeMethod* method)
{
	typedef float (DEFAULT_CALL *PInvokeFunc) (uint64_t);

	// Native function invocation
	float returnValue = reinterpret_cast<PInvokeFunc>(maxst_Trackable_getHeight)(___Trackable_cPtr0);

	return returnValue;
}
IL2CPP_EXTERN_C uint64_t DEFAULT_CALL maxst_TrackingState_getTrackingResult(uint64_t);
// System.UInt64 maxstAR.NativeAPI::maxst_TrackingState_getTrackingResult(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint64_t NativeAPI_maxst_TrackingState_getTrackingResult_m663F7A5997448E4FF7726D09A8901FDCFD976FE1 (uint64_t ___TrackingState_cPtr0, const RuntimeMethod* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (uint64_t);

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_TrackingState_getTrackingResult)(___TrackingState_cPtr0);

	return returnValue;
}
IL2CPP_EXTERN_C uint64_t DEFAULT_CALL maxst_TrackingState_getImage(uint64_t);
// System.UInt64 maxstAR.NativeAPI::maxst_TrackingState_getImage(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint64_t NativeAPI_maxst_TrackingState_getImage_mBEE338FF02F972166CA81F5117BE653C6156003B (uint64_t ___TrackingState_cPtr0, const RuntimeMethod* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (uint64_t);

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_TrackingState_getImage)(___TrackingState_cPtr0);

	return returnValue;
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_TrackingState_getCodeScanResultLength(uint64_t);
// System.Int32 maxstAR.NativeAPI::maxst_TrackingState_getCodeScanResultLength(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_TrackingState_getCodeScanResultLength_mE478E4BA4BEC1CA111EA9A265209E2AF38A484D3 (uint64_t ___TrackingState_cPtr0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint64_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_TrackingState_getCodeScanResultLength)(___TrackingState_cPtr0);

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_TrackingState_getCodeScanResult(uint64_t, uint8_t*, int32_t);
// System.Void maxstAR.NativeAPI::maxst_TrackingState_getCodeScanResult(System.UInt64,System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackingState_getCodeScanResult_m88980E71855098278471653402255093CF47A43A (uint64_t ___TrackingState_cPtr0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___result1, int32_t ___length2, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (uint64_t, uint8_t*, int32_t);

	// Marshaling of parameter '___result1' to native representation
	uint8_t* ____result1_marshaled = NULL;
	if (___result1 != NULL)
	{
		____result1_marshaled = reinterpret_cast<uint8_t*>((___result1)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_TrackingState_getCodeScanResult)(___TrackingState_cPtr0, ____result1_marshaled, ___length2);

}
IL2CPP_EXTERN_C float DEFAULT_CALL maxst_GuideInfo_getInitializingProgress(uint64_t);
// System.Single maxstAR.NativeAPI::maxst_GuideInfo_getInitializingProgress(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float NativeAPI_maxst_GuideInfo_getInitializingProgress_m473962B12C71875B6678C219BFDCA64BB9FD4BE3 (uint64_t ___GuideInfo_cPtr0, const RuntimeMethod* method)
{
	typedef float (DEFAULT_CALL *PInvokeFunc) (uint64_t);

	// Native function invocation
	float returnValue = reinterpret_cast<PInvokeFunc>(maxst_GuideInfo_getInitializingProgress)(___GuideInfo_cPtr0);

	return returnValue;
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_GuideInfo_getKeyframeCount(uint64_t);
// System.Int32 maxstAR.NativeAPI::maxst_GuideInfo_getKeyframeCount(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_GuideInfo_getKeyframeCount_m60AA17AE8822D71F3C8272A1CF9348501C3E5154 (uint64_t ___GuideInfo_cPtr0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint64_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_GuideInfo_getKeyframeCount)(___GuideInfo_cPtr0);

	return returnValue;
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_GuideInfo_getFeatureCount(uint64_t);
// System.Int32 maxstAR.NativeAPI::maxst_GuideInfo_getFeatureCount(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_GuideInfo_getFeatureCount_mEEBA38F8FD74F6415ECB0C2A6FAF6B786CEB43A8 (uint64_t ___GuideInfo_cPtr0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint64_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_GuideInfo_getFeatureCount)(___GuideInfo_cPtr0);

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_GuideInfo_getFeatureBuffer(uint64_t, float*, int32_t);
// System.Void maxstAR.NativeAPI::maxst_GuideInfo_getFeatureBuffer(System.UInt64,System.Single[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_GuideInfo_getFeatureBuffer_m9FBE90B9269C733F504CE5286E63A0704DBA8CDB (uint64_t ___GuideInfo_cPtr0, SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___data1, int32_t ___length2, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (uint64_t, float*, int32_t);

	// Marshaling of parameter '___data1' to native representation
	float* ____data1_marshaled = NULL;
	if (___data1 != NULL)
	{
		____data1_marshaled = reinterpret_cast<float*>((___data1)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_GuideInfo_getFeatureBuffer)(___GuideInfo_cPtr0, ____data1_marshaled, ___length2);

}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_GuideInfo_getTagAnchorsLength(uint64_t);
// System.Int32 maxstAR.NativeAPI::maxst_GuideInfo_getTagAnchorsLength(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_GuideInfo_getTagAnchorsLength_m01C8BA9DDA202B8A5D13EE294449CAEBD49A8235 (uint64_t ___GuideInfo_cPtr0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint64_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_GuideInfo_getTagAnchorsLength)(___GuideInfo_cPtr0);

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_GuideInfo_getTagAnchors(uint64_t, uint8_t*, int32_t);
// System.Void maxstAR.NativeAPI::maxst_GuideInfo_getTagAnchors(System.UInt64,System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_GuideInfo_getTagAnchors_mF729BEEB7CB35A14F3E3A5A300221AB9AFB2CEF1 (uint64_t ___GuideInfo_cPtr0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data1, int32_t ___length2, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (uint64_t, uint8_t*, int32_t);

	// Marshaling of parameter '___data1' to native representation
	uint8_t* ____data1_marshaled = NULL;
	if (___data1 != NULL)
	{
		____data1_marshaled = reinterpret_cast<uint8_t*>((___data1)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_GuideInfo_getTagAnchors)(___GuideInfo_cPtr0, ____data1_marshaled, ___length2);

}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_SurfaceThumbnail_getWidth(uint64_t);
// System.Int32 maxstAR.NativeAPI::maxst_SurfaceThumbnail_getWidth(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_SurfaceThumbnail_getWidth_m5374309D1ED7CE91A09C299E8C112E8F20344A74 (uint64_t ___SurfaceThumbnail_cPtr0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint64_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_SurfaceThumbnail_getWidth)(___SurfaceThumbnail_cPtr0);

	return returnValue;
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_SurfaceThumbnail_getHeight(uint64_t);
// System.Int32 maxstAR.NativeAPI::maxst_SurfaceThumbnail_getHeight(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_SurfaceThumbnail_getHeight_m68521172E9A9936326ED88B690E3E87E356000AE (uint64_t ___SurfaceThumbnail_cPtr0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint64_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_SurfaceThumbnail_getHeight)(___SurfaceThumbnail_cPtr0);

	return returnValue;
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_SurfaceThumbnail_getLength(uint64_t);
// System.Int32 maxstAR.NativeAPI::maxst_SurfaceThumbnail_getLength(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_SurfaceThumbnail_getLength_mCCC4613A0617DA641492CC0EC46F091D996725B8 (uint64_t ___SurfaceThumbnail_cPtr0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint64_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_SurfaceThumbnail_getLength)(___SurfaceThumbnail_cPtr0);

	return returnValue;
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_SurfaceThumbnail_getBpp(uint64_t);
// System.Int32 maxstAR.NativeAPI::maxst_SurfaceThumbnail_getBpp(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_SurfaceThumbnail_getBpp_m7BD8DCB38F1117093E38BF9E0996B0D3A829F026 (uint64_t ___SurfaceThumbnail_cPtr0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint64_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_SurfaceThumbnail_getBpp)(___SurfaceThumbnail_cPtr0);

	return returnValue;
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_SurfaceThumbnail_getData(uint64_t, uint8_t*, int32_t);
// System.Int32 maxstAR.NativeAPI::maxst_SurfaceThumbnail_getData(System.UInt64,System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_SurfaceThumbnail_getData_m72CC556F4243C8F4543F2902960EBB20C0117977 (uint64_t ___SurfaceThumbnail_cPtr0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data1, int32_t ___length2, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint64_t, uint8_t*, int32_t);

	// Marshaling of parameter '___data1' to native representation
	uint8_t* ____data1_marshaled = NULL;
	if (___data1 != NULL)
	{
		____data1_marshaled = reinterpret_cast<uint8_t*>((___data1)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_SurfaceThumbnail_getData)(___SurfaceThumbnail_cPtr0, ____data1_marshaled, ___length2);

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_SensorDevice_startSensor();
// System.Void maxstAR.NativeAPI::maxst_SensorDevice_startSensor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_SensorDevice_startSensor_mA7826A672D56DAB8BC392A3695B8E0C39752C85A (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_SensorDevice_startSensor)();

}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_SensorDevice_stopSensor();
// System.Void maxstAR.NativeAPI::maxst_SensorDevice_stopSensor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_SensorDevice_stopSensor_mB3D457114603E18053BC43A560581BE02752F5B6 (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_SensorDevice_stopSensor)();

}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_MapViewer_initialize(char*);
// System.Int32 maxstAR.NativeAPI::maxst_MapViewer_initialize(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_MapViewer_initialize_mE1D0936F186A301F61346D3DFEECC1A6A7BDE886 (String_t* ___fileName0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___fileName0' to native representation
	char* ____fileName0_marshaled = NULL;
	____fileName0_marshaled = il2cpp_codegen_marshal_string(___fileName0);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_MapViewer_initialize)(____fileName0_marshaled);

	// Marshaling cleanup of parameter '___fileName0' native representation
	il2cpp_codegen_marshal_free(____fileName0_marshaled);
	____fileName0_marshaled = NULL;

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_MapViewer_deInitialize();
// System.Void maxstAR.NativeAPI::maxst_MapViewer_deInitialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_MapViewer_deInitialize_m9803387B412EEA0C97F82BECF47CA2C4B90D3180 (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_MapViewer_deInitialize)();

}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_MapViewer_getJson(uint8_t*, int32_t);
// System.Void maxstAR.NativeAPI::maxst_MapViewer_getJson(System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_MapViewer_getJson_mFF2D1942D6D85FFDA8DBFBB37D9277D42CF4747E (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___jsonData0, int32_t ___length1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (uint8_t*, int32_t);

	// Marshaling of parameter '___jsonData0' to native representation
	uint8_t* ____jsonData0_marshaled = NULL;
	if (___jsonData0 != NULL)
	{
		____jsonData0_marshaled = reinterpret_cast<uint8_t*>((___jsonData0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_MapViewer_getJson)(____jsonData0_marshaled, ___length1);

}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_MapViewer_create(int32_t);
// System.Int32 maxstAR.NativeAPI::maxst_MapViewer_create(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_MapViewer_create_mA8A51DE668DA01D1EEFCD1CDE0607DD9E05A3EA6 (int32_t ___idx0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_MapViewer_create)(___idx0);

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_MapViewer_getIndices(int32_t*);
// System.Void maxstAR.NativeAPI::maxst_MapViewer_getIndices(System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_MapViewer_getIndices_m7BE3DB492739E0227CED9F498E42E7A0C76987B1 (int32_t* ___indices0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t*);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_MapViewer_getIndices)(___indices0);

}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_MapViewer_getTexCoords(float*);
// System.Void maxstAR.NativeAPI::maxst_MapViewer_getTexCoords(System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_MapViewer_getTexCoords_mC4F8441C2CEB2512FA632CBD82512FF06946429B (float* ___texCoords0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (float*);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_MapViewer_getTexCoords)(___texCoords0);

}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_MapViewer_getImageSize(int32_t);
// System.Int32 maxstAR.NativeAPI::maxst_MapViewer_getImageSize(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_MapViewer_getImageSize_m3E303CD3A68C838A42E359CD9AB18E29B05C12F7 (int32_t ___idx0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_MapViewer_getImageSize)(___idx0);

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_MapViewer_getImage(int32_t, uint8_t*);
// System.Void maxstAR.NativeAPI::maxst_MapViewer_getImage(System.Int32,System.Byte&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_MapViewer_getImage_m39F99EEC2A336BC6610FD511FE28038836936687 (int32_t ___idx0, uint8_t* ___image1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, uint8_t*);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_MapViewer_getImage)(___idx0, ___image1);

}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_WearableCalibration_isActivated();
// System.Boolean maxstAR.NativeAPI::maxst_WearableCalibration_isActivated()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeAPI_maxst_WearableCalibration_isActivated_m9143F692C8392D25C750D38EC06B266105B72EBD (const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_WearableCalibration_isActivated)();

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_WearableCalibration_init(char*);
// System.Boolean maxstAR.NativeAPI::maxst_WearableCalibration_init(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeAPI_maxst_WearableCalibration_init_m772CE19706D3AE2AB4E896585B5FB7EC8A4704AF (String_t* ___modelName0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___modelName0' to native representation
	char* ____modelName0_marshaled = NULL;
	____modelName0_marshaled = il2cpp_codegen_marshal_string(___modelName0);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_WearableCalibration_init)(____modelName0_marshaled);

	// Marshaling cleanup of parameter '___modelName0' native representation
	il2cpp_codegen_marshal_free(____modelName0_marshaled);
	____modelName0_marshaled = NULL;

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_WearableCalibration_deinit();
// System.Void maxstAR.NativeAPI::maxst_WearableCalibration_deinit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_WearableCalibration_deinit_m174A61A633706CCF0CCFAD86AF3CD91892AF8367 (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_WearableCalibration_deinit)();

}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_WearableCalibration_setSurfaceSize(int32_t, int32_t);
// System.Void maxstAR.NativeAPI::maxst_WearableCalibration_setSurfaceSize(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_WearableCalibration_setSurfaceSize_mE84BDEFC9470F81B1CAB9DF7A2092787A182DCB4 (int32_t ___width0, int32_t ___height1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_WearableCalibration_setSurfaceSize)(___width0, ___height1);

}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_WearableCalibration_getProjectionMatrix(float*, int32_t);
// System.Void maxstAR.NativeAPI::maxst_WearableCalibration_getProjectionMatrix(System.Single[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_WearableCalibration_getProjectionMatrix_mCD2BC624AB1D8DEFEADA180420AA58535AB0353A (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___projection0, int32_t ___eyeType1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (float*, int32_t);

	// Marshaling of parameter '___projection0' to native representation
	float* ____projection0_marshaled = NULL;
	if (___projection0 != NULL)
	{
		____projection0_marshaled = reinterpret_cast<float*>((___projection0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_WearableCalibration_getProjectionMatrix)(____projection0_marshaled, ___eyeType1);

}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_TrackedImage_getIndex(uint64_t);
// System.Int32 maxstAR.NativeAPI::maxst_TrackedImage_getIndex(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_TrackedImage_getIndex_mEEB773E5420A16C287B64850530C2430BCDF530E (uint64_t ___Image_cPtr0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint64_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_TrackedImage_getIndex)(___Image_cPtr0);

	return returnValue;
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_TrackedImage_getWidth(uint64_t);
// System.Int32 maxstAR.NativeAPI::maxst_TrackedImage_getWidth(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_TrackedImage_getWidth_m668590037D74A9A8529245D389A52E7FA74E5933 (uint64_t ___Image_cPtr0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint64_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_TrackedImage_getWidth)(___Image_cPtr0);

	return returnValue;
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_TrackedImage_getHeight(uint64_t);
// System.Int32 maxstAR.NativeAPI::maxst_TrackedImage_getHeight(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_TrackedImage_getHeight_m694B87A48F1267C12A88DCA22C5DFFD821C8E8A0 (uint64_t ___Image_cPtr0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint64_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_TrackedImage_getHeight)(___Image_cPtr0);

	return returnValue;
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_TrackedImage_getLength(uint64_t);
// System.Int32 maxstAR.NativeAPI::maxst_TrackedImage_getLength(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_TrackedImage_getLength_m4AD455C6AA5D2126478F92288A431367202CEFD2 (uint64_t ___Image_cPtr0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint64_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_TrackedImage_getLength)(___Image_cPtr0);

	return returnValue;
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL maxst_TrackedImage_getFormat(uint64_t);
// System.Int32 maxstAR.NativeAPI::maxst_TrackedImage_getFormat(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeAPI_maxst_TrackedImage_getFormat_m775B4491F0F0CDF95904BA5C7CCDCE8819ADFAA0 (uint64_t ___Image_cPtr0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint64_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_TrackedImage_getFormat)(___Image_cPtr0);

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_TrackedImage_getData(uint64_t, uint8_t*, int32_t);
// System.Void maxstAR.NativeAPI::maxst_TrackedImage_getData(System.UInt64,System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackedImage_getData_mD323FBE5A761CD2CB39DB663212524FB1FB71D95 (uint64_t ___Image_cPtr0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer1, int32_t ___size2, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (uint64_t, uint8_t*, int32_t);

	// Marshaling of parameter '___buffer1' to native representation
	uint8_t* ____buffer1_marshaled = NULL;
	if (___buffer1 != NULL)
	{
		____buffer1_marshaled = reinterpret_cast<uint8_t*>((___buffer1)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_TrackedImage_getData)(___Image_cPtr0, ____buffer1_marshaled, ___size2);

}
IL2CPP_EXTERN_C uint64_t DEFAULT_CALL maxst_TrackedImage_getDataPtr(uint64_t);
// System.UInt64 maxstAR.NativeAPI::maxst_TrackedImage_getDataPtr(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint64_t NativeAPI_maxst_TrackedImage_getDataPtr_mD62AC911086BD8845DBDB9547225539C37EFCA3F (uint64_t ___Image_cPtr0, const RuntimeMethod* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (uint64_t);

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_TrackedImage_getDataPtr)(___Image_cPtr0);

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL maxst_TrackedImage_getYuv420spY_UVPtr(uint64_t, intptr_t*, intptr_t*);
// System.Void maxstAR.NativeAPI::maxst_TrackedImage_getYuv420spY_UVPtr(System.UInt64,System.IntPtr&,System.IntPtr&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_maxst_TrackedImage_getYuv420spY_UVPtr_mC80CE7D9A89A78E3FF325ECB66907D7AEEAE418B (uint64_t ___Image_cPtr0, intptr_t* ___y_Ptr1, intptr_t* ___uv_Ptr2, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (uint64_t, intptr_t*, intptr_t*);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(maxst_TrackedImage_getYuv420spY_UVPtr)(___Image_cPtr0, ___y_Ptr1, ___uv_Ptr2);

}
IL2CPP_EXTERN_C uint64_t DEFAULT_CALL maxst_TrackedImage_getYuv420spY_U_VPtr(uint64_t, intptr_t*, intptr_t*, intptr_t*);
// System.UInt64 maxstAR.NativeAPI::maxst_TrackedImage_getYuv420spY_U_VPtr(System.UInt64,System.IntPtr&,System.IntPtr&,System.IntPtr&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint64_t NativeAPI_maxst_TrackedImage_getYuv420spY_U_VPtr_m906C3214FA840F6D0B696010ACB4BDCC10F771BD (uint64_t ___Image_cPtr0, intptr_t* ___y_Ptr1, intptr_t* ___u_Ptr2, intptr_t* ___v_Ptr3, const RuntimeMethod* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (uint64_t, intptr_t*, intptr_t*, intptr_t*);

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_TrackedImage_getYuv420spY_U_VPtr)(___Image_cPtr0, ___y_Ptr1, ___u_Ptr2, ___v_Ptr3);

	return returnValue;
}
IL2CPP_EXTERN_C uint64_t DEFAULT_CALL maxst_TrackedImage_getYuv420_888YUVPtr(uint64_t, intptr_t*, intptr_t*, intptr_t*, int32_t);
// System.UInt64 maxstAR.NativeAPI::maxst_TrackedImage_getYuv420_888YUVPtr(System.UInt64,System.IntPtr&,System.IntPtr&,System.IntPtr&,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint64_t NativeAPI_maxst_TrackedImage_getYuv420_888YUVPtr_mCF529A3F15BDD5D195375005588D18D1EA2863CC (uint64_t ___Image_cPtr0, intptr_t* ___y_Ptr1, intptr_t* ___u_Ptr2, intptr_t* ___v_Ptr3, bool ___supportRG16Texture4, const RuntimeMethod* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (uint64_t, intptr_t*, intptr_t*, intptr_t*, int32_t);

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(maxst_TrackedImage_getYuv420_888YUVPtr)(___Image_cPtr0, ___y_Ptr1, ___u_Ptr2, ___v_Ptr3, static_cast<int32_t>(___supportRG16Texture4));

	return returnValue;
}
// System.Void maxstAR.NativeAPI::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI__ctor_m128CF9DC4569AE87E3E6BFA215BA49606269D5C1 (NativeAPI_t0BA3114B7A4135C0933B0BE26E867AF74342D4BD * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void maxstAR.ObjectTrackableBehaviour::OnTrackSuccess(System.String,System.String,UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectTrackableBehaviour_OnTrackSuccess_m85E36967C60137B76E4BE3DDEE610A222D48EFC0 (ObjectTrackableBehaviour_t2137BE450300155EE7BD6A66AC50B25B5511C2A0 * __this, String_t* ___id0, String_t* ___name1, Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___poseMatrix2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectTrackableBehaviour_OnTrackSuccess_m85E36967C60137B76E4BE3DDEE610A222D48EFC0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* V_0 = NULL;
	RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* V_1 = NULL;
	int32_t V_2 = 0;
	ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* V_3 = NULL;
	{
		RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* L_0 = Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_m43A34BC668D3320EE3C92BC1BF613857DEA0154F(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_m43A34BC668D3320EE3C92BC1BF613857DEA0154F_RuntimeMethod_var);
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_1 = Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_mA906598D1F38C24AC5F6191124D6EE52D3F036D5(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_mA906598D1F38C24AC5F6191124D6EE52D3F036D5_RuntimeMethod_var);
		V_0 = L_1;
		V_1 = L_0;
		V_2 = 0;
		goto IL_0021;
	}

IL_0014:
	{
		RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		Renderer_set_enabled_m0933766657F2685BAAE3340B0A984C0E63925303(L_5, (bool)1, /*hidden argument*/NULL);
		int32_t L_6 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_0021:
	{
		int32_t L_7 = V_2;
		RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* L_8 = V_1;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_8)->max_length)))))))
		{
			goto IL_0014;
		}
	}
	{
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_9 = V_0;
		V_3 = L_9;
		V_2 = 0;
		goto IL_003a;
	}

IL_002d:
	{
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_10 = V_3;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_13);
		Collider_set_enabled_mF84DE8B0C8CAF33ACDB7F29BC055D9C8CFACB57B(L_13, (bool)1, /*hidden argument*/NULL);
		int32_t L_14 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_003a:
	{
		int32_t L_15 = V_2;
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_16 = V_3;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_16)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_17 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_18 = ___poseMatrix2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_19 = MatrixUtils_PositionFromMatrix_m7C6A297E614128E22FB75E01B62F47050B77EF2C(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_17, L_19, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_20 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_21 = ___poseMatrix2;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_22 = MatrixUtils_QuaternionFromMatrix_m9BD557FFF4F96359C9C363E6F31CDBADD5BD976D(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935(L_20, L_22, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_23 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_24 = ___poseMatrix2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_25 = MatrixUtils_ScaleFromMatrix_m807628CAC045208C9DA8BC8F23D1B1AE9A4CAFA4(L_24, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_23, L_25, /*hidden argument*/NULL);
		return;
	}
}
// System.Void maxstAR.ObjectTrackableBehaviour::OnTrackFail()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectTrackableBehaviour_OnTrackFail_mCBCC54510C6305A4A61CFA092F6B3AC0F0B8AF0A (ObjectTrackableBehaviour_t2137BE450300155EE7BD6A66AC50B25B5511C2A0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectTrackableBehaviour_OnTrackFail_mCBCC54510C6305A4A61CFA092F6B3AC0F0B8AF0A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* V_0 = NULL;
	RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* V_1 = NULL;
	int32_t V_2 = 0;
	ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* V_3 = NULL;
	{
		RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* L_0 = Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_m43A34BC668D3320EE3C92BC1BF613857DEA0154F(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_m43A34BC668D3320EE3C92BC1BF613857DEA0154F_RuntimeMethod_var);
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_1 = Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_mA906598D1F38C24AC5F6191124D6EE52D3F036D5(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_mA906598D1F38C24AC5F6191124D6EE52D3F036D5_RuntimeMethod_var);
		V_0 = L_1;
		V_1 = L_0;
		V_2 = 0;
		goto IL_0021;
	}

IL_0014:
	{
		RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		Renderer_set_enabled_m0933766657F2685BAAE3340B0A984C0E63925303(L_5, (bool)0, /*hidden argument*/NULL);
		int32_t L_6 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_0021:
	{
		int32_t L_7 = V_2;
		RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* L_8 = V_1;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_8)->max_length)))))))
		{
			goto IL_0014;
		}
	}
	{
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_9 = V_0;
		V_3 = L_9;
		V_2 = 0;
		goto IL_003a;
	}

IL_002d:
	{
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_10 = V_3;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_13);
		Collider_set_enabled_mF84DE8B0C8CAF33ACDB7F29BC055D9C8CFACB57B(L_13, (bool)0, /*hidden argument*/NULL);
		int32_t L_14 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_003a:
	{
		int32_t L_15 = V_2;
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_16 = V_3;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_16)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		return;
	}
}
// System.Void maxstAR.ObjectTrackableBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectTrackableBehaviour__ctor_mC54648BBEE976461518110A1F39D7BDA6E5C03EA (ObjectTrackableBehaviour_t2137BE450300155EE7BD6A66AC50B25B5511C2A0 * __this, const RuntimeMethod* method)
{
	{
		AbstractObjectTrackableBehaviour__ctor_m96A3DB625F240201C4E849BCD7ED1953C2C889EA(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void maxstAR.Point3Df::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Point3Df__ctor_m934B342CC4021CACF855676356D026CE130870B5 (Point3Df_t6B4A646639DE7A0C0BB07B29BE49F30B45433B94 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String maxstAR.QrCodeTrackableBehaviour::get_QrCodeSearchingWords()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* QrCodeTrackableBehaviour_get_QrCodeSearchingWords_m42AF3F5A445D1163AA6A36AA5021CB8E2BD596AF (QrCodeTrackableBehaviour_tC5612CD43E2F7043C9E80D17C92CBF696CED617E * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_qrCodeSearchingWords_12();
		return L_0;
	}
}
// System.Void maxstAR.QrCodeTrackableBehaviour::set_QrCodeSearchingWords(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void QrCodeTrackableBehaviour_set_QrCodeSearchingWords_m650B90F2DA2300225C2E2906DB4914CDF45A94FB (QrCodeTrackableBehaviour_tC5612CD43E2F7043C9E80D17C92CBF696CED617E * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_qrCodeSearchingWords_12(L_0);
		return;
	}
}
// System.Void maxstAR.QrCodeTrackableBehaviour::OnTrackSuccess(System.String,System.String,UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void QrCodeTrackableBehaviour_OnTrackSuccess_mA052BB30766CF011D64024713C64605925B98F63 (QrCodeTrackableBehaviour_tC5612CD43E2F7043C9E80D17C92CBF696CED617E * __this, String_t* ___id0, String_t* ___name1, Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___poseMatrix2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QrCodeTrackableBehaviour_OnTrackSuccess_mA052BB30766CF011D64024713C64605925B98F63_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* V_0 = NULL;
	RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* V_1 = NULL;
	int32_t V_2 = 0;
	ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* V_3 = NULL;
	{
		RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* L_0 = Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_m43A34BC668D3320EE3C92BC1BF613857DEA0154F(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_m43A34BC668D3320EE3C92BC1BF613857DEA0154F_RuntimeMethod_var);
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_1 = Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_mA906598D1F38C24AC5F6191124D6EE52D3F036D5(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_mA906598D1F38C24AC5F6191124D6EE52D3F036D5_RuntimeMethod_var);
		V_0 = L_1;
		V_1 = L_0;
		V_2 = 0;
		goto IL_0021;
	}

IL_0014:
	{
		RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		Renderer_set_enabled_m0933766657F2685BAAE3340B0A984C0E63925303(L_5, (bool)1, /*hidden argument*/NULL);
		int32_t L_6 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_0021:
	{
		int32_t L_7 = V_2;
		RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* L_8 = V_1;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_8)->max_length)))))))
		{
			goto IL_0014;
		}
	}
	{
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_9 = V_0;
		V_3 = L_9;
		V_2 = 0;
		goto IL_003a;
	}

IL_002d:
	{
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_10 = V_3;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_13);
		Collider_set_enabled_mF84DE8B0C8CAF33ACDB7F29BC055D9C8CFACB57B(L_13, (bool)1, /*hidden argument*/NULL);
		int32_t L_14 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_003a:
	{
		int32_t L_15 = V_2;
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_16 = V_3;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_16)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_17 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_18 = ___poseMatrix2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_19 = MatrixUtils_PositionFromMatrix_m7C6A297E614128E22FB75E01B62F47050B77EF2C(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_17, L_19, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_20 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_21 = ___poseMatrix2;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_22 = MatrixUtils_QuaternionFromMatrix_m9BD557FFF4F96359C9C363E6F31CDBADD5BD976D(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935(L_20, L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void maxstAR.QrCodeTrackableBehaviour::OnTrackFail()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void QrCodeTrackableBehaviour_OnTrackFail_m9F348B8961826820969AB9E16B5EE7588B841599 (QrCodeTrackableBehaviour_tC5612CD43E2F7043C9E80D17C92CBF696CED617E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QrCodeTrackableBehaviour_OnTrackFail_m9F348B8961826820969AB9E16B5EE7588B841599_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* V_0 = NULL;
	RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* V_1 = NULL;
	int32_t V_2 = 0;
	ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* V_3 = NULL;
	{
		RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* L_0 = Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_m43A34BC668D3320EE3C92BC1BF613857DEA0154F(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_m43A34BC668D3320EE3C92BC1BF613857DEA0154F_RuntimeMethod_var);
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_1 = Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_mA906598D1F38C24AC5F6191124D6EE52D3F036D5(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_mA906598D1F38C24AC5F6191124D6EE52D3F036D5_RuntimeMethod_var);
		V_0 = L_1;
		V_1 = L_0;
		V_2 = 0;
		goto IL_0021;
	}

IL_0014:
	{
		RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		Renderer_set_enabled_m0933766657F2685BAAE3340B0A984C0E63925303(L_5, (bool)0, /*hidden argument*/NULL);
		int32_t L_6 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_0021:
	{
		int32_t L_7 = V_2;
		RendererU5BU5D_t711BACBBBFC0E06179ADB8932DBA208665108C93* L_8 = V_1;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_8)->max_length)))))))
		{
			goto IL_0014;
		}
	}
	{
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_9 = V_0;
		V_3 = L_9;
		V_2 = 0;
		goto IL_003a;
	}

IL_002d:
	{
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_10 = V_3;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_13);
		Collider_set_enabled_mF84DE8B0C8CAF33ACDB7F29BC055D9C8CFACB57B(L_13, (bool)0, /*hidden argument*/NULL);
		int32_t L_14 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_003a:
	{
		int32_t L_15 = V_2;
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_16 = V_3;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_16)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		return;
	}
}
// System.Void maxstAR.QrCodeTrackableBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void QrCodeTrackableBehaviour__ctor_mF04F26B6F2853C13C716A9A6134558BDF0BA07BD (QrCodeTrackableBehaviour_tC5612CD43E2F7043C9E80D17C92CBF696CED617E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QrCodeTrackableBehaviour__ctor_mF04F26B6F2853C13C716A9A6134558BDF0BA07BD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_qrCodeSearchingWords_12(_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		AbstractQrCodeTrackableBehaviour__ctor_mE9A8DF0B052262EB04E75A2C2CC1D39AD77CACC2(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// maxstAR.SensorDevice maxstAR.SensorDevice::GetInstance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA * SensorDevice_GetInstance_m7F13A44D48036B73D11E9CD55A9EA7ABAE1D430F (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SensorDevice_GetInstance_m7F13A44D48036B73D11E9CD55A9EA7ABAE1D430F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA_il2cpp_TypeInfo_var);
		SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA * L_0 = ((SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA_StaticFields*)il2cpp_codegen_static_fields_for(SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA_il2cpp_TypeInfo_var))->get_instance_0();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA * L_1 = (SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA *)il2cpp_codegen_object_new(SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA_il2cpp_TypeInfo_var);
		SensorDevice__ctor_m7203FB445DDFA689277FCFDE08A9D69B89CA346C(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA_il2cpp_TypeInfo_var);
		((SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA_StaticFields*)il2cpp_codegen_static_fields_for(SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA_il2cpp_TypeInfo_var))->set_instance_0(L_1);
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA_il2cpp_TypeInfo_var);
		SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA * L_2 = ((SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA_StaticFields*)il2cpp_codegen_static_fields_for(SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA_il2cpp_TypeInfo_var))->get_instance_0();
		return L_2;
	}
}
// System.Void maxstAR.SensorDevice::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SensorDevice__ctor_m7203FB445DDFA689277FCFDE08A9D69B89CA346C (SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void maxstAR.SensorDevice::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SensorDevice_Start_m36EF038173578054700423CC007B17AC66DBD6D9 (SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA * __this, const RuntimeMethod* method)
{
	{
		NativeAPI_maxst_SensorDevice_startSensor_mA7826A672D56DAB8BC392A3695B8E0C39752C85A(/*hidden argument*/NULL);
		return;
	}
}
// System.Void maxstAR.SensorDevice::Stop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SensorDevice_Stop_mD2DE3E49BD054C25601EE1A274B7038C7BF7EABE (SensorDevice_t2CE6E4794B9F02A667F4A543142B4D746F093BAA * __this, const RuntimeMethod* method)
{
	{
		NativeAPI_maxst_SensorDevice_stopSensor_mB3D457114603E18053BC43A560581BE02752F5B6(/*hidden argument*/NULL);
		return;
	}
}
// System.Void maxstAR.SensorDevice::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SensorDevice__cctor_m821F54269B8B6BB3947D6EA253CCC98AABE2FEC0 (const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void maxstAR.SurfaceThumbnail::.ctor(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SurfaceThumbnail__ctor_m0DD58C3F68E19DECAA5378921816128B06360334 (SurfaceThumbnail_t101EA84FE2D39AF996B5F5F8E89E7DE3638DC824 * __this, uint64_t ___cPtr0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		uint64_t L_0 = ___cPtr0;
		__this->set_cPtr_0(L_0);
		return;
	}
}
// System.Int32 maxstAR.SurfaceThumbnail::GetWidth()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SurfaceThumbnail_GetWidth_m2DAAAE442D0F0D28B8ABB83DF3B96DAAAE74574D (SurfaceThumbnail_t101EA84FE2D39AF996B5F5F8E89E7DE3638DC824 * __this, const RuntimeMethod* method)
{
	{
		uint64_t L_0 = __this->get_cPtr_0();
		int32_t L_1 = NativeAPI_maxst_SurfaceThumbnail_getWidth_m5374309D1ED7CE91A09C299E8C112E8F20344A74(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 maxstAR.SurfaceThumbnail::GetHeight()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SurfaceThumbnail_GetHeight_m8DE428A2D928FA3960D8B1706012DAC5A4E4DD4B (SurfaceThumbnail_t101EA84FE2D39AF996B5F5F8E89E7DE3638DC824 * __this, const RuntimeMethod* method)
{
	{
		uint64_t L_0 = __this->get_cPtr_0();
		int32_t L_1 = NativeAPI_maxst_SurfaceThumbnail_getHeight_m68521172E9A9936326ED88B690E3E87E356000AE(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 maxstAR.SurfaceThumbnail::GetLength()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SurfaceThumbnail_GetLength_m2A9A3AC873AB4069E48B88AF8D1D24AF81FFB47F (SurfaceThumbnail_t101EA84FE2D39AF996B5F5F8E89E7DE3638DC824 * __this, const RuntimeMethod* method)
{
	{
		uint64_t L_0 = __this->get_cPtr_0();
		int32_t L_1 = NativeAPI_maxst_SurfaceThumbnail_getLength_mCCC4613A0617DA641492CC0EC46F091D996725B8(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Byte[] maxstAR.SurfaceThumbnail::GetData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* SurfaceThumbnail_GetData_mB4539CA663D4B080D677C046FFBBF33A03F3B428 (SurfaceThumbnail_t101EA84FE2D39AF996B5F5F8E89E7DE3638DC824 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SurfaceThumbnail_GetData_mB4539CA663D4B080D677C046FFBBF33A03F3B428_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_1 = NULL;
	{
		int32_t L_0 = SurfaceThumbnail_GetLength_m2A9A3AC873AB4069E48B88AF8D1D24AF81FFB47F(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)L_1);
		V_1 = L_2;
		uint64_t L_3 = __this->get_cPtr_0();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = V_1;
		int32_t L_5 = V_0;
		NativeAPI_maxst_SurfaceThumbnail_getData_m72CC556F4243C8F4543F2902960EBB20C0117977(L_3, L_4, L_5, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = V_1;
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String maxstAR.TagAnchor::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* TagAnchor_get_name_m99C9E247B0771C777A7A09CD8B3A2D2FE0964240 (TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CnameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void maxstAR.TagAnchor::set_name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TagAnchor_set_name_m443B327E4B533C8F67F3173D955C15526E1D0C79 (TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CnameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Single maxstAR.TagAnchor::get_positionX()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TagAnchor_get_positionX_m4BA6F4722CB2624F28E04F70B1B669E1D44C6115 (TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CpositionXU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void maxstAR.TagAnchor::set_positionX(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TagAnchor_set_positionX_m9C85C1BE8641AA561F5E78C35ECD34B47B2E7068 (TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CpositionXU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Single maxstAR.TagAnchor::get_positionY()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TagAnchor_get_positionY_mA4FECF70AA30253F3A9C8379DBF7C28E11FBC46D (TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CpositionYU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void maxstAR.TagAnchor::set_positionY(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TagAnchor_set_positionY_m44F3448EBACBE4404CC470ADEF20003BA65842CE (TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CpositionYU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Single maxstAR.TagAnchor::get_positionZ()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TagAnchor_get_positionZ_m43C90B29A0D5F78F8CBA8370B50CD1E678718671 (TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CpositionZU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void maxstAR.TagAnchor::set_positionZ(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TagAnchor_set_positionZ_m8930567E2FAD7386F0B5FEED38AC4B2097E528C4 (TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CpositionZU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Single maxstAR.TagAnchor::get_rotationX()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TagAnchor_get_rotationX_mE3F3509DE799F285E067105834716D2118880FD3 (TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CrotationXU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void maxstAR.TagAnchor::set_rotationX(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TagAnchor_set_rotationX_mC19A47B642FF9C74C1D9A42DEAA0CEBC527A0609 (TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CrotationXU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Single maxstAR.TagAnchor::get_rotationY()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TagAnchor_get_rotationY_m3C25FA7B23C3900A234BA4A424B11B1EAAA40741 (TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CrotationYU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void maxstAR.TagAnchor::set_rotationY(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TagAnchor_set_rotationY_m79330569E15C6C5D3FE5963589D34EC5B46B772A (TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CrotationYU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Single maxstAR.TagAnchor::get_rotationZ()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TagAnchor_get_rotationZ_m53B878CB1D016601745D41E200784B4E373A1D28 (TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CrotationZU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void maxstAR.TagAnchor::set_rotationZ(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TagAnchor_set_rotationZ_m4B0052670F90E53A36CB302B99CE9B4951DA0F70 (TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CrotationZU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Single maxstAR.TagAnchor::get_scaleX()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TagAnchor_get_scaleX_m2E449DC6C0A2DC69CC442A296EF8ABD66053361B (TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CscaleXU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void maxstAR.TagAnchor::set_scaleX(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TagAnchor_set_scaleX_m343BCB76E871D5290A302F91E938311D78E8485F (TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CscaleXU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Single maxstAR.TagAnchor::get_scaleY()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TagAnchor_get_scaleY_m390023FE173DD6CB7DE0A8A109299F2E9D2B3922 (TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CscaleYU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void maxstAR.TagAnchor::set_scaleY(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TagAnchor_set_scaleY_m1F81421BD263D9D41332AFBC86CA014D56721256 (TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CscaleYU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Single maxstAR.TagAnchor::get_scaleZ()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TagAnchor_get_scaleZ_m772C067FCBA84BCFD7058A1F284253669FE30C6B (TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CscaleZU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void maxstAR.TagAnchor::set_scaleZ(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TagAnchor_set_scaleZ_m75F59D542E2F765CE2C0895282807A30F3238500 (TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CscaleZU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Void maxstAR.TagAnchor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TagAnchor__ctor_mD1A039D323CC5DBA758D093572C4E6BE8CCCE56B (TagAnchor_t62343CB4D8763132347C18557B96636FD5548D50 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void maxstAR.TagAnchors::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TagAnchors__ctor_m5DB5FB2917283C43ACC648245EC4E876284AAB1F (TagAnchors_t475197F1A136EB47B1E2A2C92D7F1A1C2091311E * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void maxstAR.Trackable::.ctor(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Trackable__ctor_m28195F5B5619EBC04AF14B18F8D5773F9A37B195 (Trackable_t525A99BA508C48FC5286F43525D4C12CB6CD1D27 * __this, uint64_t ___cPtr0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Trackable__ctor_m28195F5B5619EBC04AF14B18F8D5773F9A37B195_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_0 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)SZArrayNew(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16));
		__this->set_glPoseMatrix_1(L_0);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)100));
		__this->set_idBytes_2(L_1);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)100));
		__this->set_nameBytes_3(L_2);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)100));
		__this->set_cloudNameBytes_4(L_3);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)1048576));
		__this->set_cloudMetaBytes_5(L_4);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		uint64_t L_5 = ___cPtr0;
		__this->set_cPtr_0(L_5);
		return;
	}
}
// System.String maxstAR.Trackable::GetId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Trackable_GetId_mA44F5BE3181F6C793190AF897E101D04DB40BCA9 (Trackable_t525A99BA508C48FC5286F43525D4C12CB6CD1D27 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Trackable_GetId_mA44F5BE3181F6C793190AF897E101D04DB40BCA9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = __this->get_idBytes_2();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = __this->get_idBytes_2();
		NullCheck(L_1);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_0, 0, (((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length)))), /*hidden argument*/NULL);
		uint64_t L_2 = __this->get_cPtr_0();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = __this->get_idBytes_2();
		NativeAPI_maxst_Trackable_getId_m65E02BA95C26BC662C8DE07DBCB80FF71F34A7F2(L_2, L_3, /*hidden argument*/NULL);
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_4 = Encoding_get_UTF8_m67C8652936B681E7BC7505E459E88790E0FF16D9(/*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = __this->get_idBytes_2();
		NullCheck(L_4);
		String_t* L_6 = VirtFuncInvoker1< String_t*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(33 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_4, L_5);
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_7 = (CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)SZArrayNew(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2_il2cpp_TypeInfo_var, (uint32_t)1);
		NullCheck(L_6);
		String_t* L_8 = String_TrimEnd_m8D4905B71A4AEBF9D0BC36C6003FC9A5AD630403(L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.String maxstAR.Trackable::GetName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Trackable_GetName_mD9121445DC81017509E298724A29BF043A619D8D (Trackable_t525A99BA508C48FC5286F43525D4C12CB6CD1D27 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Trackable_GetName_mD9121445DC81017509E298724A29BF043A619D8D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = __this->get_nameBytes_3();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = __this->get_nameBytes_3();
		NullCheck(L_1);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_0, 0, (((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length)))), /*hidden argument*/NULL);
		uint64_t L_2 = __this->get_cPtr_0();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = __this->get_nameBytes_3();
		NativeAPI_maxst_Trackable_getName_mDEAA3725E5E6CB1E45555AACC612F1F1BDAD114F(L_2, L_3, /*hidden argument*/NULL);
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_4 = Encoding_get_UTF8_m67C8652936B681E7BC7505E459E88790E0FF16D9(/*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = __this->get_nameBytes_3();
		NullCheck(L_4);
		String_t* L_6 = VirtFuncInvoker1< String_t*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(33 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_4, L_5);
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_7 = (CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)SZArrayNew(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2_il2cpp_TypeInfo_var, (uint32_t)1);
		NullCheck(L_6);
		String_t* L_8 = String_TrimEnd_m8D4905B71A4AEBF9D0BC36C6003FC9A5AD630403(L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.String maxstAR.Trackable::GetCloudName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Trackable_GetCloudName_m591936B50E988289EBA0BDF07AB98935099EF404 (Trackable_t525A99BA508C48FC5286F43525D4C12CB6CD1D27 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Trackable_GetCloudName_m591936B50E988289EBA0BDF07AB98935099EF404_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = __this->get_cloudNameBytes_4();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = __this->get_cloudNameBytes_4();
		NullCheck(L_1);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_0, 0, (((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length)))), /*hidden argument*/NULL);
		uint64_t L_2 = __this->get_cPtr_0();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = __this->get_cloudNameBytes_4();
		NativeAPI_maxst_Trackable_getCloudName_m45027EEE9B6AB68067EB5C233B50B1BEE93ECE3D(L_2, L_3, /*hidden argument*/NULL);
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_4 = Encoding_get_UTF8_m67C8652936B681E7BC7505E459E88790E0FF16D9(/*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = __this->get_cloudNameBytes_4();
		NullCheck(L_4);
		String_t* L_6 = VirtFuncInvoker1< String_t*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(33 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_4, L_5);
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_7 = (CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)SZArrayNew(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2_il2cpp_TypeInfo_var, (uint32_t)1);
		NullCheck(L_6);
		String_t* L_8 = String_TrimEnd_m8D4905B71A4AEBF9D0BC36C6003FC9A5AD630403(L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.String maxstAR.Trackable::GetCloudMeta()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Trackable_GetCloudMeta_m6B439B693DA1C113940981E0A27CABEAB5D90152 (Trackable_t525A99BA508C48FC5286F43525D4C12CB6CD1D27 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Trackable_GetCloudMeta_m6B439B693DA1C113940981E0A27CABEAB5D90152_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* V_0 = NULL;
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = __this->get_cloudMetaBytes_5();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = __this->get_cloudMetaBytes_5();
		NullCheck(L_1);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_0, 0, (((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length)))), /*hidden argument*/NULL);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_2 = (Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)SZArrayNew(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83_il2cpp_TypeInfo_var, (uint32_t)1);
		V_0 = L_2;
		uint64_t L_3 = __this->get_cPtr_0();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = __this->get_cloudMetaBytes_5();
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_5 = V_0;
		NativeAPI_maxst_Trackable_getCloudMeta_mEE9F177E4A24BD1FCF987F11028E96FCD17C6515(L_3, L_4, L_5, /*hidden argument*/NULL);
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_6 = Encoding_get_UTF8_m67C8652936B681E7BC7505E459E88790E0FF16D9(/*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = __this->get_cloudMetaBytes_5();
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		int32_t L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_6);
		String_t* L_11 = VirtFuncInvoker3< String_t*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t >::Invoke(34 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_6, L_7, 0, L_10);
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_12 = (CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)SZArrayNew(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2_il2cpp_TypeInfo_var, (uint32_t)1);
		NullCheck(L_11);
		String_t* L_13 = String_TrimEnd_m8D4905B71A4AEBF9D0BC36C6003FC9A5AD630403(L_11, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// UnityEngine.Matrix4x4 maxstAR.Trackable::GetPose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  Trackable_GetPose_m755CCDD0E4B0A606A6C5DDEB16F1C3775030FF25 (Trackable_t525A99BA508C48FC5286F43525D4C12CB6CD1D27 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Trackable_GetPose_m755CCDD0E4B0A606A6C5DDEB16F1C3775030FF25_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		uint64_t L_0 = __this->get_cPtr_0();
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_1 = __this->get_glPoseMatrix_1();
		NativeAPI_maxst_Trackable_getPose_m656155FAC79B8376B46E70AD1B284C27F088DB82(L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3_il2cpp_TypeInfo_var);
		AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3 * L_2 = AbstractARManager_get_Instance_mC07EB5ACE0F0F4833142661BD1C74554899EB94E(/*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = AbstractARManager_get_WorldCenterModeSetting_mCF8F7602828C0F38B39A879BB852DB2EA48FAD5C_inline(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_il2cpp_TypeInfo_var);
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_4 = Matrix4x4_get_identity_mA0CECDE2A5E85CF014375084624F3770B5B7B79B(/*hidden argument*/NULL);
		return L_4;
	}

IL_0024:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_5 = __this->get_glPoseMatrix_1();
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_6 = MatrixUtils_GetUnityPoseMatrix_m3F6D97B070AB4B2518E76AF1AE4653F936D0B7F5(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_7 = Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05((90.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		V_1 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3_il2cpp_TypeInfo_var);
		AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3 * L_8 = AbstractARManager_get_Instance_mC07EB5ACE0F0F4833142661BD1C74554899EB94E(/*hidden argument*/NULL);
		NullCheck(L_8);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_9 = AbstractARManager_GetARCamera_m40EF848C5AE6860CF3081C0572C016490F2D3748_inline(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_10, /*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_12 = V_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13;
		memset((&L_13), 0, sizeof(L_13));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_13), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_il2cpp_TypeInfo_var);
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_14 = Matrix4x4_TRS_m5BB2EBA1152301BAC92FDC7F33ECA732BAE57990(L_11, L_12, L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_15 = V_2;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_16 = V_0;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_17 = Matrix4x4_op_Multiply_mF6693A950E1917204E356366892C3CCB0553436E(L_15, L_16, /*hidden argument*/NULL);
		V_0 = L_17;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_18 = Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05((-90.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		V_1 = L_18;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_19 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_20 = V_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_21;
		memset((&L_21), 0, sizeof(L_21));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_21), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_22 = Matrix4x4_TRS_m5BB2EBA1152301BAC92FDC7F33ECA732BAE57990(L_19, L_20, L_21, /*hidden argument*/NULL);
		V_2 = L_22;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_23 = V_0;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_24 = V_2;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_25 = Matrix4x4_op_Multiply_mF6693A950E1917204E356366892C3CCB0553436E(L_23, L_24, /*hidden argument*/NULL);
		V_0 = L_25;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_26 = V_0;
		return L_26;
	}
}
// UnityEngine.Matrix4x4 maxstAR.Trackable::GetTargetPose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  Trackable_GetTargetPose_mB77AB3E7D80F5057056E86CE1FA689C6428BEF21 (Trackable_t525A99BA508C48FC5286F43525D4C12CB6CD1D27 * __this, const RuntimeMethod* method)
{
	{
		uint64_t L_0 = __this->get_cPtr_0();
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_1 = __this->get_glPoseMatrix_1();
		NativeAPI_maxst_Trackable_getPose_m656155FAC79B8376B46E70AD1B284C27F088DB82(L_0, L_1, /*hidden argument*/NULL);
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_2 = __this->get_glPoseMatrix_1();
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_3 = MatrixUtils_GetUnityPoseMatrix_m3F6D97B070AB4B2518E76AF1AE4653F936D0B7F5(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single maxstAR.Trackable::GetWidth()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Trackable_GetWidth_mEF8DC3868A6261E258DD397994C1E9776720A4EF (Trackable_t525A99BA508C48FC5286F43525D4C12CB6CD1D27 * __this, const RuntimeMethod* method)
{
	{
		uint64_t L_0 = __this->get_cPtr_0();
		float L_1 = NativeAPI_maxst_Trackable_getWidth_m98564B655F63F5CF6674E8EDA4F8BD0E4FB5A715(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single maxstAR.Trackable::GetHeight()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Trackable_GetHeight_m793558B204134A0CED986087BEA5F88770541607 (Trackable_t525A99BA508C48FC5286F43525D4C12CB6CD1D27 * __this, const RuntimeMethod* method)
{
	{
		uint64_t L_0 = __this->get_cPtr_0();
		float L_1 = NativeAPI_maxst_Trackable_getHeight_mD2C8D6D0DAEA052AFFDAF7E886EA4658A54D40AE(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void maxstAR.TrackedImage::.ctor(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackedImage__ctor_mD5DB1118197FD89E30C46B0AA42CC748E4928BD2 (TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1 * __this, uint64_t ___cPtr0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		uint64_t L_0 = ___cPtr0;
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return;
	}

IL_000a:
	{
		uint64_t L_1 = ___cPtr0;
		__this->set_trackedImageCPtr_1(L_1);
		uint64_t L_2 = __this->get_trackedImageCPtr_1();
		int32_t L_3 = NativeAPI_maxst_TrackedImage_getIndex_mEEB773E5420A16C287B64850530C2430BCDF530E(L_2, /*hidden argument*/NULL);
		__this->set_index_2(L_3);
		uint64_t L_4 = __this->get_trackedImageCPtr_1();
		int32_t L_5 = NativeAPI_maxst_TrackedImage_getWidth_m668590037D74A9A8529245D389A52E7FA74E5933(L_4, /*hidden argument*/NULL);
		__this->set_width_3(L_5);
		uint64_t L_6 = __this->get_trackedImageCPtr_1();
		int32_t L_7 = NativeAPI_maxst_TrackedImage_getHeight_m694B87A48F1267C12A88DCA22C5DFFD821C8E8A0(L_6, /*hidden argument*/NULL);
		__this->set_height_4(L_7);
		uint64_t L_8 = __this->get_trackedImageCPtr_1();
		int32_t L_9 = NativeAPI_maxst_TrackedImage_getLength_m4AD455C6AA5D2126478F92288A431367202CEFD2(L_8, /*hidden argument*/NULL);
		__this->set_length_5(L_9);
		uint64_t L_10 = __this->get_trackedImageCPtr_1();
		int32_t L_11 = NativeAPI_maxst_TrackedImage_getFormat_m775B4491F0F0CDF95904BA5C7CCDCE8819ADFAA0(L_10, /*hidden argument*/NULL);
		__this->set_colorFormat_6(L_11);
		return;
	}
}
// System.Void maxstAR.TrackedImage::.ctor(System.UInt64,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackedImage__ctor_m0F370A499837DC0DD4580FEE1B99C728F4B8CE2E (TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1 * __this, uint64_t ___cPtr0, bool ___splitYuv1, const RuntimeMethod* method)
{
	{
		uint64_t L_0 = ___cPtr0;
		TrackedImage__ctor_mD5DB1118197FD89E30C46B0AA42CC748E4928BD2(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = ___splitYuv1;
		__this->set_splitYuv_7(L_1);
		return;
	}
}
// System.Int32 maxstAR.TrackedImage::GetIndex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TrackedImage_GetIndex_m5D91235D3021505E5E96F1D3051D4669ABD6E906 (TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_index_2();
		return L_0;
	}
}
// System.Int32 maxstAR.TrackedImage::GetWidth()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TrackedImage_GetWidth_m1FAE46C61FDDB4C057A115D3FC9533E93091102D (TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_width_3();
		return L_0;
	}
}
// System.Int32 maxstAR.TrackedImage::GetHeight()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TrackedImage_GetHeight_m45AF66C5B35A8BEA2FC4E67C75015219043FFB8A (TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_height_4();
		return L_0;
	}
}
// System.Int32 maxstAR.TrackedImage::GetLength()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TrackedImage_GetLength_m6B837456F54502CC5F96968B358FB3322E71E3E6 (TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_length_5();
		return L_0;
	}
}
// maxstAR.ColorFormat maxstAR.TrackedImage::GetFormat()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TrackedImage_GetFormat_m933A4B083B65AF504D206645349A06EBC352F88B (TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_colorFormat_6();
		return L_0;
	}
}
// System.UInt64 maxstAR.TrackedImage::GetImageCptr()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint64_t TrackedImage_GetImageCptr_mF7E979531F3221DB5537277E7B8A62DCD0EAC8C0 (TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1 * __this, const RuntimeMethod* method)
{
	{
		uint64_t L_0 = __this->get_trackedImageCPtr_1();
		return L_0;
	}
}
// System.Byte[] maxstAR.TrackedImage::GetData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* TrackedImage_GetData_mAC39DBDDAF5858CC99748102BC56B13BB2DA3724 (TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackedImage_GetData_mAC39DBDDAF5858CC99748102BC56B13BB2DA3724_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_length_5();
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)NULL;
	}

IL_000a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = ((TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1_StaticFields*)il2cpp_codegen_static_fields_for(TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1_il2cpp_TypeInfo_var))->get_data_0();
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_2 = __this->get_length_5();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)L_2);
		IL2CPP_RUNTIME_CLASS_INIT(TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1_il2cpp_TypeInfo_var);
		((TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1_StaticFields*)il2cpp_codegen_static_fields_for(TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1_il2cpp_TypeInfo_var))->set_data_0(L_3);
	}

IL_0021:
	{
		uint64_t L_4 = __this->get_trackedImageCPtr_1();
		IL2CPP_RUNTIME_CLASS_INIT(TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = ((TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1_StaticFields*)il2cpp_codegen_static_fields_for(TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1_il2cpp_TypeInfo_var))->get_data_0();
		int32_t L_6 = __this->get_length_5();
		NativeAPI_maxst_TrackedImage_getData_mD323FBE5A761CD2CB39DB663212524FB1FB71D95(L_4, L_5, L_6, /*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = ((TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1_StaticFields*)il2cpp_codegen_static_fields_for(TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1_il2cpp_TypeInfo_var))->get_data_0();
		return L_7;
	}
}
// System.IntPtr maxstAR.TrackedImage::GetDataPtr()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t TrackedImage_GetDataPtr_mD5C6FA76DFF8AADD046F0E08EEFDE4D478B1719C (TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackedImage_GetDataPtr_mD5C6FA76DFF8AADD046F0E08EEFDE4D478B1719C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_length_5();
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		return (intptr_t)(0);
	}

IL_000e:
	{
		uint64_t L_1 = __this->get_trackedImageCPtr_1();
		uint64_t L_2 = NativeAPI_maxst_TrackedImage_getDataPtr_mD62AC911086BD8845DBDB9547225539C37EFCA3F(L_1, /*hidden argument*/NULL);
		intptr_t L_3 = IntPtr_op_Explicit_m534152049CE3084CEFA1CD8B1BA74F3C085A2ABF(L_2, /*hidden argument*/NULL);
		return (intptr_t)L_3;
	}
}
// System.Void maxstAR.TrackedImage::GetYuv420spYUVPtr(System.IntPtr&,System.IntPtr&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackedImage_GetYuv420spYUVPtr_mB5B2AEE498E9F78116A88FCADE165F213D54EB6F (TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1 * __this, intptr_t* ___yPtr0, intptr_t* ___uvPtr1, const RuntimeMethod* method)
{
	{
		uint64_t L_0 = __this->get_trackedImageCPtr_1();
		intptr_t* L_1 = ___yPtr0;
		intptr_t* L_2 = ___uvPtr1;
		NativeAPI_maxst_TrackedImage_getYuv420spY_UVPtr_mC80CE7D9A89A78E3FF325ECB66907D7AEEAE418B(L_0, (intptr_t*)L_1, (intptr_t*)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void maxstAR.TrackedImage::GetYuv420spYUVPtr(System.IntPtr&,System.IntPtr&,System.IntPtr&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackedImage_GetYuv420spYUVPtr_m77B51E991E3C6BBA78F607391FA6EF22A0A9E418 (TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1 * __this, intptr_t* ___yPtr0, intptr_t* ___uPtr1, intptr_t* ___vPtr2, const RuntimeMethod* method)
{
	{
		uint64_t L_0 = __this->get_trackedImageCPtr_1();
		intptr_t* L_1 = ___yPtr0;
		intptr_t* L_2 = ___uPtr1;
		intptr_t* L_3 = ___vPtr2;
		NativeAPI_maxst_TrackedImage_getYuv420spY_U_VPtr_m906C3214FA840F6D0B696010ACB4BDCC10F771BD(L_0, (intptr_t*)L_1, (intptr_t*)L_2, (intptr_t*)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void maxstAR.TrackedImage::GetYuv420_888YUVPtr(System.IntPtr&,System.IntPtr&,System.IntPtr&,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackedImage_GetYuv420_888YUVPtr_mC60F7DF572FABF68ED9B7F6B85807FEDDA305A8A (TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1 * __this, intptr_t* ___yPtr0, intptr_t* ___uPtr1, intptr_t* ___vPtr2, bool ___support16bitUVTexture3, const RuntimeMethod* method)
{
	{
		uint64_t L_0 = __this->get_trackedImageCPtr_1();
		intptr_t* L_1 = ___yPtr0;
		intptr_t* L_2 = ___uPtr1;
		intptr_t* L_3 = ___vPtr2;
		bool L_4 = ___support16bitUVTexture3;
		NativeAPI_maxst_TrackedImage_getYuv420_888YUVPtr_mCF529A3F15BDD5D195375005588D18D1EA2863CC(L_0, (intptr_t*)L_1, (intptr_t*)L_2, (intptr_t*)L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void maxstAR.TrackedImage::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackedImage__cctor_mF38C28B4C55A79C5699CD5F605CBAD7668926A4B (const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// maxstAR.TrackerManager maxstAR.TrackerManager::GetInstance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * TrackerManager_GetInstance_m46A27E965CA5109148150899D4CD2B26CA6F53E6 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackerManager_GetInstance_m46A27E965CA5109148150899D4CD2B26CA6F53E6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5_il2cpp_TypeInfo_var);
		TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * L_0 = ((TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5_StaticFields*)il2cpp_codegen_static_fields_for(TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5_il2cpp_TypeInfo_var))->get_instance_8();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * L_1 = (TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 *)il2cpp_codegen_object_new(TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5_il2cpp_TypeInfo_var);
		TrackerManager__ctor_m170A81D4AD8E76A78178882F1CC4CD2C7959D3A5(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5_il2cpp_TypeInfo_var);
		((TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5_StaticFields*)il2cpp_codegen_static_fields_for(TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5_il2cpp_TypeInfo_var))->set_instance_8(L_1);
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5_il2cpp_TypeInfo_var);
		TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * L_2 = ((TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5_StaticFields*)il2cpp_codegen_static_fields_for(TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5_il2cpp_TypeInfo_var))->get_instance_8();
		return L_2;
	}
}
// System.Void maxstAR.TrackerManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackerManager__ctor_m170A81D4AD8E76A78178882F1CC4CD2C7959D3A5 (TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackerManager__ctor_m170A81D4AD8E76A78178882F1CC4CD2C7959D3A5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_0 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)SZArrayNew(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16));
		__this->set_glPoseMatrix_9(L_0);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void maxstAR.TrackerManager::InitializeCloud()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackerManager_InitializeCloud_m16BEED93FCF7207B81027A789BC393248ECB615A (TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackerManager_InitializeCloud_m16BEED93FCF7207B81027A789BC393248ECB615A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MaxstSingleton_1_t0187F7FDC19F311CF5D000320AA39A295136CFDB_il2cpp_TypeInfo_var);
		CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * L_0 = MaxstSingleton_1_get_Instance_m1346F6D4BE1CE718B2CD992D56A6E6FB8900A341(/*hidden argument*/MaxstSingleton_1_get_Instance_m1346F6D4BE1CE718B2CD992D56A6E6FB8900A341_RuntimeMethod_var);
		__this->set_cloudRecognitionController_15(L_0);
		return;
	}
}
// System.Void maxstAR.TrackerManager::SetCloudRecognitionSecretIdAndSecretKey(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackerManager_SetCloudRecognitionSecretIdAndSecretKey_m4D1B3EF4FB196AA715B5F3CDCBC080E649045429 (TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * __this, String_t* ___secretId0, String_t* ___secretKey1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___secretId0;
		__this->set_secretId_12(L_0);
		String_t* L_1 = ___secretKey1;
		__this->set_secretKey_13(L_1);
		return;
	}
}
// System.Void maxstAR.TrackerManager::StartTracker(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackerManager_StartTracker_mEDE0B41014FC73A9DDD6F2FCB60BB548EDD69088 (TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * __this, int32_t ___trackerType0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackerManager_StartTracker_mEDE0B41014FC73A9DDD6F2FCB60BB548EDD69088_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___trackerType0;
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)48)))))
		{
			goto IL_0044;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5_il2cpp_TypeInfo_var);
		TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * L_1 = ((TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5_StaticFields*)il2cpp_codegen_static_fields_for(TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5_il2cpp_TypeInfo_var))->get_instance_8();
		NullCheck(L_1);
		CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * L_2 = L_1->get_cloudRecognitionController_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_2, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5_il2cpp_TypeInfo_var);
		TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * L_4 = ((TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5_StaticFields*)il2cpp_codegen_static_fields_for(TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5_il2cpp_TypeInfo_var))->get_instance_8();
		NullCheck(L_4);
		TrackerManager_InitializeCloud_m16BEED93FCF7207B81027A789BC393248ECB615A(L_4, /*hidden argument*/NULL);
	}

IL_0021:
	{
		CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * L_5 = __this->get_cloudRecognitionController_15();
		String_t* L_6 = __this->get_secretId_12();
		String_t* L_7 = __this->get_secretKey_13();
		NullCheck(L_5);
		CloudRecognitionController_SetCloudRecognitionSecretIdAndSecretKey_mC7C9F09C20600730341C96E2038A35258DD4EC27(L_5, L_6, L_7, /*hidden argument*/NULL);
		CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * L_8 = __this->get_cloudRecognitionController_15();
		NullCheck(L_8);
		CloudRecognitionController_StartTracker_m422BD955682D9242C165CB4AE1BC99754D52FB3C(L_8, /*hidden argument*/NULL);
		return;
	}

IL_0044:
	{
		int32_t L_9 = ___trackerType0;
		NativeAPI_maxst_TrackerManager_startTracker_mD2A013E2FC6B619155F37192F9AB7DABB44CB121(L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void maxstAR.TrackerManager::StopTracker()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackerManager_StopTracker_m8A7C1E8BA74DA0EF19A7A822B17DCEF17420C165 (TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackerManager_StopTracker_m8A7C1E8BA74DA0EF19A7A822B17DCEF17420C165_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * L_0 = __this->get_cloudRecognitionController_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * L_2 = __this->get_cloudRecognitionController_15();
		NullCheck(L_2);
		CloudRecognitionController_StopTracker_mC79E9729FE27C56117D2EB767CA02D329A9E6678(L_2, /*hidden argument*/NULL);
	}

IL_0019:
	{
		NativeAPI_maxst_TrackerManager_stopTracker_m2AC86CAD2BE61A2E1451B47F8FA2488D813989F2(/*hidden argument*/NULL);
		return;
	}
}
// System.Void maxstAR.TrackerManager::DestroyTracker()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackerManager_DestroyTracker_mF416B8798B95E8E4309A55AFEA7C2E437DA16DC1 (TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackerManager_DestroyTracker_mF416B8798B95E8E4309A55AFEA7C2E437DA16DC1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * L_0 = __this->get_cloudRecognitionController_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * L_2 = __this->get_cloudRecognitionController_15();
		NullCheck(L_2);
		CloudRecognitionController_DestroyTracker_mDE30FC70302DD304368995A083933CF3C5C6EC1C(L_2, /*hidden argument*/NULL);
	}

IL_0019:
	{
		NativeAPI_maxst_TrackerManager_destroyTracker_m1174ED1C73195AEE3AB49531497301A9D9366262(/*hidden argument*/NULL);
		return;
	}
}
// System.Void maxstAR.TrackerManager::AddTrackerData(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackerManager_AddTrackerData_mAC9FB7FDD7E4128DB351527636D9A2BA2CC9702C (TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * __this, String_t* ___trackingFileName0, bool ___isAndroidAssetFile1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___trackingFileName0;
		bool L_1 = ___isAndroidAssetFile1;
		NativeAPI_maxst_TrackerManager_addTrackerData_m51C22A522B54749FDBC43568D1B0BAE9E3B6C497(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void maxstAR.TrackerManager::RemoveTrackerData(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackerManager_RemoveTrackerData_mF161537F0A3A302653945CAFFE25468A43D82F5E (TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * __this, String_t* ___trackingFileName0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___trackingFileName0;
		NativeAPI_maxst_TrackerManager_removeTrackerData_mEB0715C35637542C19BEC1A33417F97CC370369B(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void maxstAR.TrackerManager::LoadTrackerData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackerManager_LoadTrackerData_m45AF37872DD6F8EC6DB3BAEE185348EC314B7099 (TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * __this, const RuntimeMethod* method)
{
	{
		NativeAPI_maxst_TrackerManager_loadTrackerData_mD7B98D711F885256BE5A3E37221387D6CB67EBAF(/*hidden argument*/NULL);
		return;
	}
}
// System.Void maxstAR.TrackerManager::SetTrackingOption(maxstAR.TrackerManager_TrackingOption)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackerManager_SetTrackingOption_m5AD2D75526B7CC533691AD083604EF3469B76538 (TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * __this, int32_t ___trackingOption0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackerManager_SetTrackingOption_m5AD2D75526B7CC533691AD083604EF3469B76538_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___trackingOption0;
		if ((((int32_t)L_0) == ((int32_t)((int32_t)32))))
		{
			goto IL_000a;
		}
	}
	{
		int32_t L_1 = ___trackingOption0;
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)64)))))
		{
			goto IL_0029;
		}
	}

IL_000a:
	{
		int32_t L_2 = ___trackingOption0;
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)32)))))
		{
			goto IL_001c;
		}
	}
	{
		CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * L_3 = __this->get_cloudRecognitionController_15();
		NullCheck(L_3);
		CloudRecognitionController_SetAutoEnable_mDE82AB035CC5D2A1A17710F5157AF552E9E44886_inline(L_3, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_001c:
	{
		CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * L_4 = __this->get_cloudRecognitionController_15();
		NullCheck(L_4);
		CloudRecognitionController_SetAutoEnable_mDE82AB035CC5D2A1A17710F5157AF552E9E44886_inline(L_4, (bool)1, /*hidden argument*/NULL);
		return;
	}

IL_0029:
	{
		int32_t L_5 = ___trackingOption0;
		NativeAPI_maxst_TrackerManager_setTrackingOption_m002A7BE8981B92CF58464C56E6C7BD699699FADB(L_5, /*hidden argument*/NULL);
		int32_t L_6 = ___trackingOption0;
		if ((!(((uint32_t)L_6) == ((uint32_t)4))))
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3_il2cpp_TypeInfo_var);
		AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3 * L_7 = AbstractARManager_get_Instance_mC07EB5ACE0F0F4833142661BD1C74554899EB94E(/*hidden argument*/NULL);
		NullCheck(L_7);
		AbstractARManager_SetWorldCenterMode_m39E9C716D2DDF61F6B7C0D1CD2BA1B5D3BCD538E_inline(L_7, 0, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Boolean maxstAR.TrackerManager::IsTrackerDataLoadCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TrackerManager_IsTrackerDataLoadCompleted_mAE91E8657349A70A17422998C138055642A9BCB0 (TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = NativeAPI_maxst_TrackerManager_isTrackerDataLoadCompleted_mE1E670296E50E476D4434C251FADEDB1F8CD48DA(/*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void maxstAR.TrackerManager::SetVocabulary(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackerManager_SetVocabulary_mF6289C9A63280DA2F4C85C7F32AA889DFA3476A2 (TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * __this, String_t* ___vocFilename0, bool ___isAndroidAssetFile1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___vocFilename0;
		bool L_1 = ___isAndroidAssetFile1;
		NativeAPI_maxst_TrackerManager_setVocabulary_m9D20A8B8F7760E8A505E8B6FE179B70BB72C03BF(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// maxstAR.TrackingState maxstAR.TrackerManager::UpdateTrackingState()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TrackingState_t1B55706E8B57210198976B3EC273F3ABAB00D365 * TrackerManager_UpdateTrackingState_mDD67C1785159119C1897E1088BEEB38D0596067C (TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackerManager_UpdateTrackingState_mDD67C1785159119C1897E1088BEEB38D0596067C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint64_t V_0 = 0;
	{
		V_0 = (((int64_t)((int64_t)0)));
		uint64_t L_0 = NativeAPI_maxst_TrackerManager_updateTrackingState_m1FF8B6E79093D760E2FF3FC0FBDA947FAB86F11E(/*hidden argument*/NULL);
		V_0 = L_0;
		uint64_t L_1 = V_0;
		TrackingState_t1B55706E8B57210198976B3EC273F3ABAB00D365 * L_2 = (TrackingState_t1B55706E8B57210198976B3EC273F3ABAB00D365 *)il2cpp_codegen_object_new(TrackingState_t1B55706E8B57210198976B3EC273F3ABAB00D365_il2cpp_TypeInfo_var);
		TrackingState__ctor_m511846572F1B67AC12D0596AC945498728CB2844(L_2, L_1, /*hidden argument*/NULL);
		__this->set_trackingState_10(L_2);
		TrackingState_t1B55706E8B57210198976B3EC273F3ABAB00D365 * L_3 = __this->get_trackingState_10();
		return L_3;
	}
}
// maxstAR.TrackingState maxstAR.TrackerManager::GetTrackingState()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TrackingState_t1B55706E8B57210198976B3EC273F3ABAB00D365 * TrackerManager_GetTrackingState_m6B4BEF73784906EDB343CD03B15C4670972702A3 (TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * __this, const RuntimeMethod* method)
{
	{
		TrackingState_t1B55706E8B57210198976B3EC273F3ABAB00D365 * L_0 = __this->get_trackingState_10();
		return L_0;
	}
}
// UnityEngine.Vector3 maxstAR.TrackerManager::GetWorldPositionFromScreenCoordinate(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  TrackerManager_GetWorldPositionFromScreenCoordinate_m15F077BC5892CDE906BA94FFFE6C687E70413187 (TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screen0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackerManager_GetWorldPositionFromScreenCoordinate_m15F077BC5892CDE906BA94FFFE6C687E70413187_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* V_0 = NULL;
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_0 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)SZArrayNew(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5_il2cpp_TypeInfo_var, (uint32_t)2);
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_1 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)SZArrayNew(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5_il2cpp_TypeInfo_var, (uint32_t)3);
		V_0 = L_1;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_2 = L_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_3 = ___screen0;
		float L_4 = L_3.get_x_0();
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_4);
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_5 = L_2;
		int32_t L_6 = Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150(/*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_7 = ___screen0;
		float L_8 = L_7.get_y_1();
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)((float)il2cpp_codegen_subtract((float)(((float)((float)L_6))), (float)L_8)));
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_9 = V_0;
		NativeAPI_maxst_TrackerManager_getWorldPositionFromScreenCoordinate_m06785004CF3907F36BA753A6E0293B466DC56990(L_5, L_9, /*hidden argument*/NULL);
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = 0;
		float L_12 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = 2;
		float L_15 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_16 = V_0;
		NullCheck(L_16);
		int32_t L_17 = 1;
		float L_18 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_19;
		memset((&L_19), 0, sizeof(L_19));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_19), L_12, ((-L_15)), ((-L_18)), /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Void maxstAR.TrackerManager::FindSurface()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackerManager_FindSurface_m6457296E8C13EDD137679FE51E619DB2A4222B36 (TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * __this, const RuntimeMethod* method)
{
	{
		NativeAPI_maxst_TrackerManager_findSurface_m2F01DD3DA099836016D8C7EB80CFA1EB7770D202(/*hidden argument*/NULL);
		return;
	}
}
// System.Void maxstAR.TrackerManager::QuitFindingSurface()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackerManager_QuitFindingSurface_mA995D66E070DF9FB38060B3A8EE211A8D1F8D750 (TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * __this, const RuntimeMethod* method)
{
	{
		NativeAPI_maxst_TrackerManager_quitFindingSurface_m383FB2DFD3D0404740C354D781CCA20509073AED(/*hidden argument*/NULL);
		return;
	}
}
// System.Void maxstAR.TrackerManager::FindImageOfCloudRecognition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackerManager_FindImageOfCloudRecognition_m65D09E068BC45015C4597C7F8C6495C079B8A0A9 (TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * __this, const RuntimeMethod* method)
{
	{
		CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * L_0 = __this->get_cloudRecognitionController_15();
		String_t* L_1 = __this->get_secretId_12();
		String_t* L_2 = __this->get_secretKey_13();
		NullCheck(L_0);
		CloudRecognitionController_SetCloudRecognitionSecretIdAndSecretKey_mC7C9F09C20600730341C96E2038A35258DD4EC27(L_0, L_1, L_2, /*hidden argument*/NULL);
		CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * L_3 = __this->get_cloudRecognitionController_15();
		NullCheck(L_3);
		CloudRecognitionController_FindImageOfCloudRecognition_mF067EE8BD5A498574C4B817AFABBF1551B757EC0(L_3, /*hidden argument*/NULL);
		return;
	}
}
// maxstAR.GuideInfo maxstAR.TrackerManager::GetGuideInfo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5 * TrackerManager_GetGuideInfo_m4ABE739552D344AB9AD9695F0ECD15F9213CCF57 (TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackerManager_GetGuideInfo_m4ABE739552D344AB9AD9695F0ECD15F9213CCF57_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5 * L_0 = __this->get_guideInfo_14();
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5 * L_1 = (GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5 *)il2cpp_codegen_object_new(GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5_il2cpp_TypeInfo_var);
		GuideInfo__ctor_m4D0A3F456C4757B7037423BEF13AABEE98A67315(L_1, /*hidden argument*/NULL);
		__this->set_guideInfo_14(L_1);
	}

IL_0013:
	{
		GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5 * L_2 = __this->get_guideInfo_14();
		NullCheck(L_2);
		GuideInfo_UpdateGuideInfo_mAB8A020344958F11C97A3248D2B8BC63450DF17C(L_2, /*hidden argument*/NULL);
		GuideInfo_tAD8F667DDAA2A8CC187B1AB5B9A8D0382F8AEBC5 * L_3 = __this->get_guideInfo_14();
		return L_3;
	}
}
// maxstAR.SurfaceThumbnail maxstAR.TrackerManager::SaveSurfaceData(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SurfaceThumbnail_t101EA84FE2D39AF996B5F5F8E89E7DE3638DC824 * TrackerManager_SaveSurfaceData_m3C2BFE33F6C0CAB1814551F4276AC70D9DB09CFA (TrackerManager_tCE1454584236076911751A96CE6D0B0C6A9E0DC5 * __this, String_t* ___outputFileName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackerManager_SaveSurfaceData_m3C2BFE33F6C0CAB1814551F4276AC70D9DB09CFA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SurfaceThumbnail_t101EA84FE2D39AF996B5F5F8E89E7DE3638DC824 * V_0 = NULL;
	uint64_t V_1 = 0;
	{
		V_0 = (SurfaceThumbnail_t101EA84FE2D39AF996B5F5F8E89E7DE3638DC824 *)NULL;
		V_1 = (((int64_t)((int64_t)0)));
		String_t* L_0 = ___outputFileName0;
		uint64_t L_1 = NativeAPI_maxst_TrackerManager_saveSurfaceData_m5FB3E8361B4713D63718A2D372961B299B94728A(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		uint64_t L_2 = V_1;
		if (L_2)
		{
			goto IL_0011;
		}
	}
	{
		return (SurfaceThumbnail_t101EA84FE2D39AF996B5F5F8E89E7DE3638DC824 *)NULL;
	}

IL_0011:
	{
		uint64_t L_3 = V_1;
		SurfaceThumbnail_t101EA84FE2D39AF996B5F5F8E89E7DE3638DC824 * L_4 = (SurfaceThumbnail_t101EA84FE2D39AF996B5F5F8E89E7DE3638DC824 *)il2cpp_codegen_object_new(SurfaceThumbnail_t101EA84FE2D39AF996B5F5F8E89E7DE3638DC824_il2cpp_TypeInfo_var);
		SurfaceThumbnail__ctor_m0DD58C3F68E19DECAA5378921816128B06360334(L_4, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		SurfaceThumbnail_t101EA84FE2D39AF996B5F5F8E89E7DE3638DC824 * L_5 = V_0;
		return L_5;
	}
}
// System.Void maxstAR.TrackerManager::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackerManager__cctor_m5D92506D52DF722C2DA70230DA94BFB2F9B3C63F (const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void maxstAR.TrackingResult::.ctor(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackingResult__ctor_m16B9702EAC0B7D0B44B2B53B9521E31682724682 (TrackingResult_t3B01C878A45EA0F96D8A10870D925EA0290F7E07 * __this, uint64_t ___cPtr0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		uint64_t L_0 = ___cPtr0;
		__this->set_cPtr_0(L_0);
		return;
	}
}
// System.Int32 maxstAR.TrackingResult::GetCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TrackingResult_GetCount_mA693AF5B453F04E891DC992DE7F147425D4447C1 (TrackingResult_t3B01C878A45EA0F96D8A10870D925EA0290F7E07 * __this, const RuntimeMethod* method)
{
	{
		uint64_t L_0 = __this->get_cPtr_0();
		int32_t L_1 = NativeAPI_maxst_TrackingResult_getCount_mC3FBA8BCB14A66845A307310DED26C50257E5082(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// maxstAR.Trackable maxstAR.TrackingResult::GetTrackable(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Trackable_t525A99BA508C48FC5286F43525D4C12CB6CD1D27 * TrackingResult_GetTrackable_mBAF43C221FEBE35839E049B9A727C47C265DE128 (TrackingResult_t3B01C878A45EA0F96D8A10870D925EA0290F7E07 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackingResult_GetTrackable_mBAF43C221FEBE35839E049B9A727C47C265DE128_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		uint64_t L_0 = __this->get_cPtr_0();
		int32_t L_1 = ___index0;
		uint64_t L_2 = NativeAPI_maxst_TrackingResult_getTrackable_m0F5189AD387C9735BFF31D519CE9F0E27CA95B9B(L_0, L_1, /*hidden argument*/NULL);
		Trackable_t525A99BA508C48FC5286F43525D4C12CB6CD1D27 * L_3 = (Trackable_t525A99BA508C48FC5286F43525D4C12CB6CD1D27 *)il2cpp_codegen_object_new(Trackable_t525A99BA508C48FC5286F43525D4C12CB6CD1D27_il2cpp_TypeInfo_var);
		Trackable__ctor_m28195F5B5619EBC04AF14B18F8D5773F9A37B195(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void maxstAR.TrackingState::.ctor(System.UInt64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrackingState__ctor_m511846572F1B67AC12D0596AC945498728CB2844 (TrackingState_t1B55706E8B57210198976B3EC273F3ABAB00D365 * __this, uint64_t ___trackingStateCPtr0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		uint64_t L_0 = ___trackingStateCPtr0;
		__this->set_cPtr_0(L_0);
		return;
	}
}
// System.UInt64 maxstAR.TrackingState::GetTrackingStateCPtr()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint64_t TrackingState_GetTrackingStateCPtr_m9FF15DA1E662E4D837B2FAAF53E6BB369F6D2ED9 (TrackingState_t1B55706E8B57210198976B3EC273F3ABAB00D365 * __this, const RuntimeMethod* method)
{
	{
		uint64_t L_0 = __this->get_cPtr_0();
		return L_0;
	}
}
// maxstAR.TrackingResult maxstAR.TrackingState::GetTrackingResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TrackingResult_t3B01C878A45EA0F96D8A10870D925EA0290F7E07 * TrackingState_GetTrackingResult_mD0779CB0C659620D558136BE5FCD7EA090ADB413 (TrackingState_t1B55706E8B57210198976B3EC273F3ABAB00D365 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackingState_GetTrackingResult_mD0779CB0C659620D558136BE5FCD7EA090ADB413_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		uint64_t L_0 = __this->get_cPtr_0();
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return (TrackingResult_t3B01C878A45EA0F96D8A10870D925EA0290F7E07 *)NULL;
	}

IL_000a:
	{
		uint64_t L_1 = __this->get_cPtr_0();
		uint64_t L_2 = NativeAPI_maxst_TrackingState_getTrackingResult_m663F7A5997448E4FF7726D09A8901FDCFD976FE1(L_1, /*hidden argument*/NULL);
		TrackingResult_t3B01C878A45EA0F96D8A10870D925EA0290F7E07 * L_3 = (TrackingResult_t3B01C878A45EA0F96D8A10870D925EA0290F7E07 *)il2cpp_codegen_object_new(TrackingResult_t3B01C878A45EA0F96D8A10870D925EA0290F7E07_il2cpp_TypeInfo_var);
		TrackingResult__ctor_m16B9702EAC0B7D0B44B2B53B9521E31682724682(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String maxstAR.TrackingState::GetCodeScanResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* TrackingState_GetCodeScanResult_m2F19F1EC66C4563183E8784AE2ED355F28EBD7C0 (TrackingState_t1B55706E8B57210198976B3EC273F3ABAB00D365 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackingState_GetCodeScanResult_m2F19F1EC66C4563183E8784AE2ED355F28EBD7C0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_1 = NULL;
	{
		uint64_t L_0 = __this->get_cPtr_0();
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		return _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
	}

IL_000e:
	{
		uint64_t L_1 = __this->get_cPtr_0();
		int32_t L_2 = NativeAPI_maxst_TrackingState_getCodeScanResultLength_mE478E4BA4BEC1CA111EA9A265209E2AF38A484D3(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_4 = V_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)L_4);
		V_1 = L_5;
		uint64_t L_6 = __this->get_cPtr_0();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = V_1;
		int32_t L_8 = V_0;
		NativeAPI_maxst_TrackingState_getCodeScanResult_m88980E71855098278471653402255093CF47A43A(L_6, L_7, L_8, /*hidden argument*/NULL);
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_9 = Encoding_get_UTF8_m67C8652936B681E7BC7505E459E88790E0FF16D9(/*hidden argument*/NULL);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_10 = V_1;
		NullCheck(L_9);
		String_t* L_11 = VirtFuncInvoker1< String_t*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(33 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_9, L_10);
		return L_11;
	}

IL_003e:
	{
		return _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
	}
}
// maxstAR.TrackedImage maxstAR.TrackingState::GetImage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1 * TrackingState_GetImage_mD1C946E7955D0607CA070ABCB7164388D35F3627 (TrackingState_t1B55706E8B57210198976B3EC273F3ABAB00D365 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackingState_GetImage_mD1C946E7955D0607CA070ABCB7164388D35F3627_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		uint64_t L_0 = __this->get_cPtr_0();
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return (TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1 *)NULL;
	}

IL_000a:
	{
		uint64_t L_1 = __this->get_cPtr_0();
		uint64_t L_2 = NativeAPI_maxst_TrackingState_getImage_mBEE338FF02F972166CA81F5117BE653C6156003B(L_1, /*hidden argument*/NULL);
		TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1 * L_3 = (TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1 *)il2cpp_codegen_object_new(TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1_il2cpp_TypeInfo_var);
		TrackedImage__ctor_mD5DB1118197FD89E30C46B0AA42CC748E4928BD2(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// maxstAR.TrackedImage maxstAR.TrackingState::GetImage(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1 * TrackingState_GetImage_m6B379D48A3D7E6D0C46970D92317C1BE3D38F4C8 (TrackingState_t1B55706E8B57210198976B3EC273F3ABAB00D365 * __this, bool ___splitYUV0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackingState_GetImage_m6B379D48A3D7E6D0C46970D92317C1BE3D38F4C8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		uint64_t L_0 = __this->get_cPtr_0();
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return (TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1 *)NULL;
	}

IL_000a:
	{
		uint64_t L_1 = __this->get_cPtr_0();
		uint64_t L_2 = NativeAPI_maxst_TrackingState_getImage_mBEE338FF02F972166CA81F5117BE653C6156003B(L_1, /*hidden argument*/NULL);
		bool L_3 = ___splitYUV0;
		TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1 * L_4 = (TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1 *)il2cpp_codegen_object_new(TrackedImage_t68FAFCD38997BF4538E7738833A77E15F285F2F1_il2cpp_TypeInfo_var);
		TrackedImage__ctor_m0F370A499837DC0DD4580FEE1B99C728F4B8CE2E(L_4, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// maxstAR.TrackingState_TrackingStatus maxstAR.TrackingState::GetTrackingStatus()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TrackingState_GetTrackingStatus_m494F6BEA0863C65070ED186FC320FF5EB59C2A45 (TrackingState_t1B55706E8B57210198976B3EC273F3ABAB00D365 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackingState_GetTrackingStatus_m494F6BEA0863C65070ED186FC320FF5EB59C2A45_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MaxstSingleton_1_t0187F7FDC19F311CF5D000320AA39A295136CFDB_il2cpp_TypeInfo_var);
		CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * L_0 = MaxstSingleton_1_get_Instance_m1346F6D4BE1CE718B2CD992D56A6E6FB8900A341(/*hidden argument*/MaxstSingleton_1_get_Instance_m1346F6D4BE1CE718B2CD992D56A6E6FB8900A341_RuntimeMethod_var);
		V_0 = L_0;
		CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = CloudRecognitionController_GetCloudStatus_m58CEA89750D776137CE0B9367902C9ABE6126446_inline(L_1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)55)))))
		{
			goto IL_0012;
		}
	}
	{
		return (int32_t)(1);
	}

IL_0012:
	{
		CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = CloudRecognitionController_GetCloudStatus_m58CEA89750D776137CE0B9367902C9ABE6126446_inline(L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)54)))))
		{
			goto IL_001e;
		}
	}
	{
		return (int32_t)(2);
	}

IL_001e:
	{
		CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = CloudRecognitionController_GetCloudStatus_m58CEA89750D776137CE0B9367902C9ABE6126446_inline(L_5, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)53)))))
		{
			goto IL_002a;
		}
	}
	{
		return (int32_t)(4);
	}

IL_002a:
	{
		CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = CloudRecognitionController_GetCloudStatus_m58CEA89750D776137CE0B9367902C9ABE6126446_inline(L_7, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)52)))))
		{
			goto IL_0036;
		}
	}
	{
		return (int32_t)(5);
	}

IL_0036:
	{
		CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = CloudRecognitionController_GetCloudStatus_m58CEA89750D776137CE0B9367902C9ABE6126446_inline(L_9, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)51)))))
		{
			goto IL_0042;
		}
	}
	{
		return (int32_t)(5);
	}

IL_0042:
	{
		CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = CloudRecognitionController_GetCloudStatus_m58CEA89750D776137CE0B9367902C9ABE6126446_inline(L_11, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)56)))))
		{
			goto IL_004e;
		}
	}
	{
		return (int32_t)(3);
	}

IL_004e:
	{
		return (int32_t)(7);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void maxstAR.WearableCalibration::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WearableCalibration__ctor_m1F9219EF3026C4A8C32ACCD9EA9880A19DC18955 (WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String maxstAR.WearableCalibration::get_activeProfile()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* WearableCalibration_get_activeProfile_m672CB16C8B236CD00926A73290B274563CC1FA5E (WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CactiveProfileU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void maxstAR.WearableCalibration::set_activeProfile(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WearableCalibration_set_activeProfile_m407EFDCA52FF56603F4BD3338ACE6FACC2135357 (WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CactiveProfileU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Boolean maxstAR.WearableCalibration::IsActivated()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WearableCalibration_IsActivated_m26AFCBA7985A262D1A636BFD1909E6D7BEA21897 (WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = NativeAPI_maxst_WearableCalibration_isActivated_m9143F692C8392D25C750D38EC06B266105B72EBD(/*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean maxstAR.WearableCalibration::Init(System.String,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WearableCalibration_Init_mD794728749B60144E5CA54A448995426AF9EDD25 (WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * __this, String_t* ___modelName0, int32_t ___width1, int32_t ___height2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WearableCalibration_Init_mD794728749B60144E5CA54A448995426AF9EDD25_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE * V_1 = NULL;
	AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * V_2 = NULL;
	AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * V_3 = NULL;
	{
		V_0 = (bool)0;
		String_t* L_0 = ___modelName0;
		bool L_1 = NativeAPI_maxst_WearableCalibration_init_m772CE19706D3AE2AB4E896585B5FB7EC8A4704AF(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = Boolean_ToString_m62D1EFD5F6D5F6B6AF0D14A07BF5741C94413301((bool*)(&V_0), /*hidden argument*/NULL);
		String_t* L_3 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral782BCC6135CB96896F3B2086E7FA556CCBC0EC32, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_3, /*hidden argument*/NULL);
		AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE * L_4 = (AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE *)il2cpp_codegen_object_new(AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_mAE416E812DB3911279C0FE87A7760247CE1BBFA8(L_4, _stringLiteralA004F7A71A664C60394CBEBEC2BC71B669D9F986, /*hidden argument*/NULL);
		V_1 = L_4;
		AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE * L_5 = V_1;
		NullCheck(L_5);
		AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * L_6 = AndroidJavaObject_GetStatic_TisAndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D_m944E5987B38F1229EB3E8122E030D0439D4513B0(L_5, _stringLiteral32EAF9220C696B2C222E361F16ACA8FB08EC6083, /*hidden argument*/AndroidJavaObject_GetStatic_TisAndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D_m944E5987B38F1229EB3E8122E030D0439D4513B0_RuntimeMethod_var);
		V_2 = L_6;
		AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE * L_7 = (AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE *)il2cpp_codegen_object_new(AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_mAE416E812DB3911279C0FE87A7760247CE1BBFA8(L_7, _stringLiteral3D7E79A85549F13CEB3CAA70B10CC3069177FB56, /*hidden argument*/NULL);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_8 = Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_inline(/*hidden argument*/Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_RuntimeMethod_var);
		NullCheck(L_7);
		AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * L_9 = AndroidJavaObject_CallStatic_TisAndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D_m6CAE75FB51C5A02521C239A7232735573C51EAE7(L_7, _stringLiteral40804CE792BB4A6BA242156DAD17CBE6C9731E14, L_8, /*hidden argument*/AndroidJavaObject_CallStatic_TisAndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D_m6CAE75FB51C5A02521C239A7232735573C51EAE7_RuntimeMethod_var);
		V_3 = L_9;
		AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * L_10 = V_3;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_11 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)2);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_12 = L_11;
		AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * L_13 = V_2;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_13);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_13);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_14 = L_12;
		String_t* L_15 = ___modelName0;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_15);
		NullCheck(L_10);
		bool L_16 = AndroidJavaObject_Call_TisBoolean_tB53F6830F670160873277339AA58F15CAED4399C_m57EE1ACB271D15DD0E2DDD6B28805C31799A0976(L_10, _stringLiteral70E6EC9A382CC19D47F282CAD67350FA744B646D, L_14, /*hidden argument*/AndroidJavaObject_Call_TisBoolean_tB53F6830F670160873277339AA58F15CAED4399C_m57EE1ACB271D15DD0E2DDD6B28805C31799A0976_RuntimeMethod_var);
		if (L_16)
		{
			goto IL_0075;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteralFC5BC92101C3C283A92BB8C14DF6A1845CD34B65, /*hidden argument*/NULL);
	}

IL_0075:
	{
		AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * L_17 = V_3;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_18 = Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_inline(/*hidden argument*/Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_RuntimeMethod_var);
		NullCheck(L_17);
		String_t* L_19 = AndroidJavaObject_Call_TisString_t_m5EAE53C9E2A8893FD8FEA710378D22C162A0FDEA(L_17, _stringLiteral45C8223002A464F0E383D093C9D897062EE7C828, L_18, /*hidden argument*/AndroidJavaObject_Call_TisString_t_m5EAE53C9E2A8893FD8FEA710378D22C162A0FDEA_RuntimeMethod_var);
		WearableCalibration_set_activeProfile_m407EFDCA52FF56603F4BD3338ACE6FACC2135357_inline(__this, L_19, /*hidden argument*/NULL);
		AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * L_20 = V_2;
		NullCheck(L_20);
		AndroidJavaObject_Dispose_m02D1B6D8F3E902E5F0D181BF6C1753856B0DE144(L_20, /*hidden argument*/NULL);
		AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE * L_21 = V_1;
		NullCheck(L_21);
		AndroidJavaObject_Dispose_m02D1B6D8F3E902E5F0D181BF6C1753856B0DE144(L_21, /*hidden argument*/NULL);
		bool L_22 = V_0;
		return L_22;
	}
}
// System.Void maxstAR.WearableCalibration::Deinit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WearableCalibration_Deinit_m0B548B265954A3EF9F5D3B6DED4F63F4F8D01EDB (WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WearableCalibration_Deinit_m0B548B265954A3EF9F5D3B6DED4F63F4F8D01EDB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NativeAPI_maxst_WearableCalibration_deinit_m174A61A633706CCF0CCFAD86AF3CD91892AF8367(/*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_eyeLeft_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = __this->get_eyeLeft_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446(L_2, /*hidden argument*/NULL);
	}

IL_001e:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = __this->get_eyeRight_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_3, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0037;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = __this->get_eyeRight_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446(L_5, /*hidden argument*/NULL);
	}

IL_0037:
	{
		return;
	}
}
// System.Single[] maxstAR.WearableCalibration::GetViewport(maxstAR.WearableCalibration_EyeType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* WearableCalibration_GetViewport_mF25EA372885DF1EE51D921638F3311725335B469 (WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * __this, int32_t ___eyeType0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WearableCalibration_GetViewport_mF25EA372885DF1EE51D921638F3311725335B469_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* V_0 = NULL;
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_0 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)SZArrayNew(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5_il2cpp_TypeInfo_var, (uint32_t)4);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)8))))
		{
			goto IL_0031;
		}
	}
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_2 = V_0;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)(0.0f));
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_3 = V_0;
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)(0.0f));
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_4 = V_0;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)(0.5f));
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_5 = V_0;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (float)(1.0f));
		goto IL_0051;
	}

IL_0031:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_6 = V_0;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)(0.5f));
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_7 = V_0;
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)(0.0f));
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_8 = V_0;
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)(0.5f));
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_9 = V_0;
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (float)(1.0f));
	}

IL_0051:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_10 = V_0;
		return L_10;
	}
}
// System.Single[] maxstAR.WearableCalibration::GetProjectionMatrix(maxstAR.WearableCalibration_EyeType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* WearableCalibration_GetProjectionMatrix_mDDCBE1373398DEA571D8B1A9DB3DA9758992F1DC (WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * __this, int32_t ___eyeType0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WearableCalibration_GetProjectionMatrix_mDDCBE1373398DEA571D8B1A9DB3DA9758992F1DC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_0 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)SZArrayNew(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16));
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_1 = L_0;
		int32_t L_2 = ___eyeType0;
		NativeAPI_maxst_WearableCalibration_getProjectionMatrix_mCD2BC624AB1D8DEFEADA180420AA58535AB0353A(L_1, L_2, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void maxstAR.WearableCalibration::CreateWearableEye(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WearableCalibration_CreateWearableEye_m0614E4FF2FF1B484383A0A15B950BFEAB7D33EE7 (WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * __this, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___cameraTransform0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WearableCalibration_CreateWearableEye_m0614E4FF2FF1B484383A0A15B950BFEAB7D33EE7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * V_0 = NULL;
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * V_1 = NULL;
	int32_t V_2 = 0;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  V_3;
	memset((&V_3), 0, sizeof(V_3));
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* V_7 = NULL;
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  V_8;
	memset((&V_8), 0, sizeof(V_8));
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  V_9;
	memset((&V_9), 0, sizeof(V_9));
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_eyeLeft_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002a;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)il2cpp_codegen_object_new(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var);
		GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686(L_2, _stringLiteral69284A621D6E033E1C75AE520AB9A8F48A54609D, /*hidden argument*/NULL);
		__this->set_eyeLeft_1(L_2);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = __this->get_eyeLeft_1();
		NullCheck(L_3);
		GameObject_AddComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m7DF3B4DB4CA60855860B6760DBA2B1AE5883D20F(L_3, /*hidden argument*/GameObject_AddComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m7DF3B4DB4CA60855860B6760DBA2B1AE5883D20F_RuntimeMethod_var);
	}

IL_002a:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = __this->get_eyeRight_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_4, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0054;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)il2cpp_codegen_object_new(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var);
		GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686(L_6, _stringLiteralC4BF429D7BCF859C0958FCDF0E6DFD90A59FC4A6, /*hidden argument*/NULL);
		__this->set_eyeRight_2(L_6);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = __this->get_eyeRight_2();
		NullCheck(L_7);
		GameObject_AddComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m7DF3B4DB4CA60855860B6760DBA2B1AE5883D20F(L_7, /*hidden argument*/GameObject_AddComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m7DF3B4DB4CA60855860B6760DBA2B1AE5883D20F_RuntimeMethod_var);
	}

IL_0054:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_8 = __this->get_eyeLeft_1();
		NullCheck(L_8);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_8, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = ___cameraTransform0;
		NullCheck(L_9);
		Transform_set_parent_m65B8E4660B2C554069C57A957D9E55FECA7AA73E(L_9, L_10, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_11 = __this->get_eyeRight_2();
		NullCheck(L_11);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_12 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_11, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_13 = ___cameraTransform0;
		NullCheck(L_12);
		Transform_set_parent_m65B8E4660B2C554069C57A957D9E55FECA7AA73E(L_12, L_13, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_14 = __this->get_eyeLeft_1();
		NullCheck(L_14);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_15 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_15, L_16, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_17 = __this->get_eyeLeft_1();
		NullCheck(L_17);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_18 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_19 = Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64(/*hidden argument*/NULL);
		NullCheck(L_18);
		Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A(L_18, L_19, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_20 = __this->get_eyeLeft_1();
		NullCheck(L_20);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_21 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_20, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = Vector3_get_one_mA11B83037CB269C6076CBCF754E24C8F3ACEC2AB(/*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_21, L_22, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_23 = __this->get_eyeRight_2();
		NullCheck(L_23);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_24 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_23, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_25 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_24, L_25, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_26 = __this->get_eyeRight_2();
		NullCheck(L_26);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_27 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_26, /*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_28 = Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64(/*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A(L_27, L_28, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_29 = __this->get_eyeRight_2();
		NullCheck(L_29);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_30 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_29, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_31 = Vector3_get_one_mA11B83037CB269C6076CBCF754E24C8F3ACEC2AB(/*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_30, L_31, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_32 = ___cameraTransform0;
		NullCheck(L_32);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_33 = Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_mB090F51A34716700C0F4F1B08F9330C6F503DB9E(L_32, /*hidden argument*/Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_mB090F51A34716700C0F4F1B08F9330C6F503DB9E_RuntimeMethod_var);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_34 = __this->get_eyeLeft_1();
		NullCheck(L_34);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_35 = GameObject_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_mCF3938D33A4B2D1D9B65321455F867660E72C3FD(L_34, /*hidden argument*/GameObject_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_mCF3938D33A4B2D1D9B65321455F867660E72C3FD_RuntimeMethod_var);
		V_0 = L_35;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_36 = __this->get_eyeRight_2();
		NullCheck(L_36);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_37 = GameObject_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_mCF3938D33A4B2D1D9B65321455F867660E72C3FD(L_36, /*hidden argument*/GameObject_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_mCF3938D33A4B2D1D9B65321455F867660E72C3FD_RuntimeMethod_var);
		V_1 = L_37;
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_38 = L_33;
		NullCheck(L_38);
		int32_t L_39 = Camera_get_clearFlags_m1D02BA1ABD7310269F6121C58AF41DCDEF1E0266(L_38, /*hidden argument*/NULL);
		V_2 = L_39;
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_40 = L_38;
		NullCheck(L_40);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_41 = Camera_get_backgroundColor_m14496C5DC24582D7227277AF71DBE96F8E9E64FF(L_40, /*hidden argument*/NULL);
		V_3 = L_41;
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_42 = L_40;
		NullCheck(L_42);
		float L_43 = Camera_get_depth_m436C49A1C7669E4AD5665A1F1107BDFBA38742CD(L_42, /*hidden argument*/NULL);
		V_4 = ((float)il2cpp_codegen_add((float)L_43, (float)(1.0f)));
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_44 = L_42;
		NullCheck(L_44);
		float L_45 = Camera_get_nearClipPlane_mD9D3E3D27186BBAC2CC354CE3609E6118A5BF66C(L_44, /*hidden argument*/NULL);
		V_5 = L_45;
		NullCheck(L_44);
		float L_46 = Camera_get_farClipPlane_mF51F1FF5BE87719CFAC293E272B1138DC1EFFD4B(L_44, /*hidden argument*/NULL);
		V_6 = L_46;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_47 = WearableCalibration_GetProjectionMatrix_mDDCBE1373398DEA571D8B1A9DB3DA9758992F1DC(__this, 0, /*hidden argument*/NULL);
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_48 = WearableCalibration_GetProjectionMatrix_mDDCBE1373398DEA571D8B1A9DB3DA9758992F1DC(__this, 1, /*hidden argument*/NULL);
		V_7 = L_48;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_49 = MatrixUtils_ConvertGLProjectionToUnityProjection_mDB182F79BC65EB2063F2D87D95518EF34E3D96B0(L_47, /*hidden argument*/NULL);
		V_8 = L_49;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_50 = V_7;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_51 = MatrixUtils_ConvertGLProjectionToUnityProjection_mDB182F79BC65EB2063F2D87D95518EF34E3D96B0(L_50, /*hidden argument*/NULL);
		V_9 = L_51;
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_52 = V_0;
		int32_t L_53 = V_2;
		NullCheck(L_52);
		Camera_set_clearFlags_m805DFBD136AA3E1E46A2E61441965D174E87FE50(L_52, L_53, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_54 = V_0;
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_55 = V_3;
		NullCheck(L_54);
		Camera_set_backgroundColor_mDB9CA1B37FE2D52493823914AC5BC9F8C1935D6F(L_54, L_55, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_56 = V_0;
		float L_57 = V_4;
		NullCheck(L_56);
		Camera_set_depth_m4A83CCCF7370B8AD4BDB2CD5528A6E12A409AE58(L_56, L_57, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_58 = V_0;
		float L_59 = V_5;
		NullCheck(L_58);
		Camera_set_nearClipPlane_m9D81E50F8658C16319BEF3774E78B93DEB208C6B(L_58, L_59, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_60 = V_0;
		float L_61 = V_6;
		NullCheck(L_60);
		Camera_set_farClipPlane_m52986DC40B7F577255C4D5A4F780FD8A7D862626(L_60, L_61, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_62 = V_0;
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_63;
		memset((&L_63), 0, sizeof(L_63));
		Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280((&L_63), (0.0f), (0.0f), (0.5f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_62);
		Camera_set_rect_m6DB9964EA6E519E2B07561C8CE6AA423980FEC11(L_62, L_63, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_64 = V_0;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_65 = V_8;
		NullCheck(L_64);
		Camera_set_projectionMatrix_mC726156CC9AE07A46297C91212655D836E1C6720(L_64, L_65, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_66 = V_1;
		int32_t L_67 = V_2;
		NullCheck(L_66);
		Camera_set_clearFlags_m805DFBD136AA3E1E46A2E61441965D174E87FE50(L_66, L_67, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_68 = V_1;
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_69 = V_3;
		NullCheck(L_68);
		Camera_set_backgroundColor_mDB9CA1B37FE2D52493823914AC5BC9F8C1935D6F(L_68, L_69, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_70 = V_1;
		float L_71 = V_4;
		NullCheck(L_70);
		Camera_set_depth_m4A83CCCF7370B8AD4BDB2CD5528A6E12A409AE58(L_70, L_71, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_72 = V_1;
		float L_73 = V_5;
		NullCheck(L_72);
		Camera_set_nearClipPlane_m9D81E50F8658C16319BEF3774E78B93DEB208C6B(L_72, L_73, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_74 = V_1;
		float L_75 = V_6;
		NullCheck(L_74);
		Camera_set_farClipPlane_m52986DC40B7F577255C4D5A4F780FD8A7D862626(L_74, L_75, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_76 = V_1;
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_77;
		memset((&L_77), 0, sizeof(L_77));
		Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280((&L_77), (0.5f), (0.0f), (0.5f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_76);
		Camera_set_rect_m6DB9964EA6E519E2B07561C8CE6AA423980FEC11(L_76, L_77, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_78 = V_1;
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_79 = V_9;
		NullCheck(L_78);
		Camera_set_projectionMatrix_mC726156CC9AE07A46297C91212655D836E1C6720(L_78, L_79, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void maxstAR.WearableDeviceController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WearableDeviceController__ctor_m831CA944A05E1A51C2B807D97E4590DB3ADC8897 (WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void maxstAR.WearableDeviceController::Init()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WearableDeviceController_Init_m3E8465A7C1727F11B7FD53F0779105F88E964D61 (WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WearableDeviceController_Init_m3E8465A7C1727F11B7FD53F0779105F88E964D61_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE * V_0 = NULL;
	AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * V_1 = NULL;
	AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE * V_2 = NULL;
	{
		int32_t L_0 = Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0058;
		}
	}
	{
		AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE * L_1 = (AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE *)il2cpp_codegen_object_new(AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_mAE416E812DB3911279C0FE87A7760247CE1BBFA8(L_1, _stringLiteralA004F7A71A664C60394CBEBEC2BC71B669D9F986, /*hidden argument*/NULL);
		V_0 = L_1;
		AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE * L_2 = V_0;
		NullCheck(L_2);
		AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * L_3 = AndroidJavaObject_GetStatic_TisAndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D_m944E5987B38F1229EB3E8122E030D0439D4513B0(L_2, _stringLiteral32EAF9220C696B2C222E361F16ACA8FB08EC6083, /*hidden argument*/AndroidJavaObject_GetStatic_TisAndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D_m944E5987B38F1229EB3E8122E030D0439D4513B0_RuntimeMethod_var);
		V_1 = L_3;
		AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE * L_4 = (AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE *)il2cpp_codegen_object_new(AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_mAE416E812DB3911279C0FE87A7760247CE1BBFA8(L_4, _stringLiteralC625B4E6C60DE430D2743BCDAC612D51309A4C94, /*hidden argument*/NULL);
		V_2 = L_4;
		AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE * L_5 = V_2;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_6 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_7 = L_6;
		AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * L_8 = V_1;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_8);
		NullCheck(L_5);
		AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * L_9 = AndroidJavaObject_CallStatic_TisAndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D_m6CAE75FB51C5A02521C239A7232735573C51EAE7(L_5, _stringLiteral320A422F13763FA7F9060ACA1310CF89A728DA71, L_7, /*hidden argument*/AndroidJavaObject_CallStatic_TisAndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D_m6CAE75FB51C5A02521C239A7232735573C51EAE7_RuntimeMethod_var);
		__this->set_javaObject_0(L_9);
		AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * L_10 = V_1;
		NullCheck(L_10);
		AndroidJavaObject_Dispose_m02D1B6D8F3E902E5F0D181BF6C1753856B0DE144(L_10, /*hidden argument*/NULL);
		AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE * L_11 = V_0;
		NullCheck(L_11);
		AndroidJavaObject_Dispose_m02D1B6D8F3E902E5F0D181BF6C1753856B0DE144(L_11, /*hidden argument*/NULL);
		AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE * L_12 = V_2;
		NullCheck(L_12);
		AndroidJavaObject_Dispose_m02D1B6D8F3E902E5F0D181BF6C1753856B0DE144(L_12, /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
// System.Void maxstAR.WearableDeviceController::DeInit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WearableDeviceController_DeInit_mA817CAF9604E3C65A2D0063B568213E86CBC8DC5 (WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_001b;
		}
	}
	{
		AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * L_1 = __this->get_javaObject_0();
		NullCheck(L_1);
		AndroidJavaObject_Dispose_m02D1B6D8F3E902E5F0D181BF6C1753856B0DE144(L_1, /*hidden argument*/NULL);
		__this->set_javaObject_0((AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D *)NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Boolean maxstAR.WearableDeviceController::IsSupportedWearableDevice()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WearableDeviceController_IsSupportedWearableDevice_m707E8F7754330646B9D1E3C887E4473D1638D3DC (WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WearableDeviceController_IsSupportedWearableDevice_m707E8F7754330646B9D1E3C887E4473D1638D3DC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_001f;
		}
	}
	{
		AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * L_1 = __this->get_javaObject_0();
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_inline(/*hidden argument*/Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_RuntimeMethod_var);
		NullCheck(L_1);
		bool L_3 = AndroidJavaObject_Call_TisBoolean_tB53F6830F670160873277339AA58F15CAED4399C_m57EE1ACB271D15DD0E2DDD6B28805C31799A0976(L_1, _stringLiteral2683ECBB7FBD84854A0C8F9B797D67FBE8535C56, L_2, /*hidden argument*/AndroidJavaObject_Call_TisBoolean_tB53F6830F670160873277339AA58F15CAED4399C_m57EE1ACB271D15DD0E2DDD6B28805C31799A0976_RuntimeMethod_var);
		return L_3;
	}

IL_001f:
	{
		return (bool)0;
	}
}
// System.String maxstAR.WearableDeviceController::GetModelName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* WearableDeviceController_GetModelName_m99A1302B5AAF1D4DE11EEBA587262B6C15025C99 (WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WearableDeviceController_GetModelName_m99A1302B5AAF1D4DE11EEBA587262B6C15025C99_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_001f;
		}
	}
	{
		AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * L_1 = __this->get_javaObject_0();
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_inline(/*hidden argument*/Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_RuntimeMethod_var);
		NullCheck(L_1);
		String_t* L_3 = AndroidJavaObject_Call_TisString_t_m5EAE53C9E2A8893FD8FEA710378D22C162A0FDEA(L_1, _stringLiteral129C423EB1EC8D09ABFD7D302E0CFF501BD642F5, L_2, /*hidden argument*/AndroidJavaObject_Call_TisString_t_m5EAE53C9E2A8893FD8FEA710378D22C162A0FDEA_RuntimeMethod_var);
		return L_3;
	}

IL_001f:
	{
		return _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
	}
}
// System.Void maxstAR.WearableDeviceController::SetStereoMode(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WearableDeviceController_SetStereoMode_m2ECFA9DC6D80551B52000B8456E57C8DC544BCF5 (WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92 * __this, bool ___toggle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WearableDeviceController_SetStereoMode_m2ECFA9DC6D80551B52000B8456E57C8DC544BCF5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0028;
		}
	}
	{
		AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * L_1 = __this->get_javaObject_0();
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_3 = L_2;
		bool L_4 = ___toggle0;
		bool L_5 = L_4;
		RuntimeObject * L_6 = Box(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_6);
		NullCheck(L_1);
		AndroidJavaObject_Call_m0FEBE4E59445D8527C88C992AA2D00EEF749AB56(L_1, _stringLiteralFF66FF0BB5DF6B8CD0C3A592D5AEC5E39939C6EF, L_3, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean maxstAR.WearableDeviceController::IsStereoEnabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WearableDeviceController_IsStereoEnabled_m2DEF96147706CF9842E0B7C328799D95D475A5D3 (WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WearableDeviceController_IsStereoEnabled_m2DEF96147706CF9842E0B7C328799D95D475A5D3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_001f;
		}
	}
	{
		AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * L_1 = __this->get_javaObject_0();
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_inline(/*hidden argument*/Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_RuntimeMethod_var);
		NullCheck(L_1);
		bool L_3 = AndroidJavaObject_Call_TisBoolean_tB53F6830F670160873277339AA58F15CAED4399C_m57EE1ACB271D15DD0E2DDD6B28805C31799A0976(L_1, _stringLiteralC434B6DED53825899F206B76DF0CEEA4304A2699, L_2, /*hidden argument*/AndroidJavaObject_Call_TisBoolean_tB53F6830F670160873277339AA58F15CAED4399C_m57EE1ACB271D15DD0E2DDD6B28805C31799A0976_RuntimeMethod_var);
		return L_3;
	}

IL_001f:
	{
		return (bool)0;
	}
}
// System.Boolean maxstAR.WearableDeviceController::IsSideBySideType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WearableDeviceController_IsSideBySideType_mCC4857F2F8C11AB9ACE7151DFE2A9A270DD70841 (WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WearableDeviceController_IsSideBySideType_mCC4857F2F8C11AB9ACE7151DFE2A9A270DD70841_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_001f;
		}
	}
	{
		AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * L_1 = __this->get_javaObject_0();
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_inline(/*hidden argument*/Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_RuntimeMethod_var);
		NullCheck(L_1);
		bool L_3 = AndroidJavaObject_Call_TisBoolean_tB53F6830F670160873277339AA58F15CAED4399C_m57EE1ACB271D15DD0E2DDD6B28805C31799A0976(L_1, _stringLiteralBDFA84592F3B00EDB1C3CB86DFD31858EC56A047, L_2, /*hidden argument*/AndroidJavaObject_Call_TisBoolean_tB53F6830F670160873277339AA58F15CAED4399C_m57EE1ACB271D15DD0E2DDD6B28805C31799A0976_RuntimeMethod_var);
		return L_3;
	}

IL_001f:
	{
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// maxstAR.WearableManager maxstAR.WearableManager::GetInstance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4 * WearableManager_GetInstance_m55F007C74CC1B329AE9A8AE3C05715E7CD20BE83 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WearableManager_GetInstance_m55F007C74CC1B329AE9A8AE3C05715E7CD20BE83_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4_il2cpp_TypeInfo_var);
		WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4 * L_0 = ((WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4_StaticFields*)il2cpp_codegen_static_fields_for(WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4_il2cpp_TypeInfo_var))->get_Instance_0();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4 * L_1 = (WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4 *)il2cpp_codegen_object_new(WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4_il2cpp_TypeInfo_var);
		WearableManager__ctor_m797FE745D17CE179C55D14E0322372200010A04A(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4_il2cpp_TypeInfo_var);
		((WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4_StaticFields*)il2cpp_codegen_static_fields_for(WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4_il2cpp_TypeInfo_var))->set_Instance_0(L_1);
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4_il2cpp_TypeInfo_var);
		WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4 * L_2 = ((WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4_StaticFields*)il2cpp_codegen_static_fields_for(WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4_il2cpp_TypeInfo_var))->get_Instance_0();
		return L_2;
	}
}
// System.Void maxstAR.WearableManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WearableManager__ctor_m797FE745D17CE179C55D14E0322372200010A04A (WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WearableManager__ctor_m797FE745D17CE179C55D14E0322372200010A04A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92 * L_0 = (WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92 *)il2cpp_codegen_object_new(WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92_il2cpp_TypeInfo_var);
		WearableDeviceController__ctor_m831CA944A05E1A51C2B807D97E4590DB3ADC8897(L_0, /*hidden argument*/NULL);
		__this->set_wearableDeviceController_1(L_0);
		WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92 * L_1 = __this->get_wearableDeviceController_1();
		NullCheck(L_1);
		WearableDeviceController_Init_m3E8465A7C1727F11B7FD53F0779105F88E964D61(L_1, /*hidden argument*/NULL);
		WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92 * L_2 = __this->get_wearableDeviceController_1();
		NullCheck(L_2);
		bool L_3 = WearableDeviceController_IsSupportedWearableDevice_m707E8F7754330646B9D1E3C887E4473D1638D3DC(L_2, /*hidden argument*/NULL);
		WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92 * L_4 = __this->get_wearableDeviceController_1();
		NullCheck(L_4);
		String_t* L_5 = WearableDeviceController_GetModelName_m99A1302B5AAF1D4DE11EEBA587262B6C15025C99(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		String_t* L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_6, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0084;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteral5F378B3813B277B3B57245C13A898B720C7FBFC1, /*hidden argument*/NULL);
		WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * L_7 = (WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D *)il2cpp_codegen_object_new(WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D_il2cpp_TypeInfo_var);
		WearableCalibration__ctor_m1F9219EF3026C4A8C32ACCD9EA9880A19DC18955(L_7, /*hidden argument*/NULL);
		__this->set_wearableCalibration_2(L_7);
		WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * L_8 = __this->get_wearableCalibration_2();
		String_t* L_9 = V_0;
		int32_t L_10 = Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3(/*hidden argument*/NULL);
		int32_t L_11 = Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150(/*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_12 = WearableCalibration_Init_mD794728749B60144E5CA54A448995426AF9EDD25(L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0084;
		}
	}
	{
		WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * L_13 = __this->get_wearableCalibration_2();
		NullCheck(L_13);
		String_t* L_14 = WearableCalibration_get_activeProfile_m672CB16C8B236CD00926A73290B274563CC1FA5E_inline(L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		String_t* L_15 = V_1;
		String_t* L_16 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral3ED58087C40E5F59ED8E8077F55D6D2738AED341, L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_16, /*hidden argument*/NULL);
	}

IL_0084:
	{
		return;
	}
}
// maxstAR.WearableDeviceController maxstAR.WearableManager::GetDeviceController()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92 * WearableManager_GetDeviceController_m8186D9023B3B4E4783E802F6031D1887B7CD6675 (WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4 * __this, const RuntimeMethod* method)
{
	{
		WearableDeviceController_t8C161EBD16CF0A8256D19144DFA654ACE3D92F92 * L_0 = __this->get_wearableDeviceController_1();
		return L_0;
	}
}
// maxstAR.WearableCalibration maxstAR.WearableManager::GetCalibration()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * WearableManager_GetCalibration_m8B08F7C53CB3208F1B886F55772A956D8C320096 (WearableManager_t76482728F380CB60FA8159E9A4AB898265FDC3E4 * __this, const RuntimeMethod* method)
{
	{
		WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * L_0 = __this->get_wearableCalibration_2();
		return L_0;
	}
}
// System.Void maxstAR.WearableManager::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WearableManager__cctor_m96521043C146B2AAB2B20079FF72DD367BE8F39E (const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t AbstractARManager_get_WorldCenterModeSetting_mCF8F7602828C0F38B39A879BB852DB2EA48FAD5C_inline (AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_worldCenterMode_13();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * AbstractARManager_GetARCamera_m40EF848C5AE6860CF3081C0572C016490F2D3748_inline (AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3 * __this, const RuntimeMethod* method)
{
	{
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_0 = __this->get_arCamera_10();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void CloudRecognitionController_SetAutoEnable_mDE82AB035CC5D2A1A17710F5157AF552E9E44886_inline (CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * __this, bool ___enable0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___enable0;
		__this->set_autoState_11(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AbstractARManager_SetWorldCenterMode_m39E9C716D2DDF61F6B7C0D1CD2BA1B5D3BCD538E_inline (AbstractARManager_tD3C72ADF4239FEFA4803414E50EB15DCDD3F9CD3 * __this, int32_t ___worldCenterMode0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___worldCenterMode0;
		__this->set_worldCenterMode_13(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t CloudRecognitionController_GetCloudStatus_m58CEA89750D776137CE0B9367902C9ABE6126446_inline (CloudRecognitionController_t1056CF68491113D4295C9E394D38E10645C98033 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_cloudState_8();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void WearableCalibration_set_activeProfile_m407EFDCA52FF56603F4BD3338ACE6FACC2135357_inline (WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CactiveProfileU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* WearableCalibration_get_activeProfile_m672CB16C8B236CD00926A73290B274563CC1FA5E_inline (WearableCalibration_t39622CBF20C27EE6E0B74CEA6A2C17451B21CC3D * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CactiveProfileU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_gshared_inline (const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = ((EmptyArray_1_tCF137C88A5824F413EFB5A2F31664D8207E61D26_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Value_0();
		return L_0;
	}
}
