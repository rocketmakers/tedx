﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String JsonFx.Json.JsonSpecifiedPropertyAttribute::get_SpecifiedProperty()
extern void JsonSpecifiedPropertyAttribute_get_SpecifiedProperty_mE86D8FC1F7D4DB0D7CACC156A9F34AF8942315E7 ();
// 0x00000002 System.String JsonFx.Json.JsonSpecifiedPropertyAttribute::GetJsonSpecifiedProperty(System.Reflection.MemberInfo)
extern void JsonSpecifiedPropertyAttribute_GetJsonSpecifiedProperty_m93A65B10974855AE7EC7F5B6F1B14CFC14EEC2A6 ();
// 0x00000003 System.Void JsonFx.Json.JsonReader::.ctor(System.String)
extern void JsonReader__ctor_mD669FC28EDB5578CAC43F8EAF30C4B0A719E2DEA ();
// 0x00000004 System.Void JsonFx.Json.JsonReader::.ctor(System.String,JsonFx.Json.JsonReaderSettings)
extern void JsonReader__ctor_m4108B2839741F3325D82776AD0FAFF9E3C713B7F ();
// 0x00000005 T JsonFx.Json.JsonReader::Deserialize()
// 0x00000006 System.Object JsonFx.Json.JsonReader::Deserialize(System.Int32,System.Type)
extern void JsonReader_Deserialize_mD1A9E9B0C25DF0C5D94AFC711657CD8C70DFF1FB ();
// 0x00000007 System.Object JsonFx.Json.JsonReader::Read(System.Type,System.Boolean)
extern void JsonReader_Read_m7F9C103441F37E24FF0DA2CDA3A763E58C41C0D6 ();
// 0x00000008 System.Object JsonFx.Json.JsonReader::ReadObject(System.Type)
extern void JsonReader_ReadObject_mE8C6875E1A8AC9B528E5A2A652C06422CBDEF311 ();
// 0x00000009 System.Collections.IEnumerable JsonFx.Json.JsonReader::ReadArray(System.Type)
extern void JsonReader_ReadArray_m43204159B4A9E4F0D3D9E2A005BB8F2CE4835B8F ();
// 0x0000000A System.String JsonFx.Json.JsonReader::ReadUnquotedKey()
extern void JsonReader_ReadUnquotedKey_mF0E827ECC2E9B75D2E36D3C392E74EF70F8EC80E ();
// 0x0000000B System.Object JsonFx.Json.JsonReader::ReadString(System.Type)
extern void JsonReader_ReadString_m4EDED0CB9C3DEF61CB53BF308E85A1097F69ECFF ();
// 0x0000000C System.Object JsonFx.Json.JsonReader::ReadNumber(System.Type)
extern void JsonReader_ReadNumber_mD5C165D1850CFC024CF5ACD30F1559834FA9D9C4 ();
// 0x0000000D T JsonFx.Json.JsonReader::Deserialize(System.String)
// 0x0000000E System.Object JsonFx.Json.JsonReader::Deserialize(System.String,System.Int32,System.Type)
extern void JsonReader_Deserialize_m71E4BEA58D4FA9B338A881EC78BC92C0FC9E11BC ();
// 0x0000000F JsonFx.Json.JsonToken JsonFx.Json.JsonReader::Tokenize()
extern void JsonReader_Tokenize_mD64CDED06219B05E058C22E9B7A5524D9FCE2CB1 ();
// 0x00000010 JsonFx.Json.JsonToken JsonFx.Json.JsonReader::Tokenize(System.Boolean)
extern void JsonReader_Tokenize_mFC446B66A59F7798D2F4DB8563DD40186E0FEB1F ();
// 0x00000011 System.Boolean JsonFx.Json.JsonReader::MatchLiteral(System.String)
extern void JsonReader_MatchLiteral_m3000CF34D40D87A9FB9B00C37A30D917815AC0C9 ();
// 0x00000012 System.Void JsonFx.Json.JsonWriter::.ctor(System.Text.StringBuilder)
extern void JsonWriter__ctor_m44528C95A3B303BB212485792F285ABE5DAADC70 ();
// 0x00000013 System.Void JsonFx.Json.JsonWriter::.ctor(System.Text.StringBuilder,JsonFx.Json.JsonWriterSettings)
extern void JsonWriter__ctor_mE2152F2B858BA35A24028347D3F8F1AED9139FEE ();
// 0x00000014 System.String JsonFx.Json.JsonWriter::Serialize(System.Object)
extern void JsonWriter_Serialize_m42F997E8776437DC5B9FD8314A19290024238E75 ();
// 0x00000015 System.Void JsonFx.Json.JsonWriter::Write(System.Object)
extern void JsonWriter_Write_mAB3146C5C739157535492E9C401B36D45A6FCB58 ();
// 0x00000016 System.Void JsonFx.Json.JsonWriter::Write(System.Object,System.Boolean)
extern void JsonWriter_Write_m7E232376035D7B4505A35BD28D924F71F12A608A ();
// 0x00000017 System.Void JsonFx.Json.JsonWriter::Write(System.DateTime)
extern void JsonWriter_Write_m976D908048B83A994B2FEF624AC9C39F11CDCF28 ();
// 0x00000018 System.Void JsonFx.Json.JsonWriter::Write(System.Guid)
extern void JsonWriter_Write_mA150B93DD0D129077DCE1C1D2A1426E328198CE7 ();
// 0x00000019 System.Void JsonFx.Json.JsonWriter::Write(System.Enum)
extern void JsonWriter_Write_m1E76469DF860ED670DAF0B82DCDBA7162315CB7C ();
// 0x0000001A System.Void JsonFx.Json.JsonWriter::Write(System.String)
extern void JsonWriter_Write_mE485F975E74C8B364BD7D647213747AE5674ACDD ();
// 0x0000001B System.Void JsonFx.Json.JsonWriter::Write(System.Boolean)
extern void JsonWriter_Write_m98CFD0BB99396076681469ADA518FCFB7D075877 ();
// 0x0000001C System.Void JsonFx.Json.JsonWriter::Write(System.Byte)
extern void JsonWriter_Write_m0C5447C5A053D67E8B6ACCE5AEB7A1D3F83599EE ();
// 0x0000001D System.Void JsonFx.Json.JsonWriter::Write(System.SByte)
extern void JsonWriter_Write_mF8BFDD2E893D018DCFF9B04AA4FE76ECF04C7E97 ();
// 0x0000001E System.Void JsonFx.Json.JsonWriter::Write(System.Int16)
extern void JsonWriter_Write_m168FE1A3793DF1776311E64EA3310A238175E8B2 ();
// 0x0000001F System.Void JsonFx.Json.JsonWriter::Write(System.UInt16)
extern void JsonWriter_Write_mD357CB0069DDB3B758EF935A9FAAAA315103E624 ();
// 0x00000020 System.Void JsonFx.Json.JsonWriter::Write(System.Int32)
extern void JsonWriter_Write_mD9D3420E0DD2DAC4288F001B9F0AD1DA73D89A49 ();
// 0x00000021 System.Void JsonFx.Json.JsonWriter::Write(System.UInt32)
extern void JsonWriter_Write_mCAEEDCDA0A4D3FF555B0476E006C2E30E35AC342 ();
// 0x00000022 System.Void JsonFx.Json.JsonWriter::Write(System.Int64)
extern void JsonWriter_Write_m809E7F0921B0F760E843D5A01A5DD59D1E2D56BB ();
// 0x00000023 System.Void JsonFx.Json.JsonWriter::Write(System.UInt64)
extern void JsonWriter_Write_m43C92A32F2E6487C85204EECC1C74F4634CBBAAF ();
// 0x00000024 System.Void JsonFx.Json.JsonWriter::Write(System.Single)
extern void JsonWriter_Write_mE67AEEA89C414CEE89DADFE28EE282A99890F79B ();
// 0x00000025 System.Void JsonFx.Json.JsonWriter::Write(System.Double)
extern void JsonWriter_Write_m01711F633E189B7B86DA14BFB20CB1BF43DCC4A8 ();
// 0x00000026 System.Void JsonFx.Json.JsonWriter::Write(System.Decimal)
extern void JsonWriter_Write_mD2A90E4D11192E274E0D9ECB5FF744EC543D1C10 ();
// 0x00000027 System.Void JsonFx.Json.JsonWriter::Write(System.Char)
extern void JsonWriter_Write_m4B73B1FCD88F714866B8B8C155304B44867E9995 ();
// 0x00000028 System.Void JsonFx.Json.JsonWriter::Write(System.TimeSpan)
extern void JsonWriter_Write_m2350E1F4C9257620643AFA5D58A348685A26137C ();
// 0x00000029 System.Void JsonFx.Json.JsonWriter::Write(System.Uri)
extern void JsonWriter_Write_m35ECB1CDBBA4BB0D5A736A7776B8B360BE522EF8 ();
// 0x0000002A System.Void JsonFx.Json.JsonWriter::Write(System.Version)
extern void JsonWriter_Write_m1D8134CD36C0D7DBEC06A8100D3F5A61EF87AEFD ();
// 0x0000002B System.Void JsonFx.Json.JsonWriter::Write(System.Xml.XmlNode)
extern void JsonWriter_Write_mDAA00ED5AFDB6E3BAFF46639B252839F8164C84B ();
// 0x0000002C System.Void JsonFx.Json.JsonWriter::WriteArray(System.Collections.IEnumerable)
extern void JsonWriter_WriteArray_m7256F1BCE22E598266F15256ED5F3BF16DA872F3 ();
// 0x0000002D System.Void JsonFx.Json.JsonWriter::WriteArrayItem(System.Object)
extern void JsonWriter_WriteArrayItem_mC6222637A1242794BBEBD63DA3011D4D15A05648 ();
// 0x0000002E System.Void JsonFx.Json.JsonWriter::WriteObject(System.Collections.IDictionary)
extern void JsonWriter_WriteObject_m54628E791AD4730BF3431D74E08469AEDAB52EDA ();
// 0x0000002F System.Void JsonFx.Json.JsonWriter::WriteDictionary(System.Collections.IEnumerable)
extern void JsonWriter_WriteDictionary_m145C4E3DE1598D2167F28322BBFC58A953A7C3AE ();
// 0x00000030 System.Void JsonFx.Json.JsonWriter::WriteObjectProperty(System.String,System.Object)
extern void JsonWriter_WriteObjectProperty_m296EA3652BC14AADFDA4B02C8A178D2AA1976246 ();
// 0x00000031 System.Void JsonFx.Json.JsonWriter::WriteObjectPropertyName(System.String)
extern void JsonWriter_WriteObjectPropertyName_mFEE866A2CD3CD4E587888BB29A68960DC37B325A ();
// 0x00000032 System.Void JsonFx.Json.JsonWriter::WriteObjectPropertyValue(System.Object)
extern void JsonWriter_WriteObjectPropertyValue_mA2843D073A0F0CD3BBDA9C2E3A7CF173E56119ED ();
// 0x00000033 System.Void JsonFx.Json.JsonWriter::WriteObject(System.Object,System.Type)
extern void JsonWriter_WriteObject_m980AC0C8CB40384D5E6AEC77980B9F80F85AC7C9 ();
// 0x00000034 System.Void JsonFx.Json.JsonWriter::WriteArrayItemDelim()
extern void JsonWriter_WriteArrayItemDelim_m40522E6F9E6E4AB181299352458D9C45B292121E ();
// 0x00000035 System.Void JsonFx.Json.JsonWriter::WriteObjectPropertyDelim()
extern void JsonWriter_WriteObjectPropertyDelim_m521F90CAD24CC222F1DA650E07FC29C3C0061283 ();
// 0x00000036 System.Void JsonFx.Json.JsonWriter::WriteLine()
extern void JsonWriter_WriteLine_m2C958713FB694419CD1C462508DED4593D8F8861 ();
// 0x00000037 System.Boolean JsonFx.Json.JsonWriter::IsIgnored(System.Type,System.Reflection.MemberInfo,System.Object)
extern void JsonWriter_IsIgnored_mEBA309224C37535D96BEC2A93C464FCA00D850FC ();
// 0x00000038 System.Boolean JsonFx.Json.JsonWriter::IsDefaultValue(System.Reflection.MemberInfo,System.Object)
extern void JsonWriter_IsDefaultValue_mBE04C53F16CACA9B44FE01EF1019F12BB7CCABB7 ();
// 0x00000039 System.Enum[] JsonFx.Json.JsonWriter::GetFlagList(System.Type,System.Object)
extern void JsonWriter_GetFlagList_mA3CFB3A6C75D4BC31C8437C4248EF6B500D38294 ();
// 0x0000003A System.Boolean JsonFx.Json.JsonWriter::InvalidIeee754(System.Decimal)
extern void JsonWriter_InvalidIeee754_m3050CF5BEB3A72C6F8A6E25C8748C79C0BAA3D02 ();
// 0x0000003B System.Void JsonFx.Json.JsonWriter::System.IDisposable.Dispose()
extern void JsonWriter_System_IDisposable_Dispose_m29339D9F3F2E84D0D5690BFAF5115EFDD08E0BBD ();
// 0x0000003C System.Void JsonFx.Json.JsonSerializationException::.ctor(System.String)
extern void JsonSerializationException__ctor_mE1D225D3BF5681977C5946142859A1F623123ACB ();
// 0x0000003D System.Void JsonFx.Json.JsonDeserializationException::.ctor(System.String,System.Int32)
extern void JsonDeserializationException__ctor_mC3CDF19A33E1411816928D79287A12801D7245A1 ();
// 0x0000003E System.Void JsonFx.Json.JsonTypeCoercionException::.ctor(System.String)
extern void JsonTypeCoercionException__ctor_mB3CE879CC92ED52994A26B727F5AB03B8FB4754D ();
// 0x0000003F System.Void JsonFx.Json.JsonTypeCoercionException::.ctor(System.String,System.Exception)
extern void JsonTypeCoercionException__ctor_m545187B577C8A8C64D02D94AA14B62C64A5930A4 ();
// 0x00000040 System.Boolean JsonFx.Json.JsonReaderSettings::get_AllowUnquotedObjectKeys()
extern void JsonReaderSettings_get_AllowUnquotedObjectKeys_mFCC41656245303C4758DED60E4FB5D676AB85DE6 ();
// 0x00000041 System.Boolean JsonFx.Json.JsonReaderSettings::IsTypeHintName(System.String)
extern void JsonReaderSettings_IsTypeHintName_m89CAE40286B9455945E90D7F81F8D8CFE23274C1 ();
// 0x00000042 System.Void JsonFx.Json.JsonReaderSettings::.ctor()
extern void JsonReaderSettings__ctor_mFE76336ECC6FD56E3205ED103AACD49990A32E9D ();
// 0x00000043 System.Void JsonFx.Json.WriteDelegate`1::.ctor(System.Object,System.IntPtr)
// 0x00000044 System.Void JsonFx.Json.WriteDelegate`1::Invoke(JsonFx.Json.JsonWriter,T)
// 0x00000045 System.IAsyncResult JsonFx.Json.WriteDelegate`1::BeginInvoke(JsonFx.Json.JsonWriter,T,System.AsyncCallback,System.Object)
// 0x00000046 System.Void JsonFx.Json.WriteDelegate`1::EndInvoke(System.IAsyncResult)
// 0x00000047 System.String JsonFx.Json.JsonWriterSettings::get_TypeHintName()
extern void JsonWriterSettings_get_TypeHintName_m9D3C22BBF1A1248F3FCE7903B83FCD12A3D753A2 ();
// 0x00000048 System.Boolean JsonFx.Json.JsonWriterSettings::get_PrettyPrint()
extern void JsonWriterSettings_get_PrettyPrint_mA5082AED8922508C4785B7CDC8788EC06B185272 ();
// 0x00000049 System.String JsonFx.Json.JsonWriterSettings::get_Tab()
extern void JsonWriterSettings_get_Tab_mD3453D1F9D4075B5801E56A3CA65635092BF06B1 ();
// 0x0000004A System.String JsonFx.Json.JsonWriterSettings::get_NewLine()
extern void JsonWriterSettings_get_NewLine_m99E06609BCB072C650E1AF1DE3BF429AE0F8A2B0 ();
// 0x0000004B System.Int32 JsonFx.Json.JsonWriterSettings::get_MaxDepth()
extern void JsonWriterSettings_get_MaxDepth_m41AA4C4706E4C4855596585789EEE038FA8D5688 ();
// 0x0000004C System.Boolean JsonFx.Json.JsonWriterSettings::get_UseXmlSerializationAttributes()
extern void JsonWriterSettings_get_UseXmlSerializationAttributes_m25285DC2FE135AA6ABAAF5163F53643BD2DFB8F1 ();
// 0x0000004D JsonFx.Json.WriteDelegate`1<System.DateTime> JsonFx.Json.JsonWriterSettings::get_DateTimeSerializer()
extern void JsonWriterSettings_get_DateTimeSerializer_mA7E2F63F4F949CE053A5882C5559682425226B35 ();
// 0x0000004E System.Void JsonFx.Json.JsonWriterSettings::.ctor()
extern void JsonWriterSettings__ctor_mDEB9A7392ABF047D42E9506D3C72A10697EACC64 ();
// 0x0000004F System.String JsonFx.Json.JsonNameAttribute::get_Name()
extern void JsonNameAttribute_get_Name_mA3072ACFBE5EDE5343ABCB86994A67EEDC21226B ();
// 0x00000050 System.String JsonFx.Json.JsonNameAttribute::GetJsonName(System.Object)
extern void JsonNameAttribute_GetJsonName_m40A1EC307C78DC6F05F9B582FEF6C0FC449B4D44 ();
// 0x00000051 System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>> JsonFx.Json.TypeCoercionUtility::get_MemberMapCache()
extern void TypeCoercionUtility_get_MemberMapCache_m7BE16E09BAEA5B2BFAF4340D7B300682EACE2083 ();
// 0x00000052 System.Object JsonFx.Json.TypeCoercionUtility::ProcessTypeHint(System.Collections.IDictionary,System.String,System.Type&,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>&)
extern void TypeCoercionUtility_ProcessTypeHint_m09A0513EE810A4717806C04A8281146699D8935D ();
// 0x00000053 System.Object JsonFx.Json.TypeCoercionUtility::InstantiateObject(System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>&)
extern void TypeCoercionUtility_InstantiateObject_m24625FFB51510150E0634B7FC89054F568699DAD ();
// 0x00000054 System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo> JsonFx.Json.TypeCoercionUtility::CreateMemberMap(System.Type)
extern void TypeCoercionUtility_CreateMemberMap_m20C7C103DE1469FD9A736CDD9A41114413DF4F67 ();
// 0x00000055 System.Type JsonFx.Json.TypeCoercionUtility::GetMemberInfo(System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>,System.String,System.Reflection.MemberInfo&)
extern void TypeCoercionUtility_GetMemberInfo_m746AA913C80C09A1333EA0034571EE09E31E7C66 ();
// 0x00000056 System.Void JsonFx.Json.TypeCoercionUtility::SetMemberValue(System.Object,System.Type,System.Reflection.MemberInfo,System.Object)
extern void TypeCoercionUtility_SetMemberValue_m97D2D1A1EFB50660DD3BBFA105CC2E2E8717F827 ();
// 0x00000057 System.Object JsonFx.Json.TypeCoercionUtility::CoerceType(System.Type,System.Object)
extern void TypeCoercionUtility_CoerceType_mA446AC40E86E4A26D6D2CF370CADBB541EB5512B ();
// 0x00000058 System.Object JsonFx.Json.TypeCoercionUtility::CoerceType(System.Type,System.Collections.IDictionary,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>&)
extern void TypeCoercionUtility_CoerceType_mD60B0CC031DFC7029DA3FF7A35CADEFBBF1BE1EF ();
// 0x00000059 System.Object JsonFx.Json.TypeCoercionUtility::CoerceList(System.Type,System.Type,System.Collections.IEnumerable)
extern void TypeCoercionUtility_CoerceList_m96B60071279651BA40A566AF9341F2FF65EC03DA ();
// 0x0000005A System.Array JsonFx.Json.TypeCoercionUtility::CoerceArray(System.Type,System.Collections.IEnumerable)
extern void TypeCoercionUtility_CoerceArray_m2FB27F8ACABD0459156E1DF446BE4B9AD63CC64C ();
// 0x0000005B System.Boolean JsonFx.Json.TypeCoercionUtility::IsNullable(System.Type)
extern void TypeCoercionUtility_IsNullable_m3DF953D3ED7E3060D7006D0104B487B8FE6D7D61 ();
// 0x0000005C System.Void JsonFx.Json.TypeCoercionUtility::.ctor()
extern void TypeCoercionUtility__ctor_m77AB69F57C051490AF9BA1D511F96906B3DDD094 ();
// 0x0000005D System.Void JsonFx.Json.IJsonSerializable::WriteJson(JsonFx.Json.JsonWriter)
// 0x0000005E System.Boolean JsonFx.Json.JsonIgnoreAttribute::IsJsonIgnore(System.Object)
extern void JsonIgnoreAttribute_IsJsonIgnore_m189AAD2BB10F3E71DB5E7D5E14F5097617E52D89 ();
// 0x0000005F System.Boolean JsonFx.Json.JsonIgnoreAttribute::IsXmlIgnore(System.Object)
extern void JsonIgnoreAttribute_IsXmlIgnore_mDD52CCDE04D83068CEC43F2CC332F2B10D56698A ();
static Il2CppMethodPointer s_methodPointers[95] = 
{
	JsonSpecifiedPropertyAttribute_get_SpecifiedProperty_mE86D8FC1F7D4DB0D7CACC156A9F34AF8942315E7,
	JsonSpecifiedPropertyAttribute_GetJsonSpecifiedProperty_m93A65B10974855AE7EC7F5B6F1B14CFC14EEC2A6,
	JsonReader__ctor_mD669FC28EDB5578CAC43F8EAF30C4B0A719E2DEA,
	JsonReader__ctor_m4108B2839741F3325D82776AD0FAFF9E3C713B7F,
	NULL,
	JsonReader_Deserialize_mD1A9E9B0C25DF0C5D94AFC711657CD8C70DFF1FB,
	JsonReader_Read_m7F9C103441F37E24FF0DA2CDA3A763E58C41C0D6,
	JsonReader_ReadObject_mE8C6875E1A8AC9B528E5A2A652C06422CBDEF311,
	JsonReader_ReadArray_m43204159B4A9E4F0D3D9E2A005BB8F2CE4835B8F,
	JsonReader_ReadUnquotedKey_mF0E827ECC2E9B75D2E36D3C392E74EF70F8EC80E,
	JsonReader_ReadString_m4EDED0CB9C3DEF61CB53BF308E85A1097F69ECFF,
	JsonReader_ReadNumber_mD5C165D1850CFC024CF5ACD30F1559834FA9D9C4,
	NULL,
	JsonReader_Deserialize_m71E4BEA58D4FA9B338A881EC78BC92C0FC9E11BC,
	JsonReader_Tokenize_mD64CDED06219B05E058C22E9B7A5524D9FCE2CB1,
	JsonReader_Tokenize_mFC446B66A59F7798D2F4DB8563DD40186E0FEB1F,
	JsonReader_MatchLiteral_m3000CF34D40D87A9FB9B00C37A30D917815AC0C9,
	JsonWriter__ctor_m44528C95A3B303BB212485792F285ABE5DAADC70,
	JsonWriter__ctor_mE2152F2B858BA35A24028347D3F8F1AED9139FEE,
	JsonWriter_Serialize_m42F997E8776437DC5B9FD8314A19290024238E75,
	JsonWriter_Write_mAB3146C5C739157535492E9C401B36D45A6FCB58,
	JsonWriter_Write_m7E232376035D7B4505A35BD28D924F71F12A608A,
	JsonWriter_Write_m976D908048B83A994B2FEF624AC9C39F11CDCF28,
	JsonWriter_Write_mA150B93DD0D129077DCE1C1D2A1426E328198CE7,
	JsonWriter_Write_m1E76469DF860ED670DAF0B82DCDBA7162315CB7C,
	JsonWriter_Write_mE485F975E74C8B364BD7D647213747AE5674ACDD,
	JsonWriter_Write_m98CFD0BB99396076681469ADA518FCFB7D075877,
	JsonWriter_Write_m0C5447C5A053D67E8B6ACCE5AEB7A1D3F83599EE,
	JsonWriter_Write_mF8BFDD2E893D018DCFF9B04AA4FE76ECF04C7E97,
	JsonWriter_Write_m168FE1A3793DF1776311E64EA3310A238175E8B2,
	JsonWriter_Write_mD357CB0069DDB3B758EF935A9FAAAA315103E624,
	JsonWriter_Write_mD9D3420E0DD2DAC4288F001B9F0AD1DA73D89A49,
	JsonWriter_Write_mCAEEDCDA0A4D3FF555B0476E006C2E30E35AC342,
	JsonWriter_Write_m809E7F0921B0F760E843D5A01A5DD59D1E2D56BB,
	JsonWriter_Write_m43C92A32F2E6487C85204EECC1C74F4634CBBAAF,
	JsonWriter_Write_mE67AEEA89C414CEE89DADFE28EE282A99890F79B,
	JsonWriter_Write_m01711F633E189B7B86DA14BFB20CB1BF43DCC4A8,
	JsonWriter_Write_mD2A90E4D11192E274E0D9ECB5FF744EC543D1C10,
	JsonWriter_Write_m4B73B1FCD88F714866B8B8C155304B44867E9995,
	JsonWriter_Write_m2350E1F4C9257620643AFA5D58A348685A26137C,
	JsonWriter_Write_m35ECB1CDBBA4BB0D5A736A7776B8B360BE522EF8,
	JsonWriter_Write_m1D8134CD36C0D7DBEC06A8100D3F5A61EF87AEFD,
	JsonWriter_Write_mDAA00ED5AFDB6E3BAFF46639B252839F8164C84B,
	JsonWriter_WriteArray_m7256F1BCE22E598266F15256ED5F3BF16DA872F3,
	JsonWriter_WriteArrayItem_mC6222637A1242794BBEBD63DA3011D4D15A05648,
	JsonWriter_WriteObject_m54628E791AD4730BF3431D74E08469AEDAB52EDA,
	JsonWriter_WriteDictionary_m145C4E3DE1598D2167F28322BBFC58A953A7C3AE,
	JsonWriter_WriteObjectProperty_m296EA3652BC14AADFDA4B02C8A178D2AA1976246,
	JsonWriter_WriteObjectPropertyName_mFEE866A2CD3CD4E587888BB29A68960DC37B325A,
	JsonWriter_WriteObjectPropertyValue_mA2843D073A0F0CD3BBDA9C2E3A7CF173E56119ED,
	JsonWriter_WriteObject_m980AC0C8CB40384D5E6AEC77980B9F80F85AC7C9,
	JsonWriter_WriteArrayItemDelim_m40522E6F9E6E4AB181299352458D9C45B292121E,
	JsonWriter_WriteObjectPropertyDelim_m521F90CAD24CC222F1DA650E07FC29C3C0061283,
	JsonWriter_WriteLine_m2C958713FB694419CD1C462508DED4593D8F8861,
	JsonWriter_IsIgnored_mEBA309224C37535D96BEC2A93C464FCA00D850FC,
	JsonWriter_IsDefaultValue_mBE04C53F16CACA9B44FE01EF1019F12BB7CCABB7,
	JsonWriter_GetFlagList_mA3CFB3A6C75D4BC31C8437C4248EF6B500D38294,
	JsonWriter_InvalidIeee754_m3050CF5BEB3A72C6F8A6E25C8748C79C0BAA3D02,
	JsonWriter_System_IDisposable_Dispose_m29339D9F3F2E84D0D5690BFAF5115EFDD08E0BBD,
	JsonSerializationException__ctor_mE1D225D3BF5681977C5946142859A1F623123ACB,
	JsonDeserializationException__ctor_mC3CDF19A33E1411816928D79287A12801D7245A1,
	JsonTypeCoercionException__ctor_mB3CE879CC92ED52994A26B727F5AB03B8FB4754D,
	JsonTypeCoercionException__ctor_m545187B577C8A8C64D02D94AA14B62C64A5930A4,
	JsonReaderSettings_get_AllowUnquotedObjectKeys_mFCC41656245303C4758DED60E4FB5D676AB85DE6,
	JsonReaderSettings_IsTypeHintName_m89CAE40286B9455945E90D7F81F8D8CFE23274C1,
	JsonReaderSettings__ctor_mFE76336ECC6FD56E3205ED103AACD49990A32E9D,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonWriterSettings_get_TypeHintName_m9D3C22BBF1A1248F3FCE7903B83FCD12A3D753A2,
	JsonWriterSettings_get_PrettyPrint_mA5082AED8922508C4785B7CDC8788EC06B185272,
	JsonWriterSettings_get_Tab_mD3453D1F9D4075B5801E56A3CA65635092BF06B1,
	JsonWriterSettings_get_NewLine_m99E06609BCB072C650E1AF1DE3BF429AE0F8A2B0,
	JsonWriterSettings_get_MaxDepth_m41AA4C4706E4C4855596585789EEE038FA8D5688,
	JsonWriterSettings_get_UseXmlSerializationAttributes_m25285DC2FE135AA6ABAAF5163F53643BD2DFB8F1,
	JsonWriterSettings_get_DateTimeSerializer_mA7E2F63F4F949CE053A5882C5559682425226B35,
	JsonWriterSettings__ctor_mDEB9A7392ABF047D42E9506D3C72A10697EACC64,
	JsonNameAttribute_get_Name_mA3072ACFBE5EDE5343ABCB86994A67EEDC21226B,
	JsonNameAttribute_GetJsonName_m40A1EC307C78DC6F05F9B582FEF6C0FC449B4D44,
	TypeCoercionUtility_get_MemberMapCache_m7BE16E09BAEA5B2BFAF4340D7B300682EACE2083,
	TypeCoercionUtility_ProcessTypeHint_m09A0513EE810A4717806C04A8281146699D8935D,
	TypeCoercionUtility_InstantiateObject_m24625FFB51510150E0634B7FC89054F568699DAD,
	TypeCoercionUtility_CreateMemberMap_m20C7C103DE1469FD9A736CDD9A41114413DF4F67,
	TypeCoercionUtility_GetMemberInfo_m746AA913C80C09A1333EA0034571EE09E31E7C66,
	TypeCoercionUtility_SetMemberValue_m97D2D1A1EFB50660DD3BBFA105CC2E2E8717F827,
	TypeCoercionUtility_CoerceType_mA446AC40E86E4A26D6D2CF370CADBB541EB5512B,
	TypeCoercionUtility_CoerceType_mD60B0CC031DFC7029DA3FF7A35CADEFBBF1BE1EF,
	TypeCoercionUtility_CoerceList_m96B60071279651BA40A566AF9341F2FF65EC03DA,
	TypeCoercionUtility_CoerceArray_m2FB27F8ACABD0459156E1DF446BE4B9AD63CC64C,
	TypeCoercionUtility_IsNullable_m3DF953D3ED7E3060D7006D0104B487B8FE6D7D61,
	TypeCoercionUtility__ctor_m77AB69F57C051490AF9BA1D511F96906B3DDD094,
	NULL,
	JsonIgnoreAttribute_IsJsonIgnore_m189AAD2BB10F3E71DB5E7D5E14F5097617E52D89,
	JsonIgnoreAttribute_IsXmlIgnore_mDD52CCDE04D83068CEC43F2CC332F2B10D56698A,
};
static const int32_t s_InvokerIndices[95] = 
{
	14,
	0,
	26,
	27,
	-1,
	445,
	112,
	28,
	28,
	14,
	28,
	28,
	-1,
	216,
	10,
	188,
	9,
	26,
	27,
	0,
	26,
	417,
	295,
	968,
	26,
	26,
	31,
	31,
	31,
	573,
	573,
	32,
	32,
	172,
	172,
	307,
	308,
	900,
	573,
	901,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	27,
	26,
	26,
	27,
	23,
	23,
	23,
	876,
	115,
	1,
	314,
	23,
	26,
	136,
	26,
	27,
	114,
	9,
	23,
	-1,
	-1,
	-1,
	-1,
	14,
	114,
	14,
	14,
	10,
	114,
	14,
	23,
	14,
	0,
	14,
	971,
	442,
	28,
	687,
	404,
	113,
	654,
	177,
	113,
	94,
	23,
	26,
	94,
	94,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x06000005, { 0, 2 } },
	{ 0x0600000D, { 2, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[4] = 
{
	{ (Il2CppRGCTXDataType)1, 13968 },
	{ (Il2CppRGCTXDataType)2, 13968 },
	{ (Il2CppRGCTXDataType)1, 13969 },
	{ (Il2CppRGCTXDataType)2, 13969 },
};
extern const Il2CppCodeGenModule g_JsonFx_JsonCodeGenModule;
const Il2CppCodeGenModule g_JsonFx_JsonCodeGenModule = 
{
	"JsonFx.Json.dll",
	95,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	4,
	s_rgctxValues,
	NULL,
};
