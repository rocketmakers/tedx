﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1, typename T2, typename T3>
struct VirtActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct VirtFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct GenericVirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct GenericVirtFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct InterfaceFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct InterfaceFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct GenericInterfaceFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct GenericInterfaceFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};

// System.Action`1<System.Object>
struct Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0;
// System.ArgumentException
struct ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1;
// System.ArgumentNullException
struct ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Int32>[]
struct EntryU5BU5D_tAD4FDE2B2578C6625A7296B1C46DCB06DCB45186;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Int32>
struct KeyCollection_t666396E67E50284D48938851873CE562083D67F2;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Int32>
struct ValueCollection_t532E2FD863D0D47B87202BE6B4F7C7EDB5DD7CBF;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo>
struct Dictionary_2_tC88A56872F7C79DBB9582D4F3FC22ED5D8E0B98B;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A;
// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo>
struct Dictionary_2_tBA5388DBB42BF620266F9A48E8B859BBBB224E25;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_tAE7A8756D8CF0882DD348DC328FB36FEE0FB7DD0;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t1F07EAC22CC1D4F279164B144240E4718BD7E7A9;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Exception
struct Exception_t;
// System.Globalization.Calendar
struct Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB;
// System.Globalization.CompareInfo
struct CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1;
// System.Globalization.CultureData
struct CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD;
// System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8;
// System.Globalization.TextInfo
struct TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IFormatProvider
struct IFormatProvider_t4247E13AE2D97A079B88D594B7ABABF313259901;
// System.IO.StringWriter
struct StringWriter_t194EF1526E072B93984370042AA80926C2EB6139;
// System.IO.TextWriter
struct TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.InvalidOperationException
struct InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.IFormatterConverter
struct IFormatterConverter_tC3280D64D358F47EA4DAF1A65609BA0FC081888A;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26;
// System.Runtime.Serialization.SerializationInfoEnumerator
struct SerializationInfoEnumerator_tB72162C419D705A40F34DDFEB18E6ACA347C54E5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.SystemException
struct SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782;
// System.Text.DecoderFallback
struct DecoderFallback_t128445EB7676870485230893338EF044F6B72F60;
// System.Text.EncoderFallback
struct EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Text.UnicodeEncoding
struct UnicodeEncoding_t6E0E60A1D7A4BECF9901B00EB286FFC2B57F6356;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// System.Xml.Base64Encoder
struct Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922;
// System.Xml.SecureStringHasher
struct SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3;
// System.Xml.SecureStringHasher/HashCodeOfStringDelegate
struct HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E;
// System.Xml.XmlChildEnumerator
struct XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA;
// System.Xml.XmlDOMTextWriter
struct XmlDOMTextWriter_t63DEA09C11BB4D12F03C65BC12618E45C4C3E436;
// System.Xml.XmlException
struct XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E;
// System.Xml.XmlNode
struct XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB;
// System.Xml.XmlTextEncoder
struct XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475;
// System.Xml.XmlTextWriter
struct XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6;
// System.Xml.XmlTextWriter/Namespace[]
struct NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829;
// System.Xml.XmlTextWriter/State[]
struct StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2;
// System.Xml.XmlTextWriter/TagInfo[]
struct TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910;
// System.Xml.XmlTextWriterBase64Encoder
struct XmlTextWriterBase64Encoder_t1362FFBE93C9BC675B4A34503DB31AA6D3AAF01C;
// System.Xml.XmlWriter
struct XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869;

IL2CPP_EXTERN_C RuntimeClass* ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Convert_t1C7A851BFB2F0782FD7F72F6AA1DCBB7B53A9C7E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MissingManifestResourceException_tD8397DA5F4CC63B78F91916522A302782CAB0261_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringBuilder_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringWriter_t194EF1526E072B93984370042AA80926C2EB6139_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* String_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlDOMTextWriter_t63DEA09C11BB4D12F03C65BC12618E45C4C3E436_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB____5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB____6A0D50D692745A6663128CD315B71079584F3E59_1_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB____B368804F0C6DAB083B253A6B106D0783D5C32E90_2_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB____EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_3_FieldInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral03604A939E36BFD5AAB4D02EC49C56DD5FFE70A4;
IL2CPP_EXTERN_C String_t* _stringLiteral0B0C6F90D172B22857FDB7C4E16D3DD858581ACC;
IL2CPP_EXTERN_C String_t* _stringLiteral0D7652C6A78D387AD1AF2F86D37C74C5E6FB9F7A;
IL2CPP_EXTERN_C String_t* _stringLiteral0F74095E24003C6F3BF3D236FF3CE7D75E3BC68E;
IL2CPP_EXTERN_C String_t* _stringLiteral11FF44718A597D1A371E7DA9C818486F238E49E2;
IL2CPP_EXTERN_C String_t* _stringLiteral153D7A58B3A3E898FCBDD04C462AF308414BD09D;
IL2CPP_EXTERN_C String_t* _stringLiteral16A3C2A876EADFBFB7EF0193C48BCF6088E0B6AD;
IL2CPP_EXTERN_C String_t* _stringLiteral1760FCF206FD55ED5034D5189D50BD82B9A45259;
IL2CPP_EXTERN_C String_t* _stringLiteral19EDC1210777BA4D45049C29280D9CC5E1064C25;
IL2CPP_EXTERN_C String_t* _stringLiteral1AE76814588A8852BEC1A4F868B17D08026CF135;
IL2CPP_EXTERN_C String_t* _stringLiteral2152BA8F95B60B0E0D6C97D831B87EEFB1BF030D;
IL2CPP_EXTERN_C String_t* _stringLiteral2547BF06A9A1BA77AA1004FACBFABAE4834F8A18;
IL2CPP_EXTERN_C String_t* _stringLiteral2549FF1641A81EF92E721D01E5458A242BD96B83;
IL2CPP_EXTERN_C String_t* _stringLiteral29A6E802123FF6EA94EC6F96DDA470B3FA755A58;
IL2CPP_EXTERN_C String_t* _stringLiteral2D805000F0B1B718831F4576ED6B82409607A53D;
IL2CPP_EXTERN_C String_t* _stringLiteral3030E728F154BF51419109EFB93B6B8AEEC9A976;
IL2CPP_EXTERN_C String_t* _stringLiteral30510FE0BC6A8F6A62E9D5309EF4010DB2D81A82;
IL2CPP_EXTERN_C String_t* _stringLiteral307C73F00D17075B311C1EA5BD62B7C512AE2D6C;
IL2CPP_EXTERN_C String_t* _stringLiteral3159FE421B3221381B3C778DC1C3C26E4540BE37;
IL2CPP_EXTERN_C String_t* _stringLiteral3CF80C8340B4CACB2EDD9BE6CC78DDB7CED73BA6;
IL2CPP_EXTERN_C String_t* _stringLiteral45815B75A6BA88FC4268CF681E4B17FA4CEEA4EC;
IL2CPP_EXTERN_C String_t* _stringLiteral47B13CDB0607925057B8E9D36B0386D1A11EB54C;
IL2CPP_EXTERN_C String_t* _stringLiteral49FA9FD831096C82AB950BC7AA0A0A7EF8A10998;
IL2CPP_EXTERN_C String_t* _stringLiteral4AF0A653663892E6E605DC7C5B286A0FF6E4247B;
IL2CPP_EXTERN_C String_t* _stringLiteral4C7B4D20A57217BA507BD50082B44D927E116DA8;
IL2CPP_EXTERN_C String_t* _stringLiteral4F9BE057F0EA5D2BA72FD2C810E8D7B9AA98B469;
IL2CPP_EXTERN_C String_t* _stringLiteral528F5D16BFCF00BFD2F2B5D78810F653D013CBFB;
IL2CPP_EXTERN_C String_t* _stringLiteral5368F75FAEDF45E142655CD9D9FF084E577BBD2D;
IL2CPP_EXTERN_C String_t* _stringLiteral53A610E925BBC0A175E365D31241AE75AEEAD651;
IL2CPP_EXTERN_C String_t* _stringLiteral5F3ACFBEB4F6FA5007DD1137AB1E96149AF87281;
IL2CPP_EXTERN_C String_t* _stringLiteral616998B237EE9C3DBBF79DB6CA02275975880AE0;
IL2CPP_EXTERN_C String_t* _stringLiteral65F59EC6B1ECD6170D5044474043CCA9560A8071;
IL2CPP_EXTERN_C String_t* _stringLiteral6BDB3C5694D4A738A5ED7467CC26DEF7664323A6;
IL2CPP_EXTERN_C String_t* _stringLiteral6BDCD57D44C0013FD66AB0C7E3EB8C27BDCF9D07;
IL2CPP_EXTERN_C String_t* _stringLiteral70142F66475AE2FB33722D8D4750F386ECFEFE7B;
IL2CPP_EXTERN_C String_t* _stringLiteral7505D64A54E061B7ACD54CCD58B49DC43500B635;
IL2CPP_EXTERN_C String_t* _stringLiteral77B3FBA04FD4C4EEEE9AB3A7FB216ADBB7982995;
IL2CPP_EXTERN_C String_t* _stringLiteral7859E7AFFA569B5D7ACA069908DBD2ED9F05629B;
IL2CPP_EXTERN_C String_t* _stringLiteral7ABE4DB8E4E34EA3395A2A251F5922B296325989;
IL2CPP_EXTERN_C String_t* _stringLiteral7DC5BBB566674E5C157A33F9829DE3FCACCA44BD;
IL2CPP_EXTERN_C String_t* _stringLiteral7DDF988C838812A4318332F2967BBE1035B2DB75;
IL2CPP_EXTERN_C String_t* _stringLiteral7F2F6A15CF8DA2B27E5A4AF47B58E7AD71C0B3D9;
IL2CPP_EXTERN_C String_t* _stringLiteral85FB26FBD8AB37E35E0B5F53103D2B500B090CD2;
IL2CPP_EXTERN_C String_t* _stringLiteral88D86B7721D587644E9C4CF33A084202CB3B0FF0;
IL2CPP_EXTERN_C String_t* _stringLiteral895C330FF6166A7F13701F8D9B78F313787269C8;
IL2CPP_EXTERN_C String_t* _stringLiteral929FB4AD13AB3C3E59695178677C8D8149AAD0C2;
IL2CPP_EXTERN_C String_t* _stringLiteral936848C6D2B08E4C0D182C45261D876982BCFE29;
IL2CPP_EXTERN_C String_t* _stringLiteral952F375412E89FF213A8ACA383D18E5691354347;
IL2CPP_EXTERN_C String_t* _stringLiteralA086D942884A301B827479029BA19C2746237425;
IL2CPP_EXTERN_C String_t* _stringLiteralA27E2E9EF6A0C7CC58D71302FCA9E93BA677C130;
IL2CPP_EXTERN_C String_t* _stringLiteralB4F3203E222557090E52A70DF590EC32DB60D176;
IL2CPP_EXTERN_C String_t* _stringLiteralB94C6BF9E158406E9B439472751E3DC6C4AAA530;
IL2CPP_EXTERN_C String_t* _stringLiteralBA4D6E652036F4C7322CA4CF6D5A6AD5D7A37815;
IL2CPP_EXTERN_C String_t* _stringLiteralC032ADC1FF629C9B66F22749AD667E6BEADF144B;
IL2CPP_EXTERN_C String_t* _stringLiteralC692273DEB2772DA307FFE37041FEF77BF4BAA97;
IL2CPP_EXTERN_C String_t* _stringLiteralCBD550861E744B88C938D241E09AD55250E11DB4;
IL2CPP_EXTERN_C String_t* _stringLiteralD8A074E1D9365F0A70DCF0E727BD10F33EE154D9;
IL2CPP_EXTERN_C String_t* _stringLiteralDAF817FEF227A2B8E59937DAAE3E82D2A6A4644A;
IL2CPP_EXTERN_C String_t* _stringLiteralDEE1EBCD105D3D47ADF43ABA6FD674E80D1DC35F;
IL2CPP_EXTERN_C String_t* _stringLiteralE7064F0B80F61DBC65915311032D27BAA569AE2A;
IL2CPP_EXTERN_C String_t* _stringLiteralE81A0D33628A7C7FFEDE35A7FC0D572D077F40F2;
IL2CPP_EXTERN_C String_t* _stringLiteralE900984DC91DC9C7F107CA96EA386473DA13D8F8;
IL2CPP_EXTERN_C String_t* _stringLiteralE9580394535B0E66F03CAFDAD8F8AEF279BB1C12;
IL2CPP_EXTERN_C String_t* _stringLiteralEB8431F8E06C8492F2806E7A7EAA8A4A288D55D1;
IL2CPP_EXTERN_C String_t* _stringLiteralEE6600091DF72F57E330216D90436099635DAFBC;
IL2CPP_EXTERN_C String_t* _stringLiteralEE9F38E186BA06F57B7B74D7E626B94E13CE2556;
IL2CPP_EXTERN_C String_t* _stringLiteralF2B44AFF371DB997AA89F469BF68B7C11526B71D;
IL2CPP_EXTERN_C String_t* _stringLiteralFA7FF6FB00A7ACE8D98CF880A492DC6C5FFC8CA7;
IL2CPP_EXTERN_C String_t* _stringLiteralFD7A2B83CB0BA8522402983CB3068C33624C6687;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Remove_mF764B8FFA63013272F75AFEF5F54C423F340D771_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_TryGetValue_m3B7C9CE4CBBCB908CD81487D7924E93E9309FB67_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mF6BE1085F30E8C47064A8E0583DABCBE0B35DC21_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_mDBA4C5D99B9605672D2F04AF3184C5E043D1BD3A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SecureStringHasher_GetHashCodeOfString_mC56D819EA0533A7F2DCDE3260D618DCFE68F88D6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* XmlChildEnumerator_get_Current_m8ED001FF27613B8D7040CBA88CD1BF9DE3AC9872_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* XmlTextEncoder_WriteRaw_mE2BB17DA3D685325F30E98E590CC58BBB7781628_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* XmlTextEncoder_WriteSurrogateChar_mCCE79921A16872FB57C3C5C21F7518CCE5F504F3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* XmlTextEncoder_Write_mD02582AB249921681B2B7C85E71DC17E20AB3884_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* XmlTextWriter_AutoComplete_mCFC6565788633CD667823E70E1FD8A9AF241B664_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* XmlTextWriter_HandleSpecialAttribute_m8AFEBFA51F9B23D67C213F35E0ADF91171DB0ECB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* XmlTextWriter_InternalWriteEndElement_m02FCE5CCA71DBFBA885AF81FFAD702B3BC39294F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* XmlTextWriter_PushNamespace_m3DA0494C202BD5C6AE32E9C5FD7FEAC6B4ED8548_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* XmlTextWriter_VerifyPrefixXml_m44A57489D6DFFAD820FE4DF6B113DB3B58E6F8E0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* String_t_0_0_0_var;
IL2CPP_EXTERN_C const uint32_t Base64Encoder_Flush_mCC2F11678DA21435D1A00BFDA1D9FF97B9605B6D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HashCodeOfStringDelegate_BeginInvoke_m86BB834B9D3C76936DA4190EB5523FF3C5C757ED_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SR_GetString_m4356FFC0B1BA9CB3F26E73B41B74BC39FE4E8A96_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SecureStringHasher_GetHashCodeDelegate_m31822E4C8B12FCFD4E39A65D04BF5606A35EE022_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SecureStringHasher_GetHashCode_m867EF141E418F59C37589E420CAFE018C5072849_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TagInfo_Init_m631D10B6F7160EC8D54CAAF1418215168F8E6F32_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlCharType_InitInstance_m48449C6A7516A943668D92903B5D4203DD184AC6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlCharType_SetProperties_m892CAB57FF4A04EB120C6AA8D4ACA9645ED0A72C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlCharType_get_Instance_mEAAD3E43BD5AC72FA94C12096B2A9C9684557210_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlCharType_get_StaticLock_mE0FF2E8B12DC2AFFAFAB5718FDD49FAE726A6513_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_com_FromNativeMethodDefinition_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlChildEnumerator_get_Current_m8ED001FF27613B8D7040CBA88CD1BF9DE3AC9872_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlConvert_CreateException_m94C72E2D80667A0F1A8AB7B7DBC916CA6C527002_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlConvert_CreateException_m956710818209FE5D8F7999590B30376FEFBC2F90_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlConvert_CreateInvalidHighSurrogateCharException_m22453CD571E079C62CC668E732C74CC1EFE47CFD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlConvert_CreateInvalidHighSurrogateCharException_m4A1C627F6492AD6A98B4CA982A9197BF39C73DA8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlConvert_CreateInvalidHighSurrogateCharException_mAC251160BC8A0DB0389838A689D95A3C898B270B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlConvert_CreateInvalidSurrogatePairException_m7ADDD2E98288144ED30A75715AD4254E484FAC70_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlConvert_CreateInvalidSurrogatePairException_mC493A88B7ED7BFEF845DD2ABA4824D395107F812_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlConvert_CreateInvalidSurrogatePairException_mD7779092577F68BAD64FA0A0549CF1916E6B9914_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlConvert_TrimString_m89152D6729B89C0423168B5C60E0191A773AD1FA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlConvert__cctor_m1EE317B21041E9F0ABE85E4DB9EE427C21DB4F03_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlDOMTextWriter__ctor_mFF0BC0EC83712D01433AE7115529DCC96B38EE11_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlException_CreateMessage_mDD847D2C1685E48408F191251542EB3C754344D5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlException_FormatUserMessage_mDFED8BDE616EF903472039DEE5F5B1F59E5FCCFF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlException_GetObjectData_m8C9F4943DC440A3FBF70905487652549D1B0BE45_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlException__ctor_m2038069D4E6C83DC027DFDD97AD142B723C60C36_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlException__ctor_m2CA134DF0552F0FDBBDC49D5E823831CE5A0B325_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlException__ctor_mC412B820BFDD1777E4423CA896912FAAD077B783_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlNode_System_Collections_IEnumerable_GetEnumerator_mC8BE3ACFBE87B8F5ABAE7B797FAF48A900679B27_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlNode_get_OuterXml_m4198C72CA7F3F13BE8AA34E2F392B9BC7F1EFE98_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlNode_get_ParentNode_m6AC4A7F4FEE5B469A86490C129F011C12201C3E6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlReader__cctor_m3EF2489C81E4EEF249A34242FC130A861972991E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextEncoder_StartAttribute_mAFEF454A34E72F5B1457C8D634FA947EA0023D15_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextEncoder_WriteCharEntityImpl_m8DB50E2042CA07D11456C0B67B6EE9D28E8A817E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextEncoder_WriteCharEntityImpl_mEA75C2144CCCF2DF29BB91D3B91B2C06C1E6920F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextEncoder_WriteRaw_mE2BB17DA3D685325F30E98E590CC58BBB7781628_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextEncoder_WriteSurrogateChar_mCCE79921A16872FB57C3C5C21F7518CCE5F504F3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextEncoder_Write_mD02582AB249921681B2B7C85E71DC17E20AB3884_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextEncoder_get_AttributeValue_m7D58DB09133999166C984994CA05C48B469DEC7D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter_AddNamespace_m78549A0DDF0923A00664A6DE1B425157097E5ED0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter_AddToNamespaceHashtable_mEF08299106F8B3D846EC6D474D02508D5664D429_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter_AutoComplete_mCFC6565788633CD667823E70E1FD8A9AF241B664_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter_Close_m8D3926EF1A95024EB7FBD9F69C589546238E6610_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter_HandleSpecialAttribute_m8AFEBFA51F9B23D67C213F35E0ADF91171DB0ECB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter_InternalWriteEndElement_m02FCE5CCA71DBFBA885AF81FFAD702B3BC39294F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter_LookupNamespace_mA477CDBF25662EC77FF5B025FB2E83B400C381FD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter_PopNamespaces_m5D748E8059ED8B3F04DB600618EE9A7CA3A2F158_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter_PushNamespace_m3DA0494C202BD5C6AE32E9C5FD7FEAC6B4ED8548_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter_VerifyPrefixXml_m44A57489D6DFFAD820FE4DF6B113DB3B58E6F8E0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter_WriteEndStartTag_m5905B79D7EE7EE0049919B9B879979313D9664FA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter__cctor_mC61966BD08862DDA9FF9033C1B5834F9BD887CC1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter__ctor_m6FA653B70BB118A3B8BDA6E29CFF8911CA060EE2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XmlTextWriter__ctor_mED6FB595B6052C306BE5175DC8E20DD974A2CEFC_MetadataUsageId;
struct CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD_marshaled_com;
struct CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD_marshaled_pinvoke;
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_com;
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_pinvoke;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
struct NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829;
struct StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2;
struct TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tF13288789FFC191403B42FA365065C7F468479D2 
{
public:

public:
};


// System.Object


// SR
struct  SR_tBB633556516BC6B301F5A1DFDBBD62AC2079B597  : public RuntimeObject
{
public:

public:
};

struct Il2CppArrayBounds;

// System.Array


// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};


// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct  Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___buckets_0;
	// System.Collections.Generic.Dictionary`2_Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_tAD4FDE2B2578C6625A7296B1C46DCB06DCB45186* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2_KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t666396E67E50284D48938851873CE562083D67F2 * ___keys_7;
	// System.Collections.Generic.Dictionary`2_ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t532E2FD863D0D47B87202BE6B4F7C7EDB5DD7CBF * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___buckets_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___entries_1)); }
	inline EntryU5BU5D_tAD4FDE2B2578C6625A7296B1C46DCB06DCB45186* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_tAD4FDE2B2578C6625A7296B1C46DCB06DCB45186** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_tAD4FDE2B2578C6625A7296B1C46DCB06DCB45186* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___keys_7)); }
	inline KeyCollection_t666396E67E50284D48938851873CE562083D67F2 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t666396E67E50284D48938851873CE562083D67F2 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t666396E67E50284D48938851873CE562083D67F2 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___values_8)); }
	inline ValueCollection_t532E2FD863D0D47B87202BE6B4F7C7EDB5DD7CBF * get_values_8() const { return ___values_8; }
	inline ValueCollection_t532E2FD863D0D47B87202BE6B4F7C7EDB5DD7CBF ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t532E2FD863D0D47B87202BE6B4F7C7EDB5DD7CBF * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Globalization.CultureInfo
struct  CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F  : public RuntimeObject
{
public:
	// System.Boolean System.Globalization.CultureInfo::m_isReadOnly
	bool ___m_isReadOnly_3;
	// System.Int32 System.Globalization.CultureInfo::cultureID
	int32_t ___cultureID_4;
	// System.Int32 System.Globalization.CultureInfo::parent_lcid
	int32_t ___parent_lcid_5;
	// System.Int32 System.Globalization.CultureInfo::datetime_index
	int32_t ___datetime_index_6;
	// System.Int32 System.Globalization.CultureInfo::number_index
	int32_t ___number_index_7;
	// System.Int32 System.Globalization.CultureInfo::default_calendar_type
	int32_t ___default_calendar_type_8;
	// System.Boolean System.Globalization.CultureInfo::m_useUserOverride
	bool ___m_useUserOverride_9;
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::numInfo
	NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * ___numInfo_10;
	// System.Globalization.DateTimeFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::dateTimeInfo
	DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F * ___dateTimeInfo_11;
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::textInfo
	TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 * ___textInfo_12;
	// System.String System.Globalization.CultureInfo::m_name
	String_t* ___m_name_13;
	// System.String System.Globalization.CultureInfo::englishname
	String_t* ___englishname_14;
	// System.String System.Globalization.CultureInfo::nativename
	String_t* ___nativename_15;
	// System.String System.Globalization.CultureInfo::iso3lang
	String_t* ___iso3lang_16;
	// System.String System.Globalization.CultureInfo::iso2lang
	String_t* ___iso2lang_17;
	// System.String System.Globalization.CultureInfo::win3lang
	String_t* ___win3lang_18;
	// System.String System.Globalization.CultureInfo::territory
	String_t* ___territory_19;
	// System.String[] System.Globalization.CultureInfo::native_calendar_names
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___native_calendar_names_20;
	// System.Globalization.CompareInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::compareInfo
	CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * ___compareInfo_21;
	// System.Void* System.Globalization.CultureInfo::textinfo_data
	void* ___textinfo_data_22;
	// System.Int32 System.Globalization.CultureInfo::m_dataItem
	int32_t ___m_dataItem_23;
	// System.Globalization.Calendar System.Globalization.CultureInfo::calendar
	Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 * ___calendar_24;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::parent_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___parent_culture_25;
	// System.Boolean System.Globalization.CultureInfo::constructed
	bool ___constructed_26;
	// System.Byte[] System.Globalization.CultureInfo::cached_serialized_form
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___cached_serialized_form_27;
	// System.Globalization.CultureData System.Globalization.CultureInfo::m_cultureData
	CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD * ___m_cultureData_28;
	// System.Boolean System.Globalization.CultureInfo::m_isInherited
	bool ___m_isInherited_29;

public:
	inline static int32_t get_offset_of_m_isReadOnly_3() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_isReadOnly_3)); }
	inline bool get_m_isReadOnly_3() const { return ___m_isReadOnly_3; }
	inline bool* get_address_of_m_isReadOnly_3() { return &___m_isReadOnly_3; }
	inline void set_m_isReadOnly_3(bool value)
	{
		___m_isReadOnly_3 = value;
	}

	inline static int32_t get_offset_of_cultureID_4() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___cultureID_4)); }
	inline int32_t get_cultureID_4() const { return ___cultureID_4; }
	inline int32_t* get_address_of_cultureID_4() { return &___cultureID_4; }
	inline void set_cultureID_4(int32_t value)
	{
		___cultureID_4 = value;
	}

	inline static int32_t get_offset_of_parent_lcid_5() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___parent_lcid_5)); }
	inline int32_t get_parent_lcid_5() const { return ___parent_lcid_5; }
	inline int32_t* get_address_of_parent_lcid_5() { return &___parent_lcid_5; }
	inline void set_parent_lcid_5(int32_t value)
	{
		___parent_lcid_5 = value;
	}

	inline static int32_t get_offset_of_datetime_index_6() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___datetime_index_6)); }
	inline int32_t get_datetime_index_6() const { return ___datetime_index_6; }
	inline int32_t* get_address_of_datetime_index_6() { return &___datetime_index_6; }
	inline void set_datetime_index_6(int32_t value)
	{
		___datetime_index_6 = value;
	}

	inline static int32_t get_offset_of_number_index_7() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___number_index_7)); }
	inline int32_t get_number_index_7() const { return ___number_index_7; }
	inline int32_t* get_address_of_number_index_7() { return &___number_index_7; }
	inline void set_number_index_7(int32_t value)
	{
		___number_index_7 = value;
	}

	inline static int32_t get_offset_of_default_calendar_type_8() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___default_calendar_type_8)); }
	inline int32_t get_default_calendar_type_8() const { return ___default_calendar_type_8; }
	inline int32_t* get_address_of_default_calendar_type_8() { return &___default_calendar_type_8; }
	inline void set_default_calendar_type_8(int32_t value)
	{
		___default_calendar_type_8 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_9() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_useUserOverride_9)); }
	inline bool get_m_useUserOverride_9() const { return ___m_useUserOverride_9; }
	inline bool* get_address_of_m_useUserOverride_9() { return &___m_useUserOverride_9; }
	inline void set_m_useUserOverride_9(bool value)
	{
		___m_useUserOverride_9 = value;
	}

	inline static int32_t get_offset_of_numInfo_10() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___numInfo_10)); }
	inline NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * get_numInfo_10() const { return ___numInfo_10; }
	inline NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 ** get_address_of_numInfo_10() { return &___numInfo_10; }
	inline void set_numInfo_10(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * value)
	{
		___numInfo_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___numInfo_10), (void*)value);
	}

	inline static int32_t get_offset_of_dateTimeInfo_11() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___dateTimeInfo_11)); }
	inline DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F * get_dateTimeInfo_11() const { return ___dateTimeInfo_11; }
	inline DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F ** get_address_of_dateTimeInfo_11() { return &___dateTimeInfo_11; }
	inline void set_dateTimeInfo_11(DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F * value)
	{
		___dateTimeInfo_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dateTimeInfo_11), (void*)value);
	}

	inline static int32_t get_offset_of_textInfo_12() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___textInfo_12)); }
	inline TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 * get_textInfo_12() const { return ___textInfo_12; }
	inline TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 ** get_address_of_textInfo_12() { return &___textInfo_12; }
	inline void set_textInfo_12(TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 * value)
	{
		___textInfo_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textInfo_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_name_13() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_name_13)); }
	inline String_t* get_m_name_13() const { return ___m_name_13; }
	inline String_t** get_address_of_m_name_13() { return &___m_name_13; }
	inline void set_m_name_13(String_t* value)
	{
		___m_name_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_name_13), (void*)value);
	}

	inline static int32_t get_offset_of_englishname_14() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___englishname_14)); }
	inline String_t* get_englishname_14() const { return ___englishname_14; }
	inline String_t** get_address_of_englishname_14() { return &___englishname_14; }
	inline void set_englishname_14(String_t* value)
	{
		___englishname_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___englishname_14), (void*)value);
	}

	inline static int32_t get_offset_of_nativename_15() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___nativename_15)); }
	inline String_t* get_nativename_15() const { return ___nativename_15; }
	inline String_t** get_address_of_nativename_15() { return &___nativename_15; }
	inline void set_nativename_15(String_t* value)
	{
		___nativename_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nativename_15), (void*)value);
	}

	inline static int32_t get_offset_of_iso3lang_16() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___iso3lang_16)); }
	inline String_t* get_iso3lang_16() const { return ___iso3lang_16; }
	inline String_t** get_address_of_iso3lang_16() { return &___iso3lang_16; }
	inline void set_iso3lang_16(String_t* value)
	{
		___iso3lang_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___iso3lang_16), (void*)value);
	}

	inline static int32_t get_offset_of_iso2lang_17() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___iso2lang_17)); }
	inline String_t* get_iso2lang_17() const { return ___iso2lang_17; }
	inline String_t** get_address_of_iso2lang_17() { return &___iso2lang_17; }
	inline void set_iso2lang_17(String_t* value)
	{
		___iso2lang_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___iso2lang_17), (void*)value);
	}

	inline static int32_t get_offset_of_win3lang_18() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___win3lang_18)); }
	inline String_t* get_win3lang_18() const { return ___win3lang_18; }
	inline String_t** get_address_of_win3lang_18() { return &___win3lang_18; }
	inline void set_win3lang_18(String_t* value)
	{
		___win3lang_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___win3lang_18), (void*)value);
	}

	inline static int32_t get_offset_of_territory_19() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___territory_19)); }
	inline String_t* get_territory_19() const { return ___territory_19; }
	inline String_t** get_address_of_territory_19() { return &___territory_19; }
	inline void set_territory_19(String_t* value)
	{
		___territory_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___territory_19), (void*)value);
	}

	inline static int32_t get_offset_of_native_calendar_names_20() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___native_calendar_names_20)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_native_calendar_names_20() const { return ___native_calendar_names_20; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_native_calendar_names_20() { return &___native_calendar_names_20; }
	inline void set_native_calendar_names_20(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___native_calendar_names_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_calendar_names_20), (void*)value);
	}

	inline static int32_t get_offset_of_compareInfo_21() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___compareInfo_21)); }
	inline CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * get_compareInfo_21() const { return ___compareInfo_21; }
	inline CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 ** get_address_of_compareInfo_21() { return &___compareInfo_21; }
	inline void set_compareInfo_21(CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * value)
	{
		___compareInfo_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___compareInfo_21), (void*)value);
	}

	inline static int32_t get_offset_of_textinfo_data_22() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___textinfo_data_22)); }
	inline void* get_textinfo_data_22() const { return ___textinfo_data_22; }
	inline void** get_address_of_textinfo_data_22() { return &___textinfo_data_22; }
	inline void set_textinfo_data_22(void* value)
	{
		___textinfo_data_22 = value;
	}

	inline static int32_t get_offset_of_m_dataItem_23() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_dataItem_23)); }
	inline int32_t get_m_dataItem_23() const { return ___m_dataItem_23; }
	inline int32_t* get_address_of_m_dataItem_23() { return &___m_dataItem_23; }
	inline void set_m_dataItem_23(int32_t value)
	{
		___m_dataItem_23 = value;
	}

	inline static int32_t get_offset_of_calendar_24() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___calendar_24)); }
	inline Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 * get_calendar_24() const { return ___calendar_24; }
	inline Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 ** get_address_of_calendar_24() { return &___calendar_24; }
	inline void set_calendar_24(Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 * value)
	{
		___calendar_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___calendar_24), (void*)value);
	}

	inline static int32_t get_offset_of_parent_culture_25() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___parent_culture_25)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_parent_culture_25() const { return ___parent_culture_25; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_parent_culture_25() { return &___parent_culture_25; }
	inline void set_parent_culture_25(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___parent_culture_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_culture_25), (void*)value);
	}

	inline static int32_t get_offset_of_constructed_26() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___constructed_26)); }
	inline bool get_constructed_26() const { return ___constructed_26; }
	inline bool* get_address_of_constructed_26() { return &___constructed_26; }
	inline void set_constructed_26(bool value)
	{
		___constructed_26 = value;
	}

	inline static int32_t get_offset_of_cached_serialized_form_27() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___cached_serialized_form_27)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_cached_serialized_form_27() const { return ___cached_serialized_form_27; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_cached_serialized_form_27() { return &___cached_serialized_form_27; }
	inline void set_cached_serialized_form_27(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___cached_serialized_form_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cached_serialized_form_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_cultureData_28() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_cultureData_28)); }
	inline CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD * get_m_cultureData_28() const { return ___m_cultureData_28; }
	inline CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD ** get_address_of_m_cultureData_28() { return &___m_cultureData_28; }
	inline void set_m_cultureData_28(CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD * value)
	{
		___m_cultureData_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_cultureData_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_isInherited_29() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F, ___m_isInherited_29)); }
	inline bool get_m_isInherited_29() const { return ___m_isInherited_29; }
	inline bool* get_address_of_m_isInherited_29() { return &___m_isInherited_29; }
	inline void set_m_isInherited_29(bool value)
	{
		___m_isInherited_29 = value;
	}
};

struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields
{
public:
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::invariant_culture_info
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___invariant_culture_info_0;
	// System.Object System.Globalization.CultureInfo::shared_table_lock
	RuntimeObject * ___shared_table_lock_1;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::default_current_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___default_current_culture_2;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentUICulture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___s_DefaultThreadCurrentUICulture_33;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentCulture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___s_DefaultThreadCurrentCulture_34;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_number
	Dictionary_2_tC88A56872F7C79DBB9582D4F3FC22ED5D8E0B98B * ___shared_by_number_35;
	// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_name
	Dictionary_2_tBA5388DBB42BF620266F9A48E8B859BBBB224E25 * ___shared_by_name_36;
	// System.Boolean System.Globalization.CultureInfo::IsTaiwanSku
	bool ___IsTaiwanSku_37;

public:
	inline static int32_t get_offset_of_invariant_culture_info_0() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___invariant_culture_info_0)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_invariant_culture_info_0() const { return ___invariant_culture_info_0; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_invariant_culture_info_0() { return &___invariant_culture_info_0; }
	inline void set_invariant_culture_info_0(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___invariant_culture_info_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___invariant_culture_info_0), (void*)value);
	}

	inline static int32_t get_offset_of_shared_table_lock_1() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___shared_table_lock_1)); }
	inline RuntimeObject * get_shared_table_lock_1() const { return ___shared_table_lock_1; }
	inline RuntimeObject ** get_address_of_shared_table_lock_1() { return &___shared_table_lock_1; }
	inline void set_shared_table_lock_1(RuntimeObject * value)
	{
		___shared_table_lock_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shared_table_lock_1), (void*)value);
	}

	inline static int32_t get_offset_of_default_current_culture_2() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___default_current_culture_2)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_default_current_culture_2() const { return ___default_current_culture_2; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_default_current_culture_2() { return &___default_current_culture_2; }
	inline void set_default_current_culture_2(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___default_current_culture_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___default_current_culture_2), (void*)value);
	}

	inline static int32_t get_offset_of_s_DefaultThreadCurrentUICulture_33() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___s_DefaultThreadCurrentUICulture_33)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_s_DefaultThreadCurrentUICulture_33() const { return ___s_DefaultThreadCurrentUICulture_33; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_s_DefaultThreadCurrentUICulture_33() { return &___s_DefaultThreadCurrentUICulture_33; }
	inline void set_s_DefaultThreadCurrentUICulture_33(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___s_DefaultThreadCurrentUICulture_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultThreadCurrentUICulture_33), (void*)value);
	}

	inline static int32_t get_offset_of_s_DefaultThreadCurrentCulture_34() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___s_DefaultThreadCurrentCulture_34)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_s_DefaultThreadCurrentCulture_34() const { return ___s_DefaultThreadCurrentCulture_34; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_s_DefaultThreadCurrentCulture_34() { return &___s_DefaultThreadCurrentCulture_34; }
	inline void set_s_DefaultThreadCurrentCulture_34(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___s_DefaultThreadCurrentCulture_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultThreadCurrentCulture_34), (void*)value);
	}

	inline static int32_t get_offset_of_shared_by_number_35() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___shared_by_number_35)); }
	inline Dictionary_2_tC88A56872F7C79DBB9582D4F3FC22ED5D8E0B98B * get_shared_by_number_35() const { return ___shared_by_number_35; }
	inline Dictionary_2_tC88A56872F7C79DBB9582D4F3FC22ED5D8E0B98B ** get_address_of_shared_by_number_35() { return &___shared_by_number_35; }
	inline void set_shared_by_number_35(Dictionary_2_tC88A56872F7C79DBB9582D4F3FC22ED5D8E0B98B * value)
	{
		___shared_by_number_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shared_by_number_35), (void*)value);
	}

	inline static int32_t get_offset_of_shared_by_name_36() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___shared_by_name_36)); }
	inline Dictionary_2_tBA5388DBB42BF620266F9A48E8B859BBBB224E25 * get_shared_by_name_36() const { return ___shared_by_name_36; }
	inline Dictionary_2_tBA5388DBB42BF620266F9A48E8B859BBBB224E25 ** get_address_of_shared_by_name_36() { return &___shared_by_name_36; }
	inline void set_shared_by_name_36(Dictionary_2_tBA5388DBB42BF620266F9A48E8B859BBBB224E25 * value)
	{
		___shared_by_name_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shared_by_name_36), (void*)value);
	}

	inline static int32_t get_offset_of_IsTaiwanSku_37() { return static_cast<int32_t>(offsetof(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_StaticFields, ___IsTaiwanSku_37)); }
	inline bool get_IsTaiwanSku_37() const { return ___IsTaiwanSku_37; }
	inline bool* get_address_of_IsTaiwanSku_37() { return &___IsTaiwanSku_37; }
	inline void set_IsTaiwanSku_37(bool value)
	{
		___IsTaiwanSku_37 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_pinvoke
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * ___numInfo_10;
	DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F * ___dateTimeInfo_11;
	TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 * ___textInfo_12;
	char* ___m_name_13;
	char* ___englishname_14;
	char* ___nativename_15;
	char* ___iso3lang_16;
	char* ___iso2lang_17;
	char* ___win3lang_18;
	char* ___territory_19;
	char** ___native_calendar_names_20;
	CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 * ___calendar_24;
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_pinvoke* ___parent_culture_25;
	int32_t ___constructed_26;
	Il2CppSafeArray/*I1*/* ___cached_serialized_form_27;
	CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD_marshaled_pinvoke* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};
// Native definition for COM marshalling of System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_com
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * ___numInfo_10;
	DateTimeFormatInfo_tF4BB3AA482C2F772D2A9022F78BF8727830FAF5F * ___dateTimeInfo_11;
	TextInfo_t5F1E697CB6A7E5EC80F0DC3A968B9B4A70C291D8 * ___textInfo_12;
	Il2CppChar* ___m_name_13;
	Il2CppChar* ___englishname_14;
	Il2CppChar* ___nativename_15;
	Il2CppChar* ___iso3lang_16;
	Il2CppChar* ___iso2lang_17;
	Il2CppChar* ___win3lang_18;
	Il2CppChar* ___territory_19;
	Il2CppChar** ___native_calendar_names_20;
	CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_tF55A785ACD277504CF0D2F2C6AD56F76C6E91BD5 * ___calendar_24;
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_com* ___parent_culture_25;
	int32_t ___constructed_26;
	Il2CppSafeArray/*I1*/* ___cached_serialized_form_27;
	CultureData_tF43B080FFA6EB278F4F289BCDA3FB74B6C208ECD_marshaled_com* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____identity_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.Runtime.Serialization.SerializationInfo
struct  SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26  : public RuntimeObject
{
public:
	// System.String[] System.Runtime.Serialization.SerializationInfo::m_members
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___m_members_3;
	// System.Object[] System.Runtime.Serialization.SerializationInfo::m_data
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_data_4;
	// System.Type[] System.Runtime.Serialization.SerializationInfo::m_types
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___m_types_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Runtime.Serialization.SerializationInfo::m_nameToIndex
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ___m_nameToIndex_6;
	// System.Int32 System.Runtime.Serialization.SerializationInfo::m_currMember
	int32_t ___m_currMember_7;
	// System.Runtime.Serialization.IFormatterConverter System.Runtime.Serialization.SerializationInfo::m_converter
	RuntimeObject* ___m_converter_8;
	// System.String System.Runtime.Serialization.SerializationInfo::m_fullTypeName
	String_t* ___m_fullTypeName_9;
	// System.String System.Runtime.Serialization.SerializationInfo::m_assemName
	String_t* ___m_assemName_10;
	// System.Type System.Runtime.Serialization.SerializationInfo::objectType
	Type_t * ___objectType_11;
	// System.Boolean System.Runtime.Serialization.SerializationInfo::isFullTypeNameSetExplicit
	bool ___isFullTypeNameSetExplicit_12;
	// System.Boolean System.Runtime.Serialization.SerializationInfo::isAssemblyNameSetExplicit
	bool ___isAssemblyNameSetExplicit_13;
	// System.Boolean System.Runtime.Serialization.SerializationInfo::requireSameTokenInPartialTrust
	bool ___requireSameTokenInPartialTrust_14;

public:
	inline static int32_t get_offset_of_m_members_3() { return static_cast<int32_t>(offsetof(SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26, ___m_members_3)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_m_members_3() const { return ___m_members_3; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_m_members_3() { return &___m_members_3; }
	inline void set_m_members_3(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___m_members_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_members_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_data_4() { return static_cast<int32_t>(offsetof(SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26, ___m_data_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_data_4() const { return ___m_data_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_data_4() { return &___m_data_4; }
	inline void set_m_data_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_data_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_data_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_types_5() { return static_cast<int32_t>(offsetof(SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26, ___m_types_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_m_types_5() const { return ___m_types_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_m_types_5() { return &___m_types_5; }
	inline void set_m_types_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___m_types_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_types_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_nameToIndex_6() { return static_cast<int32_t>(offsetof(SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26, ___m_nameToIndex_6)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get_m_nameToIndex_6() const { return ___m_nameToIndex_6; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of_m_nameToIndex_6() { return &___m_nameToIndex_6; }
	inline void set_m_nameToIndex_6(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		___m_nameToIndex_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_nameToIndex_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_currMember_7() { return static_cast<int32_t>(offsetof(SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26, ___m_currMember_7)); }
	inline int32_t get_m_currMember_7() const { return ___m_currMember_7; }
	inline int32_t* get_address_of_m_currMember_7() { return &___m_currMember_7; }
	inline void set_m_currMember_7(int32_t value)
	{
		___m_currMember_7 = value;
	}

	inline static int32_t get_offset_of_m_converter_8() { return static_cast<int32_t>(offsetof(SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26, ___m_converter_8)); }
	inline RuntimeObject* get_m_converter_8() const { return ___m_converter_8; }
	inline RuntimeObject** get_address_of_m_converter_8() { return &___m_converter_8; }
	inline void set_m_converter_8(RuntimeObject* value)
	{
		___m_converter_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_converter_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_fullTypeName_9() { return static_cast<int32_t>(offsetof(SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26, ___m_fullTypeName_9)); }
	inline String_t* get_m_fullTypeName_9() const { return ___m_fullTypeName_9; }
	inline String_t** get_address_of_m_fullTypeName_9() { return &___m_fullTypeName_9; }
	inline void set_m_fullTypeName_9(String_t* value)
	{
		___m_fullTypeName_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_fullTypeName_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_assemName_10() { return static_cast<int32_t>(offsetof(SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26, ___m_assemName_10)); }
	inline String_t* get_m_assemName_10() const { return ___m_assemName_10; }
	inline String_t** get_address_of_m_assemName_10() { return &___m_assemName_10; }
	inline void set_m_assemName_10(String_t* value)
	{
		___m_assemName_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_assemName_10), (void*)value);
	}

	inline static int32_t get_offset_of_objectType_11() { return static_cast<int32_t>(offsetof(SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26, ___objectType_11)); }
	inline Type_t * get_objectType_11() const { return ___objectType_11; }
	inline Type_t ** get_address_of_objectType_11() { return &___objectType_11; }
	inline void set_objectType_11(Type_t * value)
	{
		___objectType_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objectType_11), (void*)value);
	}

	inline static int32_t get_offset_of_isFullTypeNameSetExplicit_12() { return static_cast<int32_t>(offsetof(SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26, ___isFullTypeNameSetExplicit_12)); }
	inline bool get_isFullTypeNameSetExplicit_12() const { return ___isFullTypeNameSetExplicit_12; }
	inline bool* get_address_of_isFullTypeNameSetExplicit_12() { return &___isFullTypeNameSetExplicit_12; }
	inline void set_isFullTypeNameSetExplicit_12(bool value)
	{
		___isFullTypeNameSetExplicit_12 = value;
	}

	inline static int32_t get_offset_of_isAssemblyNameSetExplicit_13() { return static_cast<int32_t>(offsetof(SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26, ___isAssemblyNameSetExplicit_13)); }
	inline bool get_isAssemblyNameSetExplicit_13() const { return ___isAssemblyNameSetExplicit_13; }
	inline bool* get_address_of_isAssemblyNameSetExplicit_13() { return &___isAssemblyNameSetExplicit_13; }
	inline void set_isAssemblyNameSetExplicit_13(bool value)
	{
		___isAssemblyNameSetExplicit_13 = value;
	}

	inline static int32_t get_offset_of_requireSameTokenInPartialTrust_14() { return static_cast<int32_t>(offsetof(SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26, ___requireSameTokenInPartialTrust_14)); }
	inline bool get_requireSameTokenInPartialTrust_14() const { return ___requireSameTokenInPartialTrust_14; }
	inline bool* get_address_of_requireSameTokenInPartialTrust_14() { return &___requireSameTokenInPartialTrust_14; }
	inline void set_requireSameTokenInPartialTrust_14(bool value)
	{
		___requireSameTokenInPartialTrust_14 = value;
	}
};


// System.Runtime.Serialization.SerializationInfoEnumerator
struct  SerializationInfoEnumerator_tB72162C419D705A40F34DDFEB18E6ACA347C54E5  : public RuntimeObject
{
public:
	// System.String[] System.Runtime.Serialization.SerializationInfoEnumerator::m_members
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___m_members_0;
	// System.Object[] System.Runtime.Serialization.SerializationInfoEnumerator::m_data
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_data_1;
	// System.Type[] System.Runtime.Serialization.SerializationInfoEnumerator::m_types
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___m_types_2;
	// System.Int32 System.Runtime.Serialization.SerializationInfoEnumerator::m_numItems
	int32_t ___m_numItems_3;
	// System.Int32 System.Runtime.Serialization.SerializationInfoEnumerator::m_currItem
	int32_t ___m_currItem_4;
	// System.Boolean System.Runtime.Serialization.SerializationInfoEnumerator::m_current
	bool ___m_current_5;

public:
	inline static int32_t get_offset_of_m_members_0() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_tB72162C419D705A40F34DDFEB18E6ACA347C54E5, ___m_members_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_m_members_0() const { return ___m_members_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_m_members_0() { return &___m_members_0; }
	inline void set_m_members_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___m_members_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_members_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_data_1() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_tB72162C419D705A40F34DDFEB18E6ACA347C54E5, ___m_data_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_data_1() const { return ___m_data_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_data_1() { return &___m_data_1; }
	inline void set_m_data_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_data_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_data_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_types_2() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_tB72162C419D705A40F34DDFEB18E6ACA347C54E5, ___m_types_2)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_m_types_2() const { return ___m_types_2; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_m_types_2() { return &___m_types_2; }
	inline void set_m_types_2(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___m_types_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_types_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_numItems_3() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_tB72162C419D705A40F34DDFEB18E6ACA347C54E5, ___m_numItems_3)); }
	inline int32_t get_m_numItems_3() const { return ___m_numItems_3; }
	inline int32_t* get_address_of_m_numItems_3() { return &___m_numItems_3; }
	inline void set_m_numItems_3(int32_t value)
	{
		___m_numItems_3 = value;
	}

	inline static int32_t get_offset_of_m_currItem_4() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_tB72162C419D705A40F34DDFEB18E6ACA347C54E5, ___m_currItem_4)); }
	inline int32_t get_m_currItem_4() const { return ___m_currItem_4; }
	inline int32_t* get_address_of_m_currItem_4() { return &___m_currItem_4; }
	inline void set_m_currItem_4(int32_t value)
	{
		___m_currItem_4 = value;
	}

	inline static int32_t get_offset_of_m_current_5() { return static_cast<int32_t>(offsetof(SerializationInfoEnumerator_tB72162C419D705A40F34DDFEB18E6ACA347C54E5, ___m_current_5)); }
	inline bool get_m_current_5() const { return ___m_current_5; }
	inline bool* get_address_of_m_current_5() { return &___m_current_5; }
	inline void set_m_current_5(bool value)
	{
		___m_current_5 = value;
	}
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.Text.Encoding
struct  Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_9;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB * ___dataItem_10;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_11;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_12;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * ___encoderFallback_13;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_t128445EB7676870485230893338EF044F6B72F60 * ___decoderFallback_14;

public:
	inline static int32_t get_offset_of_m_codePage_9() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___m_codePage_9)); }
	inline int32_t get_m_codePage_9() const { return ___m_codePage_9; }
	inline int32_t* get_address_of_m_codePage_9() { return &___m_codePage_9; }
	inline void set_m_codePage_9(int32_t value)
	{
		___m_codePage_9 = value;
	}

	inline static int32_t get_offset_of_dataItem_10() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___dataItem_10)); }
	inline CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB * get_dataItem_10() const { return ___dataItem_10; }
	inline CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB ** get_address_of_dataItem_10() { return &___dataItem_10; }
	inline void set_dataItem_10(CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB * value)
	{
		___dataItem_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dataItem_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_deserializedFromEverett_11() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___m_deserializedFromEverett_11)); }
	inline bool get_m_deserializedFromEverett_11() const { return ___m_deserializedFromEverett_11; }
	inline bool* get_address_of_m_deserializedFromEverett_11() { return &___m_deserializedFromEverett_11; }
	inline void set_m_deserializedFromEverett_11(bool value)
	{
		___m_deserializedFromEverett_11 = value;
	}

	inline static int32_t get_offset_of_m_isReadOnly_12() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___m_isReadOnly_12)); }
	inline bool get_m_isReadOnly_12() const { return ___m_isReadOnly_12; }
	inline bool* get_address_of_m_isReadOnly_12() { return &___m_isReadOnly_12; }
	inline void set_m_isReadOnly_12(bool value)
	{
		___m_isReadOnly_12 = value;
	}

	inline static int32_t get_offset_of_encoderFallback_13() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___encoderFallback_13)); }
	inline EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * get_encoderFallback_13() const { return ___encoderFallback_13; }
	inline EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 ** get_address_of_encoderFallback_13() { return &___encoderFallback_13; }
	inline void set_encoderFallback_13(EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * value)
	{
		___encoderFallback_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___encoderFallback_13), (void*)value);
	}

	inline static int32_t get_offset_of_decoderFallback_14() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___decoderFallback_14)); }
	inline DecoderFallback_t128445EB7676870485230893338EF044F6B72F60 * get_decoderFallback_14() const { return ___decoderFallback_14; }
	inline DecoderFallback_t128445EB7676870485230893338EF044F6B72F60 ** get_address_of_decoderFallback_14() { return &___decoderFallback_14; }
	inline void set_decoderFallback_14(DecoderFallback_t128445EB7676870485230893338EF044F6B72F60 * value)
	{
		___decoderFallback_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___decoderFallback_14), (void*)value);
	}
};

struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields
{
public:
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___latin1Encoding_7;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_15;

public:
	inline static int32_t get_offset_of_defaultEncoding_0() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___defaultEncoding_0)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_defaultEncoding_0() const { return ___defaultEncoding_0; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_defaultEncoding_0() { return &___defaultEncoding_0; }
	inline void set_defaultEncoding_0(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___defaultEncoding_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultEncoding_0), (void*)value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_1() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___unicodeEncoding_1)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_unicodeEncoding_1() const { return ___unicodeEncoding_1; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_unicodeEncoding_1() { return &___unicodeEncoding_1; }
	inline void set_unicodeEncoding_1(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___unicodeEncoding_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___unicodeEncoding_1), (void*)value);
	}

	inline static int32_t get_offset_of_bigEndianUnicode_2() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___bigEndianUnicode_2)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_bigEndianUnicode_2() const { return ___bigEndianUnicode_2; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_bigEndianUnicode_2() { return &___bigEndianUnicode_2; }
	inline void set_bigEndianUnicode_2(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___bigEndianUnicode_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bigEndianUnicode_2), (void*)value);
	}

	inline static int32_t get_offset_of_utf7Encoding_3() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___utf7Encoding_3)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_utf7Encoding_3() const { return ___utf7Encoding_3; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_utf7Encoding_3() { return &___utf7Encoding_3; }
	inline void set_utf7Encoding_3(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___utf7Encoding_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf7Encoding_3), (void*)value);
	}

	inline static int32_t get_offset_of_utf8Encoding_4() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___utf8Encoding_4)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_utf8Encoding_4() const { return ___utf8Encoding_4; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_utf8Encoding_4() { return &___utf8Encoding_4; }
	inline void set_utf8Encoding_4(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___utf8Encoding_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf8Encoding_4), (void*)value);
	}

	inline static int32_t get_offset_of_utf32Encoding_5() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___utf32Encoding_5)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_utf32Encoding_5() const { return ___utf32Encoding_5; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_utf32Encoding_5() { return &___utf32Encoding_5; }
	inline void set_utf32Encoding_5(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___utf32Encoding_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf32Encoding_5), (void*)value);
	}

	inline static int32_t get_offset_of_asciiEncoding_6() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___asciiEncoding_6)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_asciiEncoding_6() const { return ___asciiEncoding_6; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_asciiEncoding_6() { return &___asciiEncoding_6; }
	inline void set_asciiEncoding_6(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___asciiEncoding_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___asciiEncoding_6), (void*)value);
	}

	inline static int32_t get_offset_of_latin1Encoding_7() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___latin1Encoding_7)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_latin1Encoding_7() const { return ___latin1Encoding_7; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_latin1Encoding_7() { return &___latin1Encoding_7; }
	inline void set_latin1Encoding_7(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___latin1Encoding_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___latin1Encoding_7), (void*)value);
	}

	inline static int32_t get_offset_of_encodings_8() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___encodings_8)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_encodings_8() const { return ___encodings_8; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_encodings_8() { return &___encodings_8; }
	inline void set_encodings_8(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___encodings_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___encodings_8), (void*)value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_15() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___s_InternalSyncObject_15)); }
	inline RuntimeObject * get_s_InternalSyncObject_15() const { return ___s_InternalSyncObject_15; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_15() { return &___s_InternalSyncObject_15; }
	inline void set_s_InternalSyncObject_15(RuntimeObject * value)
	{
		___s_InternalSyncObject_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InternalSyncObject_15), (void*)value);
	}
};


// System.Text.StringBuilder
struct  StringBuilder_t  : public RuntimeObject
{
public:
	// System.Char[] System.Text.StringBuilder::m_ChunkChars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___m_ChunkChars_0;
	// System.Text.StringBuilder System.Text.StringBuilder::m_ChunkPrevious
	StringBuilder_t * ___m_ChunkPrevious_1;
	// System.Int32 System.Text.StringBuilder::m_ChunkLength
	int32_t ___m_ChunkLength_2;
	// System.Int32 System.Text.StringBuilder::m_ChunkOffset
	int32_t ___m_ChunkOffset_3;
	// System.Int32 System.Text.StringBuilder::m_MaxCapacity
	int32_t ___m_MaxCapacity_4;

public:
	inline static int32_t get_offset_of_m_ChunkChars_0() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkChars_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_m_ChunkChars_0() const { return ___m_ChunkChars_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_m_ChunkChars_0() { return &___m_ChunkChars_0; }
	inline void set_m_ChunkChars_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___m_ChunkChars_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChunkChars_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChunkPrevious_1() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkPrevious_1)); }
	inline StringBuilder_t * get_m_ChunkPrevious_1() const { return ___m_ChunkPrevious_1; }
	inline StringBuilder_t ** get_address_of_m_ChunkPrevious_1() { return &___m_ChunkPrevious_1; }
	inline void set_m_ChunkPrevious_1(StringBuilder_t * value)
	{
		___m_ChunkPrevious_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChunkPrevious_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChunkLength_2() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkLength_2)); }
	inline int32_t get_m_ChunkLength_2() const { return ___m_ChunkLength_2; }
	inline int32_t* get_address_of_m_ChunkLength_2() { return &___m_ChunkLength_2; }
	inline void set_m_ChunkLength_2(int32_t value)
	{
		___m_ChunkLength_2 = value;
	}

	inline static int32_t get_offset_of_m_ChunkOffset_3() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkOffset_3)); }
	inline int32_t get_m_ChunkOffset_3() const { return ___m_ChunkOffset_3; }
	inline int32_t* get_address_of_m_ChunkOffset_3() { return &___m_ChunkOffset_3; }
	inline void set_m_ChunkOffset_3(int32_t value)
	{
		___m_ChunkOffset_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_MaxCapacity_4)); }
	inline int32_t get_m_MaxCapacity_4() const { return ___m_MaxCapacity_4; }
	inline int32_t* get_address_of_m_MaxCapacity_4() { return &___m_MaxCapacity_4; }
	inline void set_m_MaxCapacity_4(int32_t value)
	{
		___m_MaxCapacity_4 = value;
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// System.Xml.Base64Encoder
struct  Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922  : public RuntimeObject
{
public:
	// System.Byte[] System.Xml.Base64Encoder::leftOverBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___leftOverBytes_0;
	// System.Int32 System.Xml.Base64Encoder::leftOverBytesCount
	int32_t ___leftOverBytesCount_1;
	// System.Char[] System.Xml.Base64Encoder::charsLine
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___charsLine_2;

public:
	inline static int32_t get_offset_of_leftOverBytes_0() { return static_cast<int32_t>(offsetof(Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922, ___leftOverBytes_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_leftOverBytes_0() const { return ___leftOverBytes_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_leftOverBytes_0() { return &___leftOverBytes_0; }
	inline void set_leftOverBytes_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___leftOverBytes_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___leftOverBytes_0), (void*)value);
	}

	inline static int32_t get_offset_of_leftOverBytesCount_1() { return static_cast<int32_t>(offsetof(Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922, ___leftOverBytesCount_1)); }
	inline int32_t get_leftOverBytesCount_1() const { return ___leftOverBytesCount_1; }
	inline int32_t* get_address_of_leftOverBytesCount_1() { return &___leftOverBytesCount_1; }
	inline void set_leftOverBytesCount_1(int32_t value)
	{
		___leftOverBytesCount_1 = value;
	}

	inline static int32_t get_offset_of_charsLine_2() { return static_cast<int32_t>(offsetof(Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922, ___charsLine_2)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_charsLine_2() const { return ___charsLine_2; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_charsLine_2() { return &___charsLine_2; }
	inline void set_charsLine_2(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___charsLine_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___charsLine_2), (void*)value);
	}
};


// System.Xml.Res
struct  Res_tAEAB44EC8659FC8A5C53E8AB74EA846A640AD03A  : public RuntimeObject
{
public:

public:
};


// System.Xml.SecureStringHasher
struct  SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.SecureStringHasher::hashCodeRandomizer
	int32_t ___hashCodeRandomizer_1;

public:
	inline static int32_t get_offset_of_hashCodeRandomizer_1() { return static_cast<int32_t>(offsetof(SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3, ___hashCodeRandomizer_1)); }
	inline int32_t get_hashCodeRandomizer_1() const { return ___hashCodeRandomizer_1; }
	inline int32_t* get_address_of_hashCodeRandomizer_1() { return &___hashCodeRandomizer_1; }
	inline void set_hashCodeRandomizer_1(int32_t value)
	{
		___hashCodeRandomizer_1 = value;
	}
};

struct SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_StaticFields
{
public:
	// System.Xml.SecureStringHasher_HashCodeOfStringDelegate System.Xml.SecureStringHasher::hashCodeDelegate
	HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E * ___hashCodeDelegate_0;

public:
	inline static int32_t get_offset_of_hashCodeDelegate_0() { return static_cast<int32_t>(offsetof(SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_StaticFields, ___hashCodeDelegate_0)); }
	inline HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E * get_hashCodeDelegate_0() const { return ___hashCodeDelegate_0; }
	inline HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E ** get_address_of_hashCodeDelegate_0() { return &___hashCodeDelegate_0; }
	inline void set_hashCodeDelegate_0(HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E * value)
	{
		___hashCodeDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hashCodeDelegate_0), (void*)value);
	}
};


// System.Xml.XmlChildEnumerator
struct  XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA  : public RuntimeObject
{
public:
	// System.Xml.XmlNode System.Xml.XmlChildEnumerator::container
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___container_0;
	// System.Xml.XmlNode System.Xml.XmlChildEnumerator::child
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___child_1;
	// System.Boolean System.Xml.XmlChildEnumerator::isFirst
	bool ___isFirst_2;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA, ___container_0)); }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * get_container_0() const { return ___container_0; }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___container_0), (void*)value);
	}

	inline static int32_t get_offset_of_child_1() { return static_cast<int32_t>(offsetof(XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA, ___child_1)); }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * get_child_1() const { return ___child_1; }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB ** get_address_of_child_1() { return &___child_1; }
	inline void set_child_1(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * value)
	{
		___child_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___child_1), (void*)value);
	}

	inline static int32_t get_offset_of_isFirst_2() { return static_cast<int32_t>(offsetof(XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA, ___isFirst_2)); }
	inline bool get_isFirst_2() const { return ___isFirst_2; }
	inline bool* get_address_of_isFirst_2() { return &___isFirst_2; }
	inline void set_isFirst_2(bool value)
	{
		___isFirst_2 = value;
	}
};


// System.Xml.XmlNode
struct  XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB  : public RuntimeObject
{
public:
	// System.Xml.XmlNode System.Xml.XmlNode::parentNode
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___parentNode_0;

public:
	inline static int32_t get_offset_of_parentNode_0() { return static_cast<int32_t>(offsetof(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB, ___parentNode_0)); }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * get_parentNode_0() const { return ___parentNode_0; }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB ** get_address_of_parentNode_0() { return &___parentNode_0; }
	inline void set_parentNode_0(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * value)
	{
		___parentNode_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parentNode_0), (void*)value);
	}
};


// System.Xml.XmlReader
struct  XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB  : public RuntimeObject
{
public:

public:
};

struct XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_StaticFields
{
public:
	// System.UInt32 System.Xml.XmlReader::IsTextualNodeBitmap
	uint32_t ___IsTextualNodeBitmap_0;
	// System.UInt32 System.Xml.XmlReader::CanReadContentAsBitmap
	uint32_t ___CanReadContentAsBitmap_1;
	// System.UInt32 System.Xml.XmlReader::HasValueBitmap
	uint32_t ___HasValueBitmap_2;

public:
	inline static int32_t get_offset_of_IsTextualNodeBitmap_0() { return static_cast<int32_t>(offsetof(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_StaticFields, ___IsTextualNodeBitmap_0)); }
	inline uint32_t get_IsTextualNodeBitmap_0() const { return ___IsTextualNodeBitmap_0; }
	inline uint32_t* get_address_of_IsTextualNodeBitmap_0() { return &___IsTextualNodeBitmap_0; }
	inline void set_IsTextualNodeBitmap_0(uint32_t value)
	{
		___IsTextualNodeBitmap_0 = value;
	}

	inline static int32_t get_offset_of_CanReadContentAsBitmap_1() { return static_cast<int32_t>(offsetof(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_StaticFields, ___CanReadContentAsBitmap_1)); }
	inline uint32_t get_CanReadContentAsBitmap_1() const { return ___CanReadContentAsBitmap_1; }
	inline uint32_t* get_address_of_CanReadContentAsBitmap_1() { return &___CanReadContentAsBitmap_1; }
	inline void set_CanReadContentAsBitmap_1(uint32_t value)
	{
		___CanReadContentAsBitmap_1 = value;
	}

	inline static int32_t get_offset_of_HasValueBitmap_2() { return static_cast<int32_t>(offsetof(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_StaticFields, ___HasValueBitmap_2)); }
	inline uint32_t get_HasValueBitmap_2() const { return ___HasValueBitmap_2; }
	inline uint32_t* get_address_of_HasValueBitmap_2() { return &___HasValueBitmap_2; }
	inline void set_HasValueBitmap_2(uint32_t value)
	{
		___HasValueBitmap_2 = value;
	}
};


// System.Xml.XmlWriter
struct  XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869  : public RuntimeObject
{
public:

public:
};


// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D416
struct  __StaticArrayInitTypeSizeU3D416_tDC7E75F123C5FA6CED5F4949F6976ADFF285E704 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D416_tDC7E75F123C5FA6CED5F4949F6976ADFF285E704__padding[416];
	};

public:
};


// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D6
struct  __StaticArrayInitTypeSizeU3D6_tB657E692303B443FF0E24AE8F75A675A844348C4 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D6_tB657E692303B443FF0E24AE8F75A675A844348C4__padding[6];
	};

public:
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct  Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.Char
struct  Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.IO.TextWriter
struct  TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.Char[] System.IO.TextWriter::CoreNewLine
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___CoreNewLine_9;
	// System.IFormatProvider System.IO.TextWriter::InternalFormatProvider
	RuntimeObject* ___InternalFormatProvider_10;

public:
	inline static int32_t get_offset_of_CoreNewLine_9() { return static_cast<int32_t>(offsetof(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0, ___CoreNewLine_9)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_CoreNewLine_9() const { return ___CoreNewLine_9; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_CoreNewLine_9() { return &___CoreNewLine_9; }
	inline void set_CoreNewLine_9(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___CoreNewLine_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CoreNewLine_9), (void*)value);
	}

	inline static int32_t get_offset_of_InternalFormatProvider_10() { return static_cast<int32_t>(offsetof(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0, ___InternalFormatProvider_10)); }
	inline RuntimeObject* get_InternalFormatProvider_10() const { return ___InternalFormatProvider_10; }
	inline RuntimeObject** get_address_of_InternalFormatProvider_10() { return &___InternalFormatProvider_10; }
	inline void set_InternalFormatProvider_10(RuntimeObject* value)
	{
		___InternalFormatProvider_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InternalFormatProvider_10), (void*)value);
	}
};

struct TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0_StaticFields
{
public:
	// System.IO.TextWriter System.IO.TextWriter::Null
	TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ___Null_1;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteCharDelegate
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ____WriteCharDelegate_2;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteStringDelegate
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ____WriteStringDelegate_3;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteCharArrayRangeDelegate
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ____WriteCharArrayRangeDelegate_4;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteLineCharDelegate
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ____WriteLineCharDelegate_5;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteLineStringDelegate
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ____WriteLineStringDelegate_6;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteLineCharArrayRangeDelegate
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ____WriteLineCharArrayRangeDelegate_7;
	// System.Action`1<System.Object> System.IO.TextWriter::_FlushDelegate
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ____FlushDelegate_8;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0_StaticFields, ___Null_1)); }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * get_Null_1() const { return ___Null_1; }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Null_1), (void*)value);
	}

	inline static int32_t get_offset_of__WriteCharDelegate_2() { return static_cast<int32_t>(offsetof(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0_StaticFields, ____WriteCharDelegate_2)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get__WriteCharDelegate_2() const { return ____WriteCharDelegate_2; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of__WriteCharDelegate_2() { return &____WriteCharDelegate_2; }
	inline void set__WriteCharDelegate_2(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		____WriteCharDelegate_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteCharDelegate_2), (void*)value);
	}

	inline static int32_t get_offset_of__WriteStringDelegate_3() { return static_cast<int32_t>(offsetof(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0_StaticFields, ____WriteStringDelegate_3)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get__WriteStringDelegate_3() const { return ____WriteStringDelegate_3; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of__WriteStringDelegate_3() { return &____WriteStringDelegate_3; }
	inline void set__WriteStringDelegate_3(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		____WriteStringDelegate_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteStringDelegate_3), (void*)value);
	}

	inline static int32_t get_offset_of__WriteCharArrayRangeDelegate_4() { return static_cast<int32_t>(offsetof(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0_StaticFields, ____WriteCharArrayRangeDelegate_4)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get__WriteCharArrayRangeDelegate_4() const { return ____WriteCharArrayRangeDelegate_4; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of__WriteCharArrayRangeDelegate_4() { return &____WriteCharArrayRangeDelegate_4; }
	inline void set__WriteCharArrayRangeDelegate_4(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		____WriteCharArrayRangeDelegate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteCharArrayRangeDelegate_4), (void*)value);
	}

	inline static int32_t get_offset_of__WriteLineCharDelegate_5() { return static_cast<int32_t>(offsetof(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0_StaticFields, ____WriteLineCharDelegate_5)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get__WriteLineCharDelegate_5() const { return ____WriteLineCharDelegate_5; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of__WriteLineCharDelegate_5() { return &____WriteLineCharDelegate_5; }
	inline void set__WriteLineCharDelegate_5(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		____WriteLineCharDelegate_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteLineCharDelegate_5), (void*)value);
	}

	inline static int32_t get_offset_of__WriteLineStringDelegate_6() { return static_cast<int32_t>(offsetof(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0_StaticFields, ____WriteLineStringDelegate_6)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get__WriteLineStringDelegate_6() const { return ____WriteLineStringDelegate_6; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of__WriteLineStringDelegate_6() { return &____WriteLineStringDelegate_6; }
	inline void set__WriteLineStringDelegate_6(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		____WriteLineStringDelegate_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteLineStringDelegate_6), (void*)value);
	}

	inline static int32_t get_offset_of__WriteLineCharArrayRangeDelegate_7() { return static_cast<int32_t>(offsetof(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0_StaticFields, ____WriteLineCharArrayRangeDelegate_7)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get__WriteLineCharArrayRangeDelegate_7() const { return ____WriteLineCharArrayRangeDelegate_7; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of__WriteLineCharArrayRangeDelegate_7() { return &____WriteLineCharArrayRangeDelegate_7; }
	inline void set__WriteLineCharArrayRangeDelegate_7(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		____WriteLineCharArrayRangeDelegate_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteLineCharArrayRangeDelegate_7), (void*)value);
	}

	inline static int32_t get_offset_of__FlushDelegate_8() { return static_cast<int32_t>(offsetof(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0_StaticFields, ____FlushDelegate_8)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get__FlushDelegate_8() const { return ____FlushDelegate_8; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of__FlushDelegate_8() { return &____FlushDelegate_8; }
	inline void set__FlushDelegate_8(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		____FlushDelegate_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____FlushDelegate_8), (void*)value);
	}
};


// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Int64
struct  Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Reflection.MethodBase
struct  MethodBase_t  : public MemberInfo_t
{
public:

public:
};


// System.Runtime.Serialization.SerializationEntry
struct  SerializationEntry_tA4CE7B0176B45BD820A7802C84479174F5EBE5EA 
{
public:
	// System.Type System.Runtime.Serialization.SerializationEntry::m_type
	Type_t * ___m_type_0;
	// System.Object System.Runtime.Serialization.SerializationEntry::m_value
	RuntimeObject * ___m_value_1;
	// System.String System.Runtime.Serialization.SerializationEntry::m_name
	String_t* ___m_name_2;

public:
	inline static int32_t get_offset_of_m_type_0() { return static_cast<int32_t>(offsetof(SerializationEntry_tA4CE7B0176B45BD820A7802C84479174F5EBE5EA, ___m_type_0)); }
	inline Type_t * get_m_type_0() const { return ___m_type_0; }
	inline Type_t ** get_address_of_m_type_0() { return &___m_type_0; }
	inline void set_m_type_0(Type_t * value)
	{
		___m_type_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_type_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_value_1() { return static_cast<int32_t>(offsetof(SerializationEntry_tA4CE7B0176B45BD820A7802C84479174F5EBE5EA, ___m_value_1)); }
	inline RuntimeObject * get_m_value_1() const { return ___m_value_1; }
	inline RuntimeObject ** get_address_of_m_value_1() { return &___m_value_1; }
	inline void set_m_value_1(RuntimeObject * value)
	{
		___m_value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_value_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_name_2() { return static_cast<int32_t>(offsetof(SerializationEntry_tA4CE7B0176B45BD820A7802C84479174F5EBE5EA, ___m_name_2)); }
	inline String_t* get_m_name_2() const { return ___m_name_2; }
	inline String_t** get_address_of_m_name_2() { return &___m_name_2; }
	inline void set_m_name_2(String_t* value)
	{
		___m_name_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_name_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Runtime.Serialization.SerializationEntry
struct SerializationEntry_tA4CE7B0176B45BD820A7802C84479174F5EBE5EA_marshaled_pinvoke
{
	Type_t * ___m_type_0;
	Il2CppIUnknown* ___m_value_1;
	char* ___m_name_2;
};
// Native definition for COM marshalling of System.Runtime.Serialization.SerializationEntry
struct SerializationEntry_tA4CE7B0176B45BD820A7802C84479174F5EBE5EA_marshaled_com
{
	Type_t * ___m_type_0;
	Il2CppIUnknown* ___m_value_1;
	Il2CppChar* ___m_name_2;
};

// System.UInt32
struct  UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// System.Xml.Serialization.XmlIgnoreAttribute
struct  XmlIgnoreAttribute_t42AF5E2677A8626D8F5A1685C84B4E1C35B54AE6  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// System.Xml.XmlCharType
struct  XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 
{
public:
	// System.Byte[] System.Xml.XmlCharType::charProperties
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___charProperties_2;

public:
	inline static int32_t get_offset_of_charProperties_2() { return static_cast<int32_t>(offsetof(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9, ___charProperties_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_charProperties_2() const { return ___charProperties_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_charProperties_2() { return &___charProperties_2; }
	inline void set_charProperties_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___charProperties_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___charProperties_2), (void*)value);
	}
};

struct XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields
{
public:
	// System.Object System.Xml.XmlCharType::s_Lock
	RuntimeObject * ___s_Lock_0;
	// System.Byte[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.XmlCharType::s_CharProperties
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___s_CharProperties_1;

public:
	inline static int32_t get_offset_of_s_Lock_0() { return static_cast<int32_t>(offsetof(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields, ___s_Lock_0)); }
	inline RuntimeObject * get_s_Lock_0() const { return ___s_Lock_0; }
	inline RuntimeObject ** get_address_of_s_Lock_0() { return &___s_Lock_0; }
	inline void set_s_Lock_0(RuntimeObject * value)
	{
		___s_Lock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Lock_0), (void*)value);
	}

	inline static int32_t get_offset_of_s_CharProperties_1() { return static_cast<int32_t>(offsetof(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields, ___s_CharProperties_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_s_CharProperties_1() const { return ___s_CharProperties_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_s_CharProperties_1() { return &___s_CharProperties_1; }
	inline void set_s_CharProperties_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___s_CharProperties_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_CharProperties_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Xml.XmlCharType
struct XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_marshaled_pinvoke
{
	Il2CppSafeArray/*I1*/* ___charProperties_2;
};
// Native definition for COM marshalling of System.Xml.XmlCharType
struct XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_marshaled_com
{
	Il2CppSafeArray/*I1*/* ___charProperties_2;
};

// System.Xml.XmlLinkedNode
struct  XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E  : public XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB
{
public:
	// System.Xml.XmlLinkedNode System.Xml.XmlLinkedNode::next
	XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * ___next_1;

public:
	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E, ___next_1)); }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * get_next_1() const { return ___next_1; }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E ** get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * value)
	{
		___next_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___next_1), (void*)value);
	}
};


// System.Xml.XmlTextWriter_Namespace
struct  Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B 
{
public:
	// System.String System.Xml.XmlTextWriter_Namespace::prefix
	String_t* ___prefix_0;
	// System.String System.Xml.XmlTextWriter_Namespace::ns
	String_t* ___ns_1;
	// System.Boolean System.Xml.XmlTextWriter_Namespace::declared
	bool ___declared_2;
	// System.Int32 System.Xml.XmlTextWriter_Namespace::prevNsIndex
	int32_t ___prevNsIndex_3;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prefix_0), (void*)value);
	}

	inline static int32_t get_offset_of_ns_1() { return static_cast<int32_t>(offsetof(Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B, ___ns_1)); }
	inline String_t* get_ns_1() const { return ___ns_1; }
	inline String_t** get_address_of_ns_1() { return &___ns_1; }
	inline void set_ns_1(String_t* value)
	{
		___ns_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ns_1), (void*)value);
	}

	inline static int32_t get_offset_of_declared_2() { return static_cast<int32_t>(offsetof(Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B, ___declared_2)); }
	inline bool get_declared_2() const { return ___declared_2; }
	inline bool* get_address_of_declared_2() { return &___declared_2; }
	inline void set_declared_2(bool value)
	{
		___declared_2 = value;
	}

	inline static int32_t get_offset_of_prevNsIndex_3() { return static_cast<int32_t>(offsetof(Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B, ___prevNsIndex_3)); }
	inline int32_t get_prevNsIndex_3() const { return ___prevNsIndex_3; }
	inline int32_t* get_address_of_prevNsIndex_3() { return &___prevNsIndex_3; }
	inline void set_prevNsIndex_3(int32_t value)
	{
		___prevNsIndex_3 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Xml.XmlTextWriter/Namespace
struct Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B_marshaled_pinvoke
{
	char* ___prefix_0;
	char* ___ns_1;
	int32_t ___declared_2;
	int32_t ___prevNsIndex_3;
};
// Native definition for COM marshalling of System.Xml.XmlTextWriter/Namespace
struct Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B_marshaled_com
{
	Il2CppChar* ___prefix_0;
	Il2CppChar* ___ns_1;
	int32_t ___declared_2;
	int32_t ___prevNsIndex_3;
};

// System.Xml.XmlTextWriterBase64Encoder
struct  XmlTextWriterBase64Encoder_t1362FFBE93C9BC675B4A34503DB31AA6D3AAF01C  : public Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922
{
public:
	// System.Xml.XmlTextEncoder System.Xml.XmlTextWriterBase64Encoder::xmlTextEncoder
	XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * ___xmlTextEncoder_3;

public:
	inline static int32_t get_offset_of_xmlTextEncoder_3() { return static_cast<int32_t>(offsetof(XmlTextWriterBase64Encoder_t1362FFBE93C9BC675B4A34503DB31AA6D3AAF01C, ___xmlTextEncoder_3)); }
	inline XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * get_xmlTextEncoder_3() const { return ___xmlTextEncoder_3; }
	inline XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 ** get_address_of_xmlTextEncoder_3() { return &___xmlTextEncoder_3; }
	inline void set_xmlTextEncoder_3(XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * value)
	{
		___xmlTextEncoder_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___xmlTextEncoder_3), (void*)value);
	}
};


// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D6 <PrivateImplementationDetails>::5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98
	__StaticArrayInitTypeSizeU3D6_tB657E692303B443FF0E24AE8F75A675A844348C4  ___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D416 <PrivateImplementationDetails>::6A0D50D692745A6663128CD315B71079584F3E59
	__StaticArrayInitTypeSizeU3D416_tDC7E75F123C5FA6CED5F4949F6976ADFF285E704  ___6A0D50D692745A6663128CD315B71079584F3E59_1;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D416 <PrivateImplementationDetails>::B368804F0C6DAB083B253A6B106D0783D5C32E90
	__StaticArrayInitTypeSizeU3D416_tDC7E75F123C5FA6CED5F4949F6976ADFF285E704  ___B368804F0C6DAB083B253A6B106D0783D5C32E90_2;
	// System.Int64 <PrivateImplementationDetails>::EBC658B067B5C785A3F0BB67D73755F6FEE7F70C
	int64_t ___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_3;

public:
	inline static int32_t get_offset_of_U35D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0)); }
	inline __StaticArrayInitTypeSizeU3D6_tB657E692303B443FF0E24AE8F75A675A844348C4  get_U35D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0() const { return ___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0; }
	inline __StaticArrayInitTypeSizeU3D6_tB657E692303B443FF0E24AE8F75A675A844348C4 * get_address_of_U35D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0() { return &___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0; }
	inline void set_U35D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0(__StaticArrayInitTypeSizeU3D6_tB657E692303B443FF0E24AE8F75A675A844348C4  value)
	{
		___5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0 = value;
	}

	inline static int32_t get_offset_of_U36A0D50D692745A6663128CD315B71079584F3E59_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___6A0D50D692745A6663128CD315B71079584F3E59_1)); }
	inline __StaticArrayInitTypeSizeU3D416_tDC7E75F123C5FA6CED5F4949F6976ADFF285E704  get_U36A0D50D692745A6663128CD315B71079584F3E59_1() const { return ___6A0D50D692745A6663128CD315B71079584F3E59_1; }
	inline __StaticArrayInitTypeSizeU3D416_tDC7E75F123C5FA6CED5F4949F6976ADFF285E704 * get_address_of_U36A0D50D692745A6663128CD315B71079584F3E59_1() { return &___6A0D50D692745A6663128CD315B71079584F3E59_1; }
	inline void set_U36A0D50D692745A6663128CD315B71079584F3E59_1(__StaticArrayInitTypeSizeU3D416_tDC7E75F123C5FA6CED5F4949F6976ADFF285E704  value)
	{
		___6A0D50D692745A6663128CD315B71079584F3E59_1 = value;
	}

	inline static int32_t get_offset_of_B368804F0C6DAB083B253A6B106D0783D5C32E90_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___B368804F0C6DAB083B253A6B106D0783D5C32E90_2)); }
	inline __StaticArrayInitTypeSizeU3D416_tDC7E75F123C5FA6CED5F4949F6976ADFF285E704  get_B368804F0C6DAB083B253A6B106D0783D5C32E90_2() const { return ___B368804F0C6DAB083B253A6B106D0783D5C32E90_2; }
	inline __StaticArrayInitTypeSizeU3D416_tDC7E75F123C5FA6CED5F4949F6976ADFF285E704 * get_address_of_B368804F0C6DAB083B253A6B106D0783D5C32E90_2() { return &___B368804F0C6DAB083B253A6B106D0783D5C32E90_2; }
	inline void set_B368804F0C6DAB083B253A6B106D0783D5C32E90_2(__StaticArrayInitTypeSizeU3D416_tDC7E75F123C5FA6CED5F4949F6976ADFF285E704  value)
	{
		___B368804F0C6DAB083B253A6B106D0783D5C32E90_2 = value;
	}

	inline static int32_t get_offset_of_EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB_StaticFields, ___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_3)); }
	inline int64_t get_EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_3() const { return ___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_3; }
	inline int64_t* get_address_of_EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_3() { return &___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_3; }
	inline void set_EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_3(int64_t value)
	{
		___EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_3 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*INT*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*INT*/* ___native_trace_ips_15;
};

// System.Globalization.NumberStyles
struct  NumberStyles_tB0ADA2D9CCAA236331AED14C42BE5832B2351592 
{
public:
	// System.Int32 System.Globalization.NumberStyles::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NumberStyles_tB0ADA2D9CCAA236331AED14C42BE5832B2351592, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.IO.StringWriter
struct  StringWriter_t194EF1526E072B93984370042AA80926C2EB6139  : public TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0
{
public:
	// System.Text.StringBuilder System.IO.StringWriter::_sb
	StringBuilder_t * ____sb_12;
	// System.Boolean System.IO.StringWriter::_isOpen
	bool ____isOpen_13;

public:
	inline static int32_t get_offset_of__sb_12() { return static_cast<int32_t>(offsetof(StringWriter_t194EF1526E072B93984370042AA80926C2EB6139, ____sb_12)); }
	inline StringBuilder_t * get__sb_12() const { return ____sb_12; }
	inline StringBuilder_t ** get_address_of__sb_12() { return &____sb_12; }
	inline void set__sb_12(StringBuilder_t * value)
	{
		____sb_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sb_12), (void*)value);
	}

	inline static int32_t get_offset_of__isOpen_13() { return static_cast<int32_t>(offsetof(StringWriter_t194EF1526E072B93984370042AA80926C2EB6139, ____isOpen_13)); }
	inline bool get__isOpen_13() const { return ____isOpen_13; }
	inline bool* get_address_of__isOpen_13() { return &____isOpen_13; }
	inline void set__isOpen_13(bool value)
	{
		____isOpen_13 = value;
	}
};

struct StringWriter_t194EF1526E072B93984370042AA80926C2EB6139_StaticFields
{
public:
	// System.Text.UnicodeEncoding modreq(System.Runtime.CompilerServices.IsVolatile) System.IO.StringWriter::m_encoding
	UnicodeEncoding_t6E0E60A1D7A4BECF9901B00EB286FFC2B57F6356 * ___m_encoding_11;

public:
	inline static int32_t get_offset_of_m_encoding_11() { return static_cast<int32_t>(offsetof(StringWriter_t194EF1526E072B93984370042AA80926C2EB6139_StaticFields, ___m_encoding_11)); }
	inline UnicodeEncoding_t6E0E60A1D7A4BECF9901B00EB286FFC2B57F6356 * get_m_encoding_11() const { return ___m_encoding_11; }
	inline UnicodeEncoding_t6E0E60A1D7A4BECF9901B00EB286FFC2B57F6356 ** get_address_of_m_encoding_11() { return &___m_encoding_11; }
	inline void set_m_encoding_11(UnicodeEncoding_t6E0E60A1D7A4BECF9901B00EB286FFC2B57F6356 * value)
	{
		___m_encoding_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_encoding_11), (void*)value);
	}
};


// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.MethodInfo
struct  MethodInfo_t  : public MethodBase_t
{
public:

public:
};


// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_t6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StreamingContextStates_t6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeFieldHandle
struct  RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF 
{
public:
	// System.IntPtr System.RuntimeFieldHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.StringComparison
struct  StringComparison_t02BAA95468CE9E91115C604577611FDF58FEDCF0 
{
public:
	// System.Int32 System.StringComparison::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StringComparison_t02BAA95468CE9E91115C604577611FDF58FEDCF0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Xml.ExceptionType
struct  ExceptionType_tCE9408DF6C9BA684F34EDB7BA5D3B2EE436663CA 
{
public:
	// System.Int32 System.Xml.ExceptionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ExceptionType_tCE9408DF6C9BA684F34EDB7BA5D3B2EE436663CA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Xml.Formatting
struct  Formatting_tA4B29B0BD063518BB29B92C6B165E39D01C553E2 
{
public:
	// System.Int32 System.Xml.Formatting::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Formatting_tA4B29B0BD063518BB29B92C6B165E39D01C553E2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Xml.WriteState
struct  WriteState_tB6E09FBE3E53ABF78DCE76F82D8587ACA70EE132 
{
public:
	// System.Int32 System.Xml.WriteState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WriteState_tB6E09FBE3E53ABF78DCE76F82D8587ACA70EE132, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Xml.XmlConvert
struct  XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7  : public RuntimeObject
{
public:

public:
};

struct XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_StaticFields
{
public:
	// System.Xml.XmlCharType System.Xml.XmlConvert::xmlCharType
	XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  ___xmlCharType_0;
	// System.Char[] System.Xml.XmlConvert::crt
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___crt_1;
	// System.Int32 System.Xml.XmlConvert::c_EncodedCharLength
	int32_t ___c_EncodedCharLength_2;
	// System.Char[] System.Xml.XmlConvert::WhitespaceChars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___WhitespaceChars_3;

public:
	inline static int32_t get_offset_of_xmlCharType_0() { return static_cast<int32_t>(offsetof(XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_StaticFields, ___xmlCharType_0)); }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  get_xmlCharType_0() const { return ___xmlCharType_0; }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * get_address_of_xmlCharType_0() { return &___xmlCharType_0; }
	inline void set_xmlCharType_0(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  value)
	{
		___xmlCharType_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___xmlCharType_0))->___charProperties_2), (void*)NULL);
	}

	inline static int32_t get_offset_of_crt_1() { return static_cast<int32_t>(offsetof(XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_StaticFields, ___crt_1)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_crt_1() const { return ___crt_1; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_crt_1() { return &___crt_1; }
	inline void set_crt_1(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___crt_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___crt_1), (void*)value);
	}

	inline static int32_t get_offset_of_c_EncodedCharLength_2() { return static_cast<int32_t>(offsetof(XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_StaticFields, ___c_EncodedCharLength_2)); }
	inline int32_t get_c_EncodedCharLength_2() const { return ___c_EncodedCharLength_2; }
	inline int32_t* get_address_of_c_EncodedCharLength_2() { return &___c_EncodedCharLength_2; }
	inline void set_c_EncodedCharLength_2(int32_t value)
	{
		___c_EncodedCharLength_2 = value;
	}

	inline static int32_t get_offset_of_WhitespaceChars_3() { return static_cast<int32_t>(offsetof(XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_StaticFields, ___WhitespaceChars_3)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_WhitespaceChars_3() const { return ___WhitespaceChars_3; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_WhitespaceChars_3() { return &___WhitespaceChars_3; }
	inline void set_WhitespaceChars_3(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___WhitespaceChars_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___WhitespaceChars_3), (void*)value);
	}
};


// System.Xml.XmlNodeType
struct  XmlNodeType_tEE56AC4F9EC36B979516EA5836C4DA730B0A21E1 
{
public:
	// System.Int32 System.Xml.XmlNodeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlNodeType_tEE56AC4F9EC36B979516EA5836C4DA730B0A21E1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Xml.XmlSpace
struct  XmlSpace_tC1A644D65B6BE72CF02BA2687B5AE169713271AB 
{
public:
	// System.Int32 System.Xml.XmlSpace::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSpace_tC1A644D65B6BE72CF02BA2687B5AE169713271AB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Xml.XmlTextEncoder
struct  XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475  : public RuntimeObject
{
public:
	// System.IO.TextWriter System.Xml.XmlTextEncoder::textWriter
	TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ___textWriter_0;
	// System.Boolean System.Xml.XmlTextEncoder::inAttribute
	bool ___inAttribute_1;
	// System.Char System.Xml.XmlTextEncoder::quoteChar
	Il2CppChar ___quoteChar_2;
	// System.Text.StringBuilder System.Xml.XmlTextEncoder::attrValue
	StringBuilder_t * ___attrValue_3;
	// System.Boolean System.Xml.XmlTextEncoder::cacheAttrValue
	bool ___cacheAttrValue_4;
	// System.Xml.XmlCharType System.Xml.XmlTextEncoder::xmlCharType
	XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  ___xmlCharType_5;

public:
	inline static int32_t get_offset_of_textWriter_0() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475, ___textWriter_0)); }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * get_textWriter_0() const { return ___textWriter_0; }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 ** get_address_of_textWriter_0() { return &___textWriter_0; }
	inline void set_textWriter_0(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * value)
	{
		___textWriter_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textWriter_0), (void*)value);
	}

	inline static int32_t get_offset_of_inAttribute_1() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475, ___inAttribute_1)); }
	inline bool get_inAttribute_1() const { return ___inAttribute_1; }
	inline bool* get_address_of_inAttribute_1() { return &___inAttribute_1; }
	inline void set_inAttribute_1(bool value)
	{
		___inAttribute_1 = value;
	}

	inline static int32_t get_offset_of_quoteChar_2() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475, ___quoteChar_2)); }
	inline Il2CppChar get_quoteChar_2() const { return ___quoteChar_2; }
	inline Il2CppChar* get_address_of_quoteChar_2() { return &___quoteChar_2; }
	inline void set_quoteChar_2(Il2CppChar value)
	{
		___quoteChar_2 = value;
	}

	inline static int32_t get_offset_of_attrValue_3() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475, ___attrValue_3)); }
	inline StringBuilder_t * get_attrValue_3() const { return ___attrValue_3; }
	inline StringBuilder_t ** get_address_of_attrValue_3() { return &___attrValue_3; }
	inline void set_attrValue_3(StringBuilder_t * value)
	{
		___attrValue_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___attrValue_3), (void*)value);
	}

	inline static int32_t get_offset_of_cacheAttrValue_4() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475, ___cacheAttrValue_4)); }
	inline bool get_cacheAttrValue_4() const { return ___cacheAttrValue_4; }
	inline bool* get_address_of_cacheAttrValue_4() { return &___cacheAttrValue_4; }
	inline void set_cacheAttrValue_4(bool value)
	{
		___cacheAttrValue_4 = value;
	}

	inline static int32_t get_offset_of_xmlCharType_5() { return static_cast<int32_t>(offsetof(XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475, ___xmlCharType_5)); }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  get_xmlCharType_5() const { return ___xmlCharType_5; }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * get_address_of_xmlCharType_5() { return &___xmlCharType_5; }
	inline void set_xmlCharType_5(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  value)
	{
		___xmlCharType_5 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___xmlCharType_5))->___charProperties_2), (void*)NULL);
	}
};


// System.Xml.XmlTextWriter_NamespaceState
struct  NamespaceState_tA6D09DE55EC80A8EB2C0C5853DC6EDA93857DD32 
{
public:
	// System.Int32 System.Xml.XmlTextWriter_NamespaceState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NamespaceState_tA6D09DE55EC80A8EB2C0C5853DC6EDA93857DD32, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Xml.XmlTextWriter_SpecialAttr
struct  SpecialAttr_tC6122E1A1EC64E66D03B8B3D17FE6FE0522D6528 
{
public:
	// System.Int32 System.Xml.XmlTextWriter_SpecialAttr::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpecialAttr_tC6122E1A1EC64E66D03B8B3D17FE6FE0522D6528, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Xml.XmlTextWriter_State
struct  State_t8831DFDA8E7A6CD6ACC98BD44F6BEF50B0C0E648 
{
public:
	// System.Int32 System.Xml.XmlTextWriter_State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t8831DFDA8E7A6CD6ACC98BD44F6BEF50B0C0E648, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Xml.XmlTextWriter_Token
struct  Token_tBCB56C1082F0646C24614CAED556F59987BAFA32 
{
public:
	// System.Int32 System.Xml.XmlTextWriter_Token::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Token_tBCB56C1082F0646C24614CAED556F59987BAFA32, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Globalization.NumberFormatInfo
struct  NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8  : public RuntimeObject
{
public:
	// System.Int32[] System.Globalization.NumberFormatInfo::numberGroupSizes
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___numberGroupSizes_1;
	// System.Int32[] System.Globalization.NumberFormatInfo::currencyGroupSizes
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___currencyGroupSizes_2;
	// System.Int32[] System.Globalization.NumberFormatInfo::percentGroupSizes
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___percentGroupSizes_3;
	// System.String System.Globalization.NumberFormatInfo::positiveSign
	String_t* ___positiveSign_4;
	// System.String System.Globalization.NumberFormatInfo::negativeSign
	String_t* ___negativeSign_5;
	// System.String System.Globalization.NumberFormatInfo::numberDecimalSeparator
	String_t* ___numberDecimalSeparator_6;
	// System.String System.Globalization.NumberFormatInfo::numberGroupSeparator
	String_t* ___numberGroupSeparator_7;
	// System.String System.Globalization.NumberFormatInfo::currencyGroupSeparator
	String_t* ___currencyGroupSeparator_8;
	// System.String System.Globalization.NumberFormatInfo::currencyDecimalSeparator
	String_t* ___currencyDecimalSeparator_9;
	// System.String System.Globalization.NumberFormatInfo::currencySymbol
	String_t* ___currencySymbol_10;
	// System.String System.Globalization.NumberFormatInfo::ansiCurrencySymbol
	String_t* ___ansiCurrencySymbol_11;
	// System.String System.Globalization.NumberFormatInfo::nanSymbol
	String_t* ___nanSymbol_12;
	// System.String System.Globalization.NumberFormatInfo::positiveInfinitySymbol
	String_t* ___positiveInfinitySymbol_13;
	// System.String System.Globalization.NumberFormatInfo::negativeInfinitySymbol
	String_t* ___negativeInfinitySymbol_14;
	// System.String System.Globalization.NumberFormatInfo::percentDecimalSeparator
	String_t* ___percentDecimalSeparator_15;
	// System.String System.Globalization.NumberFormatInfo::percentGroupSeparator
	String_t* ___percentGroupSeparator_16;
	// System.String System.Globalization.NumberFormatInfo::percentSymbol
	String_t* ___percentSymbol_17;
	// System.String System.Globalization.NumberFormatInfo::perMilleSymbol
	String_t* ___perMilleSymbol_18;
	// System.String[] System.Globalization.NumberFormatInfo::nativeDigits
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___nativeDigits_19;
	// System.Int32 System.Globalization.NumberFormatInfo::m_dataItem
	int32_t ___m_dataItem_20;
	// System.Int32 System.Globalization.NumberFormatInfo::numberDecimalDigits
	int32_t ___numberDecimalDigits_21;
	// System.Int32 System.Globalization.NumberFormatInfo::currencyDecimalDigits
	int32_t ___currencyDecimalDigits_22;
	// System.Int32 System.Globalization.NumberFormatInfo::currencyPositivePattern
	int32_t ___currencyPositivePattern_23;
	// System.Int32 System.Globalization.NumberFormatInfo::currencyNegativePattern
	int32_t ___currencyNegativePattern_24;
	// System.Int32 System.Globalization.NumberFormatInfo::numberNegativePattern
	int32_t ___numberNegativePattern_25;
	// System.Int32 System.Globalization.NumberFormatInfo::percentPositivePattern
	int32_t ___percentPositivePattern_26;
	// System.Int32 System.Globalization.NumberFormatInfo::percentNegativePattern
	int32_t ___percentNegativePattern_27;
	// System.Int32 System.Globalization.NumberFormatInfo::percentDecimalDigits
	int32_t ___percentDecimalDigits_28;
	// System.Int32 System.Globalization.NumberFormatInfo::digitSubstitution
	int32_t ___digitSubstitution_29;
	// System.Boolean System.Globalization.NumberFormatInfo::isReadOnly
	bool ___isReadOnly_30;
	// System.Boolean System.Globalization.NumberFormatInfo::m_useUserOverride
	bool ___m_useUserOverride_31;
	// System.Boolean System.Globalization.NumberFormatInfo::m_isInvariant
	bool ___m_isInvariant_32;
	// System.Boolean System.Globalization.NumberFormatInfo::validForParseAsNumber
	bool ___validForParseAsNumber_33;
	// System.Boolean System.Globalization.NumberFormatInfo::validForParseAsCurrency
	bool ___validForParseAsCurrency_34;

public:
	inline static int32_t get_offset_of_numberGroupSizes_1() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___numberGroupSizes_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_numberGroupSizes_1() const { return ___numberGroupSizes_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_numberGroupSizes_1() { return &___numberGroupSizes_1; }
	inline void set_numberGroupSizes_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___numberGroupSizes_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___numberGroupSizes_1), (void*)value);
	}

	inline static int32_t get_offset_of_currencyGroupSizes_2() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___currencyGroupSizes_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_currencyGroupSizes_2() const { return ___currencyGroupSizes_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_currencyGroupSizes_2() { return &___currencyGroupSizes_2; }
	inline void set_currencyGroupSizes_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___currencyGroupSizes_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currencyGroupSizes_2), (void*)value);
	}

	inline static int32_t get_offset_of_percentGroupSizes_3() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___percentGroupSizes_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_percentGroupSizes_3() const { return ___percentGroupSizes_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_percentGroupSizes_3() { return &___percentGroupSizes_3; }
	inline void set_percentGroupSizes_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___percentGroupSizes_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___percentGroupSizes_3), (void*)value);
	}

	inline static int32_t get_offset_of_positiveSign_4() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___positiveSign_4)); }
	inline String_t* get_positiveSign_4() const { return ___positiveSign_4; }
	inline String_t** get_address_of_positiveSign_4() { return &___positiveSign_4; }
	inline void set_positiveSign_4(String_t* value)
	{
		___positiveSign_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___positiveSign_4), (void*)value);
	}

	inline static int32_t get_offset_of_negativeSign_5() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___negativeSign_5)); }
	inline String_t* get_negativeSign_5() const { return ___negativeSign_5; }
	inline String_t** get_address_of_negativeSign_5() { return &___negativeSign_5; }
	inline void set_negativeSign_5(String_t* value)
	{
		___negativeSign_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___negativeSign_5), (void*)value);
	}

	inline static int32_t get_offset_of_numberDecimalSeparator_6() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___numberDecimalSeparator_6)); }
	inline String_t* get_numberDecimalSeparator_6() const { return ___numberDecimalSeparator_6; }
	inline String_t** get_address_of_numberDecimalSeparator_6() { return &___numberDecimalSeparator_6; }
	inline void set_numberDecimalSeparator_6(String_t* value)
	{
		___numberDecimalSeparator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___numberDecimalSeparator_6), (void*)value);
	}

	inline static int32_t get_offset_of_numberGroupSeparator_7() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___numberGroupSeparator_7)); }
	inline String_t* get_numberGroupSeparator_7() const { return ___numberGroupSeparator_7; }
	inline String_t** get_address_of_numberGroupSeparator_7() { return &___numberGroupSeparator_7; }
	inline void set_numberGroupSeparator_7(String_t* value)
	{
		___numberGroupSeparator_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___numberGroupSeparator_7), (void*)value);
	}

	inline static int32_t get_offset_of_currencyGroupSeparator_8() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___currencyGroupSeparator_8)); }
	inline String_t* get_currencyGroupSeparator_8() const { return ___currencyGroupSeparator_8; }
	inline String_t** get_address_of_currencyGroupSeparator_8() { return &___currencyGroupSeparator_8; }
	inline void set_currencyGroupSeparator_8(String_t* value)
	{
		___currencyGroupSeparator_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currencyGroupSeparator_8), (void*)value);
	}

	inline static int32_t get_offset_of_currencyDecimalSeparator_9() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___currencyDecimalSeparator_9)); }
	inline String_t* get_currencyDecimalSeparator_9() const { return ___currencyDecimalSeparator_9; }
	inline String_t** get_address_of_currencyDecimalSeparator_9() { return &___currencyDecimalSeparator_9; }
	inline void set_currencyDecimalSeparator_9(String_t* value)
	{
		___currencyDecimalSeparator_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currencyDecimalSeparator_9), (void*)value);
	}

	inline static int32_t get_offset_of_currencySymbol_10() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___currencySymbol_10)); }
	inline String_t* get_currencySymbol_10() const { return ___currencySymbol_10; }
	inline String_t** get_address_of_currencySymbol_10() { return &___currencySymbol_10; }
	inline void set_currencySymbol_10(String_t* value)
	{
		___currencySymbol_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currencySymbol_10), (void*)value);
	}

	inline static int32_t get_offset_of_ansiCurrencySymbol_11() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___ansiCurrencySymbol_11)); }
	inline String_t* get_ansiCurrencySymbol_11() const { return ___ansiCurrencySymbol_11; }
	inline String_t** get_address_of_ansiCurrencySymbol_11() { return &___ansiCurrencySymbol_11; }
	inline void set_ansiCurrencySymbol_11(String_t* value)
	{
		___ansiCurrencySymbol_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ansiCurrencySymbol_11), (void*)value);
	}

	inline static int32_t get_offset_of_nanSymbol_12() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___nanSymbol_12)); }
	inline String_t* get_nanSymbol_12() const { return ___nanSymbol_12; }
	inline String_t** get_address_of_nanSymbol_12() { return &___nanSymbol_12; }
	inline void set_nanSymbol_12(String_t* value)
	{
		___nanSymbol_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nanSymbol_12), (void*)value);
	}

	inline static int32_t get_offset_of_positiveInfinitySymbol_13() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___positiveInfinitySymbol_13)); }
	inline String_t* get_positiveInfinitySymbol_13() const { return ___positiveInfinitySymbol_13; }
	inline String_t** get_address_of_positiveInfinitySymbol_13() { return &___positiveInfinitySymbol_13; }
	inline void set_positiveInfinitySymbol_13(String_t* value)
	{
		___positiveInfinitySymbol_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___positiveInfinitySymbol_13), (void*)value);
	}

	inline static int32_t get_offset_of_negativeInfinitySymbol_14() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___negativeInfinitySymbol_14)); }
	inline String_t* get_negativeInfinitySymbol_14() const { return ___negativeInfinitySymbol_14; }
	inline String_t** get_address_of_negativeInfinitySymbol_14() { return &___negativeInfinitySymbol_14; }
	inline void set_negativeInfinitySymbol_14(String_t* value)
	{
		___negativeInfinitySymbol_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___negativeInfinitySymbol_14), (void*)value);
	}

	inline static int32_t get_offset_of_percentDecimalSeparator_15() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___percentDecimalSeparator_15)); }
	inline String_t* get_percentDecimalSeparator_15() const { return ___percentDecimalSeparator_15; }
	inline String_t** get_address_of_percentDecimalSeparator_15() { return &___percentDecimalSeparator_15; }
	inline void set_percentDecimalSeparator_15(String_t* value)
	{
		___percentDecimalSeparator_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___percentDecimalSeparator_15), (void*)value);
	}

	inline static int32_t get_offset_of_percentGroupSeparator_16() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___percentGroupSeparator_16)); }
	inline String_t* get_percentGroupSeparator_16() const { return ___percentGroupSeparator_16; }
	inline String_t** get_address_of_percentGroupSeparator_16() { return &___percentGroupSeparator_16; }
	inline void set_percentGroupSeparator_16(String_t* value)
	{
		___percentGroupSeparator_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___percentGroupSeparator_16), (void*)value);
	}

	inline static int32_t get_offset_of_percentSymbol_17() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___percentSymbol_17)); }
	inline String_t* get_percentSymbol_17() const { return ___percentSymbol_17; }
	inline String_t** get_address_of_percentSymbol_17() { return &___percentSymbol_17; }
	inline void set_percentSymbol_17(String_t* value)
	{
		___percentSymbol_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___percentSymbol_17), (void*)value);
	}

	inline static int32_t get_offset_of_perMilleSymbol_18() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___perMilleSymbol_18)); }
	inline String_t* get_perMilleSymbol_18() const { return ___perMilleSymbol_18; }
	inline String_t** get_address_of_perMilleSymbol_18() { return &___perMilleSymbol_18; }
	inline void set_perMilleSymbol_18(String_t* value)
	{
		___perMilleSymbol_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___perMilleSymbol_18), (void*)value);
	}

	inline static int32_t get_offset_of_nativeDigits_19() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___nativeDigits_19)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_nativeDigits_19() const { return ___nativeDigits_19; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_nativeDigits_19() { return &___nativeDigits_19; }
	inline void set_nativeDigits_19(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___nativeDigits_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nativeDigits_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_dataItem_20() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___m_dataItem_20)); }
	inline int32_t get_m_dataItem_20() const { return ___m_dataItem_20; }
	inline int32_t* get_address_of_m_dataItem_20() { return &___m_dataItem_20; }
	inline void set_m_dataItem_20(int32_t value)
	{
		___m_dataItem_20 = value;
	}

	inline static int32_t get_offset_of_numberDecimalDigits_21() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___numberDecimalDigits_21)); }
	inline int32_t get_numberDecimalDigits_21() const { return ___numberDecimalDigits_21; }
	inline int32_t* get_address_of_numberDecimalDigits_21() { return &___numberDecimalDigits_21; }
	inline void set_numberDecimalDigits_21(int32_t value)
	{
		___numberDecimalDigits_21 = value;
	}

	inline static int32_t get_offset_of_currencyDecimalDigits_22() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___currencyDecimalDigits_22)); }
	inline int32_t get_currencyDecimalDigits_22() const { return ___currencyDecimalDigits_22; }
	inline int32_t* get_address_of_currencyDecimalDigits_22() { return &___currencyDecimalDigits_22; }
	inline void set_currencyDecimalDigits_22(int32_t value)
	{
		___currencyDecimalDigits_22 = value;
	}

	inline static int32_t get_offset_of_currencyPositivePattern_23() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___currencyPositivePattern_23)); }
	inline int32_t get_currencyPositivePattern_23() const { return ___currencyPositivePattern_23; }
	inline int32_t* get_address_of_currencyPositivePattern_23() { return &___currencyPositivePattern_23; }
	inline void set_currencyPositivePattern_23(int32_t value)
	{
		___currencyPositivePattern_23 = value;
	}

	inline static int32_t get_offset_of_currencyNegativePattern_24() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___currencyNegativePattern_24)); }
	inline int32_t get_currencyNegativePattern_24() const { return ___currencyNegativePattern_24; }
	inline int32_t* get_address_of_currencyNegativePattern_24() { return &___currencyNegativePattern_24; }
	inline void set_currencyNegativePattern_24(int32_t value)
	{
		___currencyNegativePattern_24 = value;
	}

	inline static int32_t get_offset_of_numberNegativePattern_25() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___numberNegativePattern_25)); }
	inline int32_t get_numberNegativePattern_25() const { return ___numberNegativePattern_25; }
	inline int32_t* get_address_of_numberNegativePattern_25() { return &___numberNegativePattern_25; }
	inline void set_numberNegativePattern_25(int32_t value)
	{
		___numberNegativePattern_25 = value;
	}

	inline static int32_t get_offset_of_percentPositivePattern_26() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___percentPositivePattern_26)); }
	inline int32_t get_percentPositivePattern_26() const { return ___percentPositivePattern_26; }
	inline int32_t* get_address_of_percentPositivePattern_26() { return &___percentPositivePattern_26; }
	inline void set_percentPositivePattern_26(int32_t value)
	{
		___percentPositivePattern_26 = value;
	}

	inline static int32_t get_offset_of_percentNegativePattern_27() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___percentNegativePattern_27)); }
	inline int32_t get_percentNegativePattern_27() const { return ___percentNegativePattern_27; }
	inline int32_t* get_address_of_percentNegativePattern_27() { return &___percentNegativePattern_27; }
	inline void set_percentNegativePattern_27(int32_t value)
	{
		___percentNegativePattern_27 = value;
	}

	inline static int32_t get_offset_of_percentDecimalDigits_28() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___percentDecimalDigits_28)); }
	inline int32_t get_percentDecimalDigits_28() const { return ___percentDecimalDigits_28; }
	inline int32_t* get_address_of_percentDecimalDigits_28() { return &___percentDecimalDigits_28; }
	inline void set_percentDecimalDigits_28(int32_t value)
	{
		___percentDecimalDigits_28 = value;
	}

	inline static int32_t get_offset_of_digitSubstitution_29() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___digitSubstitution_29)); }
	inline int32_t get_digitSubstitution_29() const { return ___digitSubstitution_29; }
	inline int32_t* get_address_of_digitSubstitution_29() { return &___digitSubstitution_29; }
	inline void set_digitSubstitution_29(int32_t value)
	{
		___digitSubstitution_29 = value;
	}

	inline static int32_t get_offset_of_isReadOnly_30() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___isReadOnly_30)); }
	inline bool get_isReadOnly_30() const { return ___isReadOnly_30; }
	inline bool* get_address_of_isReadOnly_30() { return &___isReadOnly_30; }
	inline void set_isReadOnly_30(bool value)
	{
		___isReadOnly_30 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_31() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___m_useUserOverride_31)); }
	inline bool get_m_useUserOverride_31() const { return ___m_useUserOverride_31; }
	inline bool* get_address_of_m_useUserOverride_31() { return &___m_useUserOverride_31; }
	inline void set_m_useUserOverride_31(bool value)
	{
		___m_useUserOverride_31 = value;
	}

	inline static int32_t get_offset_of_m_isInvariant_32() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___m_isInvariant_32)); }
	inline bool get_m_isInvariant_32() const { return ___m_isInvariant_32; }
	inline bool* get_address_of_m_isInvariant_32() { return &___m_isInvariant_32; }
	inline void set_m_isInvariant_32(bool value)
	{
		___m_isInvariant_32 = value;
	}

	inline static int32_t get_offset_of_validForParseAsNumber_33() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___validForParseAsNumber_33)); }
	inline bool get_validForParseAsNumber_33() const { return ___validForParseAsNumber_33; }
	inline bool* get_address_of_validForParseAsNumber_33() { return &___validForParseAsNumber_33; }
	inline void set_validForParseAsNumber_33(bool value)
	{
		___validForParseAsNumber_33 = value;
	}

	inline static int32_t get_offset_of_validForParseAsCurrency_34() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8, ___validForParseAsCurrency_34)); }
	inline bool get_validForParseAsCurrency_34() const { return ___validForParseAsCurrency_34; }
	inline bool* get_address_of_validForParseAsCurrency_34() { return &___validForParseAsCurrency_34; }
	inline void set_validForParseAsCurrency_34(bool value)
	{
		___validForParseAsCurrency_34 = value;
	}
};

struct NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8_StaticFields
{
public:
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.NumberFormatInfo::invariantInfo
	NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * ___invariantInfo_0;

public:
	inline static int32_t get_offset_of_invariantInfo_0() { return static_cast<int32_t>(offsetof(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8_StaticFields, ___invariantInfo_0)); }
	inline NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * get_invariantInfo_0() const { return ___invariantInfo_0; }
	inline NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 ** get_address_of_invariantInfo_0() { return &___invariantInfo_0; }
	inline void set_invariantInfo_0(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * value)
	{
		___invariantInfo_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___invariantInfo_0), (void*)value);
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034 
{
public:
	// System.Object System.Runtime.Serialization.StreamingContext::m_additionalContext
	RuntimeObject * ___m_additionalContext_0;
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::m_state
	int32_t ___m_state_1;

public:
	inline static int32_t get_offset_of_m_additionalContext_0() { return static_cast<int32_t>(offsetof(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034, ___m_additionalContext_0)); }
	inline RuntimeObject * get_m_additionalContext_0() const { return ___m_additionalContext_0; }
	inline RuntimeObject ** get_address_of_m_additionalContext_0() { return &___m_additionalContext_0; }
	inline void set_m_additionalContext_0(RuntimeObject * value)
	{
		___m_additionalContext_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_additionalContext_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_state_1() { return static_cast<int32_t>(offsetof(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034, ___m_state_1)); }
	inline int32_t get_m_state_1() const { return ___m_state_1; }
	inline int32_t* get_address_of_m_state_1() { return &___m_state_1; }
	inline void set_m_state_1(int32_t value)
	{
		___m_state_1 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034_marshaled_pinvoke
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034_marshaled_com
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// System.Xml.XmlTextWriter
struct  XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6  : public XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869
{
public:
	// System.IO.TextWriter System.Xml.XmlTextWriter::textWriter
	TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ___textWriter_0;
	// System.Xml.XmlTextEncoder System.Xml.XmlTextWriter::xmlEncoder
	XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * ___xmlEncoder_1;
	// System.Text.Encoding System.Xml.XmlTextWriter::encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___encoding_2;
	// System.Xml.Formatting System.Xml.XmlTextWriter::formatting
	int32_t ___formatting_3;
	// System.Boolean System.Xml.XmlTextWriter::indented
	bool ___indented_4;
	// System.Int32 System.Xml.XmlTextWriter::indentation
	int32_t ___indentation_5;
	// System.Char System.Xml.XmlTextWriter::indentChar
	Il2CppChar ___indentChar_6;
	// System.Xml.XmlTextWriter_TagInfo[] System.Xml.XmlTextWriter::stack
	TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* ___stack_7;
	// System.Int32 System.Xml.XmlTextWriter::top
	int32_t ___top_8;
	// System.Xml.XmlTextWriter_State[] System.Xml.XmlTextWriter::stateTable
	StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* ___stateTable_9;
	// System.Xml.XmlTextWriter_State System.Xml.XmlTextWriter::currentState
	int32_t ___currentState_10;
	// System.Xml.XmlTextWriter_Token System.Xml.XmlTextWriter::lastToken
	int32_t ___lastToken_11;
	// System.Xml.XmlTextWriterBase64Encoder System.Xml.XmlTextWriter::base64Encoder
	XmlTextWriterBase64Encoder_t1362FFBE93C9BC675B4A34503DB31AA6D3AAF01C * ___base64Encoder_12;
	// System.Char System.Xml.XmlTextWriter::quoteChar
	Il2CppChar ___quoteChar_13;
	// System.Char System.Xml.XmlTextWriter::curQuoteChar
	Il2CppChar ___curQuoteChar_14;
	// System.Boolean System.Xml.XmlTextWriter::namespaces
	bool ___namespaces_15;
	// System.Xml.XmlTextWriter_SpecialAttr System.Xml.XmlTextWriter::specialAttr
	int32_t ___specialAttr_16;
	// System.String System.Xml.XmlTextWriter::prefixForXmlNs
	String_t* ___prefixForXmlNs_17;
	// System.Boolean System.Xml.XmlTextWriter::flush
	bool ___flush_18;
	// System.Xml.XmlTextWriter_Namespace[] System.Xml.XmlTextWriter::nsStack
	NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* ___nsStack_19;
	// System.Int32 System.Xml.XmlTextWriter::nsTop
	int32_t ___nsTop_20;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlTextWriter::nsHashtable
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ___nsHashtable_21;
	// System.Boolean System.Xml.XmlTextWriter::useNsHashtable
	bool ___useNsHashtable_22;
	// System.Xml.XmlCharType System.Xml.XmlTextWriter::xmlCharType
	XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  ___xmlCharType_23;

public:
	inline static int32_t get_offset_of_textWriter_0() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___textWriter_0)); }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * get_textWriter_0() const { return ___textWriter_0; }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 ** get_address_of_textWriter_0() { return &___textWriter_0; }
	inline void set_textWriter_0(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * value)
	{
		___textWriter_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textWriter_0), (void*)value);
	}

	inline static int32_t get_offset_of_xmlEncoder_1() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___xmlEncoder_1)); }
	inline XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * get_xmlEncoder_1() const { return ___xmlEncoder_1; }
	inline XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 ** get_address_of_xmlEncoder_1() { return &___xmlEncoder_1; }
	inline void set_xmlEncoder_1(XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * value)
	{
		___xmlEncoder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___xmlEncoder_1), (void*)value);
	}

	inline static int32_t get_offset_of_encoding_2() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___encoding_2)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_encoding_2() const { return ___encoding_2; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_encoding_2() { return &___encoding_2; }
	inline void set_encoding_2(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___encoding_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___encoding_2), (void*)value);
	}

	inline static int32_t get_offset_of_formatting_3() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___formatting_3)); }
	inline int32_t get_formatting_3() const { return ___formatting_3; }
	inline int32_t* get_address_of_formatting_3() { return &___formatting_3; }
	inline void set_formatting_3(int32_t value)
	{
		___formatting_3 = value;
	}

	inline static int32_t get_offset_of_indented_4() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___indented_4)); }
	inline bool get_indented_4() const { return ___indented_4; }
	inline bool* get_address_of_indented_4() { return &___indented_4; }
	inline void set_indented_4(bool value)
	{
		___indented_4 = value;
	}

	inline static int32_t get_offset_of_indentation_5() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___indentation_5)); }
	inline int32_t get_indentation_5() const { return ___indentation_5; }
	inline int32_t* get_address_of_indentation_5() { return &___indentation_5; }
	inline void set_indentation_5(int32_t value)
	{
		___indentation_5 = value;
	}

	inline static int32_t get_offset_of_indentChar_6() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___indentChar_6)); }
	inline Il2CppChar get_indentChar_6() const { return ___indentChar_6; }
	inline Il2CppChar* get_address_of_indentChar_6() { return &___indentChar_6; }
	inline void set_indentChar_6(Il2CppChar value)
	{
		___indentChar_6 = value;
	}

	inline static int32_t get_offset_of_stack_7() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___stack_7)); }
	inline TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* get_stack_7() const { return ___stack_7; }
	inline TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910** get_address_of_stack_7() { return &___stack_7; }
	inline void set_stack_7(TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* value)
	{
		___stack_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stack_7), (void*)value);
	}

	inline static int32_t get_offset_of_top_8() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___top_8)); }
	inline int32_t get_top_8() const { return ___top_8; }
	inline int32_t* get_address_of_top_8() { return &___top_8; }
	inline void set_top_8(int32_t value)
	{
		___top_8 = value;
	}

	inline static int32_t get_offset_of_stateTable_9() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___stateTable_9)); }
	inline StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* get_stateTable_9() const { return ___stateTable_9; }
	inline StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2** get_address_of_stateTable_9() { return &___stateTable_9; }
	inline void set_stateTable_9(StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* value)
	{
		___stateTable_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stateTable_9), (void*)value);
	}

	inline static int32_t get_offset_of_currentState_10() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___currentState_10)); }
	inline int32_t get_currentState_10() const { return ___currentState_10; }
	inline int32_t* get_address_of_currentState_10() { return &___currentState_10; }
	inline void set_currentState_10(int32_t value)
	{
		___currentState_10 = value;
	}

	inline static int32_t get_offset_of_lastToken_11() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___lastToken_11)); }
	inline int32_t get_lastToken_11() const { return ___lastToken_11; }
	inline int32_t* get_address_of_lastToken_11() { return &___lastToken_11; }
	inline void set_lastToken_11(int32_t value)
	{
		___lastToken_11 = value;
	}

	inline static int32_t get_offset_of_base64Encoder_12() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___base64Encoder_12)); }
	inline XmlTextWriterBase64Encoder_t1362FFBE93C9BC675B4A34503DB31AA6D3AAF01C * get_base64Encoder_12() const { return ___base64Encoder_12; }
	inline XmlTextWriterBase64Encoder_t1362FFBE93C9BC675B4A34503DB31AA6D3AAF01C ** get_address_of_base64Encoder_12() { return &___base64Encoder_12; }
	inline void set_base64Encoder_12(XmlTextWriterBase64Encoder_t1362FFBE93C9BC675B4A34503DB31AA6D3AAF01C * value)
	{
		___base64Encoder_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___base64Encoder_12), (void*)value);
	}

	inline static int32_t get_offset_of_quoteChar_13() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___quoteChar_13)); }
	inline Il2CppChar get_quoteChar_13() const { return ___quoteChar_13; }
	inline Il2CppChar* get_address_of_quoteChar_13() { return &___quoteChar_13; }
	inline void set_quoteChar_13(Il2CppChar value)
	{
		___quoteChar_13 = value;
	}

	inline static int32_t get_offset_of_curQuoteChar_14() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___curQuoteChar_14)); }
	inline Il2CppChar get_curQuoteChar_14() const { return ___curQuoteChar_14; }
	inline Il2CppChar* get_address_of_curQuoteChar_14() { return &___curQuoteChar_14; }
	inline void set_curQuoteChar_14(Il2CppChar value)
	{
		___curQuoteChar_14 = value;
	}

	inline static int32_t get_offset_of_namespaces_15() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___namespaces_15)); }
	inline bool get_namespaces_15() const { return ___namespaces_15; }
	inline bool* get_address_of_namespaces_15() { return &___namespaces_15; }
	inline void set_namespaces_15(bool value)
	{
		___namespaces_15 = value;
	}

	inline static int32_t get_offset_of_specialAttr_16() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___specialAttr_16)); }
	inline int32_t get_specialAttr_16() const { return ___specialAttr_16; }
	inline int32_t* get_address_of_specialAttr_16() { return &___specialAttr_16; }
	inline void set_specialAttr_16(int32_t value)
	{
		___specialAttr_16 = value;
	}

	inline static int32_t get_offset_of_prefixForXmlNs_17() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___prefixForXmlNs_17)); }
	inline String_t* get_prefixForXmlNs_17() const { return ___prefixForXmlNs_17; }
	inline String_t** get_address_of_prefixForXmlNs_17() { return &___prefixForXmlNs_17; }
	inline void set_prefixForXmlNs_17(String_t* value)
	{
		___prefixForXmlNs_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prefixForXmlNs_17), (void*)value);
	}

	inline static int32_t get_offset_of_flush_18() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___flush_18)); }
	inline bool get_flush_18() const { return ___flush_18; }
	inline bool* get_address_of_flush_18() { return &___flush_18; }
	inline void set_flush_18(bool value)
	{
		___flush_18 = value;
	}

	inline static int32_t get_offset_of_nsStack_19() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___nsStack_19)); }
	inline NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* get_nsStack_19() const { return ___nsStack_19; }
	inline NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829** get_address_of_nsStack_19() { return &___nsStack_19; }
	inline void set_nsStack_19(NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* value)
	{
		___nsStack_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nsStack_19), (void*)value);
	}

	inline static int32_t get_offset_of_nsTop_20() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___nsTop_20)); }
	inline int32_t get_nsTop_20() const { return ___nsTop_20; }
	inline int32_t* get_address_of_nsTop_20() { return &___nsTop_20; }
	inline void set_nsTop_20(int32_t value)
	{
		___nsTop_20 = value;
	}

	inline static int32_t get_offset_of_nsHashtable_21() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___nsHashtable_21)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get_nsHashtable_21() const { return ___nsHashtable_21; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of_nsHashtable_21() { return &___nsHashtable_21; }
	inline void set_nsHashtable_21(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		___nsHashtable_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nsHashtable_21), (void*)value);
	}

	inline static int32_t get_offset_of_useNsHashtable_22() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___useNsHashtable_22)); }
	inline bool get_useNsHashtable_22() const { return ___useNsHashtable_22; }
	inline bool* get_address_of_useNsHashtable_22() { return &___useNsHashtable_22; }
	inline void set_useNsHashtable_22(bool value)
	{
		___useNsHashtable_22 = value;
	}

	inline static int32_t get_offset_of_xmlCharType_23() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___xmlCharType_23)); }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  get_xmlCharType_23() const { return ___xmlCharType_23; }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * get_address_of_xmlCharType_23() { return &___xmlCharType_23; }
	inline void set_xmlCharType_23(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  value)
	{
		___xmlCharType_23 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___xmlCharType_23))->___charProperties_2), (void*)NULL);
	}
};

struct XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields
{
public:
	// System.String[] System.Xml.XmlTextWriter::stateName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___stateName_24;
	// System.String[] System.Xml.XmlTextWriter::tokenName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___tokenName_25;
	// System.Xml.XmlTextWriter_State[] System.Xml.XmlTextWriter::stateTableDefault
	StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* ___stateTableDefault_26;
	// System.Xml.XmlTextWriter_State[] System.Xml.XmlTextWriter::stateTableDocument
	StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* ___stateTableDocument_27;

public:
	inline static int32_t get_offset_of_stateName_24() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields, ___stateName_24)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_stateName_24() const { return ___stateName_24; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_stateName_24() { return &___stateName_24; }
	inline void set_stateName_24(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___stateName_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stateName_24), (void*)value);
	}

	inline static int32_t get_offset_of_tokenName_25() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields, ___tokenName_25)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_tokenName_25() const { return ___tokenName_25; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_tokenName_25() { return &___tokenName_25; }
	inline void set_tokenName_25(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___tokenName_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tokenName_25), (void*)value);
	}

	inline static int32_t get_offset_of_stateTableDefault_26() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields, ___stateTableDefault_26)); }
	inline StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* get_stateTableDefault_26() const { return ___stateTableDefault_26; }
	inline StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2** get_address_of_stateTableDefault_26() { return &___stateTableDefault_26; }
	inline void set_stateTableDefault_26(StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* value)
	{
		___stateTableDefault_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stateTableDefault_26), (void*)value);
	}

	inline static int32_t get_offset_of_stateTableDocument_27() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields, ___stateTableDocument_27)); }
	inline StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* get_stateTableDocument_27() const { return ___stateTableDocument_27; }
	inline StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2** get_address_of_stateTableDocument_27() { return &___stateTableDocument_27; }
	inline void set_stateTableDocument_27(StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* value)
	{
		___stateTableDocument_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stateTableDocument_27), (void*)value);
	}
};


// System.Xml.XmlTextWriter_TagInfo
struct  TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5 
{
public:
	// System.String System.Xml.XmlTextWriter_TagInfo::name
	String_t* ___name_0;
	// System.String System.Xml.XmlTextWriter_TagInfo::prefix
	String_t* ___prefix_1;
	// System.String System.Xml.XmlTextWriter_TagInfo::defaultNs
	String_t* ___defaultNs_2;
	// System.Xml.XmlTextWriter_NamespaceState System.Xml.XmlTextWriter_TagInfo::defaultNsState
	int32_t ___defaultNsState_3;
	// System.Xml.XmlSpace System.Xml.XmlTextWriter_TagInfo::xmlSpace
	int32_t ___xmlSpace_4;
	// System.String System.Xml.XmlTextWriter_TagInfo::xmlLang
	String_t* ___xmlLang_5;
	// System.Int32 System.Xml.XmlTextWriter_TagInfo::prevNsTop
	int32_t ___prevNsTop_6;
	// System.Int32 System.Xml.XmlTextWriter_TagInfo::prefixCount
	int32_t ___prefixCount_7;
	// System.Boolean System.Xml.XmlTextWriter_TagInfo::mixed
	bool ___mixed_8;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_prefix_1() { return static_cast<int32_t>(offsetof(TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5, ___prefix_1)); }
	inline String_t* get_prefix_1() const { return ___prefix_1; }
	inline String_t** get_address_of_prefix_1() { return &___prefix_1; }
	inline void set_prefix_1(String_t* value)
	{
		___prefix_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prefix_1), (void*)value);
	}

	inline static int32_t get_offset_of_defaultNs_2() { return static_cast<int32_t>(offsetof(TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5, ___defaultNs_2)); }
	inline String_t* get_defaultNs_2() const { return ___defaultNs_2; }
	inline String_t** get_address_of_defaultNs_2() { return &___defaultNs_2; }
	inline void set_defaultNs_2(String_t* value)
	{
		___defaultNs_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultNs_2), (void*)value);
	}

	inline static int32_t get_offset_of_defaultNsState_3() { return static_cast<int32_t>(offsetof(TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5, ___defaultNsState_3)); }
	inline int32_t get_defaultNsState_3() const { return ___defaultNsState_3; }
	inline int32_t* get_address_of_defaultNsState_3() { return &___defaultNsState_3; }
	inline void set_defaultNsState_3(int32_t value)
	{
		___defaultNsState_3 = value;
	}

	inline static int32_t get_offset_of_xmlSpace_4() { return static_cast<int32_t>(offsetof(TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5, ___xmlSpace_4)); }
	inline int32_t get_xmlSpace_4() const { return ___xmlSpace_4; }
	inline int32_t* get_address_of_xmlSpace_4() { return &___xmlSpace_4; }
	inline void set_xmlSpace_4(int32_t value)
	{
		___xmlSpace_4 = value;
	}

	inline static int32_t get_offset_of_xmlLang_5() { return static_cast<int32_t>(offsetof(TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5, ___xmlLang_5)); }
	inline String_t* get_xmlLang_5() const { return ___xmlLang_5; }
	inline String_t** get_address_of_xmlLang_5() { return &___xmlLang_5; }
	inline void set_xmlLang_5(String_t* value)
	{
		___xmlLang_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___xmlLang_5), (void*)value);
	}

	inline static int32_t get_offset_of_prevNsTop_6() { return static_cast<int32_t>(offsetof(TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5, ___prevNsTop_6)); }
	inline int32_t get_prevNsTop_6() const { return ___prevNsTop_6; }
	inline int32_t* get_address_of_prevNsTop_6() { return &___prevNsTop_6; }
	inline void set_prevNsTop_6(int32_t value)
	{
		___prevNsTop_6 = value;
	}

	inline static int32_t get_offset_of_prefixCount_7() { return static_cast<int32_t>(offsetof(TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5, ___prefixCount_7)); }
	inline int32_t get_prefixCount_7() const { return ___prefixCount_7; }
	inline int32_t* get_address_of_prefixCount_7() { return &___prefixCount_7; }
	inline void set_prefixCount_7(int32_t value)
	{
		___prefixCount_7 = value;
	}

	inline static int32_t get_offset_of_mixed_8() { return static_cast<int32_t>(offsetof(TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5, ___mixed_8)); }
	inline bool get_mixed_8() const { return ___mixed_8; }
	inline bool* get_address_of_mixed_8() { return &___mixed_8; }
	inline void set_mixed_8(bool value)
	{
		___mixed_8 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Xml.XmlTextWriter/TagInfo
struct TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5_marshaled_pinvoke
{
	char* ___name_0;
	char* ___prefix_1;
	char* ___defaultNs_2;
	int32_t ___defaultNsState_3;
	int32_t ___xmlSpace_4;
	char* ___xmlLang_5;
	int32_t ___prevNsTop_6;
	int32_t ___prefixCount_7;
	int32_t ___mixed_8;
};
// Native definition for COM marshalling of System.Xml.XmlTextWriter/TagInfo
struct TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5_marshaled_com
{
	Il2CppChar* ___name_0;
	Il2CppChar* ___prefix_1;
	Il2CppChar* ___defaultNs_2;
	int32_t ___defaultNsState_3;
	int32_t ___xmlSpace_4;
	Il2CppChar* ___xmlLang_5;
	int32_t ___prevNsTop_6;
	int32_t ___prefixCount_7;
	int32_t ___mixed_8;
};

// System.ArgumentException
struct  ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_paramName_17), (void*)value);
	}
};


// System.AsyncCallback
struct  AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4  : public MulticastDelegate_t
{
public:

public:
};


// System.InvalidOperationException
struct  InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};


// System.Resources.MissingManifestResourceException
struct  MissingManifestResourceException_tD8397DA5F4CC63B78F91916522A302782CAB0261  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};


// System.Xml.SecureStringHasher_HashCodeOfStringDelegate
struct  HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E  : public MulticastDelegate_t
{
public:

public:
};


// System.Xml.XmlDOMTextWriter
struct  XmlDOMTextWriter_t63DEA09C11BB4D12F03C65BC12618E45C4C3E436  : public XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6
{
public:

public:
};


// System.Xml.XmlException
struct  XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.Xml.XmlException::res
	String_t* ___res_17;
	// System.String[] System.Xml.XmlException::args
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___args_18;
	// System.Int32 System.Xml.XmlException::lineNumber
	int32_t ___lineNumber_19;
	// System.Int32 System.Xml.XmlException::linePosition
	int32_t ___linePosition_20;
	// System.String System.Xml.XmlException::sourceUri
	String_t* ___sourceUri_21;
	// System.String System.Xml.XmlException::message
	String_t* ___message_22;

public:
	inline static int32_t get_offset_of_res_17() { return static_cast<int32_t>(offsetof(XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D, ___res_17)); }
	inline String_t* get_res_17() const { return ___res_17; }
	inline String_t** get_address_of_res_17() { return &___res_17; }
	inline void set_res_17(String_t* value)
	{
		___res_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___res_17), (void*)value);
	}

	inline static int32_t get_offset_of_args_18() { return static_cast<int32_t>(offsetof(XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D, ___args_18)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_args_18() const { return ___args_18; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_args_18() { return &___args_18; }
	inline void set_args_18(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___args_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___args_18), (void*)value);
	}

	inline static int32_t get_offset_of_lineNumber_19() { return static_cast<int32_t>(offsetof(XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D, ___lineNumber_19)); }
	inline int32_t get_lineNumber_19() const { return ___lineNumber_19; }
	inline int32_t* get_address_of_lineNumber_19() { return &___lineNumber_19; }
	inline void set_lineNumber_19(int32_t value)
	{
		___lineNumber_19 = value;
	}

	inline static int32_t get_offset_of_linePosition_20() { return static_cast<int32_t>(offsetof(XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D, ___linePosition_20)); }
	inline int32_t get_linePosition_20() const { return ___linePosition_20; }
	inline int32_t* get_address_of_linePosition_20() { return &___linePosition_20; }
	inline void set_linePosition_20(int32_t value)
	{
		___linePosition_20 = value;
	}

	inline static int32_t get_offset_of_sourceUri_21() { return static_cast<int32_t>(offsetof(XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D, ___sourceUri_21)); }
	inline String_t* get_sourceUri_21() const { return ___sourceUri_21; }
	inline String_t** get_address_of_sourceUri_21() { return &___sourceUri_21; }
	inline void set_sourceUri_21(String_t* value)
	{
		___sourceUri_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sourceUri_21), (void*)value);
	}

	inline static int32_t get_offset_of_message_22() { return static_cast<int32_t>(offsetof(XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D, ___message_22)); }
	inline String_t* get_message_22() const { return ___message_22; }
	inline String_t** get_address_of_message_22() { return &___message_22; }
	inline void set_message_22(String_t* value)
	{
		___message_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___message_22), (void*)value);
	}
};


// System.ArgumentNullException
struct  ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD  : public ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1
{
public:

public:
};


// System.ArgumentOutOfRangeException
struct  ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA  : public ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1
{
public:
	// System.Object System.ArgumentOutOfRangeException::m_actualValue
	RuntimeObject * ___m_actualValue_19;

public:
	inline static int32_t get_offset_of_m_actualValue_19() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA, ___m_actualValue_19)); }
	inline RuntimeObject * get_m_actualValue_19() const { return ___m_actualValue_19; }
	inline RuntimeObject ** get_address_of_m_actualValue_19() { return &___m_actualValue_19; }
	inline void set_m_actualValue_19(RuntimeObject * value)
	{
		___m_actualValue_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_actualValue_19), (void*)value);
	}
};

struct ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_StaticFields
{
public:
	// System.String modreq(System.Runtime.CompilerServices.IsVolatile) System.ArgumentOutOfRangeException::_rangeMessage
	String_t* ____rangeMessage_18;

public:
	inline static int32_t get_offset_of__rangeMessage_18() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_StaticFields, ____rangeMessage_18)); }
	inline String_t* get__rangeMessage_18() const { return ____rangeMessage_18; }
	inline String_t** get_address_of__rangeMessage_18() { return &____rangeMessage_18; }
	inline void set__rangeMessage_18(String_t* value)
	{
		____rangeMessage_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rangeMessage_18), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Xml.XmlTextWriter_Namespace[]
struct NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B  m_Items[1];

public:
	inline Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___prefix_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___ns_1), (void*)NULL);
		#endif
	}
	inline Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___prefix_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___ns_1), (void*)NULL);
		#endif
	}
};
// System.Xml.XmlTextWriter_TagInfo[]
struct TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5  m_Items[1];

public:
	inline TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___name_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___prefix_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___defaultNs_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___xmlLang_5), (void*)NULL);
		#endif
	}
	inline TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___name_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___prefix_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___defaultNs_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___xmlLang_5), (void*)NULL);
		#endif
	}
};
// System.Xml.XmlTextWriter_State[]
struct StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_mDD1137A3D7E0D3991BCFA8C2BF359B1DD536EE70_gshared (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * __this, RuntimeObject* ___comparer0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::TryGetValue(!0,!1&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_mE7571EF8ACAF5941AF78909A00CD9CE5FB07C69C_gshared (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * __this, RuntimeObject * ___key0, int32_t* ___value1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::set_Item(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_set_Item_mC87D8EECD8406043786CC95870458389CEF82CDF_gshared (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Remove(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_Remove_m53A3271E51DB23EFC5BD1A0A2E26154B4C41AB1E_gshared (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * __this, RuntimeObject * ___key0, const RuntimeMethod* method);

// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_InvariantCulture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * CultureInfo_get_InvariantCulture_mF13B47F8A763CE6A9C8A8BB2EED33FF8F7A63A72 (const RuntimeMethod* method);
// System.String SR::GetString(System.Globalization.CultureInfo,System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SR_GetString_m7D86468A7BD047EB88A26F75A073C46A51B827E3 (CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___culture0, String_t* ___name1, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args2, const RuntimeMethod* method);
// System.String System.String::Format(System.IFormatProvider,System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mF68EE0DEC1AA5ADE9DFEF9AE0508E428FBB10EFD (RuntimeObject* ___provider0, String_t* ___format1, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args2, const RuntimeMethod* method);
// System.Int32 System.Convert::ToBase64CharArray(System.Byte[],System.Int32,System.Int32,System.Char[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Convert_ToBase64CharArray_m22C304194795A2FB2FE5A5C90D5A1BC5C2D052F8 (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___inArray0, int32_t ___offsetIn1, int32_t ___length2, CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___outArray3, int32_t ___offsetOut4, const RuntimeMethod* method);
// System.String SR::GetString(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SR_GetString_m4356FFC0B1BA9CB3F26E73B41B74BC39FE4E8A96 (String_t* ___name0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Int32 System.Environment::get_TickCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Environment_get_TickCount_m0A119BE4354EA90C82CC48E559588C987A79FE0C (const RuntimeMethod* method);
// System.Boolean System.String::Equals(System.String,System.String,System.StringComparison)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_Equals_m1A3302D7214F75FB06302101934BF3EE9282AA43 (String_t* ___a0, String_t* ___b1, int32_t ___comparisonType2, const RuntimeMethod* method);
// System.Xml.SecureStringHasher/HashCodeOfStringDelegate System.Xml.SecureStringHasher::GetHashCodeDelegate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E * SecureStringHasher_GetHashCodeDelegate_m31822E4C8B12FCFD4E39A65D04BF5606A35EE022 (const RuntimeMethod* method);
// System.Int32 System.String::get_Length()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018_inline (String_t* __this, const RuntimeMethod* method);
// System.Int32 System.Xml.SecureStringHasher/HashCodeOfStringDelegate::Invoke(System.String,System.Int32,System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t HashCodeOfStringDelegate_Invoke_m39DA6A0DEEEEE55ABD7723E993744834314930A2 (HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E * __this, String_t* ___s0, int32_t ___sLen1, int64_t ___additionalEntropy2, const RuntimeMethod* method);
// System.Char System.String::get_Chars(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96 (String_t* __this, int32_t ___index0, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6 (RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ___handle0, const RuntimeMethod* method);
// System.Reflection.MethodInfo System.Type::GetMethod(System.String,System.Reflection.BindingFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MethodInfo_t * Type_GetMethod_m9EC42D4B1F765B882F516EE6D7970D51CF5D80DD (Type_t * __this, String_t* ___name0, int32_t ___bindingAttr1, const RuntimeMethod* method);
// System.Boolean System.Reflection.MethodInfo::op_Inequality(System.Reflection.MethodInfo,System.Reflection.MethodInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MethodInfo_op_Inequality_m76AC38C8B8FB8F28C21E6F9A3F0268FF8E4CC237 (MethodInfo_t * ___left0, MethodInfo_t * ___right1, const RuntimeMethod* method);
// System.Delegate System.Delegate::CreateDelegate(System.Type,System.Reflection.MethodInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_CreateDelegate_mD7C5EDDB32C63A9BD9DE43AC879AFF4EBC6641D1 (Type_t * ___type0, MethodInfo_t * ___method1, const RuntimeMethod* method);
// System.Void System.Xml.SecureStringHasher/HashCodeOfStringDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HashCodeOfStringDelegate__ctor_m5DB801F642659F1D15EE9E458BCFAE393325DAC8 (HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Object System.Xml.XmlCharType::get_StaticLock()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * XmlCharType_get_StaticLock_mE0FF2E8B12DC2AFFAFAB5718FDD49FAE726A6513 (const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Enter(System.Object,System.Boolean&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Enter_mC5B353DD83A0B0155DF6FBCC4DF5A580C25534C5 (RuntimeObject * ___obj0, bool* ___lockTaken1, const RuntimeMethod* method);
// System.Void System.Xml.XmlCharType::SetProperties(System.String,System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlCharType_SetProperties_m892CAB57FF4A04EB120C6AA8D4ACA9645ED0A72C (String_t* ___ranges0, uint8_t ___value1, const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Exit(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2 (RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Void System.Xml.XmlCharType::.ctor(System.Byte[])
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void XmlCharType__ctor_m0B65BC6BD912979FA16676884EC3120565FD6DCD_inline (XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___charProperties0, const RuntimeMethod* method);
// System.Void System.Xml.XmlCharType::InitInstance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlCharType_InitInstance_m48449C6A7516A943668D92903B5D4203DD184AC6 (const RuntimeMethod* method);
// System.Boolean System.Xml.XmlCharType::InRange(System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool XmlCharType_InRange_m33777634EBF78BF6177E79CA362BA6A8138EB3B5 (int32_t ___value0, int32_t ___start1, int32_t ___end2, const RuntimeMethod* method);
// System.Boolean System.Xml.XmlChildEnumerator::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool XmlChildEnumerator_MoveNext_mCF3EB51E714C7F2E5B627F515BF880F9BF1878CE (XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA * __this, const RuntimeMethod* method);
// System.Xml.XmlNode System.Xml.XmlChildEnumerator::get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * XmlChildEnumerator_get_Current_m8ED001FF27613B8D7040CBA88CD1BF9DE3AC9872 (XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA * __this, const RuntimeMethod* method);
// System.String System.Xml.Res::GetString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Res_GetString_m47B81D62E4B5E4C48C06BCC7995B9ED5218EE7A2 (String_t* ___name0, const RuntimeMethod* method);
// System.Void System.InvalidOperationException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvalidOperationException__ctor_m72027D5F1D513C25C05137E203EEED8FD8297706 (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.String System.String::Trim(System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Trim_m788DE5AEFDAC40E778745C4DF4AFD45A4BC1007E (String_t* __this, CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___trimChars0, const RuntimeMethod* method);
// System.String System.Xml.Res::GetString(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Res_GetString_mBDB7AFD1EB8C2C577012518DC77B8646A3045E78 (String_t* ___name0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method);
// System.Void System.ArgumentException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m9A85EF7FEFEC21DDD525A67E831D77278E5165B7 (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void System.Xml.XmlException::.ctor(System.String,System.String,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_m2CA134DF0552F0FDBBDC49D5E823831CE5A0B325 (XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D * __this, String_t* ___res0, String_t* ___arg1, int32_t ___lineNumber2, int32_t ___linePosition3, const RuntimeMethod* method);
// System.Void System.Xml.XmlException::.ctor(System.String,System.String[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_mEAC3FCF0B0088BA1343DA1293874C92ECE9217EB (XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D * __this, String_t* ___res0, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___args1, int32_t ___lineNumber2, int32_t ___linePosition3, const RuntimeMethod* method);
// System.Exception System.Xml.XmlConvert::CreateInvalidSurrogatePairException(System.Char,System.Char,System.Xml.ExceptionType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidSurrogatePairException_mC493A88B7ED7BFEF845DD2ABA4824D395107F812 (Il2CppChar ___low0, Il2CppChar ___hi1, int32_t ___exceptionType2, const RuntimeMethod* method);
// System.Exception System.Xml.XmlConvert::CreateInvalidSurrogatePairException(System.Char,System.Char,System.Xml.ExceptionType,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidSurrogatePairException_mD7779092577F68BAD64FA0A0549CF1916E6B9914 (Il2CppChar ___low0, Il2CppChar ___hi1, int32_t ___exceptionType2, int32_t ___lineNo3, int32_t ___linePos4, const RuntimeMethod* method);
// System.String System.UInt32::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UInt32_ToString_m57BE7A0F4A653986FEAC4794CD13B04CE012F4EE (uint32_t* __this, String_t* ___format0, RuntimeObject* ___provider1, const RuntimeMethod* method);
// System.Exception System.Xml.XmlConvert::CreateException(System.String,System.String[],System.Xml.ExceptionType,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateException_m956710818209FE5D8F7999590B30376FEFBC2F90 (String_t* ___res0, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___args1, int32_t ___exceptionType2, int32_t ___lineNo3, int32_t ___linePos4, const RuntimeMethod* method);
// System.Exception System.Xml.XmlConvert::CreateInvalidHighSurrogateCharException(System.Char,System.Xml.ExceptionType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidHighSurrogateCharException_m22453CD571E079C62CC668E732C74CC1EFE47CFD (Il2CppChar ___hi0, int32_t ___exceptionType1, const RuntimeMethod* method);
// System.Exception System.Xml.XmlConvert::CreateInvalidHighSurrogateCharException(System.Char,System.Xml.ExceptionType,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidHighSurrogateCharException_mAC251160BC8A0DB0389838A689D95A3C898B270B (Il2CppChar ___hi0, int32_t ___exceptionType1, int32_t ___lineNo2, int32_t ___linePos3, const RuntimeMethod* method);
// System.Exception System.Xml.XmlConvert::CreateException(System.String,System.String,System.Xml.ExceptionType,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateException_m94C72E2D80667A0F1A8AB7B7DBC916CA6C527002 (String_t* ___res0, String_t* ___arg1, int32_t ___exceptionType2, int32_t ___lineNo3, int32_t ___linePos4, const RuntimeMethod* method);
// System.Xml.XmlCharType System.Xml.XmlCharType::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  XmlCharType_get_Instance_mEAAD3E43BD5AC72FA94C12096B2A9C9684557210 (const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeHelpers_InitializeArray_m29F50CDFEEE0AB868200291366253DD4737BC76A (RuntimeArray * ___array0, RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  ___fldHandle1, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::.ctor(System.IO.TextWriter)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter__ctor_m6FA653B70BB118A3B8BDA6E29CFF8911CA060EE2 (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ___w0, const RuntimeMethod* method);
// System.Void System.SystemException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SystemException__ctor_mB0550111A1A8D18B697B618F811A5B20C160D949 (SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782 * __this, SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * ___info0, StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  ___context1, const RuntimeMethod* method);
// System.Object System.Runtime.Serialization.SerializationInfo::GetValue(System.String,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * SerializationInfo_GetValue_m7910CE6C68888C1F863D7A35915391FA33463ECF (SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * __this, String_t* ___name0, Type_t * ___type1, const RuntimeMethod* method);
// System.Runtime.Serialization.SerializationInfoEnumerator System.Runtime.Serialization.SerializationInfo::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SerializationInfoEnumerator_tB72162C419D705A40F34DDFEB18E6ACA347C54E5 * SerializationInfo_GetEnumerator_m9796C5CB43B69B5236D530A547A4FC24ABB0B575 (SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * __this, const RuntimeMethod* method);
// System.Runtime.Serialization.SerializationEntry System.Runtime.Serialization.SerializationInfoEnumerator::get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SerializationEntry_tA4CE7B0176B45BD820A7802C84479174F5EBE5EA  SerializationInfoEnumerator_get_Current_m8C18D86105BD6390B0FB268A1C1E8151D8AF3C33 (SerializationInfoEnumerator_tB72162C419D705A40F34DDFEB18E6ACA347C54E5 * __this, const RuntimeMethod* method);
// System.String System.Runtime.Serialization.SerializationEntry::get_Name()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* SerializationEntry_get_Name_m364D6CAEAD32EE66700B47B65E80C03D80596DC4_inline (SerializationEntry_tA4CE7B0176B45BD820A7802C84479174F5EBE5EA * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Object System.Runtime.Serialization.SerializationEntry::get_Value()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR RuntimeObject * SerializationEntry_get_Value_m6E7295904D91A38BFE2C47C662E75E8063ABC048_inline (SerializationEntry_tA4CE7B0176B45BD820A7802C84479174F5EBE5EA * __this, const RuntimeMethod* method);
// System.Boolean System.Runtime.Serialization.SerializationInfoEnumerator::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SerializationInfoEnumerator_MoveNext_m74D8DE9528E7DDD141DD45ABF4B54F832DE35701 (SerializationInfoEnumerator_tB72162C419D705A40F34DDFEB18E6ACA347C54E5 * __this, const RuntimeMethod* method);
// System.String System.Xml.XmlException::CreateMessage(System.String,System.String[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* XmlException_CreateMessage_mDD847D2C1685E48408F191251542EB3C754344D5 (String_t* ___res0, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___args1, int32_t ___lineNumber2, int32_t ___linePosition3, const RuntimeMethod* method);
// System.Void System.Exception::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception_GetObjectData_m76F759ED00FA218FFC522C32626B851FDE849AD6 (Exception_t * __this, SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * ___info0, StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  ___context1, const RuntimeMethod* method);
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializationInfo_AddValue_mC9D1E16476E24E1AFE7C59368D3BC0B35F64FBC6 (SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * __this, String_t* ___name0, RuntimeObject * ___value1, const RuntimeMethod* method);
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializationInfo_AddValue_m7C73917D9DC4B8FE4AFEF4BA8EBEDAB046A8D0BD (SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * __this, String_t* ___name0, int32_t ___value1, const RuntimeMethod* method);
// System.Void System.Xml.XmlException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_m3C6F865CCA07051F36F465F1BBE24251906765A9 (XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void System.Xml.XmlException::.ctor(System.String,System.Exception,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_m9600B3E2828022680728997A9FCA2668850588E4 (XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___lineNumber2, int32_t ___linePosition3, const RuntimeMethod* method);
// System.Void System.Xml.XmlException::.ctor(System.String,System.Exception,System.Int32,System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_m2038069D4E6C83DC027DFDD97AD142B723C60C36 (XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___lineNumber2, int32_t ___linePosition3, String_t* ___sourceUri4, const RuntimeMethod* method);
// System.String System.Xml.XmlException::FormatUserMessage(System.String,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* XmlException_FormatUserMessage_mDFED8BDE616EF903472039DEE5F5B1F59E5FCCFF (String_t* ___message0, int32_t ___lineNumber1, int32_t ___linePosition2, const RuntimeMethod* method);
// System.Void System.SystemException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SystemException__ctor_mA18D2EA5642C066F35CB8C965398F9A542C33B0A (SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782 * __this, String_t* ___message0, Exception_t * ___innerException1, const RuntimeMethod* method);
// System.Void System.Exception::set_HResult(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void Exception_set_HResult_m920DF8C728D8A0EC0759685FED890C775FA08B99_inline (Exception_t * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void System.Xml.XmlException::.ctor(System.String,System.String[],System.Exception,System.Int32,System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_m4B8E63B6F3EB704FFEF6BA4AA0546B90042FC100 (XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D * __this, String_t* ___res0, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___args1, Exception_t * ___innerException2, int32_t ___lineNumber3, int32_t ___linePosition4, String_t* ___sourceUri5, const RuntimeMethod* method);
// System.String System.Int32::ToString(System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m1D0AF82BDAB5D4710527DD3FEFA6F01246D128A5 (int32_t* __this, RuntimeObject* ___provider0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mF4626905368D6558695A823466A1AF65EADB9923 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
// System.String System.Exception::get_Message()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Exception_get_Message_m4315B19A04019652708F20C1B855805157F23CFD (Exception_t * __this, const RuntimeMethod* method);
// System.Void System.Xml.XmlChildEnumerator::.ctor(System.Xml.XmlNode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlChildEnumerator__ctor_m9D966497AE59A0784E4FE7CB883226B7A332F311 (XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA * __this, XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___container0, const RuntimeMethod* method);
// System.Void System.IO.StringWriter::.ctor(System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringWriter__ctor_m4D44D4D5B0CFDEEB172C7D61171340D76432A1EE (StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 * __this, RuntimeObject* ___formatProvider0, const RuntimeMethod* method);
// System.Void System.Xml.XmlDOMTextWriter::.ctor(System.IO.TextWriter)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlDOMTextWriter__ctor_mFF0BC0EC83712D01433AE7115529DCC96B38EE11 (XmlDOMTextWriter_t63DEA09C11BB4D12F03C65BC12618E45C4C3E436 * __this, TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ___w0, const RuntimeMethod* method);
// System.Void System.Text.StringBuilder::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringBuilder__ctor_mF928376F82E8C8FF3C11842C562DB8CF28B2735E (StringBuilder_t * __this, const RuntimeMethod* method);
// System.Void System.Text.StringBuilder::set_Length(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringBuilder_set_Length_m84AF318230AE5C3D0D48F1CE7C2170F6F5C19F5B (StringBuilder_t * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Boolean System.Xml.XmlCharType::IsLowSurrogate(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool XmlCharType_IsLowSurrogate_m0CB63DE5C97F9C09E2E7C67A53BB8682D8FD07D8 (int32_t ___ch0, const RuntimeMethod* method);
// System.Boolean System.Xml.XmlCharType::IsHighSurrogate(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool XmlCharType_IsHighSurrogate_m6E9E01B1A14D2CF127B6D39D333E506F90FFF98E (int32_t ___ch0, const RuntimeMethod* method);
// System.Exception System.Xml.XmlConvert::CreateInvalidSurrogatePairException(System.Char,System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidSurrogatePairException_m7ADDD2E98288144ED30A75715AD4254E484FAC70 (Il2CppChar ___low0, Il2CppChar ___hi1, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260 (StringBuilder_t * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextEncoder::WriteStringFragment(System.String,System.Int32,System.Int32,System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteStringFragment_mBDB749E06C09EE5CDA591A6AB22893B2DDBB3BD1 (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, String_t* ___str0, int32_t ___offset1, int32_t ___count2, CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___helperBuffer3, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextEncoder::WriteCharEntityImpl(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteCharEntityImpl_mEA75C2144CCCF2DF29BB91D3B91B2C06C1E6920F (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, Il2CppChar ___ch0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextEncoder::WriteEntityRefImpl(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteEntityRefImpl_m3DA44922F283F75B20D43E043A1E6D0F8010E5E9 (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextEncoder::WriteSurrogateChar(System.Char,System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteSurrogateChar_mCCE79921A16872FB57C3C5C21F7518CCE5F504F3 (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, Il2CppChar ___lowChar0, Il2CppChar ___highChar1, const RuntimeMethod* method);
// System.Exception System.Xml.XmlConvert::CreateInvalidHighSurrogateCharException(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidHighSurrogateCharException_m4A1C627F6492AD6A98B4CA982A9197BF39C73DA8 (Il2CppChar ___hi0, const RuntimeMethod* method);
// System.Void System.ArgumentNullException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * __this, String_t* ___paramName0, const RuntimeMethod* method);
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentOutOfRangeException__ctor_m6B36E60C989DC798A8B44556DB35960282B133A6 (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * __this, String_t* ___paramName0, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_m549C532422286A982F7956C9BAE197D00B30DCA8 (StringBuilder_t * __this, CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___value0, int32_t ___startIndex1, int32_t ___charCount2, const RuntimeMethod* method);
// System.Void System.String::CopyTo(System.Int32,System.Char[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void String_CopyTo_m054B8FF2ACBBA74F60199D98259E88395EAD3661 (String_t* __this, int32_t ___sourceIndex0, CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___destination1, int32_t ___destinationIndex2, int32_t ___count3, const RuntimeMethod* method);
// System.Globalization.NumberFormatInfo System.Globalization.NumberFormatInfo::get_InvariantInfo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * NumberFormatInfo_get_InvariantInfo_m55FDCA552CC3CD15E01E10DBFDD5756AB1DCC54D (const RuntimeMethod* method);
// System.String System.Int32::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_mE527694B0C55AE14FDCBE1D9C848446C18E22C09 (int32_t* __this, String_t* ___format0, RuntimeObject* ___provider1, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextEncoder::WriteCharEntityImpl(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteCharEntityImpl_m8DB50E2042CA07D11456C0B67B6EE9D28E8A817E (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, String_t* ___strVal0, const RuntimeMethod* method);
// System.Void System.Xml.XmlWriter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlWriter__ctor_m1D2B58DC035709A720317204246AD58118C15740 (XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * __this, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter/TagInfo::Init(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TagInfo_Init_m631D10B6F7160EC8D54CAAF1418215168F8E6F32 (TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5 * __this, int32_t ___nsTop0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter__ctor_mED6FB595B6052C306BE5175DC8E20DD974A2CEFC (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextEncoder::.ctor(System.IO.TextWriter)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder__ctor_mDC642C841EF38B20B5521384E3C9026D594CE332 (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ___textWriter0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextEncoder::set_QuoteChar(System.Char)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void XmlTextEncoder_set_QuoteChar_m28C9CC005913B64E3444E1522EE8D26B4F5F0C88_inline (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, Il2CppChar ___value0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::InternalWriteEndElement(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_InternalWriteEndElement_m02FCE5CCA71DBFBA885AF81FFAD702B3BC39294F (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, bool ___longFormat0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::AutoCompleteAll()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_AutoCompleteAll_mB42EF751B442A2213ED31F11AAA25A0A39FF302D (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::Indent(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_Indent_m5CB04F1718940F0069DD7C9F8E80A1D2134C4267 (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, bool ___beforeEndElement0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::WriteEndAttributeQuote()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_WriteEndAttributeQuote_m057DC07F77F3640F8A59AF53FCC77C6528136490 (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::WriteEndStartTag(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_WriteEndStartTag_m5905B79D7EE7EE0049919B9B879979313D9664FA (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, bool ___empty0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::FlushEncoders()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_FlushEncoders_m131B51A9CECD553DB91EC663D92AFE3D0197B725 (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::AutoComplete(System.Xml.XmlTextWriter/Token)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_AutoComplete_mCFC6565788633CD667823E70E1FD8A9AF241B664 (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, int32_t ___token0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::PopNamespaces(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_PopNamespaces_m5D748E8059ED8B3F04DB600618EE9A7CA3A2F158 (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, int32_t ___indexFrom0, int32_t ___indexTo1, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextEncoder::StartAttribute(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_StartAttribute_mAFEF454A34E72F5B1457C8D634FA947EA0023D15 (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, bool ___cacheAttrValue0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextEncoder::Write(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_Write_mD02582AB249921681B2B7C85E71DC17E20AB3884 (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, String_t* ___text0, const RuntimeMethod* method);
// System.Boolean System.String::op_Inequality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Inequality_m0BD184A74F453A72376E81CC6CAEE2556B80493E (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextEncoder::EndAttribute()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_EndAttribute_m5988FFECF6A66F36D9079FD6CA67BB56A2A98D66 (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::HandleSpecialAttribute()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_HandleSpecialAttribute_m8AFEBFA51F9B23D67C213F35E0ADF91171DB0ECB (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, const RuntimeMethod* method);
// System.Int32 System.Xml.XmlTextWriter::LookupNamespace(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t XmlTextWriter_LookupNamespace_mA477CDBF25662EC77FF5B025FB2E83B400C381FD (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, String_t* ___prefix0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::AddNamespace(System.String,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_AddNamespace_m78549A0DDF0923A00664A6DE1B425157097E5ED0 (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, String_t* ___prefix0, String_t* ___ns1, bool ___declared2, const RuntimeMethod* method);
// System.Void System.Array::Copy(System.Array,System.Array,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Copy_m2D96731C600DE8A167348CA8BA796344E64F7434 (RuntimeArray * ___sourceArray0, RuntimeArray * ___destinationArray1, int32_t ___length2, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter/Namespace::Set(System.String,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Namespace_Set_mEF74FB920EF6CB43BAFDC1FFA58AA95235177979 (Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B * __this, String_t* ___prefix0, String_t* ___ns1, bool ___declared2, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::AddToNamespaceHashtable(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_AddToNamespaceHashtable_mEF08299106F8B3D846EC6D474D02508D5664D429 (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, int32_t ___namespaceIndex0, const RuntimeMethod* method);
// System.Void System.Xml.SecureStringHasher::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SecureStringHasher__ctor_mAAE07435BFBE6F3C9A4840F845DE0EB38EA73AD7 (SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<!0>)
inline void Dictionary_2__ctor_mF6BE1085F30E8C47064A8E0583DABCBE0B35DC21 (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * __this, RuntimeObject* ___comparer0, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *, RuntimeObject*, const RuntimeMethod*))Dictionary_2__ctor_mDD1137A3D7E0D3991BCFA8C2BF359B1DD536EE70_gshared)(__this, ___comparer0, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&)
inline bool Dictionary_2_TryGetValue_m3B7C9CE4CBBCB908CD81487D7924E93E9309FB67 (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * __this, String_t* ___key0, int32_t* ___value1, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *, String_t*, int32_t*, const RuntimeMethod*))Dictionary_2_TryGetValue_mE7571EF8ACAF5941AF78909A00CD9CE5FB07C69C_gshared)(__this, ___key0, ___value1, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::set_Item(!0,!1)
inline void Dictionary_2_set_Item_mDBA4C5D99B9605672D2F04AF3184C5E043D1BD3A (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * __this, String_t* ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *, String_t*, int32_t, const RuntimeMethod*))Dictionary_2_set_Item_mC87D8EECD8406043786CC95870458389CEF82CDF_gshared)(__this, ___key0, ___value1, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Remove(!0)
inline bool Dictionary_2_Remove_mF764B8FFA63013272F75AFEF5F54C423F340D771 (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *, String_t*, const RuntimeMethod*))Dictionary_2_Remove_m53A3271E51DB23EFC5BD1A0A2E26154B4C41AB1E_gshared)(__this, ___key0, method);
}
// System.String System.Xml.XmlTextEncoder::get_AttributeValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* XmlTextEncoder_get_AttributeValue_m7D58DB09133999166C984994CA05C48B469DEC7D (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, const RuntimeMethod* method);
// System.String System.Xml.XmlConvert::TrimString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* XmlConvert_TrimString_m89152D6729B89C0423168B5C60E0191A773AD1FA (String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::VerifyPrefixXml(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_VerifyPrefixXml_m44A57489D6DFFAD820FE4DF6B113DB3B58E6F8E0 (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, String_t* ___prefix0, String_t* ___ns1, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextWriter::PushNamespace(System.String,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_PushNamespace_m3DA0494C202BD5C6AE32E9C5FD7FEAC6B4ED8548 (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, String_t* ___prefix0, String_t* ___ns1, bool ___declared2, const RuntimeMethod* method);
// System.Void System.Xml.Base64Encoder::Flush()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Base64Encoder_Flush_mCC2F11678DA21435D1A00BFDA1D9FF97B9605B6D (Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922 * __this, const RuntimeMethod* method);
// System.Void System.Xml.XmlTextEncoder::WriteRaw(System.Char[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteRaw_mE2BB17DA3D685325F30E98E590CC58BBB7781628 (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___array0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String SR::GetString(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SR_GetString_m4356FFC0B1BA9CB3F26E73B41B74BC39FE4E8A96 (String_t* ___name0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SR_GetString_m4356FFC0B1BA9CB3F26E73B41B74BC39FE4E8A96_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_il2cpp_TypeInfo_var);
		CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * L_0 = CultureInfo_get_InvariantCulture_mF13B47F8A763CE6A9C8A8BB2EED33FF8F7A63A72(/*hidden argument*/NULL);
		String_t* L_1 = ___name0;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = ___args1;
		String_t* L_3 = SR_GetString_m7D86468A7BD047EB88A26F75A073C46A51B827E3(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String SR::GetString(System.Globalization.CultureInfo,System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SR_GetString_m7D86468A7BD047EB88A26F75A073C46A51B827E3 (CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___culture0, String_t* ___name1, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args2, const RuntimeMethod* method)
{
	{
		CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * L_0 = ___culture0;
		String_t* L_1 = ___name1;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = ___args2;
		String_t* L_3 = String_Format_mF68EE0DEC1AA5ADE9DFEF9AE0508E428FBB10EFD(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.Base64Encoder::Flush()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Base64Encoder_Flush_mCC2F11678DA21435D1A00BFDA1D9FF97B9605B6D (Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Base64Encoder_Flush_mCC2F11678DA21435D1A00BFDA1D9FF97B9605B6D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_leftOverBytesCount_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0038;
		}
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = __this->get_leftOverBytes_0();
		int32_t L_2 = __this->get_leftOverBytesCount_1();
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_3 = __this->get_charsLine_2();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1C7A851BFB2F0782FD7F72F6AA1DCBB7B53A9C7E_il2cpp_TypeInfo_var);
		int32_t L_4 = Convert_ToBase64CharArray_m22C304194795A2FB2FE5A5C90D5A1BC5C2D052F8(L_1, 0, L_2, L_3, 0, /*hidden argument*/NULL);
		V_0 = L_4;
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_5 = __this->get_charsLine_2();
		int32_t L_6 = V_0;
		VirtActionInvoker3< CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*, int32_t, int32_t >::Invoke(4 /* System.Void System.Xml.Base64Encoder::WriteChars(System.Char[],System.Int32,System.Int32) */, __this, L_5, 0, L_6);
		__this->set_leftOverBytesCount_1(0);
	}

IL_0038:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String System.Xml.Res::GetString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Res_GetString_m47B81D62E4B5E4C48C06BCC7995B9ED5218EE7A2 (String_t* ___name0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		return L_0;
	}
}
// System.String System.Xml.Res::GetString(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Res_GetString_mBDB7AFD1EB8C2C577012518DC77B8646A3045E78 (String_t* ___name0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = ___args1;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		String_t* L_1 = ___name0;
		return L_1;
	}

IL_0005:
	{
		String_t* L_2 = ___name0;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_3 = ___args1;
		String_t* L_4 = SR_GetString_m4356FFC0B1BA9CB3F26E73B41B74BC39FE4E8A96(L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.SecureStringHasher::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SecureStringHasher__ctor_mAAE07435BFBE6F3C9A4840F845DE0EB38EA73AD7 (SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = Environment_get_TickCount_m0A119BE4354EA90C82CC48E559588C987A79FE0C(/*hidden argument*/NULL);
		__this->set_hashCodeRandomizer_1(L_0);
		return;
	}
}
// System.Boolean System.Xml.SecureStringHasher::Equals(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SecureStringHasher_Equals_m6E11A5878C2D8D9DA983636305AADB06AC3B400F (SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3 * __this, String_t* ___x0, String_t* ___y1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___x0;
		String_t* L_1 = ___y1;
		bool L_2 = String_Equals_m1A3302D7214F75FB06302101934BF3EE9282AA43(L_0, L_1, 4, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 System.Xml.SecureStringHasher::GetHashCode(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SecureStringHasher_GetHashCode_m867EF141E418F59C37589E420CAFE018C5072849 (SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SecureStringHasher_GetHashCode_m867EF141E418F59C37589E420CAFE018C5072849_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E * L_0 = ((SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_StaticFields*)il2cpp_codegen_static_fields_for(SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_il2cpp_TypeInfo_var))->get_hashCodeDelegate_0();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E * L_1 = SecureStringHasher_GetHashCodeDelegate_m31822E4C8B12FCFD4E39A65D04BF5606A35EE022(/*hidden argument*/NULL);
		((SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_StaticFields*)il2cpp_codegen_static_fields_for(SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_il2cpp_TypeInfo_var))->set_hashCodeDelegate_0(L_1);
	}

IL_0011:
	{
		HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E * L_2 = ((SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_StaticFields*)il2cpp_codegen_static_fields_for(SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_il2cpp_TypeInfo_var))->get_hashCodeDelegate_0();
		String_t* L_3 = ___key0;
		String_t* L_4 = ___key0;
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018_inline(L_4, /*hidden argument*/NULL);
		int32_t L_6 = __this->get_hashCodeRandomizer_1();
		NullCheck(L_2);
		int32_t L_7 = HashCodeOfStringDelegate_Invoke_m39DA6A0DEEEEE55ABD7723E993744834314930A2(L_2, L_3, L_5, (((int64_t)((int64_t)L_6))), /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Int32 System.Xml.SecureStringHasher::GetHashCodeOfString(System.String,System.Int32,System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SecureStringHasher_GetHashCodeOfString_mC56D819EA0533A7F2DCDE3260D618DCFE68F88D6 (String_t* ___key0, int32_t ___sLen1, int64_t ___additionalEntropy2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int64_t L_0 = ___additionalEntropy2;
		V_0 = (((int32_t)((int32_t)L_0)));
		V_1 = 0;
		goto IL_0019;
	}

IL_0007:
	{
		int32_t L_1 = V_0;
		int32_t L_2 = V_0;
		String_t* L_3 = ___key0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		Il2CppChar L_5 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_3, L_4, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2<<(int32_t)7))^(int32_t)L_5))));
		int32_t L_6 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_0019:
	{
		int32_t L_7 = V_1;
		String_t* L_8 = ___key0;
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018_inline(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_7) < ((int32_t)L_9)))
		{
			goto IL_0007;
		}
	}
	{
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_10, (int32_t)((int32_t)((int32_t)L_11>>(int32_t)((int32_t)17)))));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)11)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_14, (int32_t)((int32_t)((int32_t)L_15>>(int32_t)5))));
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.Xml.SecureStringHasher_HashCodeOfStringDelegate System.Xml.SecureStringHasher::GetHashCodeDelegate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E * SecureStringHasher_GetHashCodeDelegate_m31822E4C8B12FCFD4E39A65D04BF5606A35EE022 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SecureStringHasher_GetHashCodeDelegate_m31822E4C8B12FCFD4E39A65D04BF5606A35EE022_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_0 = { reinterpret_cast<intptr_t> (String_t_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		MethodInfo_t * L_2 = Type_GetMethod_m9EC42D4B1F765B882F516EE6D7970D51CF5D80DD(L_1, _stringLiteral2547BF06A9A1BA77AA1004FACBFABAE4834F8A18, ((int32_t)40), /*hidden argument*/NULL);
		V_0 = L_2;
		MethodInfo_t * L_3 = V_0;
		bool L_4 = MethodInfo_op_Inequality_m76AC38C8B8FB8F28C21E6F9A3F0268FF8E4CC237(L_3, (MethodInfo_t *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_5 = { reinterpret_cast<intptr_t> (HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_5, /*hidden argument*/NULL);
		MethodInfo_t * L_7 = V_0;
		Delegate_t * L_8 = Delegate_CreateDelegate_mD7C5EDDB32C63A9BD9DE43AC879AFF4EBC6641D1(L_6, L_7, /*hidden argument*/NULL);
		return ((HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E *)CastclassSealed((RuntimeObject*)L_8, HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E_il2cpp_TypeInfo_var));
	}

IL_0036:
	{
		HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E * L_9 = (HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E *)il2cpp_codegen_object_new(HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E_il2cpp_TypeInfo_var);
		HashCodeOfStringDelegate__ctor_m5DB801F642659F1D15EE9E458BCFAE393325DAC8(L_9, NULL, (intptr_t)((intptr_t)SecureStringHasher_GetHashCodeOfString_mC56D819EA0533A7F2DCDE3260D618DCFE68F88D6_RuntimeMethod_var), /*hidden argument*/NULL);
		return L_9;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  int32_t DelegatePInvokeWrapper_HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E (HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E * __this, String_t* ___s0, int32_t ___sLen1, int64_t ___additionalEntropy2, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc)(char*, int32_t, int64_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___s0' to native representation
	char* ____s0_marshaled = NULL;
	____s0_marshaled = il2cpp_codegen_marshal_string(___s0);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(____s0_marshaled, ___sLen1, ___additionalEntropy2);

	// Marshaling cleanup of parameter '___s0' native representation
	il2cpp_codegen_marshal_free(____s0_marshaled);
	____s0_marshaled = NULL;

	return returnValue;
}
// System.Void System.Xml.SecureStringHasher_HashCodeOfStringDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HashCodeOfStringDelegate__ctor_m5DB801F642659F1D15EE9E458BCFAE393325DAC8 (HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 System.Xml.SecureStringHasher_HashCodeOfStringDelegate::Invoke(System.String,System.Int32,System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t HashCodeOfStringDelegate_Invoke_m39DA6A0DEEEEE55ABD7723E993744834314930A2 (HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E * __this, String_t* ___s0, int32_t ___sLen1, int64_t ___additionalEntropy2, const RuntimeMethod* method)
{
	int32_t result = 0;
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 3)
			{
				// open
				typedef int32_t (*FunctionPointerType) (String_t*, int32_t, int64_t, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___s0, ___sLen1, ___additionalEntropy2, targetMethod);
			}
			else
			{
				// closed
				typedef int32_t (*FunctionPointerType) (void*, String_t*, int32_t, int64_t, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___s0, ___sLen1, ___additionalEntropy2, targetMethod);
			}
		}
		else if (___parameterCount != 3)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker2< int32_t, int32_t, int64_t >::Invoke(targetMethod, ___s0, ___sLen1, ___additionalEntropy2);
					else
						result = GenericVirtFuncInvoker2< int32_t, int32_t, int64_t >::Invoke(targetMethod, ___s0, ___sLen1, ___additionalEntropy2);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker2< int32_t, int32_t, int64_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___s0, ___sLen1, ___additionalEntropy2);
					else
						result = VirtFuncInvoker2< int32_t, int32_t, int64_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___s0, ___sLen1, ___additionalEntropy2);
				}
			}
			else
			{
				typedef int32_t (*FunctionPointerType) (String_t*, int32_t, int64_t, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___s0, ___sLen1, ___additionalEntropy2, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef int32_t (*FunctionPointerType) (String_t*, int32_t, int64_t, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___s0, ___sLen1, ___additionalEntropy2, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker3< int32_t, String_t*, int32_t, int64_t >::Invoke(targetMethod, targetThis, ___s0, ___sLen1, ___additionalEntropy2);
					else
						result = GenericVirtFuncInvoker3< int32_t, String_t*, int32_t, int64_t >::Invoke(targetMethod, targetThis, ___s0, ___sLen1, ___additionalEntropy2);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker3< int32_t, String_t*, int32_t, int64_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___s0, ___sLen1, ___additionalEntropy2);
					else
						result = VirtFuncInvoker3< int32_t, String_t*, int32_t, int64_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___s0, ___sLen1, ___additionalEntropy2);
				}
			}
			else
			{
				typedef int32_t (*FunctionPointerType) (void*, String_t*, int32_t, int64_t, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___s0, ___sLen1, ___additionalEntropy2, targetMethod);
			}
		}
	}
	return result;
}
// System.IAsyncResult System.Xml.SecureStringHasher_HashCodeOfStringDelegate::BeginInvoke(System.String,System.Int32,System.Int64,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* HashCodeOfStringDelegate_BeginInvoke_m86BB834B9D3C76936DA4190EB5523FF3C5C757ED (HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E * __this, String_t* ___s0, int32_t ___sLen1, int64_t ___additionalEntropy2, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback3, RuntimeObject * ___object4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HashCodeOfStringDelegate_BeginInvoke_m86BB834B9D3C76936DA4190EB5523FF3C5C757ED_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___s0;
	__d_args[1] = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &___sLen1);
	__d_args[2] = Box(Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436_il2cpp_TypeInfo_var, &___additionalEntropy2);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback3, (RuntimeObject*)___object4);
}
// System.Int32 System.Xml.SecureStringHasher_HashCodeOfStringDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t HashCodeOfStringDelegate_EndInvoke_m8539B72C9FC5BE2EE2ACCC2F3C4379314840F5CB (HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((RuntimeObject*)__result);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: System.Xml.XmlCharType
IL2CPP_EXTERN_C void XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_marshal_pinvoke(const XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9& unmarshaled, XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_marshaled_pinvoke& marshaled)
{
	marshaled.___charProperties_2 = il2cpp_codegen_com_marshal_safe_array(IL2CPP_VT_I1, unmarshaled.get_charProperties_2());
}
IL2CPP_EXTERN_C void XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_marshal_pinvoke_back(const XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_marshaled_pinvoke& marshaled, XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_charProperties_2((ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07_il2cpp_TypeInfo_var, marshaled.___charProperties_2));
}
// Conversion method for clean up from marshalling of: System.Xml.XmlCharType
IL2CPP_EXTERN_C void XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_marshal_pinvoke_cleanup(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_com_destroy_safe_array(marshaled.___charProperties_2);
	marshaled.___charProperties_2 = NULL;
}
// Conversion methods for marshalling of: System.Xml.XmlCharType
IL2CPP_EXTERN_C void XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_marshal_com(const XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9& unmarshaled, XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_marshaled_com& marshaled)
{
	marshaled.___charProperties_2 = il2cpp_codegen_com_marshal_safe_array(IL2CPP_VT_I1, unmarshaled.get_charProperties_2());
}
IL2CPP_EXTERN_C void XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_marshal_com_back(const XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_marshaled_com& marshaled, XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_charProperties_2((ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07_il2cpp_TypeInfo_var, marshaled.___charProperties_2));
}
// Conversion method for clean up from marshalling of: System.Xml.XmlCharType
IL2CPP_EXTERN_C void XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_marshal_com_cleanup(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_marshaled_com& marshaled)
{
	il2cpp_codegen_com_destroy_safe_array(marshaled.___charProperties_2);
	marshaled.___charProperties_2 = NULL;
}
// System.Object System.Xml.XmlCharType::get_StaticLock()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * XmlCharType_get_StaticLock_mE0FF2E8B12DC2AFFAFAB5718FDD49FAE726A6513 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlCharType_get_StaticLock_mE0FF2E8B12DC2AFFAFAB5718FDD49FAE726A6513_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = ((XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_il2cpp_TypeInfo_var))->get_s_Lock_0();
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		RuntimeObject * L_1 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(L_1, /*hidden argument*/NULL);
		V_0 = L_1;
		RuntimeObject * L_2 = V_0;
		InterlockedCompareExchangeImpl<RuntimeObject *>((RuntimeObject **)(((XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_il2cpp_TypeInfo_var))->get_address_of_s_Lock_0()), L_2, NULL);
	}

IL_001a:
	{
		RuntimeObject * L_3 = ((XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_il2cpp_TypeInfo_var))->get_s_Lock_0();
		return L_3;
	}
}
// System.Void System.Xml.XmlCharType::InitInstance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlCharType_InitInstance_m48449C6A7516A943668D92903B5D4203DD184AC6 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlCharType_InitInstance_m48449C6A7516A943668D92903B5D4203DD184AC6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		RuntimeObject * L_0 = XmlCharType_get_StaticLock_mE0FF2E8B12DC2AFFAFAB5718FDD49FAE726A6513(/*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = (bool)0;
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_1 = V_0;
			Monitor_Enter_mC5B353DD83A0B0155DF6FBCC4DF5A580C25534C5(L_1, (bool*)(&V_1), /*hidden argument*/NULL);
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = ((XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_il2cpp_TypeInfo_var))->get_s_CharProperties_1();
			il2cpp_codegen_memory_barrier();
			if (!L_2)
			{
				goto IL_001b;
			}
		}

IL_0019:
		{
			IL2CPP_LEAVE(0x97, FINALLY_008d);
		}

IL_001b:
		{
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)65536));
			il2cpp_codegen_memory_barrier();
			((XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_il2cpp_TypeInfo_var))->set_s_CharProperties_1(L_3);
			XmlCharType_SetProperties_m892CAB57FF4A04EB120C6AA8D4ACA9645ED0A72C(_stringLiteral77B3FBA04FD4C4EEEE9AB3A7FB216ADBB7982995, (uint8_t)1, /*hidden argument*/NULL);
			XmlCharType_SetProperties_m892CAB57FF4A04EB120C6AA8D4ACA9645ED0A72C(_stringLiteral2D805000F0B1B718831F4576ED6B82409607A53D, (uint8_t)2, /*hidden argument*/NULL);
			XmlCharType_SetProperties_m892CAB57FF4A04EB120C6AA8D4ACA9645ED0A72C(_stringLiteral7ABE4DB8E4E34EA3395A2A251F5922B296325989, (uint8_t)4, /*hidden argument*/NULL);
			XmlCharType_SetProperties_m892CAB57FF4A04EB120C6AA8D4ACA9645ED0A72C(_stringLiteral03604A939E36BFD5AAB4D02EC49C56DD5FFE70A4, (uint8_t)8, /*hidden argument*/NULL);
			XmlCharType_SetProperties_m892CAB57FF4A04EB120C6AA8D4ACA9645ED0A72C(_stringLiteralE9580394535B0E66F03CAFDAD8F8AEF279BB1C12, (uint8_t)((int32_t)16), /*hidden argument*/NULL);
			XmlCharType_SetProperties_m892CAB57FF4A04EB120C6AA8D4ACA9645ED0A72C(_stringLiteral03604A939E36BFD5AAB4D02EC49C56DD5FFE70A4, (uint8_t)((int32_t)32), /*hidden argument*/NULL);
			XmlCharType_SetProperties_m892CAB57FF4A04EB120C6AA8D4ACA9645ED0A72C(_stringLiteral1AE76814588A8852BEC1A4F868B17D08026CF135, (uint8_t)((int32_t)64), /*hidden argument*/NULL);
			XmlCharType_SetProperties_m892CAB57FF4A04EB120C6AA8D4ACA9645ED0A72C(_stringLiteral30510FE0BC6A8F6A62E9D5309EF4010DB2D81A82, (uint8_t)((int32_t)128), /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x97, FINALLY_008d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_008d;
	}

FINALLY_008d:
	{ // begin finally (depth: 1)
		{
			bool L_4 = V_1;
			if (!L_4)
			{
				goto IL_0096;
			}
		}

IL_0090:
		{
			RuntimeObject * L_5 = V_0;
			Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_5, /*hidden argument*/NULL);
		}

IL_0096:
		{
			IL2CPP_END_FINALLY(141)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(141)
	{
		IL2CPP_JUMP_TBL(0x97, IL_0097)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0097:
	{
		return;
	}
}
// System.Void System.Xml.XmlCharType::SetProperties(System.String,System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlCharType_SetProperties_m892CAB57FF4A04EB120C6AA8D4ACA9645ED0A72C (String_t* ___ranges0, uint8_t ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlCharType_SetProperties_m892CAB57FF4A04EB120C6AA8D4ACA9645ED0A72C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		goto IL_0037;
	}

IL_0004:
	{
		String_t* L_0 = ___ranges0;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Il2CppChar L_2 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_0, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		String_t* L_3 = ___ranges0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Il2CppChar L_5 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_3, ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1)), /*hidden argument*/NULL);
		V_2 = L_5;
		goto IL_002f;
	}

IL_0018:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = ((XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_il2cpp_TypeInfo_var))->get_s_CharProperties_1();
		il2cpp_codegen_memory_barrier();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		uint8_t* L_8 = ((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_7)));
		int32_t L_9 = *((uint8_t*)L_8);
		uint8_t L_10 = ___value1;
		*((int8_t*)L_8) = (int8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_9|(int32_t)L_10)))));
		int32_t L_11 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_002f:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_2;
		if ((((int32_t)L_12) <= ((int32_t)L_13)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_14 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)2));
	}

IL_0037:
	{
		int32_t L_15 = V_0;
		String_t* L_16 = ___ranges0;
		NullCheck(L_16);
		int32_t L_17 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018_inline(L_16, /*hidden argument*/NULL);
		if ((((int32_t)L_15) < ((int32_t)L_17)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void System.Xml.XmlCharType::.ctor(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlCharType__ctor_m0B65BC6BD912979FA16676884EC3120565FD6DCD (XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___charProperties0, const RuntimeMethod* method)
{
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = ___charProperties0;
		__this->set_charProperties_2(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void XmlCharType__ctor_m0B65BC6BD912979FA16676884EC3120565FD6DCD_AdjustorThunk (RuntimeObject * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___charProperties0, const RuntimeMethod* method)
{
	XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * _thisAdjusted = reinterpret_cast<XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 *>(__this + 1);
	XmlCharType__ctor_m0B65BC6BD912979FA16676884EC3120565FD6DCD_inline(_thisAdjusted, ___charProperties0, method);
}
// System.Xml.XmlCharType System.Xml.XmlCharType::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  XmlCharType_get_Instance_mEAAD3E43BD5AC72FA94C12096B2A9C9684557210 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlCharType_get_Instance_mEAAD3E43BD5AC72FA94C12096B2A9C9684557210_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = ((XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_il2cpp_TypeInfo_var))->get_s_CharProperties_1();
		il2cpp_codegen_memory_barrier();
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		XmlCharType_InitInstance_m48449C6A7516A943668D92903B5D4203DD184AC6(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = ((XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields*)il2cpp_codegen_static_fields_for(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_il2cpp_TypeInfo_var))->get_s_CharProperties_1();
		il2cpp_codegen_memory_barrier();
		XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  L_2;
		memset((&L_2), 0, sizeof(L_2));
		XmlCharType__ctor_m0B65BC6BD912979FA16676884EC3120565FD6DCD_inline((&L_2), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean System.Xml.XmlCharType::IsHighSurrogate(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool XmlCharType_IsHighSurrogate_m6E9E01B1A14D2CF127B6D39D333E506F90FFF98E (int32_t ___ch0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___ch0;
		bool L_1 = XmlCharType_InRange_m33777634EBF78BF6177E79CA362BA6A8138EB3B5(L_0, ((int32_t)55296), ((int32_t)56319), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Xml.XmlCharType::IsLowSurrogate(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool XmlCharType_IsLowSurrogate_m0CB63DE5C97F9C09E2E7C67A53BB8682D8FD07D8 (int32_t ___ch0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___ch0;
		bool L_1 = XmlCharType_InRange_m33777634EBF78BF6177E79CA362BA6A8138EB3B5(L_0, ((int32_t)56320), ((int32_t)57343), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Xml.XmlCharType::InRange(System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool XmlCharType_InRange_m33777634EBF78BF6177E79CA362BA6A8138EB3B5 (int32_t ___value0, int32_t ___start1, int32_t ___end2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = ___start1;
		int32_t L_2 = ___end2;
		int32_t L_3 = ___start1;
		return (bool)((((int32_t)((!(((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)L_1))) <= ((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)L_3)))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.XmlChildEnumerator::.ctor(System.Xml.XmlNode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlChildEnumerator__ctor_m9D966497AE59A0784E4FE7CB883226B7A332F311 (XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA * __this, XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___container0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_0 = ___container0;
		__this->set_container_0(L_0);
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_1 = ___container0;
		NullCheck(L_1);
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_2 = VirtFuncInvoker0< XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * >::Invoke(8 /* System.Xml.XmlNode System.Xml.XmlNode::get_FirstChild() */, L_1);
		__this->set_child_1(L_2);
		__this->set_isFirst_2((bool)1);
		return;
	}
}
// System.Boolean System.Xml.XmlChildEnumerator::System.Collections.IEnumerator.MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool XmlChildEnumerator_System_Collections_IEnumerator_MoveNext_m42A648E7F0B79C7F47D7EF9CE5095D54EA372EDB (XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = XmlChildEnumerator_MoveNext_mCF3EB51E714C7F2E5B627F515BF880F9BF1878CE(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XmlChildEnumerator::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool XmlChildEnumerator_MoveNext_mCF3EB51E714C7F2E5B627F515BF880F9BF1878CE (XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_isFirst_2();
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_1 = __this->get_container_0();
		NullCheck(L_1);
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_2 = VirtFuncInvoker0< XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * >::Invoke(8 /* System.Xml.XmlNode System.Xml.XmlNode::get_FirstChild() */, L_1);
		__this->set_child_1(L_2);
		__this->set_isFirst_2((bool)0);
		goto IL_003b;
	}

IL_0022:
	{
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_3 = __this->get_child_1();
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_4 = __this->get_child_1();
		NullCheck(L_4);
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_5 = VirtFuncInvoker0< XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * >::Invoke(7 /* System.Xml.XmlNode System.Xml.XmlNode::get_NextSibling() */, L_4);
		__this->set_child_1(L_5);
	}

IL_003b:
	{
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_6 = __this->get_child_1();
		return (bool)((!(((RuntimeObject*)(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB *)L_6) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}
}
// System.Void System.Xml.XmlChildEnumerator::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlChildEnumerator_System_Collections_IEnumerator_Reset_m3EF3533A19F3926CDCCD9CD82498D01B5F7EA08A (XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA * __this, const RuntimeMethod* method)
{
	{
		__this->set_isFirst_2((bool)1);
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_0 = __this->get_container_0();
		NullCheck(L_0);
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_1 = VirtFuncInvoker0< XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * >::Invoke(8 /* System.Xml.XmlNode System.Xml.XmlNode::get_FirstChild() */, L_0);
		__this->set_child_1(L_1);
		return;
	}
}
// System.Object System.Xml.XmlChildEnumerator::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * XmlChildEnumerator_System_Collections_IEnumerator_get_Current_m2E52170DBEF6245A93126D3C7FF60ED17DDE2BD1 (XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA * __this, const RuntimeMethod* method)
{
	{
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_0 = XmlChildEnumerator_get_Current_m8ED001FF27613B8D7040CBA88CD1BF9DE3AC9872(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Xml.XmlNode System.Xml.XmlChildEnumerator::get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * XmlChildEnumerator_get_Current_m8ED001FF27613B8D7040CBA88CD1BF9DE3AC9872 (XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlChildEnumerator_get_Current_m8ED001FF27613B8D7040CBA88CD1BF9DE3AC9872_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_isFirst_2();
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_1 = __this->get_child_1();
		if (L_1)
		{
			goto IL_0020;
		}
	}

IL_0010:
	{
		String_t* L_2 = Res_GetString_m47B81D62E4B5E4C48C06BCC7995B9ED5218EE7A2(_stringLiteral29A6E802123FF6EA94EC6F96DDA470B3FA755A58, /*hidden argument*/NULL);
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_3 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m72027D5F1D513C25C05137E203EEED8FD8297706(L_3, L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, NULL, XmlChildEnumerator_get_Current_m8ED001FF27613B8D7040CBA88CD1BF9DE3AC9872_RuntimeMethod_var);
	}

IL_0020:
	{
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_4 = __this->get_child_1();
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String System.Xml.XmlConvert::TrimString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* XmlConvert_TrimString_m89152D6729B89C0423168B5C60E0191A773AD1FA (String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert_TrimString_m89152D6729B89C0423168B5C60E0191A773AD1FA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_il2cpp_TypeInfo_var);
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_1 = ((XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_StaticFields*)il2cpp_codegen_static_fields_for(XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_il2cpp_TypeInfo_var))->get_WhitespaceChars_3();
		NullCheck(L_0);
		String_t* L_2 = String_Trim_m788DE5AEFDAC40E778745C4DF4AFD45A4BC1007E(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Exception System.Xml.XmlConvert::CreateException(System.String,System.String,System.Xml.ExceptionType,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateException_m94C72E2D80667A0F1A8AB7B7DBC916CA6C527002 (String_t* ___res0, String_t* ___arg1, int32_t ___exceptionType2, int32_t ___lineNo3, int32_t ___linePos4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert_CreateException_m94C72E2D80667A0F1A8AB7B7DBC916CA6C527002_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___exceptionType2;
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		int32_t L_1 = ___exceptionType2;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_001f;
	}

IL_0009:
	{
		String_t* L_2 = ___res0;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_3 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_4 = L_3;
		String_t* L_5 = ___arg1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		String_t* L_6 = Res_GetString_mBDB7AFD1EB8C2C577012518DC77B8646A3045E78(L_2, L_4, /*hidden argument*/NULL);
		ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * L_7 = (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 *)il2cpp_codegen_object_new(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m9A85EF7FEFEC21DDD525A67E831D77278E5165B7(L_7, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_001f:
	{
		String_t* L_8 = ___res0;
		String_t* L_9 = ___arg1;
		int32_t L_10 = ___lineNo3;
		int32_t L_11 = ___linePos4;
		XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D * L_12 = (XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D *)il2cpp_codegen_object_new(XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D_il2cpp_TypeInfo_var);
		XmlException__ctor_m2CA134DF0552F0FDBBDC49D5E823831CE5A0B325(L_12, L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Exception System.Xml.XmlConvert::CreateException(System.String,System.String[],System.Xml.ExceptionType,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateException_m956710818209FE5D8F7999590B30376FEFBC2F90 (String_t* ___res0, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___args1, int32_t ___exceptionType2, int32_t ___lineNo3, int32_t ___linePos4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert_CreateException_m956710818209FE5D8F7999590B30376FEFBC2F90_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___exceptionType2;
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		int32_t L_1 = ___exceptionType2;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0016;
		}
	}
	{
		goto IL_0016;
	}

IL_0009:
	{
		String_t* L_2 = ___res0;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_3 = ___args1;
		String_t* L_4 = Res_GetString_mBDB7AFD1EB8C2C577012518DC77B8646A3045E78(L_2, (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)L_3, /*hidden argument*/NULL);
		ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * L_5 = (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 *)il2cpp_codegen_object_new(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m9A85EF7FEFEC21DDD525A67E831D77278E5165B7(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0016:
	{
		String_t* L_6 = ___res0;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_7 = ___args1;
		int32_t L_8 = ___lineNo3;
		int32_t L_9 = ___linePos4;
		XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D * L_10 = (XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D *)il2cpp_codegen_object_new(XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D_il2cpp_TypeInfo_var);
		XmlException__ctor_mEAC3FCF0B0088BA1343DA1293874C92ECE9217EB(L_10, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Exception System.Xml.XmlConvert::CreateInvalidSurrogatePairException(System.Char,System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidSurrogatePairException_m7ADDD2E98288144ED30A75715AD4254E484FAC70 (Il2CppChar ___low0, Il2CppChar ___hi1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert_CreateInvalidSurrogatePairException_m7ADDD2E98288144ED30A75715AD4254E484FAC70_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppChar L_0 = ___low0;
		Il2CppChar L_1 = ___hi1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_il2cpp_TypeInfo_var);
		Exception_t * L_2 = XmlConvert_CreateInvalidSurrogatePairException_mC493A88B7ED7BFEF845DD2ABA4824D395107F812(L_0, L_1, 0, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Exception System.Xml.XmlConvert::CreateInvalidSurrogatePairException(System.Char,System.Char,System.Xml.ExceptionType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidSurrogatePairException_mC493A88B7ED7BFEF845DD2ABA4824D395107F812 (Il2CppChar ___low0, Il2CppChar ___hi1, int32_t ___exceptionType2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert_CreateInvalidSurrogatePairException_mC493A88B7ED7BFEF845DD2ABA4824D395107F812_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppChar L_0 = ___low0;
		Il2CppChar L_1 = ___hi1;
		int32_t L_2 = ___exceptionType2;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_il2cpp_TypeInfo_var);
		Exception_t * L_3 = XmlConvert_CreateInvalidSurrogatePairException_mD7779092577F68BAD64FA0A0549CF1916E6B9914(L_0, L_1, L_2, 0, 0, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Exception System.Xml.XmlConvert::CreateInvalidSurrogatePairException(System.Char,System.Char,System.Xml.ExceptionType,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidSurrogatePairException_mD7779092577F68BAD64FA0A0549CF1916E6B9914 (Il2CppChar ___low0, Il2CppChar ___hi1, int32_t ___exceptionType2, int32_t ___lineNo3, int32_t ___linePos4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert_CreateInvalidSurrogatePairException_mD7779092577F68BAD64FA0A0549CF1916E6B9914_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* V_0 = NULL;
	uint32_t V_1 = 0;
	{
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_0 = (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)SZArrayNew(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var, (uint32_t)2);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_1 = L_0;
		Il2CppChar L_2 = ___hi1;
		V_1 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_il2cpp_TypeInfo_var);
		CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * L_3 = CultureInfo_get_InvariantCulture_mF13B47F8A763CE6A9C8A8BB2EED33FF8F7A63A72(/*hidden argument*/NULL);
		String_t* L_4 = UInt32_ToString_m57BE7A0F4A653986FEAC4794CD13B04CE012F4EE((uint32_t*)(&V_1), _stringLiteralC032ADC1FF629C9B66F22749AD667E6BEADF144B, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_4);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_5 = L_1;
		Il2CppChar L_6 = ___low0;
		V_1 = L_6;
		CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * L_7 = CultureInfo_get_InvariantCulture_mF13B47F8A763CE6A9C8A8BB2EED33FF8F7A63A72(/*hidden argument*/NULL);
		String_t* L_8 = UInt32_ToString_m57BE7A0F4A653986FEAC4794CD13B04CE012F4EE((uint32_t*)(&V_1), _stringLiteralC032ADC1FF629C9B66F22749AD667E6BEADF144B, L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_8);
		V_0 = L_5;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_9 = V_0;
		int32_t L_10 = ___exceptionType2;
		int32_t L_11 = ___lineNo3;
		int32_t L_12 = ___linePos4;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_il2cpp_TypeInfo_var);
		Exception_t * L_13 = XmlConvert_CreateException_m956710818209FE5D8F7999590B30376FEFBC2F90(_stringLiteral2152BA8F95B60B0E0D6C97D831B87EEFB1BF030D, L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Exception System.Xml.XmlConvert::CreateInvalidHighSurrogateCharException(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidHighSurrogateCharException_m4A1C627F6492AD6A98B4CA982A9197BF39C73DA8 (Il2CppChar ___hi0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert_CreateInvalidHighSurrogateCharException_m4A1C627F6492AD6A98B4CA982A9197BF39C73DA8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppChar L_0 = ___hi0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_il2cpp_TypeInfo_var);
		Exception_t * L_1 = XmlConvert_CreateInvalidHighSurrogateCharException_m22453CD571E079C62CC668E732C74CC1EFE47CFD(L_0, 0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Exception System.Xml.XmlConvert::CreateInvalidHighSurrogateCharException(System.Char,System.Xml.ExceptionType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidHighSurrogateCharException_m22453CD571E079C62CC668E732C74CC1EFE47CFD (Il2CppChar ___hi0, int32_t ___exceptionType1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert_CreateInvalidHighSurrogateCharException_m22453CD571E079C62CC668E732C74CC1EFE47CFD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppChar L_0 = ___hi0;
		int32_t L_1 = ___exceptionType1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_il2cpp_TypeInfo_var);
		Exception_t * L_2 = XmlConvert_CreateInvalidHighSurrogateCharException_mAC251160BC8A0DB0389838A689D95A3C898B270B(L_0, L_1, 0, 0, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Exception System.Xml.XmlConvert::CreateInvalidHighSurrogateCharException(System.Char,System.Xml.ExceptionType,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * XmlConvert_CreateInvalidHighSurrogateCharException_mAC251160BC8A0DB0389838A689D95A3C898B270B (Il2CppChar ___hi0, int32_t ___exceptionType1, int32_t ___lineNo2, int32_t ___linePos3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert_CreateInvalidHighSurrogateCharException_mAC251160BC8A0DB0389838A689D95A3C898B270B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		Il2CppChar L_0 = ___hi0;
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_il2cpp_TypeInfo_var);
		CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * L_1 = CultureInfo_get_InvariantCulture_mF13B47F8A763CE6A9C8A8BB2EED33FF8F7A63A72(/*hidden argument*/NULL);
		String_t* L_2 = UInt32_ToString_m57BE7A0F4A653986FEAC4794CD13B04CE012F4EE((uint32_t*)(&V_0), _stringLiteralC032ADC1FF629C9B66F22749AD667E6BEADF144B, L_1, /*hidden argument*/NULL);
		int32_t L_3 = ___exceptionType1;
		int32_t L_4 = ___lineNo2;
		int32_t L_5 = ___linePos3;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_il2cpp_TypeInfo_var);
		Exception_t * L_6 = XmlConvert_CreateException_m94C72E2D80667A0F1A8AB7B7DBC916CA6C527002(_stringLiteral45815B75A6BA88FC4268CF681E4B17FA4CEEA4EC, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void System.Xml.XmlConvert::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlConvert__cctor_m1EE317B21041E9F0ABE85E4DB9EE427C21DB4F03 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert__cctor_m1EE317B21041E9F0ABE85E4DB9EE427C21DB4F03_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  L_0 = XmlCharType_get_Instance_mEAAD3E43BD5AC72FA94C12096B2A9C9684557210(/*hidden argument*/NULL);
		((XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_StaticFields*)il2cpp_codegen_static_fields_for(XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_il2cpp_TypeInfo_var))->set_xmlCharType_0(L_0);
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_1 = (CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)SZArrayNew(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2_il2cpp_TypeInfo_var, (uint32_t)3);
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_2 = L_1;
		RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  L_3 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB____5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98_0_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m29F50CDFEEE0AB868200291366253DD4737BC76A((RuntimeArray *)(RuntimeArray *)L_2, L_3, /*hidden argument*/NULL);
		((XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_StaticFields*)il2cpp_codegen_static_fields_for(XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_il2cpp_TypeInfo_var))->set_crt_1(L_2);
		((XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_StaticFields*)il2cpp_codegen_static_fields_for(XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_il2cpp_TypeInfo_var))->set_c_EncodedCharLength_2(7);
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_4 = (CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)SZArrayNew(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2_il2cpp_TypeInfo_var, (uint32_t)4);
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_5 = L_4;
		RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  L_6 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB____EBC658B067B5C785A3F0BB67D73755F6FEE7F70C_3_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m29F50CDFEEE0AB868200291366253DD4737BC76A((RuntimeArray *)(RuntimeArray *)L_5, L_6, /*hidden argument*/NULL);
		((XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_StaticFields*)il2cpp_codegen_static_fields_for(XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_il2cpp_TypeInfo_var))->set_WhitespaceChars_3(L_5);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.XmlDOMTextWriter::.ctor(System.IO.TextWriter)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlDOMTextWriter__ctor_mFF0BC0EC83712D01433AE7115529DCC96B38EE11 (XmlDOMTextWriter_t63DEA09C11BB4D12F03C65BC12618E45C4C3E436 * __this, TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ___w0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlDOMTextWriter__ctor_mFF0BC0EC83712D01433AE7115529DCC96B38EE11_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_0 = ___w0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_il2cpp_TypeInfo_var);
		XmlTextWriter__ctor_m6FA653B70BB118A3B8BDA6E29CFF8911CA060EE2(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.XmlException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_mC412B820BFDD1777E4423CA896912FAAD077B783 (XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D * __this, SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * ___info0, StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlException__ctor_mC412B820BFDD1777E4423CA896912FAAD077B783_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	SerializationInfoEnumerator_tB72162C419D705A40F34DDFEB18E6ACA347C54E5 * V_1 = NULL;
	SerializationEntry_tA4CE7B0176B45BD820A7802C84479174F5EBE5EA  V_2;
	memset((&V_2), 0, sizeof(V_2));
	String_t* V_3 = NULL;
	{
		SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * L_0 = ___info0;
		StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  L_1 = ___context1;
		SystemException__ctor_mB0550111A1A8D18B697B618F811A5B20C160D949(__this, L_0, L_1, /*hidden argument*/NULL);
		SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * L_2 = ___info0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_3 = { reinterpret_cast<intptr_t> (String_t_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		RuntimeObject * L_5 = SerializationInfo_GetValue_m7910CE6C68888C1F863D7A35915391FA33463ECF(L_2, _stringLiteralDEE1EBCD105D3D47ADF43ABA6FD674E80D1DC35F, L_4, /*hidden argument*/NULL);
		__this->set_res_17(((String_t*)CastclassSealed((RuntimeObject*)L_5, String_t_il2cpp_TypeInfo_var)));
		SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * L_6 = ___info0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_7 = { reinterpret_cast<intptr_t> (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_0_0_0_var) };
		Type_t * L_8 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		RuntimeObject * L_9 = SerializationInfo_GetValue_m7910CE6C68888C1F863D7A35915391FA33463ECF(L_6, _stringLiteral3030E728F154BF51419109EFB93B6B8AEEC9A976, L_8, /*hidden argument*/NULL);
		__this->set_args_18(((StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)Castclass((RuntimeObject*)L_9, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var)));
		SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * L_10 = ___info0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_11 = { reinterpret_cast<intptr_t> (Int32_t585191389E07734F19F3156FF88FB3EF4800D102_0_0_0_var) };
		Type_t * L_12 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		RuntimeObject * L_13 = SerializationInfo_GetValue_m7910CE6C68888C1F863D7A35915391FA33463ECF(L_10, _stringLiteralD8A074E1D9365F0A70DCF0E727BD10F33EE154D9, L_12, /*hidden argument*/NULL);
		__this->set_lineNumber_19(((*(int32_t*)((int32_t*)UnBox(L_13, Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var)))));
		SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * L_14 = ___info0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_15 = { reinterpret_cast<intptr_t> (Int32_t585191389E07734F19F3156FF88FB3EF4800D102_0_0_0_var) };
		Type_t * L_16 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		RuntimeObject * L_17 = SerializationInfo_GetValue_m7910CE6C68888C1F863D7A35915391FA33463ECF(L_14, _stringLiteral4AF0A653663892E6E605DC7C5B286A0FF6E4247B, L_16, /*hidden argument*/NULL);
		__this->set_linePosition_20(((*(int32_t*)((int32_t*)UnBox(L_17, Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var)))));
		String_t* L_18 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_sourceUri_21(L_18);
		V_0 = (String_t*)NULL;
		SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * L_19 = ___info0;
		NullCheck(L_19);
		SerializationInfoEnumerator_tB72162C419D705A40F34DDFEB18E6ACA347C54E5 * L_20 = SerializationInfo_GetEnumerator_m9796C5CB43B69B5236D530A547A4FC24ABB0B575(L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		goto IL_00ea;
	}

IL_009e:
	{
		SerializationInfoEnumerator_tB72162C419D705A40F34DDFEB18E6ACA347C54E5 * L_21 = V_1;
		NullCheck(L_21);
		SerializationEntry_tA4CE7B0176B45BD820A7802C84479174F5EBE5EA  L_22 = SerializationInfoEnumerator_get_Current_m8C18D86105BD6390B0FB268A1C1E8151D8AF3C33(L_21, /*hidden argument*/NULL);
		V_2 = L_22;
		String_t* L_23 = SerializationEntry_get_Name_m364D6CAEAD32EE66700B47B65E80C03D80596DC4_inline((SerializationEntry_tA4CE7B0176B45BD820A7802C84479174F5EBE5EA *)(&V_2), /*hidden argument*/NULL);
		V_3 = L_23;
		String_t* L_24 = V_3;
		bool L_25 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_24, _stringLiteral5368F75FAEDF45E142655CD9D9FF084E577BBD2D, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_00c9;
		}
	}
	{
		String_t* L_26 = V_3;
		bool L_27 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_26, _stringLiteralC692273DEB2772DA307FFE37041FEF77BF4BAA97, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_00dd;
		}
	}
	{
		goto IL_00ea;
	}

IL_00c9:
	{
		RuntimeObject * L_28 = SerializationEntry_get_Value_m6E7295904D91A38BFE2C47C662E75E8063ABC048_inline((SerializationEntry_tA4CE7B0176B45BD820A7802C84479174F5EBE5EA *)(&V_2), /*hidden argument*/NULL);
		__this->set_sourceUri_21(((String_t*)CastclassSealed((RuntimeObject*)L_28, String_t_il2cpp_TypeInfo_var)));
		goto IL_00ea;
	}

IL_00dd:
	{
		RuntimeObject * L_29 = SerializationEntry_get_Value_m6E7295904D91A38BFE2C47C662E75E8063ABC048_inline((SerializationEntry_tA4CE7B0176B45BD820A7802C84479174F5EBE5EA *)(&V_2), /*hidden argument*/NULL);
		V_0 = ((String_t*)CastclassSealed((RuntimeObject*)L_29, String_t_il2cpp_TypeInfo_var));
	}

IL_00ea:
	{
		SerializationInfoEnumerator_tB72162C419D705A40F34DDFEB18E6ACA347C54E5 * L_30 = V_1;
		NullCheck(L_30);
		bool L_31 = SerializationInfoEnumerator_MoveNext_m74D8DE9528E7DDD141DD45ABF4B54F832DE35701(L_30, /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_009e;
		}
	}
	{
		String_t* L_32 = V_0;
		if (L_32)
		{
			goto IL_0119;
		}
	}
	{
		String_t* L_33 = __this->get_res_17();
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_34 = __this->get_args_18();
		int32_t L_35 = __this->get_lineNumber_19();
		int32_t L_36 = __this->get_linePosition_20();
		String_t* L_37 = XmlException_CreateMessage_mDD847D2C1685E48408F191251542EB3C754344D5(L_33, L_34, L_35, L_36, /*hidden argument*/NULL);
		__this->set_message_22(L_37);
		return;
	}

IL_0119:
	{
		__this->set_message_22((String_t*)NULL);
		return;
	}
}
// System.Void System.Xml.XmlException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException_GetObjectData_m8C9F4943DC440A3FBF70905487652549D1B0BE45 (XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D * __this, SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * ___info0, StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlException_GetObjectData_m8C9F4943DC440A3FBF70905487652549D1B0BE45_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * L_0 = ___info0;
		StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  L_1 = ___context1;
		Exception_GetObjectData_m76F759ED00FA218FFC522C32626B851FDE849AD6(__this, L_0, L_1, /*hidden argument*/NULL);
		SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * L_2 = ___info0;
		String_t* L_3 = __this->get_res_17();
		NullCheck(L_2);
		SerializationInfo_AddValue_mC9D1E16476E24E1AFE7C59368D3BC0B35F64FBC6(L_2, _stringLiteralDEE1EBCD105D3D47ADF43ABA6FD674E80D1DC35F, L_3, /*hidden argument*/NULL);
		SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * L_4 = ___info0;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_5 = __this->get_args_18();
		NullCheck(L_4);
		SerializationInfo_AddValue_mC9D1E16476E24E1AFE7C59368D3BC0B35F64FBC6(L_4, _stringLiteral3030E728F154BF51419109EFB93B6B8AEEC9A976, (RuntimeObject *)(RuntimeObject *)L_5, /*hidden argument*/NULL);
		SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * L_6 = ___info0;
		int32_t L_7 = __this->get_lineNumber_19();
		NullCheck(L_6);
		SerializationInfo_AddValue_m7C73917D9DC4B8FE4AFEF4BA8EBEDAB046A8D0BD(L_6, _stringLiteralD8A074E1D9365F0A70DCF0E727BD10F33EE154D9, L_7, /*hidden argument*/NULL);
		SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * L_8 = ___info0;
		int32_t L_9 = __this->get_linePosition_20();
		NullCheck(L_8);
		SerializationInfo_AddValue_m7C73917D9DC4B8FE4AFEF4BA8EBEDAB046A8D0BD(L_8, _stringLiteral4AF0A653663892E6E605DC7C5B286A0FF6E4247B, L_9, /*hidden argument*/NULL);
		SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * L_10 = ___info0;
		String_t* L_11 = __this->get_sourceUri_21();
		NullCheck(L_10);
		SerializationInfo_AddValue_mC9D1E16476E24E1AFE7C59368D3BC0B35F64FBC6(L_10, _stringLiteral5368F75FAEDF45E142655CD9D9FF084E577BBD2D, L_11, /*hidden argument*/NULL);
		SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * L_12 = ___info0;
		NullCheck(L_12);
		SerializationInfo_AddValue_mC9D1E16476E24E1AFE7C59368D3BC0B35F64FBC6(L_12, _stringLiteralC692273DEB2772DA307FFE37041FEF77BF4BAA97, _stringLiteral70142F66475AE2FB33722D8D4750F386ECFEFE7B, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XmlException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_m3290623E5F23862FF7D0DCEC2CA1FCB7AC02B469 (XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D * __this, const RuntimeMethod* method)
{
	{
		XmlException__ctor_m3C6F865CCA07051F36F465F1BBE24251906765A9(__this, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XmlException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_m3C6F865CCA07051F36F465F1BBE24251906765A9 (XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D * __this, String_t* ___message0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___message0;
		XmlException__ctor_m9600B3E2828022680728997A9FCA2668850588E4(__this, L_0, (Exception_t *)NULL, 0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XmlException::.ctor(System.String,System.Exception,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_m9600B3E2828022680728997A9FCA2668850588E4 (XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___lineNumber2, int32_t ___linePosition3, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t * L_1 = ___innerException1;
		int32_t L_2 = ___lineNumber2;
		int32_t L_3 = ___linePosition3;
		XmlException__ctor_m2038069D4E6C83DC027DFDD97AD142B723C60C36(__this, L_0, L_1, L_2, L_3, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XmlException::.ctor(System.String,System.Exception,System.Int32,System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_m2038069D4E6C83DC027DFDD97AD142B723C60C36 (XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D * __this, String_t* ___message0, Exception_t * ___innerException1, int32_t ___lineNumber2, int32_t ___linePosition3, String_t* ___sourceUri4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlException__ctor_m2038069D4E6C83DC027DFDD97AD142B723C60C36_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D * G_B2_0 = NULL;
	XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D * G_B3_1 = NULL;
	{
		String_t* L_0 = ___message0;
		int32_t L_1 = ___lineNumber2;
		int32_t L_2 = ___linePosition3;
		String_t* L_3 = XmlException_FormatUserMessage_mDFED8BDE616EF903472039DEE5F5B1F59E5FCCFF(L_0, L_1, L_2, /*hidden argument*/NULL);
		Exception_t * L_4 = ___innerException1;
		SystemException__ctor_mA18D2EA5642C066F35CB8C965398F9A542C33B0A(__this, L_3, L_4, /*hidden argument*/NULL);
		Exception_set_HResult_m920DF8C728D8A0EC0759685FED890C775FA08B99_inline(__this, ((int32_t)-2146232000), /*hidden argument*/NULL);
		String_t* L_5 = ___message0;
		G_B1_0 = __this;
		if (!L_5)
		{
			G_B2_0 = __this;
			goto IL_0026;
		}
	}
	{
		G_B3_0 = _stringLiteral0B0C6F90D172B22857FDB7C4E16D3DD858581ACC;
		G_B3_1 = G_B1_0;
		goto IL_002b;
	}

IL_0026:
	{
		G_B3_0 = _stringLiteral16A3C2A876EADFBFB7EF0193C48BCF6088E0B6AD;
		G_B3_1 = G_B2_0;
	}

IL_002b:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_res_17(G_B3_0);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_6 = (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)SZArrayNew(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var, (uint32_t)1);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_7 = L_6;
		String_t* L_8 = ___message0;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_8);
		__this->set_args_18(L_7);
		String_t* L_9 = ___sourceUri4;
		__this->set_sourceUri_21(L_9);
		int32_t L_10 = ___lineNumber2;
		__this->set_lineNumber_19(L_10);
		int32_t L_11 = ___linePosition3;
		__this->set_linePosition_20(L_11);
		return;
	}
}
// System.Void System.Xml.XmlException::.ctor(System.String,System.String,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_m2CA134DF0552F0FDBBDC49D5E823831CE5A0B325 (XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D * __this, String_t* ___res0, String_t* ___arg1, int32_t ___lineNumber2, int32_t ___linePosition3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlException__ctor_m2CA134DF0552F0FDBBDC49D5E823831CE5A0B325_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___res0;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_1 = (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)SZArrayNew(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var, (uint32_t)1);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_2 = L_1;
		String_t* L_3 = ___arg1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_3);
		int32_t L_4 = ___lineNumber2;
		int32_t L_5 = ___linePosition3;
		XmlException__ctor_m4B8E63B6F3EB704FFEF6BA4AA0546B90042FC100(__this, L_0, L_2, (Exception_t *)NULL, L_4, L_5, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XmlException::.ctor(System.String,System.String[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_mEAC3FCF0B0088BA1343DA1293874C92ECE9217EB (XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D * __this, String_t* ___res0, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___args1, int32_t ___lineNumber2, int32_t ___linePosition3, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___res0;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_1 = ___args1;
		int32_t L_2 = ___lineNumber2;
		int32_t L_3 = ___linePosition3;
		XmlException__ctor_m4B8E63B6F3EB704FFEF6BA4AA0546B90042FC100(__this, L_0, L_1, (Exception_t *)NULL, L_2, L_3, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XmlException::.ctor(System.String,System.String[],System.Exception,System.Int32,System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlException__ctor_m4B8E63B6F3EB704FFEF6BA4AA0546B90042FC100 (XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D * __this, String_t* ___res0, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___args1, Exception_t * ___innerException2, int32_t ___lineNumber3, int32_t ___linePosition4, String_t* ___sourceUri5, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___res0;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_1 = ___args1;
		int32_t L_2 = ___lineNumber3;
		int32_t L_3 = ___linePosition4;
		String_t* L_4 = XmlException_CreateMessage_mDD847D2C1685E48408F191251542EB3C754344D5(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		Exception_t * L_5 = ___innerException2;
		SystemException__ctor_mA18D2EA5642C066F35CB8C965398F9A542C33B0A(__this, L_4, L_5, /*hidden argument*/NULL);
		Exception_set_HResult_m920DF8C728D8A0EC0759685FED890C775FA08B99_inline(__this, ((int32_t)-2146232000), /*hidden argument*/NULL);
		String_t* L_6 = ___res0;
		__this->set_res_17(L_6);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_7 = ___args1;
		__this->set_args_18(L_7);
		String_t* L_8 = ___sourceUri5;
		__this->set_sourceUri_21(L_8);
		int32_t L_9 = ___lineNumber3;
		__this->set_lineNumber_19(L_9);
		int32_t L_10 = ___linePosition4;
		__this->set_linePosition_20(L_10);
		return;
	}
}
// System.String System.Xml.XmlException::FormatUserMessage(System.String,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* XmlException_FormatUserMessage_mDFED8BDE616EF903472039DEE5F5B1F59E5FCCFF (String_t* ___message0, int32_t ___lineNumber1, int32_t ___linePosition2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlException_FormatUserMessage_mDFED8BDE616EF903472039DEE5F5B1F59E5FCCFF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_1 = ___lineNumber1;
		int32_t L_2 = ___linePosition2;
		String_t* L_3 = XmlException_CreateMessage_mDD847D2C1685E48408F191251542EB3C754344D5(_stringLiteral16A3C2A876EADFBFB7EF0193C48BCF6088E0B6AD, (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)NULL, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0011:
	{
		int32_t L_4 = ___lineNumber1;
		if (L_4)
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_5 = ___linePosition2;
		if (L_5)
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_6 = ___message0;
		return L_6;
	}

IL_0019:
	{
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_7 = (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)SZArrayNew(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var, (uint32_t)1);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_8 = L_7;
		String_t* L_9 = ___message0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_9);
		int32_t L_10 = ___lineNumber1;
		int32_t L_11 = ___linePosition2;
		String_t* L_12 = XmlException_CreateMessage_mDD847D2C1685E48408F191251542EB3C754344D5(_stringLiteral0B0C6F90D172B22857FDB7C4E16D3DD858581ACC, L_8, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.String System.Xml.XmlException::CreateMessage(System.String,System.String[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* XmlException_CreateMessage_mDD847D2C1685E48408F191251542EB3C754344D5 (String_t* ___res0, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___args1, int32_t ___lineNumber2, int32_t ___linePosition3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlException_CreateMessage_mDD847D2C1685E48408F191251542EB3C754344D5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___lineNumber2;
			if (L_0)
			{
				goto IL_000d;
			}
		}

IL_0003:
		{
			String_t* L_1 = ___res0;
			StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_2 = ___args1;
			String_t* L_3 = Res_GetString_mBDB7AFD1EB8C2C577012518DC77B8646A3045E78(L_1, (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			goto IL_004c;
		}

IL_000d:
		{
			IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_il2cpp_TypeInfo_var);
			CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * L_4 = CultureInfo_get_InvariantCulture_mF13B47F8A763CE6A9C8A8BB2EED33FF8F7A63A72(/*hidden argument*/NULL);
			String_t* L_5 = Int32_ToString_m1D0AF82BDAB5D4710527DD3FEFA6F01246D128A5((int32_t*)(&___lineNumber2), L_4, /*hidden argument*/NULL);
			V_1 = L_5;
			CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * L_6 = CultureInfo_get_InvariantCulture_mF13B47F8A763CE6A9C8A8BB2EED33FF8F7A63A72(/*hidden argument*/NULL);
			String_t* L_7 = Int32_ToString_m1D0AF82BDAB5D4710527DD3FEFA6F01246D128A5((int32_t*)(&___linePosition3), L_6, /*hidden argument*/NULL);
			V_2 = L_7;
			String_t* L_8 = ___res0;
			StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_9 = ___args1;
			String_t* L_10 = Res_GetString_mBDB7AFD1EB8C2C577012518DC77B8646A3045E78(L_8, (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)L_9, /*hidden argument*/NULL);
			V_0 = L_10;
			StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_11 = (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)SZArrayNew(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var, (uint32_t)3);
			StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_12 = L_11;
			String_t* L_13 = V_0;
			NullCheck(L_12);
			ArrayElementTypeCheck (L_12, L_13);
			(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_13);
			StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_14 = L_12;
			String_t* L_15 = V_1;
			NullCheck(L_14);
			ArrayElementTypeCheck (L_14, L_15);
			(L_14)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_15);
			StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_16 = L_14;
			String_t* L_17 = V_2;
			NullCheck(L_16);
			ArrayElementTypeCheck (L_16, L_17);
			(L_16)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_17);
			String_t* L_18 = Res_GetString_mBDB7AFD1EB8C2C577012518DC77B8646A3045E78(_stringLiteral929FB4AD13AB3C3E59695178677C8D8149AAD0C2, (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)L_16, /*hidden argument*/NULL);
			V_0 = L_18;
		}

IL_004c:
		{
			String_t* L_19 = V_0;
			V_3 = L_19;
			goto IL_0064;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (MissingManifestResourceException_tD8397DA5F4CC63B78F91916522A302782CAB0261_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0050;
		throw e;
	}

CATCH_0050:
	{ // begin catch(System.Resources.MissingManifestResourceException)
		String_t* L_20 = ___res0;
		String_t* L_21 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(_stringLiteral6BDCD57D44C0013FD66AB0C7E3EB8C27BDCF9D07, L_20, _stringLiteralE7064F0B80F61DBC65915311032D27BAA569AE2A, /*hidden argument*/NULL);
		V_3 = L_21;
		goto IL_0064;
	} // end catch (depth: 1)

IL_0064:
	{
		String_t* L_22 = V_3;
		return L_22;
	}
}
// System.String System.Xml.XmlException::get_Message()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* XmlException_get_Message_m8B67F069D39C92A254CAF168FAE0FFAF6424AC38 (XmlException_tD7DA2CBE07488D18EE7A284862B7523E3CD4365D * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_message_22();
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		String_t* L_1 = __this->get_message_22();
		return L_1;
	}

IL_000f:
	{
		String_t* L_2 = Exception_get_Message_m4315B19A04019652708F20C1B855805157F23CFD(__this, /*hidden argument*/NULL);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Xml.XmlNode System.Xml.XmlLinkedNode::get_NextSibling()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * XmlLinkedNode_get_NextSibling_m6F858A5E1ED045F63D7885A243CD10FFDA891BA6 (XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * __this, const RuntimeMethod* method)
{
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * V_0 = NULL;
	{
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_0 = VirtFuncInvoker0< XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * >::Invoke(6 /* System.Xml.XmlNode System.Xml.XmlNode::get_ParentNode() */, __this);
		V_0 = L_0;
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * L_2 = __this->get_next_1();
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_3 = V_0;
		NullCheck(L_3);
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_4 = VirtFuncInvoker0< XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * >::Invoke(8 /* System.Xml.XmlNode System.Xml.XmlNode::get_FirstChild() */, L_3);
		if ((((RuntimeObject*)(XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E *)L_2) == ((RuntimeObject*)(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB *)L_4)))
		{
			goto IL_001f;
		}
	}
	{
		XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * L_5 = __this->get_next_1();
		return L_5;
	}

IL_001f:
	{
		return (XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB *)NULL;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Xml.XmlNode System.Xml.XmlNode::get_ParentNode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * XmlNode_get_ParentNode_m6AC4A7F4FEE5B469A86490C129F011C12201C3E6 (XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlNode_get_ParentNode_m6AC4A7F4FEE5B469A86490C129F011C12201C3E6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * V_0 = NULL;
	XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * V_1 = NULL;
	{
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_0 = __this->get_parentNode_0();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Xml.XmlNodeType System.Xml.XmlNode::get_NodeType() */, L_0);
		if ((((int32_t)L_1) == ((int32_t)((int32_t)9))))
		{
			goto IL_0016;
		}
	}
	{
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_2 = __this->get_parentNode_0();
		return L_2;
	}

IL_0016:
	{
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_3 = __this->get_parentNode_0();
		NullCheck(L_3);
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_4 = VirtFuncInvoker0< XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * >::Invoke(8 /* System.Xml.XmlNode System.Xml.XmlNode::get_FirstChild() */, L_3);
		V_0 = ((XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E *)IsInstClass((RuntimeObject*)L_4, XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E_il2cpp_TypeInfo_var));
		XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * L_5 = V_0;
		if (!L_5)
		{
			goto IL_0045;
		}
	}
	{
		XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * L_6 = V_0;
		V_1 = L_6;
	}

IL_002c:
	{
		XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * L_7 = V_1;
		if ((!(((RuntimeObject*)(XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E *)L_7) == ((RuntimeObject*)(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB *)__this))))
		{
			goto IL_0037;
		}
	}
	{
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_8 = __this->get_parentNode_0();
		return L_8;
	}

IL_0037:
	{
		XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * L_9 = V_1;
		NullCheck(L_9);
		XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * L_10 = L_9->get_next_1();
		V_1 = L_10;
		XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * L_11 = V_1;
		if (!L_11)
		{
			goto IL_0045;
		}
	}
	{
		XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * L_12 = V_1;
		XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * L_13 = V_0;
		if ((!(((RuntimeObject*)(XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E *)L_12) == ((RuntimeObject*)(XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E *)L_13))))
		{
			goto IL_002c;
		}
	}

IL_0045:
	{
		return (XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB *)NULL;
	}
}
// System.Xml.XmlNode System.Xml.XmlNode::get_NextSibling()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * XmlNode_get_NextSibling_m86E032192785FDB070EE88E07675B2FF304E383B (XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * __this, const RuntimeMethod* method)
{
	{
		return (XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB *)NULL;
	}
}
// System.Xml.XmlNode System.Xml.XmlNode::get_FirstChild()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * XmlNode_get_FirstChild_m43F741CF5C0565011D515B92103F04E033952410 (XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * __this, const RuntimeMethod* method)
{
	XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * V_0 = NULL;
	{
		XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * L_0 = VirtFuncInvoker0< XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * >::Invoke(9 /* System.Xml.XmlLinkedNode System.Xml.XmlNode::get_LastNode() */, __this);
		V_0 = L_0;
		XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * L_2 = V_0;
		NullCheck(L_2);
		XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * L_3 = L_2->get_next_1();
		return L_3;
	}

IL_0011:
	{
		return (XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB *)NULL;
	}
}
// System.Xml.XmlLinkedNode System.Xml.XmlNode::get_LastNode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * XmlNode_get_LastNode_mAB2DD7365BF9428D4F8AC4C3AFAE7F229FCB1B08 (XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * __this, const RuntimeMethod* method)
{
	{
		return (XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E *)NULL;
	}
}
// System.Collections.IEnumerator System.Xml.XmlNode::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* XmlNode_System_Collections_IEnumerable_GetEnumerator_mC8BE3ACFBE87B8F5ABAE7B797FAF48A900679B27 (XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlNode_System_Collections_IEnumerable_GetEnumerator_mC8BE3ACFBE87B8F5ABAE7B797FAF48A900679B27_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA * L_0 = (XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA *)il2cpp_codegen_object_new(XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA_il2cpp_TypeInfo_var);
		XmlChildEnumerator__ctor_m9D966497AE59A0784E4FE7CB883226B7A332F311(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String System.Xml.XmlNode::get_OuterXml()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* XmlNode_get_OuterXml_m4198C72CA7F3F13BE8AA34E2F392B9BC7F1EFE98 (XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlNode_get_OuterXml_m4198C72CA7F3F13BE8AA34E2F392B9BC7F1EFE98_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 * V_0 = NULL;
	XmlDOMTextWriter_t63DEA09C11BB4D12F03C65BC12618E45C4C3E436 * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_il2cpp_TypeInfo_var);
		CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * L_0 = CultureInfo_get_InvariantCulture_mF13B47F8A763CE6A9C8A8BB2EED33FF8F7A63A72(/*hidden argument*/NULL);
		StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 * L_1 = (StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 *)il2cpp_codegen_object_new(StringWriter_t194EF1526E072B93984370042AA80926C2EB6139_il2cpp_TypeInfo_var);
		StringWriter__ctor_m4D44D4D5B0CFDEEB172C7D61171340D76432A1EE(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 * L_2 = V_0;
		XmlDOMTextWriter_t63DEA09C11BB4D12F03C65BC12618E45C4C3E436 * L_3 = (XmlDOMTextWriter_t63DEA09C11BB4D12F03C65BC12618E45C4C3E436 *)il2cpp_codegen_object_new(XmlDOMTextWriter_t63DEA09C11BB4D12F03C65BC12618E45C4C3E436_il2cpp_TypeInfo_var);
		XmlDOMTextWriter__ctor_mFF0BC0EC83712D01433AE7115529DCC96B38EE11(L_3, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		XmlDOMTextWriter_t63DEA09C11BB4D12F03C65BC12618E45C4C3E436 * L_4 = V_1;
		VirtActionInvoker1< XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * >::Invoke(11 /* System.Void System.Xml.XmlNode::WriteTo(System.Xml.XmlWriter) */, __this, L_4);
		IL2CPP_LEAVE(0x22, FINALLY_001b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_001b;
	}

FINALLY_001b:
	{ // begin finally (depth: 1)
		XmlDOMTextWriter_t63DEA09C11BB4D12F03C65BC12618E45C4C3E436 * L_5 = V_1;
		NullCheck(L_5);
		VirtActionInvoker0::Invoke(7 /* System.Void System.Xml.XmlWriter::Close() */, L_5);
		IL2CPP_END_FINALLY(27)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(27)
	{
		IL2CPP_JUMP_TBL(0x22, IL_0022)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0022:
	{
		StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 * L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		return L_7;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.XmlReader::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlReader__cctor_m3EF2489C81E4EEF249A34242FC130A861972991E (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlReader__cctor_m3EF2489C81E4EEF249A34242FC130A861972991E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_StaticFields*)il2cpp_codegen_static_fields_for(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_il2cpp_TypeInfo_var))->set_IsTextualNodeBitmap_0(((int32_t)24600));
		((XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_StaticFields*)il2cpp_codegen_static_fields_for(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_il2cpp_TypeInfo_var))->set_CanReadContentAsBitmap_1(((int32_t)123324));
		((XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_StaticFields*)il2cpp_codegen_static_fields_for(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_il2cpp_TypeInfo_var))->set_HasValueBitmap_2(((int32_t)157084));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.XmlTextEncoder::.ctor(System.IO.TextWriter)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder__ctor_mDC642C841EF38B20B5521384E3C9026D594CE332 (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ___textWriter0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_0 = ___textWriter0;
		__this->set_textWriter_0(L_0);
		__this->set_quoteChar_2(((int32_t)34));
		XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  L_1 = XmlCharType_get_Instance_mEAAD3E43BD5AC72FA94C12096B2A9C9684557210(/*hidden argument*/NULL);
		__this->set_xmlCharType_5(L_1);
		return;
	}
}
// System.Void System.Xml.XmlTextEncoder::set_QuoteChar(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_set_QuoteChar_m28C9CC005913B64E3444E1522EE8D26B4F5F0C88 (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, Il2CppChar ___value0, const RuntimeMethod* method)
{
	{
		Il2CppChar L_0 = ___value0;
		__this->set_quoteChar_2(L_0);
		return;
	}
}
// System.Void System.Xml.XmlTextEncoder::StartAttribute(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_StartAttribute_mAFEF454A34E72F5B1457C8D634FA947EA0023D15 (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, bool ___cacheAttrValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextEncoder_StartAttribute_mAFEF454A34E72F5B1457C8D634FA947EA0023D15_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_inAttribute_1((bool)1);
		bool L_0 = ___cacheAttrValue0;
		__this->set_cacheAttrValue_4(L_0);
		bool L_1 = ___cacheAttrValue0;
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		StringBuilder_t * L_2 = __this->get_attrValue_3();
		if (L_2)
		{
			goto IL_0025;
		}
	}
	{
		StringBuilder_t * L_3 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_mF928376F82E8C8FF3C11842C562DB8CF28B2735E(L_3, /*hidden argument*/NULL);
		__this->set_attrValue_3(L_3);
		return;
	}

IL_0025:
	{
		StringBuilder_t * L_4 = __this->get_attrValue_3();
		NullCheck(L_4);
		StringBuilder_set_Length_m84AF318230AE5C3D0D48F1CE7C2170F6F5C19F5B(L_4, 0, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Xml.XmlTextEncoder::EndAttribute()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_EndAttribute_m5988FFECF6A66F36D9079FD6CA67BB56A2A98D66 (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_cacheAttrValue_4();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		StringBuilder_t * L_1 = __this->get_attrValue_3();
		NullCheck(L_1);
		StringBuilder_set_Length_m84AF318230AE5C3D0D48F1CE7C2170F6F5C19F5B(L_1, 0, /*hidden argument*/NULL);
	}

IL_0014:
	{
		__this->set_inAttribute_1((bool)0);
		__this->set_cacheAttrValue_4((bool)0);
		return;
	}
}
// System.String System.Xml.XmlTextEncoder::get_AttributeValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* XmlTextEncoder_get_AttributeValue_m7D58DB09133999166C984994CA05C48B469DEC7D (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextEncoder_get_AttributeValue_m7D58DB09133999166C984994CA05C48B469DEC7D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_cacheAttrValue_4();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		StringBuilder_t * L_1 = __this->get_attrValue_3();
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		return L_2;
	}

IL_0014:
	{
		String_t* L_3 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		return L_3;
	}
}
// System.Void System.Xml.XmlTextEncoder::WriteSurrogateChar(System.Char,System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteSurrogateChar_mCCE79921A16872FB57C3C5C21F7518CCE5F504F3 (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, Il2CppChar ___lowChar0, Il2CppChar ___highChar1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextEncoder_WriteSurrogateChar_mCCE79921A16872FB57C3C5C21F7518CCE5F504F3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppChar L_0 = ___lowChar0;
		bool L_1 = XmlCharType_IsLowSurrogate_m0CB63DE5C97F9C09E2E7C67A53BB8682D8FD07D8(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		Il2CppChar L_2 = ___highChar1;
		bool L_3 = XmlCharType_IsHighSurrogate_m6E9E01B1A14D2CF127B6D39D333E506F90FFF98E(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0018;
		}
	}

IL_0010:
	{
		Il2CppChar L_4 = ___lowChar0;
		Il2CppChar L_5 = ___highChar1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_il2cpp_TypeInfo_var);
		Exception_t * L_6 = XmlConvert_CreateInvalidSurrogatePairException_m7ADDD2E98288144ED30A75715AD4254E484FAC70(L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, NULL, XmlTextEncoder_WriteSurrogateChar_mCCE79921A16872FB57C3C5C21F7518CCE5F504F3_RuntimeMethod_var);
	}

IL_0018:
	{
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_7 = __this->get_textWriter_0();
		Il2CppChar L_8 = ___highChar1;
		NullCheck(L_7);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_7, L_8);
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_9 = __this->get_textWriter_0();
		Il2CppChar L_10 = ___lowChar0;
		NullCheck(L_9);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_9, L_10);
		return;
	}
}
// System.Void System.Xml.XmlTextEncoder::Write(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_Write_mD02582AB249921681B2B7C85E71DC17E20AB3884 (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, String_t* ___text0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextEncoder_Write_mD02582AB249921681B2B7C85E71DC17E20AB3884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Il2CppChar V_3 = 0x0;
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* V_4 = NULL;
	{
		String_t* L_0 = ___text0;
		if (L_0)
		{
			goto IL_0004;
		}
	}
	{
		return;
	}

IL_0004:
	{
		bool L_1 = __this->get_cacheAttrValue_4();
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		StringBuilder_t * L_2 = __this->get_attrValue_3();
		String_t* L_3 = ___text0;
		NullCheck(L_2);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0019:
	{
		String_t* L_4 = ___text0;
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018_inline(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		V_1 = 0;
		V_2 = 0;
		V_3 = 0;
		goto IL_002c;
	}

IL_0028:
	{
		int32_t L_6 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_002c:
	{
		int32_t L_7 = V_1;
		int32_t L_8 = V_0;
		if ((((int32_t)L_7) >= ((int32_t)L_8)))
		{
			goto IL_004d;
		}
	}
	{
		XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * L_9 = __this->get_address_of_xmlCharType_5();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_10 = L_9->get_charProperties_2();
		String_t* L_11 = ___text0;
		int32_t L_12 = V_1;
		NullCheck(L_11);
		Il2CppChar L_13 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_11, L_12, /*hidden argument*/NULL);
		Il2CppChar L_14 = L_13;
		V_3 = L_14;
		NullCheck(L_10);
		Il2CppChar L_15 = L_14;
		uint8_t L_16 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		if (((int32_t)((int32_t)L_16&(int32_t)((int32_t)128))))
		{
			goto IL_0028;
		}
	}

IL_004d:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((!(((uint32_t)L_17) == ((uint32_t)L_18))))
		{
			goto IL_005e;
		}
	}
	{
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_19 = __this->get_textWriter_0();
		String_t* L_20 = ___text0;
		NullCheck(L_19);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_19, L_20);
		return;
	}

IL_005e:
	{
		bool L_21 = __this->get_inAttribute_1();
		if (!L_21)
		{
			goto IL_0071;
		}
	}
	{
		Il2CppChar L_22 = V_3;
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_23 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
		goto IL_002c;
	}

IL_0071:
	{
		Il2CppChar L_24 = V_3;
		if ((((int32_t)L_24) == ((int32_t)((int32_t)9))))
		{
			goto IL_008a;
		}
	}
	{
		Il2CppChar L_25 = V_3;
		if ((((int32_t)L_25) == ((int32_t)((int32_t)10))))
		{
			goto IL_008a;
		}
	}
	{
		Il2CppChar L_26 = V_3;
		if ((((int32_t)L_26) == ((int32_t)((int32_t)13))))
		{
			goto IL_008a;
		}
	}
	{
		Il2CppChar L_27 = V_3;
		if ((((int32_t)L_27) == ((int32_t)((int32_t)34))))
		{
			goto IL_008a;
		}
	}
	{
		Il2CppChar L_28 = V_3;
		if ((!(((uint32_t)L_28) == ((uint32_t)((int32_t)39)))))
		{
			goto IL_0090;
		}
	}

IL_008a:
	{
		int32_t L_29 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)1));
		goto IL_002c;
	}

IL_0090:
	{
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_30 = (CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)SZArrayNew(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256));
		V_4 = L_30;
	}

IL_009c:
	{
		int32_t L_31 = V_2;
		int32_t L_32 = V_1;
		if ((((int32_t)L_31) >= ((int32_t)L_32)))
		{
			goto IL_00ad;
		}
	}
	{
		String_t* L_33 = ___text0;
		int32_t L_34 = V_2;
		int32_t L_35 = V_1;
		int32_t L_36 = V_2;
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_37 = V_4;
		XmlTextEncoder_WriteStringFragment_mBDB749E06C09EE5CDA591A6AB22893B2DDBB3BD1(__this, L_33, L_34, ((int32_t)il2cpp_codegen_subtract((int32_t)L_35, (int32_t)L_36)), L_37, /*hidden argument*/NULL);
	}

IL_00ad:
	{
		int32_t L_38 = V_1;
		int32_t L_39 = V_0;
		if ((((int32_t)L_38) == ((int32_t)L_39)))
		{
			goto IL_023a;
		}
	}
	{
		Il2CppChar L_40 = V_3;
		if ((!(((uint32_t)L_40) <= ((uint32_t)((int32_t)38)))))
		{
			goto IL_00e8;
		}
	}
	{
		Il2CppChar L_41 = V_3;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_41, (int32_t)((int32_t)9))))
		{
			case 0:
			{
				goto IL_00fc;
			}
			case 1:
			{
				goto IL_010d;
			}
			case 2:
			{
				goto IL_01bf;
			}
			case 3:
			{
				goto IL_01bf;
			}
			case 4:
			{
				goto IL_010d;
			}
		}
	}
	{
		Il2CppChar L_42 = V_3;
		if ((((int32_t)L_42) == ((int32_t)((int32_t)34))))
		{
			goto IL_0192;
		}
	}
	{
		Il2CppChar L_43 = V_3;
		if ((((int32_t)L_43) == ((int32_t)((int32_t)38))))
		{
			goto IL_0152;
		}
	}
	{
		goto IL_01bf;
	}

IL_00e8:
	{
		Il2CppChar L_44 = V_3;
		if ((((int32_t)L_44) == ((int32_t)((int32_t)39))))
		{
			goto IL_0162;
		}
	}
	{
		Il2CppChar L_45 = V_3;
		if ((((int32_t)L_45) == ((int32_t)((int32_t)60))))
		{
			goto IL_0132;
		}
	}
	{
		Il2CppChar L_46 = V_3;
		if ((((int32_t)L_46) == ((int32_t)((int32_t)62))))
		{
			goto IL_0142;
		}
	}
	{
		goto IL_01bf;
	}

IL_00fc:
	{
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_47 = __this->get_textWriter_0();
		Il2CppChar L_48 = V_3;
		NullCheck(L_47);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_47, L_48);
		goto IL_0205;
	}

IL_010d:
	{
		bool L_49 = __this->get_inAttribute_1();
		if (!L_49)
		{
			goto IL_0121;
		}
	}
	{
		Il2CppChar L_50 = V_3;
		XmlTextEncoder_WriteCharEntityImpl_mEA75C2144CCCF2DF29BB91D3B91B2C06C1E6920F(__this, L_50, /*hidden argument*/NULL);
		goto IL_0205;
	}

IL_0121:
	{
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_51 = __this->get_textWriter_0();
		Il2CppChar L_52 = V_3;
		NullCheck(L_51);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_51, L_52);
		goto IL_0205;
	}

IL_0132:
	{
		XmlTextEncoder_WriteEntityRefImpl_m3DA44922F283F75B20D43E043A1E6D0F8010E5E9(__this, _stringLiteral5F3ACFBEB4F6FA5007DD1137AB1E96149AF87281, /*hidden argument*/NULL);
		goto IL_0205;
	}

IL_0142:
	{
		XmlTextEncoder_WriteEntityRefImpl_m3DA44922F283F75B20D43E043A1E6D0F8010E5E9(__this, _stringLiteral7DDF988C838812A4318332F2967BBE1035B2DB75, /*hidden argument*/NULL);
		goto IL_0205;
	}

IL_0152:
	{
		XmlTextEncoder_WriteEntityRefImpl_m3DA44922F283F75B20D43E043A1E6D0F8010E5E9(__this, _stringLiteral65F59EC6B1ECD6170D5044474043CCA9560A8071, /*hidden argument*/NULL);
		goto IL_0205;
	}

IL_0162:
	{
		bool L_53 = __this->get_inAttribute_1();
		if (!L_53)
		{
			goto IL_0183;
		}
	}
	{
		Il2CppChar L_54 = __this->get_quoteChar_2();
		Il2CppChar L_55 = V_3;
		if ((!(((uint32_t)L_54) == ((uint32_t)L_55))))
		{
			goto IL_0183;
		}
	}
	{
		XmlTextEncoder_WriteEntityRefImpl_m3DA44922F283F75B20D43E043A1E6D0F8010E5E9(__this, _stringLiteral616998B237EE9C3DBBF79DB6CA02275975880AE0, /*hidden argument*/NULL);
		goto IL_0205;
	}

IL_0183:
	{
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_56 = __this->get_textWriter_0();
		NullCheck(L_56);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_56, ((int32_t)39));
		goto IL_0205;
	}

IL_0192:
	{
		bool L_57 = __this->get_inAttribute_1();
		if (!L_57)
		{
			goto IL_01b0;
		}
	}
	{
		Il2CppChar L_58 = __this->get_quoteChar_2();
		Il2CppChar L_59 = V_3;
		if ((!(((uint32_t)L_58) == ((uint32_t)L_59))))
		{
			goto IL_01b0;
		}
	}
	{
		XmlTextEncoder_WriteEntityRefImpl_m3DA44922F283F75B20D43E043A1E6D0F8010E5E9(__this, _stringLiteral11FF44718A597D1A371E7DA9C818486F238E49E2, /*hidden argument*/NULL);
		goto IL_0205;
	}

IL_01b0:
	{
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_60 = __this->get_textWriter_0();
		NullCheck(L_60);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_60, ((int32_t)34));
		goto IL_0205;
	}

IL_01bf:
	{
		Il2CppChar L_61 = V_3;
		bool L_62 = XmlCharType_IsHighSurrogate_m6E9E01B1A14D2CF127B6D39D333E506F90FFF98E(L_61, /*hidden argument*/NULL);
		if (!L_62)
		{
			goto IL_01ef;
		}
	}
	{
		int32_t L_63 = V_1;
		int32_t L_64 = V_0;
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_63, (int32_t)1))) >= ((int32_t)L_64)))
		{
			goto IL_01e1;
		}
	}
	{
		String_t* L_65 = ___text0;
		int32_t L_66 = V_1;
		int32_t L_67 = ((int32_t)il2cpp_codegen_add((int32_t)L_66, (int32_t)1));
		V_1 = L_67;
		NullCheck(L_65);
		Il2CppChar L_68 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_65, L_67, /*hidden argument*/NULL);
		Il2CppChar L_69 = V_3;
		XmlTextEncoder_WriteSurrogateChar_mCCE79921A16872FB57C3C5C21F7518CCE5F504F3(__this, L_68, L_69, /*hidden argument*/NULL);
		goto IL_0205;
	}

IL_01e1:
	{
		String_t* L_70 = ___text0;
		int32_t L_71 = V_1;
		NullCheck(L_70);
		Il2CppChar L_72 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_70, L_71, /*hidden argument*/NULL);
		Il2CppChar L_73 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_il2cpp_TypeInfo_var);
		Exception_t * L_74 = XmlConvert_CreateInvalidSurrogatePairException_m7ADDD2E98288144ED30A75715AD4254E484FAC70(L_72, L_73, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_74, NULL, XmlTextEncoder_Write_mD02582AB249921681B2B7C85E71DC17E20AB3884_RuntimeMethod_var);
	}

IL_01ef:
	{
		Il2CppChar L_75 = V_3;
		bool L_76 = XmlCharType_IsLowSurrogate_m0CB63DE5C97F9C09E2E7C67A53BB8682D8FD07D8(L_75, /*hidden argument*/NULL);
		if (!L_76)
		{
			goto IL_01fe;
		}
	}
	{
		Il2CppChar L_77 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_il2cpp_TypeInfo_var);
		Exception_t * L_78 = XmlConvert_CreateInvalidHighSurrogateCharException_m4A1C627F6492AD6A98B4CA982A9197BF39C73DA8(L_77, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_78, NULL, XmlTextEncoder_Write_mD02582AB249921681B2B7C85E71DC17E20AB3884_RuntimeMethod_var);
	}

IL_01fe:
	{
		Il2CppChar L_79 = V_3;
		XmlTextEncoder_WriteCharEntityImpl_mEA75C2144CCCF2DF29BB91D3B91B2C06C1E6920F(__this, L_79, /*hidden argument*/NULL);
	}

IL_0205:
	{
		int32_t L_80 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_80, (int32_t)1));
		int32_t L_81 = V_1;
		V_2 = L_81;
		goto IL_0211;
	}

IL_020d:
	{
		int32_t L_82 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_82, (int32_t)1));
	}

IL_0211:
	{
		int32_t L_83 = V_1;
		int32_t L_84 = V_0;
		if ((((int32_t)L_83) >= ((int32_t)L_84)))
		{
			goto IL_009c;
		}
	}
	{
		XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * L_85 = __this->get_address_of_xmlCharType_5();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_86 = L_85->get_charProperties_2();
		String_t* L_87 = ___text0;
		int32_t L_88 = V_1;
		NullCheck(L_87);
		Il2CppChar L_89 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_87, L_88, /*hidden argument*/NULL);
		Il2CppChar L_90 = L_89;
		V_3 = L_90;
		NullCheck(L_86);
		Il2CppChar L_91 = L_90;
		uint8_t L_92 = (L_86)->GetAt(static_cast<il2cpp_array_size_t>(L_91));
		if (((int32_t)((int32_t)L_92&(int32_t)((int32_t)128))))
		{
			goto IL_020d;
		}
	}
	{
		goto IL_009c;
	}

IL_023a:
	{
		return;
	}
}
// System.Void System.Xml.XmlTextEncoder::WriteRaw(System.Char[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteRaw_mE2BB17DA3D685325F30E98E590CC58BBB7781628 (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___array0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextEncoder_WriteRaw_mE2BB17DA3D685325F30E98E590CC58BBB7781628_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_0 = ___array0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_1 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_1, _stringLiteral19EDC1210777BA4D45049C29280D9CC5E1064C25, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, NULL, XmlTextEncoder_WriteRaw_mE2BB17DA3D685325F30E98E590CC58BBB7781628_RuntimeMethod_var);
	}

IL_000e:
	{
		int32_t L_2 = ___count2;
		if ((((int32_t)0) <= ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * L_3 = (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6B36E60C989DC798A8B44556DB35960282B133A6(L_3, _stringLiteralEE9F38E186BA06F57B7B74D7E626B94E13CE2556, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, NULL, XmlTextEncoder_WriteRaw_mE2BB17DA3D685325F30E98E590CC58BBB7781628_RuntimeMethod_var);
	}

IL_001d:
	{
		int32_t L_4 = ___offset1;
		if ((((int32_t)0) <= ((int32_t)L_4)))
		{
			goto IL_002c;
		}
	}
	{
		ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * L_5 = (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6B36E60C989DC798A8B44556DB35960282B133A6(L_5, _stringLiteral53A610E925BBC0A175E365D31241AE75AEEAD651, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, NULL, XmlTextEncoder_WriteRaw_mE2BB17DA3D685325F30E98E590CC58BBB7781628_RuntimeMethod_var);
	}

IL_002c:
	{
		int32_t L_6 = ___count2;
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_7 = ___array0;
		NullCheck(L_7);
		int32_t L_8 = ___offset1;
		if ((((int32_t)L_6) <= ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length)))), (int32_t)L_8)))))
		{
			goto IL_003f;
		}
	}
	{
		ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * L_9 = (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6B36E60C989DC798A8B44556DB35960282B133A6(L_9, _stringLiteralEE9F38E186BA06F57B7B74D7E626B94E13CE2556, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, NULL, XmlTextEncoder_WriteRaw_mE2BB17DA3D685325F30E98E590CC58BBB7781628_RuntimeMethod_var);
	}

IL_003f:
	{
		bool L_10 = __this->get_cacheAttrValue_4();
		if (!L_10)
		{
			goto IL_0056;
		}
	}
	{
		StringBuilder_t * L_11 = __this->get_attrValue_3();
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_12 = ___array0;
		int32_t L_13 = ___offset1;
		int32_t L_14 = ___count2;
		NullCheck(L_11);
		StringBuilder_Append_m549C532422286A982F7956C9BAE197D00B30DCA8(L_11, L_12, L_13, L_14, /*hidden argument*/NULL);
	}

IL_0056:
	{
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_15 = __this->get_textWriter_0();
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_16 = ___array0;
		int32_t L_17 = ___offset1;
		int32_t L_18 = ___count2;
		NullCheck(L_15);
		VirtActionInvoker3< CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*, int32_t, int32_t >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.Char[],System.Int32,System.Int32) */, L_15, L_16, L_17, L_18);
		return;
	}
}
// System.Void System.Xml.XmlTextEncoder::WriteStringFragment(System.String,System.Int32,System.Int32,System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteStringFragment_mBDB749E06C09EE5CDA591A6AB22893B2DDBB3BD1 (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, String_t* ___str0, int32_t ___offset1, int32_t ___count2, CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___helperBuffer3, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_0 = ___helperBuffer3;
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((RuntimeArray*)L_0)->max_length))));
		goto IL_0033;
	}

IL_0007:
	{
		int32_t L_1 = ___count2;
		V_1 = L_1;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		if ((((int32_t)L_2) <= ((int32_t)L_3)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_4 = V_0;
		V_1 = L_4;
	}

IL_000f:
	{
		String_t* L_5 = ___str0;
		int32_t L_6 = ___offset1;
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_7 = ___helperBuffer3;
		int32_t L_8 = V_1;
		NullCheck(L_5);
		String_CopyTo_m054B8FF2ACBBA74F60199D98259E88395EAD3661(L_5, L_6, L_7, 0, L_8, /*hidden argument*/NULL);
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_9 = __this->get_textWriter_0();
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_10 = ___helperBuffer3;
		int32_t L_11 = V_1;
		NullCheck(L_9);
		VirtActionInvoker3< CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*, int32_t, int32_t >::Invoke(15 /* System.Void System.IO.TextWriter::Write(System.Char[],System.Int32,System.Int32) */, L_9, L_10, 0, L_11);
		int32_t L_12 = ___offset1;
		int32_t L_13 = V_1;
		___offset1 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)L_13));
		int32_t L_14 = ___count2;
		int32_t L_15 = V_1;
		___count2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_14, (int32_t)L_15));
	}

IL_0033:
	{
		int32_t L_16 = ___count2;
		if ((((int32_t)L_16) > ((int32_t)0)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Xml.XmlTextEncoder::WriteCharEntityImpl(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteCharEntityImpl_mEA75C2144CCCF2DF29BB91D3B91B2C06C1E6920F (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, Il2CppChar ___ch0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextEncoder_WriteCharEntityImpl_mEA75C2144CCCF2DF29BB91D3B91B2C06C1E6920F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Il2CppChar L_0 = ___ch0;
		V_0 = L_0;
		NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * L_1 = NumberFormatInfo_get_InvariantInfo_m55FDCA552CC3CD15E01E10DBFDD5756AB1DCC54D(/*hidden argument*/NULL);
		String_t* L_2 = Int32_ToString_mE527694B0C55AE14FDCBE1D9C848446C18E22C09((int32_t*)(&V_0), _stringLiteralC032ADC1FF629C9B66F22749AD667E6BEADF144B, L_1, /*hidden argument*/NULL);
		XmlTextEncoder_WriteCharEntityImpl_m8DB50E2042CA07D11456C0B67B6EE9D28E8A817E(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XmlTextEncoder::WriteCharEntityImpl(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteCharEntityImpl_m8DB50E2042CA07D11456C0B67B6EE9D28E8A817E (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, String_t* ___strVal0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextEncoder_WriteCharEntityImpl_m8DB50E2042CA07D11456C0B67B6EE9D28E8A817E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_0 = __this->get_textWriter_0();
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_0, _stringLiteralFD7A2B83CB0BA8522402983CB3068C33624C6687);
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_1 = __this->get_textWriter_0();
		String_t* L_2 = ___strVal0;
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_1, L_2);
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_3 = __this->get_textWriter_0();
		NullCheck(L_3);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_3, ((int32_t)59));
		return;
	}
}
// System.Void System.Xml.XmlTextEncoder::WriteEntityRefImpl(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextEncoder_WriteEntityRefImpl_m3DA44922F283F75B20D43E043A1E6D0F8010E5E9 (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_0 = __this->get_textWriter_0();
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_0, ((int32_t)38));
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_1 = __this->get_textWriter_0();
		String_t* L_2 = ___name0;
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_1, L_2);
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_3 = __this->get_textWriter_0();
		NullCheck(L_3);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_3, ((int32_t)59));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.XmlTextWriter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter__ctor_mED6FB595B6052C306BE5175DC8E20DD974A2CEFC (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter__ctor_mED6FB595B6052C306BE5175DC8E20DD974A2CEFC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  L_0 = XmlCharType_get_Instance_mEAAD3E43BD5AC72FA94C12096B2A9C9684557210(/*hidden argument*/NULL);
		__this->set_xmlCharType_23(L_0);
		XmlWriter__ctor_m1D2B58DC035709A720317204246AD58118C15740(__this, /*hidden argument*/NULL);
		__this->set_namespaces_15((bool)1);
		__this->set_formatting_3(0);
		__this->set_indentation_5(2);
		__this->set_indentChar_6(((int32_t)32));
		NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* L_1 = (NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829*)(NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829*)SZArrayNew(NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829_il2cpp_TypeInfo_var, (uint32_t)8);
		__this->set_nsStack_19(L_1);
		__this->set_nsTop_20((-1));
		TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* L_2 = (TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910*)(TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910*)SZArrayNew(TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10));
		__this->set_stack_7(L_2);
		__this->set_top_8(0);
		TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* L_3 = __this->get_stack_7();
		int32_t L_4 = __this->get_top_8();
		NullCheck(L_3);
		TagInfo_Init_m631D10B6F7160EC8D54CAAF1418215168F8E6F32((TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4))), (-1), /*hidden argument*/NULL);
		__this->set_quoteChar_13(((int32_t)34));
		IL2CPP_RUNTIME_CLASS_INIT(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_il2cpp_TypeInfo_var);
		StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* L_5 = ((XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields*)il2cpp_codegen_static_fields_for(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_il2cpp_TypeInfo_var))->get_stateTableDefault_26();
		__this->set_stateTable_9(L_5);
		__this->set_currentState_10(0);
		__this->set_lastToken_11(((int32_t)13));
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::.ctor(System.IO.TextWriter)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter__ctor_m6FA653B70BB118A3B8BDA6E29CFF8911CA060EE2 (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ___w0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter__ctor_m6FA653B70BB118A3B8BDA6E29CFF8911CA060EE2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		XmlTextWriter__ctor_mED6FB595B6052C306BE5175DC8E20DD974A2CEFC(__this, /*hidden argument*/NULL);
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_0 = ___w0;
		__this->set_textWriter_0(L_0);
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_1 = ___w0;
		NullCheck(L_1);
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_2 = VirtFuncInvoker0< Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * >::Invoke(11 /* System.Text.Encoding System.IO.TextWriter::get_Encoding() */, L_1);
		__this->set_encoding_2(L_2);
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_3 = ___w0;
		XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * L_4 = (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 *)il2cpp_codegen_object_new(XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475_il2cpp_TypeInfo_var);
		XmlTextEncoder__ctor_mDC642C841EF38B20B5521384E3C9026D594CE332(L_4, L_3, /*hidden argument*/NULL);
		__this->set_xmlEncoder_1(L_4);
		XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * L_5 = __this->get_xmlEncoder_1();
		Il2CppChar L_6 = __this->get_quoteChar_13();
		NullCheck(L_5);
		XmlTextEncoder_set_QuoteChar_m28C9CC005913B64E3444E1522EE8D26B4F5F0C88_inline(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::WriteEndElement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_WriteEndElement_m8AC712948634628AB0BAD6C73F8E7CA2EF047612 (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, const RuntimeMethod* method)
{
	{
		XmlTextWriter_InternalWriteEndElement_m02FCE5CCA71DBFBA885AF81FFAD702B3BC39294F(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.WriteState System.Xml.XmlTextWriter::get_WriteState()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t XmlTextWriter_get_WriteState_mCCEDE0A3A6DD8F7E97C11DF024B7345C0DB39451 (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_currentState_10();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0037;
			}
			case 1:
			{
				goto IL_0039;
			}
			case 2:
			{
				goto IL_0039;
			}
			case 3:
			{
				goto IL_003b;
			}
			case 4:
			{
				goto IL_003d;
			}
			case 5:
			{
				goto IL_003f;
			}
			case 6:
			{
				goto IL_003d;
			}
			case 7:
			{
				goto IL_003f;
			}
			case 8:
			{
				goto IL_0041;
			}
			case 9:
			{
				goto IL_0043;
			}
		}
	}
	{
		goto IL_0045;
	}

IL_0037:
	{
		return (int32_t)(0);
	}

IL_0039:
	{
		return (int32_t)(1);
	}

IL_003b:
	{
		return (int32_t)(2);
	}

IL_003d:
	{
		return (int32_t)(3);
	}

IL_003f:
	{
		return (int32_t)(4);
	}

IL_0041:
	{
		return (int32_t)(6);
	}

IL_0043:
	{
		return (int32_t)(5);
	}

IL_0045:
	{
		return (int32_t)(6);
	}
}
// System.Void System.Xml.XmlTextWriter::Close()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_Close_m8D3926EF1A95024EB7FBD9F69C589546238E6610 (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter_Close_m8D3926EF1A95024EB7FBD9F69C589546238E6610_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			XmlTextWriter_AutoCompleteAll_mB42EF751B442A2213ED31F11AAA25A0A39FF302D(__this, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x1F, FINALLY_000b);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
				goto CATCH_0008;
			throw e;
		}

CATCH_0008:
		{ // begin catch(System.Object)
			IL2CPP_LEAVE(0x1F, FINALLY_000b);
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		__this->set_currentState_10(((int32_t)9));
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_0 = __this->get_textWriter_0();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(8 /* System.Void System.IO.TextWriter::Close() */, L_0);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x1F, IL_001f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_001f:
	{
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::AutoComplete(System.Xml.XmlTextWriter_Token)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_AutoComplete_mCFC6565788633CD667823E70E1FD8A9AF241B664 (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, int32_t ___token0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter_AutoComplete_mCFC6565788633CD667823E70E1FD8A9AF241B664_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_currentState_10();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_001a;
		}
	}
	{
		String_t* L_1 = Res_GetString_m47B81D62E4B5E4C48C06BCC7995B9ED5218EE7A2(_stringLiteral2549FF1641A81EF92E721D01E5458A242BD96B83, /*hidden argument*/NULL);
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_2 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m72027D5F1D513C25C05137E203EEED8FD8297706(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, XmlTextWriter_AutoComplete_mCFC6565788633CD667823E70E1FD8A9AF241B664_RuntimeMethod_var);
	}

IL_001a:
	{
		int32_t L_3 = __this->get_currentState_10();
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_004d;
		}
	}
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_4 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)2);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_5 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_il2cpp_TypeInfo_var);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_6 = ((XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields*)il2cpp_codegen_static_fields_for(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_il2cpp_TypeInfo_var))->get_tokenName_25();
		int32_t L_7 = ___token0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		String_t* L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_9);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_9);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_10 = L_5;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_11 = ((XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields*)il2cpp_codegen_static_fields_for(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_il2cpp_TypeInfo_var))->get_stateName_24();
		NullCheck(L_11);
		int32_t L_12 = 8;
		String_t* L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_13);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_13);
		String_t* L_14 = Res_GetString_mBDB7AFD1EB8C2C577012518DC77B8646A3045E78(_stringLiteral307C73F00D17075B311C1EA5BD62B7C512AE2D6C, L_10, /*hidden argument*/NULL);
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_15 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m72027D5F1D513C25C05137E203EEED8FD8297706(L_15, L_14, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15, NULL, XmlTextWriter_AutoComplete_mCFC6565788633CD667823E70E1FD8A9AF241B664_RuntimeMethod_var);
	}

IL_004d:
	{
		StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* L_16 = __this->get_stateTable_9();
		int32_t L_17 = ___token0;
		int32_t L_18 = __this->get_currentState_10();
		NullCheck(L_16);
		int32_t L_19 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_17, (int32_t)8)), (int32_t)L_18));
		int32_t L_20 = (int32_t)(L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_0 = L_20;
		int32_t L_21 = V_0;
		if ((!(((uint32_t)L_21) == ((uint32_t)8))))
		{
			goto IL_0092;
		}
	}
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_22 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)2);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_23 = L_22;
		IL2CPP_RUNTIME_CLASS_INIT(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_il2cpp_TypeInfo_var);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_24 = ((XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields*)il2cpp_codegen_static_fields_for(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_il2cpp_TypeInfo_var))->get_tokenName_25();
		int32_t L_25 = ___token0;
		NullCheck(L_24);
		int32_t L_26 = L_25;
		String_t* L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_27);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_27);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_28 = L_23;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_29 = ((XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields*)il2cpp_codegen_static_fields_for(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_il2cpp_TypeInfo_var))->get_stateName_24();
		int32_t L_30 = __this->get_currentState_10();
		NullCheck(L_29);
		int32_t L_31 = L_30;
		String_t* L_32 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, L_32);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_32);
		String_t* L_33 = Res_GetString_mBDB7AFD1EB8C2C577012518DC77B8646A3045E78(_stringLiteral307C73F00D17075B311C1EA5BD62B7C512AE2D6C, L_28, /*hidden argument*/NULL);
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_34 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m72027D5F1D513C25C05137E203EEED8FD8297706(L_34, L_33, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_34, NULL, XmlTextWriter_AutoComplete_mCFC6565788633CD667823E70E1FD8A9AF241B664_RuntimeMethod_var);
	}

IL_0092:
	{
		int32_t L_35 = ___token0;
		switch (L_35)
		{
			case 0:
			{
				goto IL_00f3;
			}
			case 1:
			{
				goto IL_00d1;
			}
			case 2:
			{
				goto IL_00f3;
			}
			case 3:
			{
				goto IL_00f3;
			}
			case 4:
			{
				goto IL_00f3;
			}
			case 5:
			{
				goto IL_015d;
			}
			case 6:
			{
				goto IL_015d;
			}
			case 7:
			{
				goto IL_01b5;
			}
			case 8:
			{
				goto IL_01fc;
			}
			case 9:
			{
				goto IL_0212;
			}
			case 10:
			{
				goto IL_0212;
			}
			case 11:
			{
				goto IL_0212;
			}
			case 12:
			{
				goto IL_0212;
			}
		}
	}
	{
		goto IL_025c;
	}

IL_00d1:
	{
		bool L_36 = __this->get_indented_4();
		if (!L_36)
		{
			goto IL_026c;
		}
	}
	{
		int32_t L_37 = __this->get_currentState_10();
		if (!L_37)
		{
			goto IL_026c;
		}
	}
	{
		XmlTextWriter_Indent_m5CB04F1718940F0069DD7C9F8E80A1D2134C4267(__this, (bool)0, /*hidden argument*/NULL);
		goto IL_026c;
	}

IL_00f3:
	{
		int32_t L_38 = __this->get_currentState_10();
		if ((!(((uint32_t)L_38) == ((uint32_t)4))))
		{
			goto IL_010b;
		}
	}
	{
		XmlTextWriter_WriteEndAttributeQuote_m057DC07F77F3640F8A59AF53FCC77C6528136490(__this, /*hidden argument*/NULL);
		XmlTextWriter_WriteEndStartTag_m5905B79D7EE7EE0049919B9B879979313D9664FA(__this, (bool)0, /*hidden argument*/NULL);
		goto IL_011b;
	}

IL_010b:
	{
		int32_t L_39 = __this->get_currentState_10();
		if ((!(((uint32_t)L_39) == ((uint32_t)3))))
		{
			goto IL_011b;
		}
	}
	{
		XmlTextWriter_WriteEndStartTag_m5905B79D7EE7EE0049919B9B879979313D9664FA(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_011b:
	{
		int32_t L_40 = ___token0;
		if ((!(((uint32_t)L_40) == ((uint32_t)3))))
		{
			goto IL_013b;
		}
	}
	{
		TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* L_41 = __this->get_stack_7();
		int32_t L_42 = __this->get_top_8();
		NullCheck(L_41);
		((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_42)))->set_mixed_8((bool)1);
		goto IL_026c;
	}

IL_013b:
	{
		bool L_43 = __this->get_indented_4();
		if (!L_43)
		{
			goto IL_026c;
		}
	}
	{
		int32_t L_44 = __this->get_currentState_10();
		if (!L_44)
		{
			goto IL_026c;
		}
	}
	{
		XmlTextWriter_Indent_m5CB04F1718940F0069DD7C9F8E80A1D2134C4267(__this, (bool)0, /*hidden argument*/NULL);
		goto IL_026c;
	}

IL_015d:
	{
		bool L_45 = __this->get_flush_18();
		if (!L_45)
		{
			goto IL_016b;
		}
	}
	{
		XmlTextWriter_FlushEncoders_m131B51A9CECD553DB91EC663D92AFE3D0197B725(__this, /*hidden argument*/NULL);
	}

IL_016b:
	{
		int32_t L_46 = __this->get_currentState_10();
		if ((!(((uint32_t)L_46) == ((uint32_t)4))))
		{
			goto IL_017a;
		}
	}
	{
		XmlTextWriter_WriteEndAttributeQuote_m057DC07F77F3640F8A59AF53FCC77C6528136490(__this, /*hidden argument*/NULL);
	}

IL_017a:
	{
		int32_t L_47 = __this->get_currentState_10();
		if ((!(((uint32_t)L_47) == ((uint32_t)5))))
		{
			goto IL_0188;
		}
	}
	{
		___token0 = 6;
		goto IL_0192;
	}

IL_0188:
	{
		int32_t L_48 = ___token0;
		XmlTextWriter_WriteEndStartTag_m5905B79D7EE7EE0049919B9B879979313D9664FA(__this, (bool)((((int32_t)L_48) == ((int32_t)5))? 1 : 0), /*hidden argument*/NULL);
	}

IL_0192:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_il2cpp_TypeInfo_var);
		StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* L_49 = ((XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields*)il2cpp_codegen_static_fields_for(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_il2cpp_TypeInfo_var))->get_stateTableDocument_27();
		StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* L_50 = __this->get_stateTable_9();
		if ((!(((RuntimeObject*)(StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2*)L_49) == ((RuntimeObject*)(StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2*)L_50))))
		{
			goto IL_026c;
		}
	}
	{
		int32_t L_51 = __this->get_top_8();
		if ((!(((uint32_t)L_51) == ((uint32_t)1))))
		{
			goto IL_026c;
		}
	}
	{
		V_0 = 7;
		goto IL_026c;
	}

IL_01b5:
	{
		bool L_52 = __this->get_flush_18();
		if (!L_52)
		{
			goto IL_01c3;
		}
	}
	{
		XmlTextWriter_FlushEncoders_m131B51A9CECD553DB91EC663D92AFE3D0197B725(__this, /*hidden argument*/NULL);
	}

IL_01c3:
	{
		int32_t L_53 = __this->get_currentState_10();
		if ((!(((uint32_t)L_53) == ((uint32_t)4))))
		{
			goto IL_01e4;
		}
	}
	{
		XmlTextWriter_WriteEndAttributeQuote_m057DC07F77F3640F8A59AF53FCC77C6528136490(__this, /*hidden argument*/NULL);
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_54 = __this->get_textWriter_0();
		NullCheck(L_54);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_54, ((int32_t)32));
		goto IL_026c;
	}

IL_01e4:
	{
		int32_t L_55 = __this->get_currentState_10();
		if ((!(((uint32_t)L_55) == ((uint32_t)3))))
		{
			goto IL_026c;
		}
	}
	{
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_56 = __this->get_textWriter_0();
		NullCheck(L_56);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_56, ((int32_t)32));
		goto IL_026c;
	}

IL_01fc:
	{
		bool L_57 = __this->get_flush_18();
		if (!L_57)
		{
			goto IL_020a;
		}
	}
	{
		XmlTextWriter_FlushEncoders_m131B51A9CECD553DB91EC663D92AFE3D0197B725(__this, /*hidden argument*/NULL);
	}

IL_020a:
	{
		XmlTextWriter_WriteEndAttributeQuote_m057DC07F77F3640F8A59AF53FCC77C6528136490(__this, /*hidden argument*/NULL);
		goto IL_026c;
	}

IL_0212:
	{
		int32_t L_58 = ___token0;
		if ((((int32_t)L_58) == ((int32_t)((int32_t)10))))
		{
			goto IL_0225;
		}
	}
	{
		bool L_59 = __this->get_flush_18();
		if (!L_59)
		{
			goto IL_0225;
		}
	}
	{
		XmlTextWriter_FlushEncoders_m131B51A9CECD553DB91EC663D92AFE3D0197B725(__this, /*hidden argument*/NULL);
	}

IL_0225:
	{
		int32_t L_60 = __this->get_currentState_10();
		if ((!(((uint32_t)L_60) == ((uint32_t)3))))
		{
			goto IL_023f;
		}
	}
	{
		int32_t L_61 = __this->get_lastToken_11();
		if ((((int32_t)L_61) == ((int32_t)((int32_t)9))))
		{
			goto IL_023f;
		}
	}
	{
		XmlTextWriter_WriteEndStartTag_m5905B79D7EE7EE0049919B9B879979313D9664FA(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_023f:
	{
		int32_t L_62 = V_0;
		if ((!(((uint32_t)L_62) == ((uint32_t)5))))
		{
			goto IL_026c;
		}
	}
	{
		TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* L_63 = __this->get_stack_7();
		int32_t L_64 = __this->get_top_8();
		NullCheck(L_63);
		((L_63)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_64)))->set_mixed_8((bool)1);
		goto IL_026c;
	}

IL_025c:
	{
		String_t* L_65 = Res_GetString_m47B81D62E4B5E4C48C06BCC7995B9ED5218EE7A2(_stringLiteral29A6E802123FF6EA94EC6F96DDA470B3FA755A58, /*hidden argument*/NULL);
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_66 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m72027D5F1D513C25C05137E203EEED8FD8297706(L_66, L_65, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_66, NULL, XmlTextWriter_AutoComplete_mCFC6565788633CD667823E70E1FD8A9AF241B664_RuntimeMethod_var);
	}

IL_026c:
	{
		int32_t L_67 = V_0;
		__this->set_currentState_10(L_67);
		int32_t L_68 = ___token0;
		__this->set_lastToken_11(L_68);
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::AutoCompleteAll()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_AutoCompleteAll_mB42EF751B442A2213ED31F11AAA25A0A39FF302D (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_flush_18();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		XmlTextWriter_FlushEncoders_m131B51A9CECD553DB91EC663D92AFE3D0197B725(__this, /*hidden argument*/NULL);
		goto IL_0016;
	}

IL_0010:
	{
		VirtActionInvoker0::Invoke(5 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, __this);
	}

IL_0016:
	{
		int32_t L_1 = __this->get_top_8();
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_0010;
		}
	}
	{
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::InternalWriteEndElement(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_InternalWriteEndElement_m02FCE5CCA71DBFBA885AF81FFAD702B3BC39294F (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, bool ___longFormat0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter_InternalWriteEndElement_m02FCE5CCA71DBFBA885AF81FFAD702B3BC39294F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * G_B4_0 = NULL;
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * G_B3_0 = NULL;
	int32_t G_B5_0 = 0;
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * G_B5_1 = NULL;

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = __this->get_top_8();
			if ((((int32_t)L_0) > ((int32_t)0)))
			{
				goto IL_0019;
			}
		}

IL_0009:
		{
			String_t* L_1 = Res_GetString_m47B81D62E4B5E4C48C06BCC7995B9ED5218EE7A2(_stringLiteral936848C6D2B08E4C0D182C45261D876982BCFE29, /*hidden argument*/NULL);
			InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_2 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m72027D5F1D513C25C05137E203EEED8FD8297706(L_2, L_1, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, XmlTextWriter_InternalWriteEndElement_m02FCE5CCA71DBFBA885AF81FFAD702B3BC39294F_RuntimeMethod_var);
		}

IL_0019:
		{
			bool L_3 = ___longFormat0;
			G_B3_0 = __this;
			if (L_3)
			{
				G_B4_0 = __this;
				goto IL_0020;
			}
		}

IL_001d:
		{
			G_B5_0 = 5;
			G_B5_1 = G_B3_0;
			goto IL_0021;
		}

IL_0020:
		{
			G_B5_0 = 6;
			G_B5_1 = G_B4_0;
		}

IL_0021:
		{
			NullCheck(G_B5_1);
			XmlTextWriter_AutoComplete_mCFC6565788633CD667823E70E1FD8A9AF241B664(G_B5_1, G_B5_0, /*hidden argument*/NULL);
			int32_t L_4 = __this->get_lastToken_11();
			if ((!(((uint32_t)L_4) == ((uint32_t)6))))
			{
				goto IL_00d7;
			}
		}

IL_0032:
		{
			bool L_5 = __this->get_indented_4();
			if (!L_5)
			{
				goto IL_0041;
			}
		}

IL_003a:
		{
			XmlTextWriter_Indent_m5CB04F1718940F0069DD7C9F8E80A1D2134C4267(__this, (bool)1, /*hidden argument*/NULL);
		}

IL_0041:
		{
			TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_6 = __this->get_textWriter_0();
			NullCheck(L_6);
			VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_6, ((int32_t)60));
			TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_7 = __this->get_textWriter_0();
			NullCheck(L_7);
			VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_7, ((int32_t)47));
			bool L_8 = __this->get_namespaces_15();
			if (!L_8)
			{
				goto IL_00a9;
			}
		}

IL_0063:
		{
			TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* L_9 = __this->get_stack_7();
			int32_t L_10 = __this->get_top_8();
			NullCheck(L_9);
			String_t* L_11 = ((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_10)))->get_prefix_1();
			if (!L_11)
			{
				goto IL_00a9;
			}
		}

IL_007b:
		{
			TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_12 = __this->get_textWriter_0();
			TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* L_13 = __this->get_stack_7();
			int32_t L_14 = __this->get_top_8();
			NullCheck(L_13);
			String_t* L_15 = ((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_14)))->get_prefix_1();
			NullCheck(L_12);
			VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_12, L_15);
			TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_16 = __this->get_textWriter_0();
			NullCheck(L_16);
			VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_16, ((int32_t)58));
		}

IL_00a9:
		{
			TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_17 = __this->get_textWriter_0();
			TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* L_18 = __this->get_stack_7();
			int32_t L_19 = __this->get_top_8();
			NullCheck(L_18);
			String_t* L_20 = ((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_19)))->get_name_0();
			NullCheck(L_17);
			VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_17, L_20);
			TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_21 = __this->get_textWriter_0();
			NullCheck(L_21);
			VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_21, ((int32_t)62));
		}

IL_00d7:
		{
			TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* L_22 = __this->get_stack_7();
			int32_t L_23 = __this->get_top_8();
			NullCheck(L_22);
			int32_t L_24 = ((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_23)))->get_prevNsTop_6();
			V_0 = L_24;
			bool L_25 = __this->get_useNsHashtable_22();
			if (!L_25)
			{
				goto IL_010e;
			}
		}

IL_00f6:
		{
			int32_t L_26 = V_0;
			int32_t L_27 = __this->get_nsTop_20();
			if ((((int32_t)L_26) >= ((int32_t)L_27)))
			{
				goto IL_010e;
			}
		}

IL_00ff:
		{
			int32_t L_28 = V_0;
			int32_t L_29 = __this->get_nsTop_20();
			XmlTextWriter_PopNamespaces_m5D748E8059ED8B3F04DB600618EE9A7CA3A2F158(__this, ((int32_t)il2cpp_codegen_add((int32_t)L_28, (int32_t)1)), L_29, /*hidden argument*/NULL);
		}

IL_010e:
		{
			int32_t L_30 = V_0;
			__this->set_nsTop_20(L_30);
			int32_t L_31 = __this->get_top_8();
			__this->set_top_8(((int32_t)il2cpp_codegen_subtract((int32_t)L_31, (int32_t)1)));
			goto IL_012f;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0125;
		throw e;
	}

CATCH_0125:
	{ // begin catch(System.Object)
		__this->set_currentState_10(8);
		IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local, NULL, XmlTextWriter_InternalWriteEndElement_m02FCE5CCA71DBFBA885AF81FFAD702B3BC39294F_RuntimeMethod_var);
	} // end catch (depth: 1)

IL_012f:
	{
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::WriteEndStartTag(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_WriteEndStartTag_m5905B79D7EE7EE0049919B9B879979313D9664FA (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, bool ___empty0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter_WriteEndStartTag_m5905B79D7EE7EE0049919B9B879979313D9664FA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * L_0 = __this->get_xmlEncoder_1();
		NullCheck(L_0);
		XmlTextEncoder_StartAttribute_mAFEF454A34E72F5B1457C8D634FA947EA0023D15(L_0, (bool)0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_nsTop_20();
		V_0 = L_1;
		goto IL_00b6;
	}

IL_0018:
	{
		NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* L_2 = __this->get_nsStack_19();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		bool L_4 = ((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3)))->get_declared_2();
		if (L_4)
		{
			goto IL_00b2;
		}
	}
	{
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_5 = __this->get_textWriter_0();
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_5, _stringLiteralEB8431F8E06C8492F2806E7A7EAA8A4A288D55D1);
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_6 = __this->get_textWriter_0();
		NullCheck(L_6);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_6, ((int32_t)58));
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_7 = __this->get_textWriter_0();
		NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* L_8 = __this->get_nsStack_19();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		String_t* L_10 = ((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9)))->get_prefix_0();
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_7, L_10);
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_11 = __this->get_textWriter_0();
		NullCheck(L_11);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_11, ((int32_t)61));
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_12 = __this->get_textWriter_0();
		Il2CppChar L_13 = __this->get_quoteChar_13();
		NullCheck(L_12);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_12, L_13);
		XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * L_14 = __this->get_xmlEncoder_1();
		NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* L_15 = __this->get_nsStack_19();
		int32_t L_16 = V_0;
		NullCheck(L_15);
		String_t* L_17 = ((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_16)))->get_ns_1();
		NullCheck(L_14);
		XmlTextEncoder_Write_mD02582AB249921681B2B7C85E71DC17E20AB3884(L_14, L_17, /*hidden argument*/NULL);
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_18 = __this->get_textWriter_0();
		Il2CppChar L_19 = __this->get_quoteChar_13();
		NullCheck(L_18);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_18, L_19);
	}

IL_00b2:
	{
		int32_t L_20 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_20, (int32_t)1));
	}

IL_00b6:
	{
		int32_t L_21 = V_0;
		TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* L_22 = __this->get_stack_7();
		int32_t L_23 = __this->get_top_8();
		NullCheck(L_22);
		int32_t L_24 = ((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_23)))->get_prevNsTop_6();
		if ((((int32_t)L_21) > ((int32_t)L_24)))
		{
			goto IL_0018;
		}
	}
	{
		TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* L_25 = __this->get_stack_7();
		int32_t L_26 = __this->get_top_8();
		NullCheck(L_25);
		String_t* L_27 = ((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))->get_defaultNs_2();
		TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* L_28 = __this->get_stack_7();
		int32_t L_29 = __this->get_top_8();
		NullCheck(L_28);
		String_t* L_30 = ((L_28)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_subtract((int32_t)L_29, (int32_t)1)))))->get_defaultNs_2();
		bool L_31 = String_op_Inequality_m0BD184A74F453A72376E81CC6CAEE2556B80493E(L_27, L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_019a;
		}
	}
	{
		TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* L_32 = __this->get_stack_7();
		int32_t L_33 = __this->get_top_8();
		NullCheck(L_32);
		int32_t L_34 = ((L_32)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33)))->get_defaultNsState_3();
		if ((!(((uint32_t)L_34) == ((uint32_t)2))))
		{
			goto IL_019a;
		}
	}
	{
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_35 = __this->get_textWriter_0();
		NullCheck(L_35);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_35, _stringLiteralEB8431F8E06C8492F2806E7A7EAA8A4A288D55D1);
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_36 = __this->get_textWriter_0();
		NullCheck(L_36);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_36, ((int32_t)61));
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_37 = __this->get_textWriter_0();
		Il2CppChar L_38 = __this->get_quoteChar_13();
		NullCheck(L_37);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_37, L_38);
		XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * L_39 = __this->get_xmlEncoder_1();
		TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* L_40 = __this->get_stack_7();
		int32_t L_41 = __this->get_top_8();
		NullCheck(L_40);
		String_t* L_42 = ((L_40)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_41)))->get_defaultNs_2();
		NullCheck(L_39);
		XmlTextEncoder_Write_mD02582AB249921681B2B7C85E71DC17E20AB3884(L_39, L_42, /*hidden argument*/NULL);
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_43 = __this->get_textWriter_0();
		Il2CppChar L_44 = __this->get_quoteChar_13();
		NullCheck(L_43);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_43, L_44);
		TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* L_45 = __this->get_stack_7();
		int32_t L_46 = __this->get_top_8();
		NullCheck(L_45);
		((L_45)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_46)))->set_defaultNsState_3(3);
	}

IL_019a:
	{
		XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * L_47 = __this->get_xmlEncoder_1();
		NullCheck(L_47);
		XmlTextEncoder_EndAttribute_m5988FFECF6A66F36D9079FD6CA67BB56A2A98D66(L_47, /*hidden argument*/NULL);
		bool L_48 = ___empty0;
		if (!L_48)
		{
			goto IL_01b8;
		}
	}
	{
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_49 = __this->get_textWriter_0();
		NullCheck(L_49);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_49, _stringLiteralCBD550861E744B88C938D241E09AD55250E11DB4);
	}

IL_01b8:
	{
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_50 = __this->get_textWriter_0();
		NullCheck(L_50);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_50, ((int32_t)62));
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::WriteEndAttributeQuote()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_WriteEndAttributeQuote_m057DC07F77F3640F8A59AF53FCC77C6528136490 (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_specialAttr_16();
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		XmlTextWriter_HandleSpecialAttribute_m8AFEBFA51F9B23D67C213F35E0ADF91171DB0ECB(__this, /*hidden argument*/NULL);
	}

IL_000e:
	{
		XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * L_1 = __this->get_xmlEncoder_1();
		NullCheck(L_1);
		XmlTextEncoder_EndAttribute_m5988FFECF6A66F36D9079FD6CA67BB56A2A98D66(L_1, /*hidden argument*/NULL);
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_2 = __this->get_textWriter_0();
		Il2CppChar L_3 = __this->get_curQuoteChar_14();
		NullCheck(L_2);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_2, L_3);
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::Indent(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_Indent_m5CB04F1718940F0069DD7C9F8E80A1D2134C4267 (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, bool ___beforeEndElement0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t G_B6_0 = 0;
	{
		int32_t L_0 = __this->get_top_8();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_1 = __this->get_textWriter_0();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(18 /* System.Void System.IO.TextWriter::WriteLine() */, L_1);
		return;
	}

IL_0014:
	{
		TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* L_2 = __this->get_stack_7();
		int32_t L_3 = __this->get_top_8();
		NullCheck(L_2);
		bool L_4 = ((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3)))->get_mixed_8();
		if (L_4)
		{
			goto IL_006f;
		}
	}
	{
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_5 = __this->get_textWriter_0();
		NullCheck(L_5);
		VirtActionInvoker0::Invoke(18 /* System.Void System.IO.TextWriter::WriteLine() */, L_5);
		bool L_6 = ___beforeEndElement0;
		if (L_6)
		{
			goto IL_0042;
		}
	}
	{
		int32_t L_7 = __this->get_top_8();
		G_B6_0 = L_7;
		goto IL_004a;
	}

IL_0042:
	{
		int32_t L_8 = __this->get_top_8();
		G_B6_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_8, (int32_t)1));
	}

IL_004a:
	{
		V_0 = G_B6_0;
		int32_t L_9 = V_0;
		int32_t L_10 = __this->get_indentation_5();
		V_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_9, (int32_t)L_10));
		goto IL_006b;
	}

IL_0056:
	{
		TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * L_11 = __this->get_textWriter_0();
		Il2CppChar L_12 = __this->get_indentChar_6();
		NullCheck(L_11);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_11, L_12);
		int32_t L_13 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_13, (int32_t)1));
	}

IL_006b:
	{
		int32_t L_14 = V_0;
		if ((((int32_t)L_14) > ((int32_t)0)))
		{
			goto IL_0056;
		}
	}

IL_006f:
	{
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::PushNamespace(System.String,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_PushNamespace_m3DA0494C202BD5C6AE32E9C5FD7FEAC6B4ED8548 (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, String_t* ___prefix0, String_t* ___ns1, bool ___declared2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter_PushNamespace_m3DA0494C202BD5C6AE32E9C5FD7FEAC6B4ED8548_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5 * G_B9_0 = NULL;
	TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5 * G_B8_0 = NULL;
	int32_t G_B10_0 = 0;
	TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5 * G_B10_1 = NULL;
	{
		String_t* L_0 = ___ns1;
		bool L_1 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(_stringLiteralA27E2E9EF6A0C7CC58D71302FCA9E93BA677C130, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		String_t* L_2 = Res_GetString_m47B81D62E4B5E4C48C06BCC7995B9ED5218EE7A2(_stringLiteral7DC5BBB566674E5C157A33F9829DE3FCACCA44BD, /*hidden argument*/NULL);
		ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * L_3 = (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 *)il2cpp_codegen_object_new(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m9A85EF7FEFEC21DDD525A67E831D77278E5165B7(L_3, L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, NULL, XmlTextWriter_PushNamespace_m3DA0494C202BD5C6AE32E9C5FD7FEAC6B4ED8548_RuntimeMethod_var);
	}

IL_001d:
	{
		String_t* L_4 = ___prefix0;
		if (L_4)
		{
			goto IL_0075;
		}
	}
	{
		TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* L_5 = __this->get_stack_7();
		int32_t L_6 = __this->get_top_8();
		NullCheck(L_5);
		int32_t L_7 = ((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_defaultNsState_3();
		V_0 = L_7;
		int32_t L_8 = V_0;
		if ((!(((uint32_t)L_8) > ((uint32_t)1))))
		{
			goto IL_0040;
		}
	}
	{
		int32_t L_9 = V_0;
		if ((((int32_t)L_9) == ((int32_t)2)))
		{
			goto IL_0057;
		}
	}
	{
		return;
	}

IL_0040:
	{
		TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* L_10 = __this->get_stack_7();
		int32_t L_11 = __this->get_top_8();
		NullCheck(L_10);
		String_t* L_12 = ___ns1;
		((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->set_defaultNs_2(L_12);
	}

IL_0057:
	{
		TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* L_13 = __this->get_stack_7();
		int32_t L_14 = __this->get_top_8();
		NullCheck(L_13);
		bool L_15 = ___declared2;
		G_B8_0 = ((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_14)));
		if (L_15)
		{
			G_B9_0 = ((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_14)));
			goto IL_006e;
		}
	}
	{
		G_B10_0 = 2;
		G_B10_1 = G_B8_0;
		goto IL_006f;
	}

IL_006e:
	{
		G_B10_0 = 3;
		G_B10_1 = G_B9_0;
	}

IL_006f:
	{
		G_B10_1->set_defaultNsState_3(G_B10_0);
		return;
	}

IL_0075:
	{
		String_t* L_16 = ___prefix0;
		NullCheck(L_16);
		int32_t L_17 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018_inline(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0095;
		}
	}
	{
		String_t* L_18 = ___ns1;
		NullCheck(L_18);
		int32_t L_19 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018_inline(L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0095;
		}
	}
	{
		String_t* L_20 = Res_GetString_m47B81D62E4B5E4C48C06BCC7995B9ED5218EE7A2(_stringLiteral1760FCF206FD55ED5034D5189D50BD82B9A45259, /*hidden argument*/NULL);
		ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * L_21 = (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 *)il2cpp_codegen_object_new(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m9A85EF7FEFEC21DDD525A67E831D77278E5165B7(L_21, L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21, NULL, XmlTextWriter_PushNamespace_m3DA0494C202BD5C6AE32E9C5FD7FEAC6B4ED8548_RuntimeMethod_var);
	}

IL_0095:
	{
		String_t* L_22 = ___prefix0;
		int32_t L_23 = XmlTextWriter_LookupNamespace_mA477CDBF25662EC77FF5B025FB2E83B400C381FD(__this, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		int32_t L_24 = V_1;
		if ((((int32_t)L_24) == ((int32_t)(-1))))
		{
			goto IL_00d0;
		}
	}
	{
		NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* L_25 = __this->get_nsStack_19();
		int32_t L_26 = V_1;
		NullCheck(L_25);
		String_t* L_27 = ((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))->get_ns_1();
		String_t* L_28 = ___ns1;
		bool L_29 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_27, L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00d0;
		}
	}
	{
		bool L_30 = ___declared2;
		if (!L_30)
		{
			goto IL_010b;
		}
	}
	{
		NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* L_31 = __this->get_nsStack_19();
		int32_t L_32 = V_1;
		NullCheck(L_31);
		((L_31)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_32)))->set_declared_2((bool)1);
		return;
	}

IL_00d0:
	{
		bool L_33 = ___declared2;
		if (!L_33)
		{
			goto IL_0102;
		}
	}
	{
		int32_t L_34 = V_1;
		if ((((int32_t)L_34) == ((int32_t)(-1))))
		{
			goto IL_0102;
		}
	}
	{
		int32_t L_35 = V_1;
		TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* L_36 = __this->get_stack_7();
		int32_t L_37 = __this->get_top_8();
		NullCheck(L_36);
		int32_t L_38 = ((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37)))->get_prevNsTop_6();
		if ((((int32_t)L_35) <= ((int32_t)L_38)))
		{
			goto IL_0102;
		}
	}
	{
		NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* L_39 = __this->get_nsStack_19();
		int32_t L_40 = V_1;
		NullCheck(L_39);
		((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40)))->set_declared_2((bool)1);
	}

IL_0102:
	{
		String_t* L_41 = ___prefix0;
		String_t* L_42 = ___ns1;
		bool L_43 = ___declared2;
		XmlTextWriter_AddNamespace_m78549A0DDF0923A00664A6DE1B425157097E5ED0(__this, L_41, L_42, L_43, /*hidden argument*/NULL);
	}

IL_010b:
	{
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::AddNamespace(System.String,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_AddNamespace_m78549A0DDF0923A00664A6DE1B425157097E5ED0 (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, String_t* ___prefix0, String_t* ___ns1, bool ___declared2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter_AddNamespace_m78549A0DDF0923A00664A6DE1B425157097E5ED0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* V_2 = NULL;
	int32_t V_3 = 0;
	{
		int32_t L_0 = __this->get_nsTop_20();
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)1));
		int32_t L_1 = V_1;
		__this->set_nsTop_20(L_1);
		int32_t L_2 = V_1;
		V_0 = L_2;
		int32_t L_3 = V_0;
		NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* L_4 = __this->get_nsStack_19();
		NullCheck(L_4);
		if ((!(((uint32_t)L_3) == ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_5 = V_0;
		NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* L_6 = (NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829*)(NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829*)SZArrayNew(NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_5, (int32_t)2)));
		V_2 = L_6;
		NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* L_7 = __this->get_nsStack_19();
		NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* L_8 = V_2;
		int32_t L_9 = V_0;
		Array_Copy_m2D96731C600DE8A167348CA8BA796344E64F7434((RuntimeArray *)(RuntimeArray *)L_7, (RuntimeArray *)(RuntimeArray *)L_8, L_9, /*hidden argument*/NULL);
		NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* L_10 = V_2;
		__this->set_nsStack_19(L_10);
	}

IL_003a:
	{
		NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* L_11 = __this->get_nsStack_19();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		String_t* L_13 = ___prefix0;
		String_t* L_14 = ___ns1;
		bool L_15 = ___declared2;
		Namespace_Set_mEF74FB920EF6CB43BAFDC1FFA58AA95235177979((Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12))), L_13, L_14, L_15, /*hidden argument*/NULL);
		bool L_16 = __this->get_useNsHashtable_22();
		if (!L_16)
		{
			goto IL_005e;
		}
	}
	{
		int32_t L_17 = V_0;
		XmlTextWriter_AddToNamespaceHashtable_mEF08299106F8B3D846EC6D474D02508D5664D429(__this, L_17, /*hidden argument*/NULL);
		return;
	}

IL_005e:
	{
		int32_t L_18 = V_0;
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_008d;
		}
	}
	{
		SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3 * L_19 = (SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3 *)il2cpp_codegen_object_new(SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_il2cpp_TypeInfo_var);
		SecureStringHasher__ctor_mAAE07435BFBE6F3C9A4840F845DE0EB38EA73AD7(L_19, /*hidden argument*/NULL);
		Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * L_20 = (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)il2cpp_codegen_object_new(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mF6BE1085F30E8C47064A8E0583DABCBE0B35DC21(L_20, L_19, /*hidden argument*/Dictionary_2__ctor_mF6BE1085F30E8C47064A8E0583DABCBE0B35DC21_RuntimeMethod_var);
		__this->set_nsHashtable_21(L_20);
		V_3 = 0;
		goto IL_0082;
	}

IL_0077:
	{
		int32_t L_21 = V_3;
		XmlTextWriter_AddToNamespaceHashtable_mEF08299106F8B3D846EC6D474D02508D5664D429(__this, L_21, /*hidden argument*/NULL);
		int32_t L_22 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
	}

IL_0082:
	{
		int32_t L_23 = V_3;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) <= ((int32_t)L_24)))
		{
			goto IL_0077;
		}
	}
	{
		__this->set_useNsHashtable_22((bool)1);
	}

IL_008d:
	{
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::AddToNamespaceHashtable(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_AddToNamespaceHashtable_mEF08299106F8B3D846EC6D474D02508D5664D429 (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, int32_t ___namespaceIndex0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter_AddToNamespaceHashtable_mEF08299106F8B3D846EC6D474D02508D5664D429_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* L_0 = __this->get_nsStack_19();
		int32_t L_1 = ___namespaceIndex0;
		NullCheck(L_0);
		String_t* L_2 = ((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_prefix_0();
		V_0 = L_2;
		Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * L_3 = __this->get_nsHashtable_21();
		String_t* L_4 = V_0;
		NullCheck(L_3);
		bool L_5 = Dictionary_2_TryGetValue_m3B7C9CE4CBBCB908CD81487D7924E93E9309FB67(L_3, L_4, (int32_t*)(&V_1), /*hidden argument*/Dictionary_2_TryGetValue_m3B7C9CE4CBBCB908CD81487D7924E93E9309FB67_RuntimeMethod_var);
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* L_6 = __this->get_nsStack_19();
		int32_t L_7 = ___namespaceIndex0;
		NullCheck(L_6);
		int32_t L_8 = V_1;
		((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_7)))->set_prevNsIndex_3(L_8);
	}

IL_0034:
	{
		Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * L_9 = __this->get_nsHashtable_21();
		String_t* L_10 = V_0;
		int32_t L_11 = ___namespaceIndex0;
		NullCheck(L_9);
		Dictionary_2_set_Item_mDBA4C5D99B9605672D2F04AF3184C5E043D1BD3A(L_9, L_10, L_11, /*hidden argument*/Dictionary_2_set_Item_mDBA4C5D99B9605672D2F04AF3184C5E043D1BD3A_RuntimeMethod_var);
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::PopNamespaces(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_PopNamespaces_m5D748E8059ED8B3F04DB600618EE9A7CA3A2F158 (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, int32_t ___indexFrom0, int32_t ___indexTo1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter_PopNamespaces_m5D748E8059ED8B3F04DB600618EE9A7CA3A2F158_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___indexTo1;
		V_0 = L_0;
		goto IL_0068;
	}

IL_0004:
	{
		NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* L_1 = __this->get_nsStack_19();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = ((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2)))->get_prevNsIndex_3();
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_0037;
		}
	}
	{
		Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * L_4 = __this->get_nsHashtable_21();
		NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* L_5 = __this->get_nsStack_19();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		String_t* L_7 = ((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_prefix_0();
		NullCheck(L_4);
		Dictionary_2_Remove_mF764B8FFA63013272F75AFEF5F54C423F340D771(L_4, L_7, /*hidden argument*/Dictionary_2_Remove_mF764B8FFA63013272F75AFEF5F54C423F340D771_RuntimeMethod_var);
		goto IL_0064;
	}

IL_0037:
	{
		Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * L_8 = __this->get_nsHashtable_21();
		NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* L_9 = __this->get_nsStack_19();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		String_t* L_11 = ((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_10)))->get_prefix_0();
		NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* L_12 = __this->get_nsStack_19();
		int32_t L_13 = V_0;
		NullCheck(L_12);
		int32_t L_14 = ((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_prevNsIndex_3();
		NullCheck(L_8);
		Dictionary_2_set_Item_mDBA4C5D99B9605672D2F04AF3184C5E043D1BD3A(L_8, L_11, L_14, /*hidden argument*/Dictionary_2_set_Item_mDBA4C5D99B9605672D2F04AF3184C5E043D1BD3A_RuntimeMethod_var);
	}

IL_0064:
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_15, (int32_t)1));
	}

IL_0068:
	{
		int32_t L_16 = V_0;
		int32_t L_17 = ___indexFrom0;
		if ((((int32_t)L_16) >= ((int32_t)L_17)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Int32 System.Xml.XmlTextWriter::LookupNamespace(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t XmlTextWriter_LookupNamespace_mA477CDBF25662EC77FF5B025FB2E83B400C381FD (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, String_t* ___prefix0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter_LookupNamespace_mA477CDBF25662EC77FF5B025FB2E83B400C381FD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		bool L_0 = __this->get_useNsHashtable_22();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * L_1 = __this->get_nsHashtable_21();
		String_t* L_2 = ___prefix0;
		NullCheck(L_1);
		bool L_3 = Dictionary_2_TryGetValue_m3B7C9CE4CBBCB908CD81487D7924E93E9309FB67(L_1, L_2, (int32_t*)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m3B7C9CE4CBBCB908CD81487D7924E93E9309FB67_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_4 = V_0;
		return L_4;
	}

IL_001a:
	{
		int32_t L_5 = __this->get_nsTop_20();
		V_1 = L_5;
		goto IL_0042;
	}

IL_0023:
	{
		NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* L_6 = __this->get_nsStack_19();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		String_t* L_8 = ((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_7)))->get_prefix_0();
		String_t* L_9 = ___prefix0;
		bool L_10 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_11 = V_1;
		return L_11;
	}

IL_003e:
	{
		int32_t L_12 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)1));
	}

IL_0042:
	{
		int32_t L_13 = V_1;
		if ((((int32_t)L_13) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}

IL_0046:
	{
		return (-1);
	}
}
// System.Void System.Xml.XmlTextWriter::HandleSpecialAttribute()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_HandleSpecialAttribute_m8AFEBFA51F9B23D67C213F35E0ADF91171DB0ECB (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter_HandleSpecialAttribute_m8AFEBFA51F9B23D67C213F35E0ADF91171DB0ECB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * L_0 = __this->get_xmlEncoder_1();
		NullCheck(L_0);
		String_t* L_1 = XmlTextEncoder_get_AttributeValue_m7D58DB09133999166C984994CA05C48B469DEC7D(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = __this->get_specialAttr_16();
		V_1 = L_2;
		int32_t L_3 = V_1;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_3, (int32_t)1)))
		{
			case 0:
			{
				goto IL_0040;
			}
			case 1:
			{
				goto IL_0028;
			}
			case 2:
			{
				goto IL_00ab;
			}
		}
	}
	{
		return;
	}

IL_0028:
	{
		TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* L_4 = __this->get_stack_7();
		int32_t L_5 = __this->get_top_8();
		NullCheck(L_4);
		String_t* L_6 = V_0;
		((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5)))->set_xmlLang_5(L_6);
		return;
	}

IL_0040:
	{
		String_t* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t0ED1642D9D0E4F0F7CB233F75C42460578F8ABF7_il2cpp_TypeInfo_var);
		String_t* L_8 = XmlConvert_TrimString_m89152D6729B89C0423168B5C60E0191A773AD1FA(L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		String_t* L_9 = V_0;
		bool L_10 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_9, _stringLiteral7505D64A54E061B7ACD54CCD58B49DC43500B635, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006c;
		}
	}
	{
		TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* L_11 = __this->get_stack_7();
		int32_t L_12 = __this->get_top_8();
		NullCheck(L_11);
		((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))->set_xmlSpace_4(1);
		return;
	}

IL_006c:
	{
		String_t* L_13 = V_0;
		bool L_14 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_13, _stringLiteralE900984DC91DC9C7F107CA96EA386473DA13D8F8, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0091;
		}
	}
	{
		TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* L_15 = __this->get_stack_7();
		int32_t L_16 = __this->get_top_8();
		NullCheck(L_15);
		((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_16)))->set_xmlSpace_4(2);
		return;
	}

IL_0091:
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_17 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_18 = L_17;
		String_t* L_19 = V_0;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_19);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_19);
		String_t* L_20 = Res_GetString_mBDB7AFD1EB8C2C577012518DC77B8646A3045E78(_stringLiteralF2B44AFF371DB997AA89F469BF68B7C11526B71D, L_18, /*hidden argument*/NULL);
		ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * L_21 = (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 *)il2cpp_codegen_object_new(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m9A85EF7FEFEC21DDD525A67E831D77278E5165B7(L_21, L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21, NULL, XmlTextWriter_HandleSpecialAttribute_m8AFEBFA51F9B23D67C213F35E0ADF91171DB0ECB_RuntimeMethod_var);
	}

IL_00ab:
	{
		String_t* L_22 = __this->get_prefixForXmlNs_17();
		String_t* L_23 = V_0;
		XmlTextWriter_VerifyPrefixXml_m44A57489D6DFFAD820FE4DF6B113DB3B58E6F8E0(__this, L_22, L_23, /*hidden argument*/NULL);
		String_t* L_24 = __this->get_prefixForXmlNs_17();
		String_t* L_25 = V_0;
		XmlTextWriter_PushNamespace_m3DA0494C202BD5C6AE32E9C5FD7FEAC6B4ED8548(__this, L_24, L_25, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::VerifyPrefixXml(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_VerifyPrefixXml_m44A57489D6DFFAD820FE4DF6B113DB3B58E6F8E0 (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, String_t* ___prefix0, String_t* ___ns1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter_VerifyPrefixXml_m44A57489D6DFFAD820FE4DF6B113DB3B58E6F8E0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___prefix0;
		if (!L_0)
		{
			goto IL_006b;
		}
	}
	{
		String_t* L_1 = ___prefix0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018_inline(L_1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)3))))
		{
			goto IL_006b;
		}
	}
	{
		String_t* L_3 = ___prefix0;
		NullCheck(L_3);
		Il2CppChar L_4 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_3, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)((int32_t)120))))
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_5 = ___prefix0;
		NullCheck(L_5);
		Il2CppChar L_6 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_5, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)88)))))
		{
			goto IL_006b;
		}
	}

IL_0022:
	{
		String_t* L_7 = ___prefix0;
		NullCheck(L_7);
		Il2CppChar L_8 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_7, 1, /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)((int32_t)109))))
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_9 = ___prefix0;
		NullCheck(L_9);
		Il2CppChar L_10 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_9, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)77)))))
		{
			goto IL_006b;
		}
	}

IL_0038:
	{
		String_t* L_11 = ___prefix0;
		NullCheck(L_11);
		Il2CppChar L_12 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_11, 2, /*hidden argument*/NULL);
		if ((((int32_t)L_12) == ((int32_t)((int32_t)108))))
		{
			goto IL_004e;
		}
	}
	{
		String_t* L_13 = ___prefix0;
		NullCheck(L_13);
		Il2CppChar L_14 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_13, 2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)76)))))
		{
			goto IL_006b;
		}
	}

IL_004e:
	{
		String_t* L_15 = ___ns1;
		bool L_16 = String_op_Inequality_m0BD184A74F453A72376E81CC6CAEE2556B80493E(_stringLiteral7859E7AFFA569B5D7ACA069908DBD2ED9F05629B, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_006b;
		}
	}
	{
		String_t* L_17 = Res_GetString_m47B81D62E4B5E4C48C06BCC7995B9ED5218EE7A2(_stringLiteral528F5D16BFCF00BFD2F2B5D78810F653D013CBFB, /*hidden argument*/NULL);
		ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * L_18 = (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 *)il2cpp_codegen_object_new(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m9A85EF7FEFEC21DDD525A67E831D77278E5165B7(L_18, L_17, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18, NULL, XmlTextWriter_VerifyPrefixXml_m44A57489D6DFFAD820FE4DF6B113DB3B58E6F8E0_RuntimeMethod_var);
	}

IL_006b:
	{
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::FlushEncoders()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter_FlushEncoders_m131B51A9CECD553DB91EC663D92AFE3D0197B725 (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6 * __this, const RuntimeMethod* method)
{
	{
		XmlTextWriterBase64Encoder_t1362FFBE93C9BC675B4A34503DB31AA6D3AAF01C * L_0 = __this->get_base64Encoder_12();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		XmlTextWriterBase64Encoder_t1362FFBE93C9BC675B4A34503DB31AA6D3AAF01C * L_1 = __this->get_base64Encoder_12();
		NullCheck(L_1);
		Base64Encoder_Flush_mCC2F11678DA21435D1A00BFDA1D9FF97B9605B6D(L_1, /*hidden argument*/NULL);
	}

IL_0013:
	{
		__this->set_flush_18((bool)0);
		return;
	}
}
// System.Void System.Xml.XmlTextWriter::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriter__cctor_mC61966BD08862DDA9FF9033C1B5834F9BD887CC1 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlTextWriter__cctor_mC61966BD08862DDA9FF9033C1B5834F9BD887CC1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_0 = (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)SZArrayNew(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10));
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral952F375412E89FF213A8ACA383D18E5691354347);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral952F375412E89FF213A8ACA383D18E5691354347);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteralBA4D6E652036F4C7322CA4CF6D5A6AD5D7A37815);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralBA4D6E652036F4C7322CA4CF6D5A6AD5D7A37815);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteralB94C6BF9E158406E9B439472751E3DC6C4AAA530);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralB94C6BF9E158406E9B439472751E3DC6C4AAA530);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral49FA9FD831096C82AB950BC7AA0A0A7EF8A10998);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral49FA9FD831096C82AB950BC7AA0A0A7EF8A10998);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_5 = L_4;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteralA086D942884A301B827479029BA19C2746237425);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteralA086D942884A301B827479029BA19C2746237425);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_6 = L_5;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral4F9BE057F0EA5D2BA72FD2C810E8D7B9AA98B469);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral4F9BE057F0EA5D2BA72FD2C810E8D7B9AA98B469);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_7 = L_6;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral0F74095E24003C6F3BF3D236FF3CE7D75E3BC68E);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral0F74095E24003C6F3BF3D236FF3CE7D75E3BC68E);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_8 = L_7;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteralDAF817FEF227A2B8E59937DAAE3E82D2A6A4644A);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteralDAF817FEF227A2B8E59937DAAE3E82D2A6A4644A);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_9 = L_8;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral7F2F6A15CF8DA2B27E5A4AF47B58E7AD71C0B3D9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(8), (String_t*)_stringLiteral7F2F6A15CF8DA2B27E5A4AF47B58E7AD71C0B3D9);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_10 = L_9;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral88D86B7721D587644E9C4CF33A084202CB3B0FF0);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)_stringLiteral88D86B7721D587644E9C4CF33A084202CB3B0FF0);
		((XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields*)il2cpp_codegen_static_fields_for(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_il2cpp_TypeInfo_var))->set_stateName_24(L_10);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_11 = (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)SZArrayNew(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var, (uint32_t)((int32_t)14));
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_12 = L_11;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteralFA7FF6FB00A7ACE8D98CF880A492DC6C5FFC8CA7);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralFA7FF6FB00A7ACE8D98CF880A492DC6C5FFC8CA7);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_13 = L_12;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteralE81A0D33628A7C7FFEDE35A7FC0D572D077F40F2);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralE81A0D33628A7C7FFEDE35A7FC0D572D077F40F2);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_14 = L_13;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteral153D7A58B3A3E898FCBDD04C462AF308414BD09D);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral153D7A58B3A3E898FCBDD04C462AF308414BD09D);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_15 = L_14;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteralB4F3203E222557090E52A70DF590EC32DB60D176);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteralB4F3203E222557090E52A70DF590EC32DB60D176);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_16 = L_15;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteral3CF80C8340B4CACB2EDD9BE6CC78DDB7CED73BA6);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3CF80C8340B4CACB2EDD9BE6CC78DDB7CED73BA6);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_17 = L_16;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, _stringLiteral6BDB3C5694D4A738A5ED7467CC26DEF7664323A6);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral6BDB3C5694D4A738A5ED7467CC26DEF7664323A6);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_18 = L_17;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteralEE6600091DF72F57E330216D90436099635DAFBC);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteralEE6600091DF72F57E330216D90436099635DAFBC);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_19 = L_18;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral4C7B4D20A57217BA507BD50082B44D927E116DA8);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral4C7B4D20A57217BA507BD50082B44D927E116DA8);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_20 = L_19;
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral85FB26FBD8AB37E35E0B5F53103D2B500B090CD2);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(8), (String_t*)_stringLiteral85FB26FBD8AB37E35E0B5F53103D2B500B090CD2);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_21 = L_20;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteral4F9BE057F0EA5D2BA72FD2C810E8D7B9AA98B469);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)_stringLiteral4F9BE057F0EA5D2BA72FD2C810E8D7B9AA98B469);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_22 = L_21;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral895C330FF6166A7F13701F8D9B78F313787269C8);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (String_t*)_stringLiteral895C330FF6166A7F13701F8D9B78F313787269C8);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_23 = L_22;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral0D7652C6A78D387AD1AF2F86D37C74C5E6FB9F7A);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (String_t*)_stringLiteral0D7652C6A78D387AD1AF2F86D37C74C5E6FB9F7A);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_24 = L_23;
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, _stringLiteral47B13CDB0607925057B8E9D36B0386D1A11EB54C);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (String_t*)_stringLiteral47B13CDB0607925057B8E9D36B0386D1A11EB54C);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_25 = L_24;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteral3159FE421B3221381B3C778DC1C3C26E4540BE37);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (String_t*)_stringLiteral3159FE421B3221381B3C778DC1C3C26E4540BE37);
		((XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields*)il2cpp_codegen_static_fields_for(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_il2cpp_TypeInfo_var))->set_tokenName_25(L_25);
		StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* L_26 = (StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2*)(StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2*)SZArrayNew(StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2_il2cpp_TypeInfo_var, (uint32_t)((int32_t)104));
		StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* L_27 = L_26;
		RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  L_28 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB____B368804F0C6DAB083B253A6B106D0783D5C32E90_2_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m29F50CDFEEE0AB868200291366253DD4737BC76A((RuntimeArray *)(RuntimeArray *)L_27, L_28, /*hidden argument*/NULL);
		((XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields*)il2cpp_codegen_static_fields_for(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_il2cpp_TypeInfo_var))->set_stateTableDefault_26(L_27);
		StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* L_29 = (StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2*)(StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2*)SZArrayNew(StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2_il2cpp_TypeInfo_var, (uint32_t)((int32_t)104));
		StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* L_30 = L_29;
		RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  L_31 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tBA431F51A4722F0776A8592A8C2A8770D6D54EFB____6A0D50D692745A6663128CD315B71079584F3E59_1_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m29F50CDFEEE0AB868200291366253DD4737BC76A((RuntimeArray *)(RuntimeArray *)L_30, L_31, /*hidden argument*/NULL);
		((XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields*)il2cpp_codegen_static_fields_for(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_il2cpp_TypeInfo_var))->set_stateTableDocument_27(L_30);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: System.Xml.XmlTextWriter/Namespace
IL2CPP_EXTERN_C void Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B_marshal_pinvoke(const Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B& unmarshaled, Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B_marshaled_pinvoke& marshaled)
{
	marshaled.___prefix_0 = il2cpp_codegen_marshal_string(unmarshaled.get_prefix_0());
	marshaled.___ns_1 = il2cpp_codegen_marshal_string(unmarshaled.get_ns_1());
	marshaled.___declared_2 = static_cast<int32_t>(unmarshaled.get_declared_2());
	marshaled.___prevNsIndex_3 = unmarshaled.get_prevNsIndex_3();
}
IL2CPP_EXTERN_C void Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B_marshal_pinvoke_back(const Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B_marshaled_pinvoke& marshaled, Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B& unmarshaled)
{
	unmarshaled.set_prefix_0(il2cpp_codegen_marshal_string_result(marshaled.___prefix_0));
	unmarshaled.set_ns_1(il2cpp_codegen_marshal_string_result(marshaled.___ns_1));
	bool unmarshaled_declared_temp_2 = false;
	unmarshaled_declared_temp_2 = static_cast<bool>(marshaled.___declared_2);
	unmarshaled.set_declared_2(unmarshaled_declared_temp_2);
	int32_t unmarshaled_prevNsIndex_temp_3 = 0;
	unmarshaled_prevNsIndex_temp_3 = marshaled.___prevNsIndex_3;
	unmarshaled.set_prevNsIndex_3(unmarshaled_prevNsIndex_temp_3);
}
// Conversion method for clean up from marshalling of: System.Xml.XmlTextWriter/Namespace
IL2CPP_EXTERN_C void Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B_marshal_pinvoke_cleanup(Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___prefix_0);
	marshaled.___prefix_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___ns_1);
	marshaled.___ns_1 = NULL;
}
// Conversion methods for marshalling of: System.Xml.XmlTextWriter/Namespace
IL2CPP_EXTERN_C void Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B_marshal_com(const Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B& unmarshaled, Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B_marshaled_com& marshaled)
{
	marshaled.___prefix_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_prefix_0());
	marshaled.___ns_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_ns_1());
	marshaled.___declared_2 = static_cast<int32_t>(unmarshaled.get_declared_2());
	marshaled.___prevNsIndex_3 = unmarshaled.get_prevNsIndex_3();
}
IL2CPP_EXTERN_C void Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B_marshal_com_back(const Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B_marshaled_com& marshaled, Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B& unmarshaled)
{
	unmarshaled.set_prefix_0(il2cpp_codegen_marshal_bstring_result(marshaled.___prefix_0));
	unmarshaled.set_ns_1(il2cpp_codegen_marshal_bstring_result(marshaled.___ns_1));
	bool unmarshaled_declared_temp_2 = false;
	unmarshaled_declared_temp_2 = static_cast<bool>(marshaled.___declared_2);
	unmarshaled.set_declared_2(unmarshaled_declared_temp_2);
	int32_t unmarshaled_prevNsIndex_temp_3 = 0;
	unmarshaled_prevNsIndex_temp_3 = marshaled.___prevNsIndex_3;
	unmarshaled.set_prevNsIndex_3(unmarshaled_prevNsIndex_temp_3);
}
// Conversion method for clean up from marshalling of: System.Xml.XmlTextWriter/Namespace
IL2CPP_EXTERN_C void Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B_marshal_com_cleanup(Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___prefix_0);
	marshaled.___prefix_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___ns_1);
	marshaled.___ns_1 = NULL;
}
// System.Void System.Xml.XmlTextWriter_Namespace::Set(System.String,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Namespace_Set_mEF74FB920EF6CB43BAFDC1FFA58AA95235177979 (Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B * __this, String_t* ___prefix0, String_t* ___ns1, bool ___declared2, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___prefix0;
		__this->set_prefix_0(L_0);
		String_t* L_1 = ___ns1;
		__this->set_ns_1(L_1);
		bool L_2 = ___declared2;
		__this->set_declared_2(L_2);
		__this->set_prevNsIndex_3((-1));
		return;
	}
}
IL2CPP_EXTERN_C  void Namespace_Set_mEF74FB920EF6CB43BAFDC1FFA58AA95235177979_AdjustorThunk (RuntimeObject * __this, String_t* ___prefix0, String_t* ___ns1, bool ___declared2, const RuntimeMethod* method)
{
	Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B * _thisAdjusted = reinterpret_cast<Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B *>(__this + 1);
	Namespace_Set_mEF74FB920EF6CB43BAFDC1FFA58AA95235177979(_thisAdjusted, ___prefix0, ___ns1, ___declared2, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: System.Xml.XmlTextWriter/TagInfo
IL2CPP_EXTERN_C void TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5_marshal_pinvoke(const TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5& unmarshaled, TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5_marshaled_pinvoke& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.get_name_0());
	marshaled.___prefix_1 = il2cpp_codegen_marshal_string(unmarshaled.get_prefix_1());
	marshaled.___defaultNs_2 = il2cpp_codegen_marshal_string(unmarshaled.get_defaultNs_2());
	marshaled.___defaultNsState_3 = unmarshaled.get_defaultNsState_3();
	marshaled.___xmlSpace_4 = unmarshaled.get_xmlSpace_4();
	marshaled.___xmlLang_5 = il2cpp_codegen_marshal_string(unmarshaled.get_xmlLang_5());
	marshaled.___prevNsTop_6 = unmarshaled.get_prevNsTop_6();
	marshaled.___prefixCount_7 = unmarshaled.get_prefixCount_7();
	marshaled.___mixed_8 = static_cast<int32_t>(unmarshaled.get_mixed_8());
}
IL2CPP_EXTERN_C void TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5_marshal_pinvoke_back(const TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5_marshaled_pinvoke& marshaled, TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_string_result(marshaled.___name_0));
	unmarshaled.set_prefix_1(il2cpp_codegen_marshal_string_result(marshaled.___prefix_1));
	unmarshaled.set_defaultNs_2(il2cpp_codegen_marshal_string_result(marshaled.___defaultNs_2));
	int32_t unmarshaled_defaultNsState_temp_3 = 0;
	unmarshaled_defaultNsState_temp_3 = marshaled.___defaultNsState_3;
	unmarshaled.set_defaultNsState_3(unmarshaled_defaultNsState_temp_3);
	int32_t unmarshaled_xmlSpace_temp_4 = 0;
	unmarshaled_xmlSpace_temp_4 = marshaled.___xmlSpace_4;
	unmarshaled.set_xmlSpace_4(unmarshaled_xmlSpace_temp_4);
	unmarshaled.set_xmlLang_5(il2cpp_codegen_marshal_string_result(marshaled.___xmlLang_5));
	int32_t unmarshaled_prevNsTop_temp_6 = 0;
	unmarshaled_prevNsTop_temp_6 = marshaled.___prevNsTop_6;
	unmarshaled.set_prevNsTop_6(unmarshaled_prevNsTop_temp_6);
	int32_t unmarshaled_prefixCount_temp_7 = 0;
	unmarshaled_prefixCount_temp_7 = marshaled.___prefixCount_7;
	unmarshaled.set_prefixCount_7(unmarshaled_prefixCount_temp_7);
	bool unmarshaled_mixed_temp_8 = false;
	unmarshaled_mixed_temp_8 = static_cast<bool>(marshaled.___mixed_8);
	unmarshaled.set_mixed_8(unmarshaled_mixed_temp_8);
}
// Conversion method for clean up from marshalling of: System.Xml.XmlTextWriter/TagInfo
IL2CPP_EXTERN_C void TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5_marshal_pinvoke_cleanup(TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___prefix_1);
	marshaled.___prefix_1 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___defaultNs_2);
	marshaled.___defaultNs_2 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___xmlLang_5);
	marshaled.___xmlLang_5 = NULL;
}
// Conversion methods for marshalling of: System.Xml.XmlTextWriter/TagInfo
IL2CPP_EXTERN_C void TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5_marshal_com(const TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5& unmarshaled, TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5_marshaled_com& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_name_0());
	marshaled.___prefix_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_prefix_1());
	marshaled.___defaultNs_2 = il2cpp_codegen_marshal_bstring(unmarshaled.get_defaultNs_2());
	marshaled.___defaultNsState_3 = unmarshaled.get_defaultNsState_3();
	marshaled.___xmlSpace_4 = unmarshaled.get_xmlSpace_4();
	marshaled.___xmlLang_5 = il2cpp_codegen_marshal_bstring(unmarshaled.get_xmlLang_5());
	marshaled.___prevNsTop_6 = unmarshaled.get_prevNsTop_6();
	marshaled.___prefixCount_7 = unmarshaled.get_prefixCount_7();
	marshaled.___mixed_8 = static_cast<int32_t>(unmarshaled.get_mixed_8());
}
IL2CPP_EXTERN_C void TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5_marshal_com_back(const TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5_marshaled_com& marshaled, TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_bstring_result(marshaled.___name_0));
	unmarshaled.set_prefix_1(il2cpp_codegen_marshal_bstring_result(marshaled.___prefix_1));
	unmarshaled.set_defaultNs_2(il2cpp_codegen_marshal_bstring_result(marshaled.___defaultNs_2));
	int32_t unmarshaled_defaultNsState_temp_3 = 0;
	unmarshaled_defaultNsState_temp_3 = marshaled.___defaultNsState_3;
	unmarshaled.set_defaultNsState_3(unmarshaled_defaultNsState_temp_3);
	int32_t unmarshaled_xmlSpace_temp_4 = 0;
	unmarshaled_xmlSpace_temp_4 = marshaled.___xmlSpace_4;
	unmarshaled.set_xmlSpace_4(unmarshaled_xmlSpace_temp_4);
	unmarshaled.set_xmlLang_5(il2cpp_codegen_marshal_bstring_result(marshaled.___xmlLang_5));
	int32_t unmarshaled_prevNsTop_temp_6 = 0;
	unmarshaled_prevNsTop_temp_6 = marshaled.___prevNsTop_6;
	unmarshaled.set_prevNsTop_6(unmarshaled_prevNsTop_temp_6);
	int32_t unmarshaled_prefixCount_temp_7 = 0;
	unmarshaled_prefixCount_temp_7 = marshaled.___prefixCount_7;
	unmarshaled.set_prefixCount_7(unmarshaled_prefixCount_temp_7);
	bool unmarshaled_mixed_temp_8 = false;
	unmarshaled_mixed_temp_8 = static_cast<bool>(marshaled.___mixed_8);
	unmarshaled.set_mixed_8(unmarshaled_mixed_temp_8);
}
// Conversion method for clean up from marshalling of: System.Xml.XmlTextWriter/TagInfo
IL2CPP_EXTERN_C void TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5_marshal_com_cleanup(TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___name_0);
	marshaled.___name_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___prefix_1);
	marshaled.___prefix_1 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___defaultNs_2);
	marshaled.___defaultNs_2 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___xmlLang_5);
	marshaled.___xmlLang_5 = NULL;
}
// System.Void System.Xml.XmlTextWriter_TagInfo::Init(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TagInfo_Init_m631D10B6F7160EC8D54CAAF1418215168F8E6F32 (TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5 * __this, int32_t ___nsTop0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TagInfo_Init_m631D10B6F7160EC8D54CAAF1418215168F8E6F32_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_name_0((String_t*)NULL);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_defaultNs_2(L_0);
		__this->set_defaultNsState_3(0);
		__this->set_xmlSpace_4(0);
		__this->set_xmlLang_5((String_t*)NULL);
		int32_t L_1 = ___nsTop0;
		__this->set_prevNsTop_6(L_1);
		__this->set_prefixCount_7(0);
		__this->set_mixed_8((bool)0);
		return;
	}
}
IL2CPP_EXTERN_C  void TagInfo_Init_m631D10B6F7160EC8D54CAAF1418215168F8E6F32_AdjustorThunk (RuntimeObject * __this, int32_t ___nsTop0, const RuntimeMethod* method)
{
	TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5 * _thisAdjusted = reinterpret_cast<TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5 *>(__this + 1);
	TagInfo_Init_m631D10B6F7160EC8D54CAAF1418215168F8E6F32(_thisAdjusted, ___nsTop0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.XmlTextWriterBase64Encoder::WriteChars(System.Char[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlTextWriterBase64Encoder_WriteChars_m339EA5F0483DFCD5D8A0EA35DE060C8E3071E97E (XmlTextWriterBase64Encoder_t1362FFBE93C9BC675B4A34503DB31AA6D3AAF01C * __this, CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___chars0, int32_t ___index1, int32_t ___count2, const RuntimeMethod* method)
{
	{
		XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * L_0 = __this->get_xmlTextEncoder_3();
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_1 = ___chars0;
		int32_t L_2 = ___index1;
		int32_t L_3 = ___count2;
		NullCheck(L_0);
		XmlTextEncoder_WriteRaw_mE2BB17DA3D685325F30E98E590CC58BBB7781628(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.XmlWriter::Close()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlWriter_Close_m35D89F339C48DF5A94DAA4EFD2706DAAADB5B25F (XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void System.Xml.XmlWriter::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlWriter_Dispose_m44475A2DD5076CED4A48FCBA03912752BA3A4FB3 (XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * __this, const RuntimeMethod* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(8 /* System.Void System.Xml.XmlWriter::Dispose(System.Boolean) */, __this, (bool)1);
		return;
	}
}
// System.Void System.Xml.XmlWriter::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlWriter_Dispose_m67E68BEEA19D569625D76E85E9399DF83B74A8F7 (XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * __this, bool ___disposing0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___disposing0;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Xml.WriteState System.Xml.XmlWriter::get_WriteState() */, __this);
		if ((((int32_t)L_1) == ((int32_t)5)))
		{
			goto IL_0012;
		}
	}
	{
		VirtActionInvoker0::Invoke(7 /* System.Void System.Xml.XmlWriter::Close() */, __this);
	}

IL_0012:
	{
		return;
	}
}
// System.Void System.Xml.XmlWriter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlWriter__ctor_m1D2B58DC035709A720317204246AD58118C15740 (XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018_inline (String_t* __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_stringLength_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void XmlCharType__ctor_m0B65BC6BD912979FA16676884EC3120565FD6DCD_inline (XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___charProperties0, const RuntimeMethod* method)
{
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = ___charProperties0;
		__this->set_charProperties_2(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* SerializationEntry_get_Name_m364D6CAEAD32EE66700B47B65E80C03D80596DC4_inline (SerializationEntry_tA4CE7B0176B45BD820A7802C84479174F5EBE5EA * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_m_name_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR RuntimeObject * SerializationEntry_get_Value_m6E7295904D91A38BFE2C47C662E75E8063ABC048_inline (SerializationEntry_tA4CE7B0176B45BD820A7802C84479174F5EBE5EA * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_m_value_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void Exception_set_HResult_m920DF8C728D8A0EC0759685FED890C775FA08B99_inline (Exception_t * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set__HResult_11(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void XmlTextEncoder_set_QuoteChar_m28C9CC005913B64E3444E1522EE8D26B4F5F0C88_inline (XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * __this, Il2CppChar ___value0, const RuntimeMethod* method)
{
	{
		Il2CppChar L_0 = ___value0;
		__this->set_quoteChar_2(L_0);
		return;
	}
}
