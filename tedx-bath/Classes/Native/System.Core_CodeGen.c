﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000003 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000004 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000005 System.Boolean System.Linq.Enumerable::SequenceEqual(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000006 System.Boolean System.Linq.Enumerable::SequenceEqual(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000007 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000008 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000009 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000A System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x0000000B TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000000C System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x0000000D System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x0000000E System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x0000000F System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000010 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000011 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000012 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000013 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000014 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000015 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000016 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000017 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000018 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000019 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x0000001A System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x0000001B System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x0000001C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001D System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001E System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x0000001F System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000020 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000021 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000022 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000023 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000024 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000025 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000026 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000027 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000028 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000029 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x0000002A System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x0000002B System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x0000002C System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x0000002D System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x0000002E System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000002F System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000030 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000031 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000032 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000033 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000034 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000035 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x00000036 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x00000037 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x00000038 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x00000039 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x0000003A System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x0000003B System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x0000003C System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x0000003D T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x0000003E System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x0000003F System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[63] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[63] = 
{
	0,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[14] = 
{
	{ 0x02000004, { 28, 4 } },
	{ 0x02000005, { 32, 9 } },
	{ 0x02000006, { 41, 7 } },
	{ 0x02000007, { 48, 10 } },
	{ 0x02000008, { 58, 1 } },
	{ 0x02000009, { 59, 21 } },
	{ 0x0200000B, { 80, 2 } },
	{ 0x06000003, { 0, 10 } },
	{ 0x06000004, { 10, 5 } },
	{ 0x06000005, { 15, 1 } },
	{ 0x06000006, { 16, 5 } },
	{ 0x06000007, { 21, 3 } },
	{ 0x06000008, { 24, 1 } },
	{ 0x06000009, { 25, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[82] = 
{
	{ (Il2CppRGCTXDataType)2, 15263 },
	{ (Il2CppRGCTXDataType)3, 10329 },
	{ (Il2CppRGCTXDataType)2, 15264 },
	{ (Il2CppRGCTXDataType)2, 15265 },
	{ (Il2CppRGCTXDataType)3, 10330 },
	{ (Il2CppRGCTXDataType)2, 15266 },
	{ (Il2CppRGCTXDataType)2, 15267 },
	{ (Il2CppRGCTXDataType)3, 10331 },
	{ (Il2CppRGCTXDataType)2, 15268 },
	{ (Il2CppRGCTXDataType)3, 10332 },
	{ (Il2CppRGCTXDataType)2, 15269 },
	{ (Il2CppRGCTXDataType)3, 10333 },
	{ (Il2CppRGCTXDataType)3, 10334 },
	{ (Il2CppRGCTXDataType)2, 12301 },
	{ (Il2CppRGCTXDataType)3, 10335 },
	{ (Il2CppRGCTXDataType)3, 10336 },
	{ (Il2CppRGCTXDataType)3, 10337 },
	{ (Il2CppRGCTXDataType)2, 15270 },
	{ (Il2CppRGCTXDataType)2, 12305 },
	{ (Il2CppRGCTXDataType)2, 15271 },
	{ (Il2CppRGCTXDataType)2, 12307 },
	{ (Il2CppRGCTXDataType)2, 12308 },
	{ (Il2CppRGCTXDataType)2, 15272 },
	{ (Il2CppRGCTXDataType)3, 10338 },
	{ (Il2CppRGCTXDataType)2, 12311 },
	{ (Il2CppRGCTXDataType)2, 12313 },
	{ (Il2CppRGCTXDataType)2, 15273 },
	{ (Il2CppRGCTXDataType)3, 10339 },
	{ (Il2CppRGCTXDataType)3, 10340 },
	{ (Il2CppRGCTXDataType)3, 10341 },
	{ (Il2CppRGCTXDataType)2, 12318 },
	{ (Il2CppRGCTXDataType)3, 10342 },
	{ (Il2CppRGCTXDataType)3, 10343 },
	{ (Il2CppRGCTXDataType)2, 12327 },
	{ (Il2CppRGCTXDataType)2, 15274 },
	{ (Il2CppRGCTXDataType)3, 10344 },
	{ (Il2CppRGCTXDataType)3, 10345 },
	{ (Il2CppRGCTXDataType)2, 12329 },
	{ (Il2CppRGCTXDataType)2, 15165 },
	{ (Il2CppRGCTXDataType)3, 10346 },
	{ (Il2CppRGCTXDataType)3, 10347 },
	{ (Il2CppRGCTXDataType)3, 10348 },
	{ (Il2CppRGCTXDataType)2, 12336 },
	{ (Il2CppRGCTXDataType)2, 15275 },
	{ (Il2CppRGCTXDataType)3, 10349 },
	{ (Il2CppRGCTXDataType)3, 10350 },
	{ (Il2CppRGCTXDataType)3, 9936 },
	{ (Il2CppRGCTXDataType)3, 10351 },
	{ (Il2CppRGCTXDataType)3, 10352 },
	{ (Il2CppRGCTXDataType)2, 12345 },
	{ (Il2CppRGCTXDataType)2, 15276 },
	{ (Il2CppRGCTXDataType)3, 10353 },
	{ (Il2CppRGCTXDataType)3, 10354 },
	{ (Il2CppRGCTXDataType)3, 10355 },
	{ (Il2CppRGCTXDataType)3, 10356 },
	{ (Il2CppRGCTXDataType)3, 10357 },
	{ (Il2CppRGCTXDataType)3, 9942 },
	{ (Il2CppRGCTXDataType)3, 10358 },
	{ (Il2CppRGCTXDataType)3, 10359 },
	{ (Il2CppRGCTXDataType)3, 10360 },
	{ (Il2CppRGCTXDataType)2, 15277 },
	{ (Il2CppRGCTXDataType)3, 10361 },
	{ (Il2CppRGCTXDataType)3, 10362 },
	{ (Il2CppRGCTXDataType)3, 10363 },
	{ (Il2CppRGCTXDataType)2, 12359 },
	{ (Il2CppRGCTXDataType)3, 10364 },
	{ (Il2CppRGCTXDataType)3, 10365 },
	{ (Il2CppRGCTXDataType)2, 12362 },
	{ (Il2CppRGCTXDataType)3, 10366 },
	{ (Il2CppRGCTXDataType)1, 15278 },
	{ (Il2CppRGCTXDataType)2, 12361 },
	{ (Il2CppRGCTXDataType)3, 10367 },
	{ (Il2CppRGCTXDataType)1, 12361 },
	{ (Il2CppRGCTXDataType)1, 12359 },
	{ (Il2CppRGCTXDataType)2, 15279 },
	{ (Il2CppRGCTXDataType)2, 12361 },
	{ (Il2CppRGCTXDataType)3, 10368 },
	{ (Il2CppRGCTXDataType)3, 10369 },
	{ (Il2CppRGCTXDataType)3, 10370 },
	{ (Il2CppRGCTXDataType)2, 12360 },
	{ (Il2CppRGCTXDataType)3, 10371 },
	{ (Il2CppRGCTXDataType)2, 12373 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	63,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	14,
	s_rgctxIndices,
	82,
	s_rgctxValues,
	NULL,
};
