﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapOpen : MonoBehaviour
{
    Animator m_Animator;

    // Start is called before the first frame update
    void Start()
    {
        Application.RequestUserAuthorization(UserAuthorization.WebCam);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AnimationEnd()
    {
        foreach(GameObject Pin in GameObject.FindGameObjectsWithTag("Pin"))
        {
            Pin.GetComponent<Animator>().SetBool("pinVisible", true);
        }
    }

    public void AnimationStart()
    {
        foreach(GameObject Pin in GameObject.FindGameObjectsWithTag("Pin"))
        {
            Pin.GetComponent<Animator>().SetBool("pinVisible", false);
        }
    }

    public void Rocketmakers()
    {
        Application.OpenURL("https://rocketmakers.com/");
    }

    public void TEDxBath()
    {
        Application.OpenURL("https://www.tedxbath.co.uk/");
    }
}
